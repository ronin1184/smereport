import os
import shutil
from django.db import models
from django.db.models import Q
from datetime import datetime
from Common.Tools import Tools
from smereport.settings import MEDIA_ROOT
import random
# Create your models here.


class CompanyModel(models.Manager):
    tools = Tools()
    limit = 25

    def get_all(self, conditions=None, keyword=None, page=1, order='-id'):
        try:
            offset = (page - 1) * self.limit

            if conditions is not None:
                query = self.filter(**conditions)
            else:
                query = self.all()
            if keyword is not None:
                query = query.filter(
                    Q(cty__icontains=keyword) |
                    Q(mst__icontains=keyword) |
                    Q(diachi__icontains=keyword) |
                    Q(phone__icontains=keyword) |
                    Q(email__icontains=keyword) |
                    Q(ploai__icontains=keyword)
                )
            query = query.order_by(order)

            if offset == 0:
                query = query[: self.limit]
            else:
                query = query[offset: offset + self.limit]

            reach_bottom = False
            if query.count() < self.limit:
                reach_bottom = True

            result = Tools.standardize_array(list(query.values()))
            return {
                'error': None,
                'reach_bottom': reach_bottom,
                'data': result
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error

    def get_full(self, conditions=None, fields=None):
        try:
            if conditions is None:
                query = self.all()
            else:
                query = self.filter(**conditions)
            if fields is None:
                result = Tools.standardize_array(list(query.values()))
            else:
                result = query.values(*fields)
            return {
                'error': None,
                'data': result
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error

    def get_one(self, conditions, original=False):
        try:
            query = self.filter(**conditions)
            if original is False:
                query = list(query.values())
            if not query:
                query = None
            else:
                query = query[0]
            return {
                'error': None,
                'data': query
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error

    def add_item(self, params):
        try:
            item = Company(**params)
            item.save()
            result = {
                'id': item.pk,
                'cty': item.cty,
                'phone': item.phone,
                'email': item.email
            }
            return {
                'error': None,
                'data': result
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error

    def edit_item(self, pk, params):
        try:
            item = self.get(pk=int(pk))
            if not item:
                return {
                    'error': "Item not found!",
                    'data': None
                }
            item.__dict__.update(params)
            item.save()
            result = {
                'id': item.pk,
                'cty': item.cty,
                'phone': item.phone,
                'email': item.email
            }
            return {
                'error': None,
                'data': result
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error

    def edit_owner(self, pk, params):
        try:
            input_pk = pk
            if '|' in str(pk):
                pk = [int(x) for x in pk.split('|')]
            if type(pk) is not list:
                items = self.get(pk=int(pk))
                if not items:
                    return {
                        'error': "Item not found!",
                        'data': None
                    }
                items.__dict__.update(params)
                items.save()
            else:
                items = self.filter(id__in=pk)
                if items is not None:
                    for item in items:
                        item.__dict__.update(params)
                        item.save()
            # items.save()
            result = {
                'id': input_pk
            }
            return {
                'error': None,
                'data': result
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error

    def remove_owner(self, owner, owner_type):
        try:
            if owner_type == 'sale':
                self.filter(sale=int(owner)).update(
                    sale=None,
                    saledatereturn=datetime.now()
                )
            elif owner_type == 'hub':
                self.filter(hub=int(owner)).update(
                    hub=None,
                    hubdatereturn=datetime.now(),
                    sale=None,
                    saledatereturn=datetime.now(),
                )
            elif owner_type == 'vung':
                self.filter(vung=int(owner)).update(
                    vung=None,
                    vungdatereturn=datetime.now(),
                    hub=None,
                    hubdatereturn=datetime.now(),
                    sale=None,
                    saledatereturn=datetime.now()
                )
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error

    def remove_item(self, pk):
        try:
            input_pk = pk
            if '|' in str(pk):
                pk = [int(x) for x in pk.split('|')]
            if type(pk) is not list:
                items = self.get(pk=int(pk))
                if not items:
                    return {
                        'error': "Item not found!",
                        'data': None
                    }
                path = MEDIA_ROOT + str(items.pk)
                if os.path.isdir(path) is not False:
                    shutil.rmtree(path)
            else:
                items = self.filter(id__in=pk)
                for i in pk:
                    path = MEDIA_ROOT + str(i)
                    if os.path.isdir(path) is not False:
                        shutil.rmtree(path)
            items.delete()
            result = {
                'id': input_pk
            }
            return {
                'error': None,
                'data': result
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error

    def update_random_tinh(self, list_tinh_id):
        all_items = self.all()
        for item in all_items:
            item.tinh = random.choice(list_tinh_id)
            item.save()


class Company(models.Model):
    id = models.AutoField(primary_key=True)
    tinh = models.IntegerField(blank=True, null=True)
    chinhanh = models.CharField(max_length=250, blank=True, null=True)
    cty = models.CharField(max_length=250, blank=True, null=True)
    mst = models.CharField(max_length=250, blank=True, null=True)
    huyen = models.CharField(max_length=250, blank=True, null=True)
    xa = models.CharField(max_length=250, blank=True, null=True)
    loaidn = models.CharField(max_length=250, blank=True, null=True)
    diachi = models.CharField(max_length=250, blank=True, null=True)
    namsxkd = models.IntegerField(blank=True, null=True)
    phone = models.CharField(max_length=250, blank=True, null=True)
    email = models.CharField(max_length=250, blank=True, null=True)
    nganhnghe = models.CharField(max_length=250, blank=True, null=True)
    doanhthu = models.IntegerField(blank=True, null=True)
    lntruocthue = models.IntegerField(blank=True, null=True)
    lntusx = models.FloatField(blank=True, null=True)
    dscr = models.FloatField(blank=True, null=True)
    ploai = models.CharField(max_length=250, blank=True, null=True)
    note = models.TextField(blank=True, null=True)
    report = models.IntegerField(blank=True, null=True)
    kq_cuoc_goi = models.IntegerField(blank=True, null=True)
    kq_cuoc_gap = models.IntegerField(blank=True, null=True)
    sp_tiep_can = models.IntegerField(blank=True, null=True)
    tinh_trang_khai_thac = models.IntegerField(null=True, blank=True)
    gt_pk_khac = models.CharField(max_length=50, null=True, blank=True)
    vung = models.IntegerField(blank=True, null=True)
    vungdateassign = models.DateField(blank=True, null=True)
    vungdatereturn = models.DateField(blank=True, null=True)
    hub = models.IntegerField(blank=True, null=True)
    hubdateassign = models.DateField(blank=True, null=True)
    hubdatereturn = models.DateField(blank=True, null=True)
    sale = models.IntegerField(blank=True, null=True)
    saledateassign = models.DateField(blank=True, null=True)
    saledatereturn = models.DateField(blank=True, null=True)

    objects = CompanyModel()

    class Meta:
        db_table = "company"
