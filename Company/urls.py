from django.conf.urls import patterns, url


urlpatterns = patterns(
    '',
    url(
        r'^$',
        'Company.views.index',
        name='index'),
    url(
        r'^get_all',
        'Company.views.get_all',
        name='get_all'),
    url(
        r'^get_full',
        'Company.views.get_full',
        name='get_full'),
    url(
        r'^get_one',
        'Company.views.get_one',
        name='get_one'),
    url(
        r'^add_item',
        'Company.views.add_item',
        name='add_item'),
    url(
        r'^add_by_upload',
        'Company.views.add_by_upload',
        name='add_by_upload'),
    url(
        r'^edit_item',
        'Company.views.edit_item',
        name='edit_item'),
    url(
        r'^edit_owner',
        'Company.views.edit_owner',
        name='edit_owner'),
    url(
        r'^remove_item',
        'Company.views.remove_item',
        name='remove_item'),
)
