from django.shortcuts import render
from django.http import StreamingHttpResponse
from django.template import RequestContext
from django.core.context_processors import csrf
from Common.Tools import Tools
from Administrator.models import Administrator
from Zone.models import Zone
from User.models import User
from Category.models import Category
from Dropdown.models import Dropdown
from Company.models import Company
from Report.models import Report
from Tinhthanh.models import Tinhthanh
import datetime
import xlrd
tools = Tools()
main_model = Company
admin_model = Administrator
user_model = User
zone_model = Zone
report_model = Report
category_model = Category
dropdown_model = Dropdown
tinh_thanh_model = Tinhthanh


def index(request):
    # list_tinh_id = [1001000000000, 1002000000000, 1003000000000, 1004000000000, 1005000000000, 1006000000000, 1007000000000, 1008000000000, 1009000000000, 1010000000000, 1011000000000, 1012000000000, 1013000000000, 1014000000000, 1015000000000, 1016000000000, 1017000000000, 1018000000000, 1019000000000, 1020000000000, 1021000000000, 1022000000000, 1023000000000, 1024000000000, 1025000000000, 1026000000000, 1027000000000, 1028000000000, 1029000000000, 1030000000000, 1031000000000, 1032000000000, 1033000000000, 1034000000000, 1035000000000, 1036000000000, 1037000000000, 1038000000000, 1039000000000, 1040000000000, 1041000000000, 1042000000000, 1043000000000, 1044000000000, 1045000000000, 1046000000000, 1047000000000, 1048000000000, 1049000000000, 1050000000000, 1051000000000, 1052000000000, 1053000000000, 1054000000000, 1055000000000, 1056000000000, 1057000000000, 1058000000000, 1059000000000, 1060000000000, 1061000000000, 1062000000000, 1063000000000];
    # main_model.objects.update_random_tinh(list_tinh_id)
    data = {}
    conditions = {}

    tinh_thanh_condition = tools.str_to_int(request.GET.get('tinh_thanh', None))
    sp_tiep_can_condition = tools.str_to_int(request.GET.get('sp_tiep_can', None))
    kq_cuoc_goi_condition = tools.str_to_int(request.GET.get('kq_cuoc_goi', None))
    kq_cuoc_gap_condition = tools.str_to_int(request.GET.get('kq_cuoc_gap', None))
    tt_khai_thac_condition = tools.str_to_int(request.GET.get('tt_khai_thac', None))

    if tinh_thanh_condition is not None:
        conditions['tinh'] = tinh_thanh_condition
    if sp_tiep_can_condition is not None:
        conditions['sp_tiep_can'] = sp_tiep_can_condition
    if kq_cuoc_goi_condition is not None:
        conditions['kq_cuoc_goi'] = kq_cuoc_goi_condition
    if kq_cuoc_gap_condition is not None:
        conditions['kq_cuoc_gap'] = kq_cuoc_gap_condition
    if tt_khai_thac_condition is not None:
        conditions['tinh_trang_khai_thac'] = tt_khai_thac_condition

    # tinh_thanh_cat = category_model.objects.get_one({'uid': 'tinh-thanh'}, True)['data']
    list_tinh_thanh = tinh_thanh_model.objects.get_full()['data']

    ket_qua_cuoc_goi_cat = category_model.objects.get_one({'uid': 'ket-qua-cuoc-goi'}, True)['data']
    list_ket_qua_cuoc_goi = dropdown_model.objects.get_full({'category': ket_qua_cuoc_goi_cat})['data']

    ket_qua_cuoc_gap_cat = category_model.objects.get_one({'uid': 'kq-cuoc-gap'}, True)['data']
    list_ket_qua_cuoc_gap = dropdown_model.objects.get_full({'category': ket_qua_cuoc_gap_cat})['data']

    sp_tiep_can_cat = category_model.objects.get_one({'uid': 'sp-tiep-can'}, True)['data']
    list_sp_tiep_can = dropdown_model.objects.get_full({'category': sp_tiep_can_cat.pk})['data']

    tinh_trang_khai_thac_cat = category_model.objects.get_one({'uid': 'tinh-trang-khai-thac'}, True)['data']
    list_tinh_trang_khai_thac = dropdown_model.objects.get_full({'category': tinh_trang_khai_thac_cat.pk})['data']

    if not conditions:
        conditions = None
    list_item = main_model.objects.get_all(conditions)['data']

    data['js_vars'] = {}
    data['js_vars']['list_item'] = Tools.standardize_array(list_item)

    data['js_vars']['owner_conditions'] = conditions

    data['js_vars']['list_tinh_thanh'] = Tools.standardize_array(list_tinh_thanh)
    data['js_vars']['list_ket_qua_cuoc_goi'] = Tools.standardize_array(list_ket_qua_cuoc_goi)
    data['js_vars']['list_ket_qua_cuoc_gap'] = Tools.standardize_array(list_ket_qua_cuoc_gap)
    data['js_vars']['list_sp_tiep_can'] = Tools.standardize_array(list_sp_tiep_can)
    data['js_vars']['list_tinh_trang_khai_thac'] = Tools.standardize_array(list_tinh_trang_khai_thac)

    data['js_vars']['tinh_thanh_condition'] = tinh_thanh_condition
    data['js_vars']['sp_tiep_can_condition'] = sp_tiep_can_condition
    data['js_vars']['kq_cuoc_goi_condition'] = kq_cuoc_goi_condition
    data['js_vars']['kq_cuoc_gap_condition'] = kq_cuoc_gap_condition
    data['js_vars']['tt_khai_thac_condition'] = tt_khai_thac_condition

    if len(data['js_vars']) != 0:
        data['js_vars'] = tools.encode_server_vars(data['js_vars'])
    else:
        data['js_vars'] = ''
    data.update(csrf(request))
    return render(
        request,
        tools.get_app_name(__name__) + 'company.html',
        data,
        context_instance=RequestContext(request)
    )


def get_all(request):
    conditions = {}
    conditions_1 = {}
    conditions_2 = {}
    conditions_3 = {}
    count_1 = 0
    count_2 = 0
    count_3 = 0
    page = int(request.POST.get('page'))
    keyword = request.POST.get('keyword', None)
    vung = request.POST.get('vung', None)
    hub = request.POST.get('hub', None)
    sale = request.POST.get('sale', None)

    tinh_thanh_condition = request.POST.get('tinh', None)
    sp_tiep_can_condition = request.POST.get('sp_tiep_can', None)
    kq_cuoc_goi_condition = request.POST.get('kq_cuoc_goi', None)
    kq_cuoc_gap_condition = request.POST.get('kq_cuoc_gap', None)
    tt_khai_thac_condition = request.POST.get('tinh_trang_khai_thac', None)

    if vung is not None:
        if vung == 'null' or vung == '':
            vung = None
        else:
            vung = int(vung)
        conditions_1 = {
            'vung': vung
        }
        count_1 = 1
    if hub is not None or hub == '':
        if hub == 'null':
            hub = None
        else:
            hub = int(hub)
        conditions_2 = {
            'hub': hub
        }
        count_2 = 2
    if sale is not None or sale == '':
        if sale == 'null':
            sale = None
        else:
            sale = int(sale)
        conditions_3 = {
            'sale': sale
        }
        count_3 = 4
    count = count_1 + count_2 + count_3
    if count == 0:
        conditions = {}
    elif count == 1:
        conditions = conditions_1
    elif count == 2:
        conditions = conditions_2
    elif count == 3:
        conditions = dict(list(conditions_1.items()) + list(conditions_2.items()))
    elif count == 4:
        conditions = conditions_3
    elif count == 5:
        conditions = dict(list(conditions_1.items()) + list(conditions_3.items()))
    elif count == 6:
        conditions = dict(list(conditions_2.items()) + list(conditions_3.items()))
    elif count == 7:
        conditions = dict(list(conditions_1.items()) + list(conditions_2.items()) + list(conditions_3.items()))

    if tinh_thanh_condition is not None:
        conditions['tinh'] = tinh_thanh_condition
    if sp_tiep_can_condition is not None:
        conditions['sp_tiep_can'] = sp_tiep_can_condition
    if kq_cuoc_goi_condition is not None:
        conditions['kq_cuoc_goi'] = kq_cuoc_goi_condition
    if kq_cuoc_gap_condition is not None:
        conditions['kq_cuoc_gap'] = kq_cuoc_gap_condition
    if tt_khai_thac_condition is not None:
        conditions['tinh_trang_khai_thac'] = tt_khai_thac_condition
    if not conditions:
        conditions = None
    '''
    print sp_tiep_can_condition
    print kq_cuoc_goi_condition
    print kq_cuoc_gap_condition
    print tt_khai_thac_condition
    '''

    result = main_model.objects.get_all(conditions, keyword, page)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def get_full(request):
    owner_type = request.POST.get('owner_type')
    owner_id = int(request.POST.get('owner_id'))
    # print(owner_type)
    if owner_type == 'ADMIN':
        # print('case 1')
        owner_item = admin_model.objects.get_one({'id': int(owner_id)}, True)['data']
        if owner_item.zone.level == 1:
            owner_type = 'vung'
        elif owner_item.zone.level == 2:
            owner_type = 'hub'
    else:
        # print('case 2')
        owner_item = user_model.objects.get_one({'id': int(owner_id)}, True)['data']
        owner_type = 'sale'
    conditions = {
        owner_type: int(owner_item.pk)
    }
    result = main_model.objects.get_full(conditions)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def get_one(request):
    conditions = {
        'id': request.POST.get('id', None),
    }
    if conditions['id'] is not None:
        conditions['id'] = int(conditions['id'])
    result = main_model.objects.get_one(conditions)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def add_item(request):
    data = {
        'tinh': tools.str_to_int(request.POST.get('tinh', None)),
        'chinhanh': tools.str_to_str(request.POST.get('chinhanh', None)),
        'cty': tools.str_to_str(request.POST.get('cty', None)),
        'mst': tools.str_to_str(request.POST.get('mst', None)),
        'huyen': tools.str_to_str(request.POST.get('huyen', None)),
        'xa': tools.str_to_str(request.POST.get('xa', None)),
        'loaidn': tools.str_to_str(request.POST.get('loaidn', None)),
        'diachi': tools.str_to_str(request.POST.get('diachi', None)),
        'namsxkd': tools.str_to_int(request.POST.get('namsxkd', None)),
        'phone': tools.str_to_str(request.POST.get('phone', None)),
        'email': tools.str_to_str(request.POST.get('email', None)),
        'nganhnghe': tools.str_to_str(request.POST.get('nganhnghe', None)),
        'doanhthu': tools.str_to_int(request.POST.get('doanhthu', None)),
        'lntruocthue': tools.str_to_int(request.POST.get('lntruocthue', None)),
        'lntusx': tools.str_to_float(request.POST.get('lntusx', None)),
        'dscr': tools.str_to_float(request.POST.get('dscr', None)),
        'ploai': tools.str_to_str(request.POST.get('ploai', None)),
        'note': tools.str_to_str(request.POST.get('note', None)),
    }
    if int(data['tinh']) == -1:
        data['tinh'] = None
    # print(data)
    result = main_model.objects.add_item(data)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def edit_item(request):
    data = {
        'tinh': tools.str_to_int(request.POST.get('tinh', None)),
        'chinhanh': tools.str_to_str(request.POST.get('chinhanh', None)),
        'cty': tools.str_to_str(request.POST.get('cty', None)),
        'mst': tools.str_to_str(request.POST.get('mst', None)),
        'huyen': tools.str_to_str(request.POST.get('huyen', None)),
        'xa': tools.str_to_str(request.POST.get('xa', None)),
        'loaidn': tools.str_to_str(request.POST.get('loaidn', None)),
        'diachi': tools.str_to_str(request.POST.get('diachi', None)),
        'namsxkd': tools.str_to_int(request.POST.get('namsxkd', None)),
        'phone': tools.str_to_str(request.POST.get('phone', None)),
        'email': tools.str_to_str(request.POST.get('email', None)),
        'nganhnghe': tools.str_to_str(request.POST.get('nganhnghe', None)),
        'doanhthu': tools.str_to_int(request.POST.get('doanhthu', None)),
        'lntruocthue': tools.str_to_int(request.POST.get('lntruocthue', None)),
        'lntusx': tools.str_to_float(request.POST.get('lntusx', None)),
        'dscr': tools.str_to_float(request.POST.get('dscr', None)),
        'ploai': tools.str_to_str(request.POST.get('ploai', None)),
        'note': tools.str_to_str(request.POST.get('note', None)),
    }
    pk = request.POST.get('id', None)
    result = main_model.objects.edit_item(pk, data)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def add_by_upload(request):
    try:
        data_file = request.FILES['data_file']

        book = xlrd.open_workbook(file_contents=data_file.read())
        sheet = book.sheet_by_index(0)
        number_of_cols = 18
        keys = [sheet.cell(0, col_index).value for col_index in range(number_of_cols)]

        for row_index in range(1, sheet.nrows):
            d = {keys[col_index]: sheet.cell(row_index, col_index).value for col_index in range(sheet.ncols)}
            d['tinh'] = tools.str_to_int(d['tinh'])
            d['doanhthu'] = tools.str_to_float(d['doanhthu'])
            d['lntruocthue'] = tools.str_to_float(d['lntruocthue'])
            d['lntusx'] = tools.str_to_float(d['lntusx'])
            d['dscr'] = tools.str_to_float(d['dscr'])
            if d['mst']:
                item_from_mst = main_model.objects.get_one({'mst': d['mst']}, True)['data']
                if item_from_mst is None:
                    main_model.objects.add_item(d)

        result = {
            'data': 'Upload success!',
            'error': None
        }
        return StreamingHttpResponse(
            tools.encode_server_vars(result),
            content_type='application/json'
        )
    except Exception as e:
        error = tools.return_exception(e)
        # print(error)
        return error


def edit_owner(request):
    pk = request.POST.get('id', None)
    new_owner_type = request.POST.get('owner_type', None)
    new_owner_id = request.POST.get('owner_id', None)
    current_owner_level = request.POST.get('user_level', None)
    if new_owner_type == 'ADMIN':
        current_owner_id = request.session.get('ADMIN_ID', None)
    else:
        current_owner_id = request.session.get('USER_ID', None)

    if new_owner_id is not None:
        new_owner_id = int(new_owner_id)
        if int(current_owner_level) < 2:
            new_owner_item = admin_model.objects.get_one({'pk': new_owner_id}, True)['data']
        else:
            new_owner_item = user_model.objects.get_one({'pk': new_owner_id}, True)['data']
        owner_level = new_owner_item.zone.level
    else:
        if new_owner_type == 'ADMIN':
            current_owner_item = admin_model.objects.get_one({'pk': current_owner_id}, True)['data']
        else:
            current_owner_item = user_model.objects.get_one({'pk': current_owner_id}, True)['data']
        owner_level = current_owner_item.zone.level

    if owner_level == 1:
        new_owner_type = 'vung'
    elif owner_level == 2:
        if new_owner_type == 'ADMIN':
            new_owner_type = 'hub'
        elif new_owner_type == 'USER':
            new_owner_type = 'sale'
        else:
            result = {
                'data': None,
                'error': 'Query not allowed!'
            }
            return StreamingHttpResponse(
                tools.encode_server_vars(result),
                content_type='application/json'
            )

    data = {
        new_owner_type: new_owner_id
    }

    if data[new_owner_type] is None:
        data[str(new_owner_type) + 'datereturn'] = datetime.datetime.now()
    else:
        data[str(new_owner_type) + 'dateassign'] = datetime.datetime.now()

    result = main_model.objects.edit_owner(pk, data)

    if new_owner_type == 'sale':
        if new_owner_id is None:
            # Tra chi tieu -> commit report
            # print('case 1')
            report_model.objects.update_commit(pk, current_owner_item, True)
        else:
            # Nhan chi tieu -> check exist report and assign for new user
            # print('case 2')
            report_model.objects.update_commit(pk, new_owner_item, False)

    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def remove_item(request):
    pk = request.POST.get('id', None)
    result = main_model.objects.remove_item(pk)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )
