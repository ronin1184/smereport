# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('tinh', models.IntegerField(blank=True, null=True)),
                ('chinhanh', models.CharField(max_length=250, blank=True, null=True)),
                ('cty', models.CharField(max_length=250, blank=True, null=True)),
                ('mst', models.CharField(max_length=250, blank=True, null=True)),
                ('huyen', models.CharField(max_length=250, blank=True, null=True)),
                ('xa', models.CharField(max_length=250, blank=True, null=True)),
                ('loaidn', models.CharField(max_length=250, blank=True, null=True)),
                ('diachi', models.CharField(max_length=250, blank=True, null=True)),
                ('namsxkd', models.IntegerField(blank=True, null=True)),
                ('phone', models.CharField(max_length=250, blank=True, null=True)),
                ('email', models.CharField(max_length=250, blank=True, null=True)),
                ('nganhnghe', models.CharField(max_length=250, blank=True, null=True)),
                ('doanhthu', models.IntegerField(blank=True, null=True)),
                ('lntruocthue', models.IntegerField(blank=True, null=True)),
                ('lntusx', models.FloatField(blank=True, null=True)),
                ('dscr', models.FloatField(blank=True, null=True)),
                ('ploai', models.CharField(max_length=250, blank=True, null=True)),
                ('note', models.TextField(blank=True, null=True)),
                ('report', models.IntegerField(blank=True, null=True)),
                ('kq_cuoc_goi', models.IntegerField(blank=None, null=None)),
                ('kq_cuoc_gap', models.IntegerField(blank=None, null=None)),
                ('sp_tiep_can', models.IntegerField(blank=None, null=None)),
                ('tinh_trang_khai_thac', models.IntegerField(blank=True, null=True)),
                ('gt_pk_khac', models.CharField(max_length=50, blank=True, null=True)),
                ('vung', models.IntegerField(blank=True, null=True)),
                ('vungdateassign', models.DateField(blank=True, null=True)),
                ('vungdatereturn', models.DateField(blank=True, null=True)),
                ('hub', models.IntegerField(blank=True, null=True)),
                ('hubdateassign', models.DateField(blank=True, null=True)),
                ('hubdatereturn', models.DateField(blank=True, null=True)),
                ('sale', models.IntegerField(blank=True, null=True)),
                ('saledateassign', models.DateField(blank=True, null=True)),
                ('saledatereturn', models.DateField(blank=True, null=True)),
            ],
            options={
                'db_table': 'company',
            },
            bases=(models.Model,),
        ),
    ]
