from django.conf.urls import patterns, url


urlpatterns = patterns(
    '',
    url(
        r'^(?P<category_id>\d+)$',
        'Dropdown.views.index',
        name='index'),
    url(
        r'^get_all',
        'Dropdown.views.get_all',
        name='get_all'),
    url(
        r'^get_one',
        'Dropdown.views.get_one',
        name='get_one'),
    url(
        r'^add_item',
        'Dropdown.views.add_item',
        name='add_item'),
    url(
        r'^edit_item',
        'Dropdown.views.edit_item',
        name='edit_item'),
    url(
        r'^remove_item',
        'Dropdown.views.remove_item',
        name='remove_item'),
)
