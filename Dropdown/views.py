import hashlib
import json
from django.http import Http404
from django.shortcuts import render
from django.http import StreamingHttpResponse
from django.shortcuts import redirect
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.db.models import F
from datetime import datetime
from django.utils.timezone import utc
from django.core.context_processors import csrf
from bson.json_util import dumps
from Common.Tools import Tools
from smereport.settings import MAX_DELAY_LOGIN
from smereport.settings import MAX_FAIL_LOGIN
#from Category.models import Category
from Dropdown.models import Dropdown
from Category.models import Category
tools = Tools()
main_model = Dropdown
category_model = Category


def index(request, category_id):
    data = {}
    category = category_model.objects.get_one({'id': category_id})['data']
    if not category:
        raise Http404
    conditions = {'category': category_id}
    list_item = main_model.objects.get_all(conditions)['data']
    data['js_vars'] = {}
    data['js_vars']['list_item'] = Tools.standardize_array(list_item)
    data['js_vars']['category'] = category
    if len(data['js_vars']) != 0:
        data['js_vars'] = tools.encode_server_vars(data['js_vars'])
    else:
        data['js_vars'] = ''
    data.update(csrf(request))
    return render(
        request,
        tools.get_app_name(__name__) + 'dropdown.html',
        data,
        context_instance=RequestContext(request)
    )


def get_all(request):
    #conditions = None
    page = int(request.POST.get('page'))
    keyword = request.POST.get('keyword', None)
    category = request.POST.get('category', None)
    conditions = {'category': category_model.objects.get_one({'pk': category}, True)['data']}
    result = main_model.objects.get_all(conditions, keyword, page)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def get_one(request):
    conditions = {
        'id': request.POST.get('id', None),
    }
    if conditions['id'] is not None:
        conditions['id'] = int(conditions['id'])
    result = main_model.objects.get_one(conditions)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def add_item(request):
    data = {
        'title': request.POST.get('title', None),
        'category': request.POST.get('category', None),
    }
    data['uid'] = tools.nice_url(data['title'])
    data['category'] = category_model.objects.get_one({'id': int(data['category'])}, True)['data']
    result = main_model.objects.add_item(data)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def edit_item(request):
    data = {
        'title': request.POST.get('title', None),
    }
    data['uid'] = tools.nice_url(data['title'])

    pk = request.POST.get('id', None)
    result = main_model.objects.edit_item(pk, data)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def remove_item(request):
    pk = request.POST.get('id', None)
    result = main_model.objects.remove_item(pk)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )
