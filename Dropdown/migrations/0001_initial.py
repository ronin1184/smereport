# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Category', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Dropdown',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('uid', models.CharField(max_length=150)),
                ('title', models.CharField(max_length=150)),
                ('category', models.ForeignKey(to='Category.Category')),
            ],
            options={
                'db_table': 'dropdown',
            },
            bases=(models.Model,),
        ),
    ]
