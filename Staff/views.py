import hashlib
import json
from django.shortcuts import render
from django.http import StreamingHttpResponse
from django.shortcuts import redirect
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.db.models import F
from datetime import datetime
from django.utils.timezone import utc
from django.core.context_processors import csrf
from bson.json_util import dumps
from Common.Tools import Tools
from smereport.settings import MAX_DELAY_LOGIN
from smereport.settings import MAX_FAIL_LOGIN
#from Category.models import Category
from User.models import User
from Category.models import Category
from Zone.models import Zone
from Dropdown.models import Dropdown
from Administrator.models import Administrator
tools = Tools()
main_model = User
category_model = Category
zone_model = Zone
dropdown_model = Dropdown
administrator_model = Administrator


def index(request):
    data = {}
    list_zone = zone_model.objects.get_all({'level': 2})['data']
    list_zone_full = zone_model.objects.get_full()['data']
    title_category = category_model.objects.get_one({'uid': 'chuc-danh'}, True)['data']
    list_title = dropdown_model.objects.get_all({'category': title_category})['data']

    admin_id = request.session['ADMIN_ID']
    admin_item = administrator_model.objects.get_one({'pk': int(admin_id)}, True)['data']
    #print admin_item
    #admin_level = admin_item.zone.level

    if admin_item.zone is None:
        list_item = main_model.objects.get_all()['data']
    else:
        list_zone_hierarchy = tools.get_list_hierarchy(admin_item.zone.pk, list_zone_full)
        list_zone_hierarchy.append(admin_item.zone.pk)
        list_item = main_model.objects.get_all({'zone_id__in': list_zone_hierarchy})['data']
    '''
    if admin_item['zone_id']:
        zone_item = zone_model.objects.get_one({'pk': admin_item['zone_id']})['data']
        admin_level = zone_item['level']
    else:
        admin_level = 0

    if admin_level == 0:
        list_item = main_model.objects.get_all()['data']
    elif admin_level == 1:
        conditions = {'zone': zone_item['id']}
        list_item = main_model.objects.get_all(conditions)['data']
    else:
        list_item = []
    '''

    data['js_vars'] = {}
    data['js_vars']['list_item'] = Tools.standardize_array(list_item)
    data['js_vars']['list_zone'] = list_zone
    data['js_vars']['list_title'] = list_title

    if len(data['js_vars']) != 0:
        data['js_vars'] = tools.encode_server_vars(data['js_vars'])
    else:
        data['js_vars'] = ''
    data.update(csrf(request))
    return render(
        request,
        tools.get_app_name(__name__) + 'staff.html',
        data,
        context_instance=RequestContext(request)
    )


def get_all(request):
    conditions = None
    page = int(request.POST.get('page'))
    keyword = request.POST.get('keyword', None)

    admin_id = request.session['ADMIN_ID']
    admin_item = administrator_model.objects.get_one({'pk': int(admin_id)})['data']
    if admin_item['zone_id']:
        zone_item = zone_model.objects.get_one({'pk': admin_item['zone_id']})['data']
        admin_level = zone_item['level']
    else:
        admin_level = 0

    if admin_level == 1:
        conditions = {'zone': zone_item['id']}

    if admin_level == 2:
        result = {
            'data': [],
            'error': None
        }
        return StreamingHttpResponse(
            tools.encode_server_vars(result),
            content_type='application/json'
        )

    result = main_model.objects.get_all(conditions, keyword, page)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def get_one(request):
    conditions = {
        'id': request.POST.get('id', None),
    }
    if conditions['id'] is not None:
        conditions['id'] = int(conditions['id'])
    result = main_model.objects.get_one(conditions)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def add_item(request):
    data = {
        'uid': tools.str_to_str(request.POST.get('uid', None)),
        'email': tools.str_to_str(request.POST.get('email', None)),
        'password': tools.str_to_str(request.POST.get('password', None)),
        'phone': tools.str_to_str(request.POST.get('phone', None)),
        'name': tools.str_to_str(request.POST.get('name', None)),
        'title': tools.str_to_int(request.POST.get('title', None)),
        'zone': tools.str_to_int(request.POST.get('zone', None)),
        'role': tools.str_to_str(request.POST.get('role', None)),
    }
    if data['email'] is not None:
        data['email'] = str(data['email']).lower()

    if data['zone'] == 'null' or int(data['zone']) == 0 or not data['zone']:
        data['zone'] = None
    else:
        data['zone'] = zone_model.objects.get_one({'id': int(data['zone'])}, True)['data']

    if data['title'] == 'null' or not data['title']:
        data['title'] = None
    else:
        data['title'] = dropdown_model.objects.get_one({'id': int(data['title'])}, True)['data']

    result = main_model.objects.add_item(data)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def edit_item(request):
    data = {
        'password': request.POST.get('password', None),
        'name': request.POST.get('name', None),
        'phone': request.POST.get('phone', None),
        'zone_id': request.POST.get('zone', None),
        'title_id': request.POST.get('title', None),
        'role': request.POST.get('role', None),
    }
    if data['zone_id'] == 'null' or int(data['zone_id']) == 0 or not data['zone_id']:
        data['zone_id'] = None
    else:
        data['zone_id'] = zone_model.objects.get_one({'id': int(data['zone_id'])}, True)['data']

    if data['title_id'] == 'null' or not data['title_id']:
        data['title_id'] = None
    else:
        data['title_id'] = dropdown_model.objects.get_one({'id': int(data['title_id'])}, True)['data']

    result = main_model.objects.add_item(data)
    pk = request.POST.get('id', None)
    result = main_model.objects.edit_item(pk, data)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def remove_item(request):
    pk = request.POST.get('id', None)
    result = main_model.objects.remove_item(pk)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )