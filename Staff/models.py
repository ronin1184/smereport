import os
import shutil
import hashlib
from django.db import models
from django.db.models import Q
from Zone.models import Zone
from Dropdown.models import Dropdown
from Common.Tools import Tools
from smereport.settings import MEDIA_ROOT
# Create your models here.


class UserModel(models.Manager):
    tools = Tools()
    limit = 25

    def get_all(self, conditions=None, keyword=None, page=1, order='-id'):
        try:
            offset = (page - 1) * self.limit

            if keyword is None:
                if conditions is None:
                    query = self.all()
                else:
                    query = self.filter(**conditions)
            else:
                query = self.filter(
                    Q(email__contains=keyword) |
                    Q(name__contains=keyword)
                )
                if conditions is None:
                    query.filter(**conditions)
            query.order_by(order)
            if offset == 0:
                query = query[: self.limit]
            else:
                query = query[offset: offset + self.limit]

            reach_bottom = False
            if query.count() < self.limit:
                reach_bottom = True

            result = Tools.standardize_array(list(query.values()))
            new_result = []
            for item in result:
                if item['role'] != 'SADMIN':
                    new_result.append(item)
            return {
                'error': None,
                'reach_bottom': reach_bottom,
                'data': new_result
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error

    def get_one(self, conditions):
        try:
            query = list(self.filter(**conditions).values())
            if not query:
                query = None
            else:
                query = query[0]
            return {
                'error': None,
                'data': query
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error

    def add_item(self, params):
        try:
            if 'zone' in params and params['zone'] is None:
                return {
                    'error': 'Zone is required.',
                    'data': None
                }
            if 'title' in params and params['title'] is None:
                return {
                    'error': 'Title is required.',
                    'data': None
                }
            item_from_uid = self.get_one({'uid': params['uid']})['data']
            if item_from_uid is not None:
                return {
                    'error': 'Duplicate username, please choose other one.',
                    'data': None
                }
            item_from_email = self.get_one({'email': params['email']})['data']
            if item_from_email is not None:
                return {
                    'error': 'Duplicate email, please choose other one.',
                    'data': None
                }
            params['salt'] = self.tools.random_str(16)
            params['password'] = hashlib.md5(params['password'] + params['salt']).hexdigest()
            item = User(**params)
            item.save()
            os.makedirs(MEDIA_ROOT + item.email)
            result = {
                'id': item.pk
            }
            return {
                'error': None,
                'data': result
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error

    def edit_item(self, pk, params):
        try:
            if 'zone' in params and params['zone'] is None:
                return {
                    'error': 'Zone is required.',
                    'data': None
                }
            if 'title' in params and params['title'] is None:
                return {
                    'error': 'Title is required.',
                    'data': None
                }
            item = self.get(pk=pk)
            if not item:
                return {
                    'error': "Item not found!",
                    'data': None
                }
            if 'email' in params:
                params.pop('email', None)
            if 'uid' in params:
                params.pop('uid', None)
            if 'password' in params:
                if not params['password'] or params['password'] == item.password:
                    params.pop('password', None)
                else:
                    params['salt'] = self.tools.random_str(16)
                    params['password'] = hashlib.md5(params['password'] + params['salt']).hexdigest()
            item.__dict__.update(params)
            item.save()
            result = {
                'id': item.pk
            }
            return {
                'error': None,
                'data': result
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error

    def remove_item(self, pk):
        try:
            if pk is not list:
                items = self.get(pk=pk)
                if not items:
                    return {
                        'error': "Item not found!",
                        'data': None
                    }
                path = MEDIA_ROOT + str(items.pk)
                if os.path.isdir(path) is not False:
                    shutil.rmtree(path)
            else:
                items = self.filter(id__in=pk)
                for i in pk:
                    path = MEDIA_ROOT + str(i)
                    if os.path.isdir(path) is not False:
                        shutil.rmtree(path)
            items.delete()
            result = {
                'id': pk
            }
            return {
                'error': None,
                'data': result
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error


class User(models.Model):
    id = models.AutoField(primary_key=True)
    cdate = models.DateTimeField(null=True, blank=True)
    uid = models.CharField(max_length=50)
    email = models.EmailField()
    phone = models.CharField(max_length=20)
    name = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    salt = models.CharField(max_length=100)
    title = models.ForeignKey(Dropdown)
    zone = models.ForeignKey(Zone)
    role = models.CharField(max_length=50)
    logfail = models.BooleanField(default=False)

    objects = UserModel()

    class Meta:
        db_table = "user"
