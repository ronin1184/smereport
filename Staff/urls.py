from django.conf.urls import patterns, url


urlpatterns = patterns(
    '',
    url(
        r'^$',
        'Staff.views.index',
        name='index'),
    url(
        r'^get_all',
        'Staff.views.get_all',
        name='get_all'),
    url(
        r'^get_one',
        'Staff.views.get_one',
        name='get_one'),
    url(
        r'^add_item',
        'Staff.views.add_item',
        name='add_item'),
    url(
        r'^edit_item',
        'Staff.views.edit_item',
        name='edit_item'),
    url(
        r'^remove_item',
        'Staff.views.remove_item',
        name='remove_item'),
)
