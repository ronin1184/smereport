from Common import Langs
from django import template
from Category.models import Category
register = template.Library()
category_model = Category

@register.inclusion_tag('back/category_sub_items.html')
def category_sub_items(cattype):
    data = {}
    data['cattype'] = cattype
    data['template_name'] = cattype + ':index'
    data['list_side_item'] = category_model.objects.get_all({'cattype': cattype})['data']
    return data
