from django.conf.urls import patterns, url


urlpatterns = patterns(
    '',
    url(
        r'^$',
        'Category.views.index',
        name='index'),
    url(
        r'^get_all',
        'Category.views.get_all',
        name='get_all'),
    url(
        r'^get_one',
        'Category.views.get_one',
        name='get_one'),
    url(
        r'^add_item',
        'Category.views.add_item',
        name='add_item'),
    url(
        r'^edit_item',
        'Category.views.edit_item',
        name='edit_item'),
    url(
        r'^remove_item',
        'Category.views.remove_item',
        name='remove_item'),
)
