# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('uid', models.CharField(max_length=150)),
                ('title', models.CharField(max_length=150)),
                ('cattype', models.CharField(max_length=50)),
                ('level', models.IntegerField()),
            ],
            options={
                'db_table': 'category',
            },
            bases=(models.Model,),
        ),
    ]
