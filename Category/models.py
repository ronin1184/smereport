import os
import shutil
from django.db import models
from django.db.models import Q
from Common.Tools import Tools
from smereport.settings import MEDIA_ROOT
# Create your models here.


class CategoryModel(models.Manager):
    tools = Tools()
    limit = 25

    def get_all(self, conditions=None, keyword=None, page=1, order='-id'):
        try:
            offset = (page - 1) * self.limit

            if conditions is not None:
                query = self.filter(**conditions)
            else:
                query = self.all()
            if keyword is not None:
                query = query.filter(
                    Q(uid__icontains=keyword) |
                    Q(title__icontains=keyword)
                )
            query = query.order_by(order)

            if offset == 0:
                query = query[: self.limit]
            else:
                query = query[offset: offset + self.limit]

            reach_bottom = False
            if query.count() < self.limit:
                reach_bottom = True

            result = Tools.standardize_array(list(query.values()))
            return {
                'error': None,
                'reach_bottom': reach_bottom,
                'data': result
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error

    def get_one(self, conditions, original=False):
        try:
            query = self.filter(**conditions)
            if original is False:
                query = list(query.values())
            if not query:
                query = None
            else:
                query = query[0]
            return {
                'error': None,
                'data': query
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error

    def add_item(self, params):
        try:
            if params['uid'] is None or params['uid'] == 'null':
                return {
                    'error': 'ID can not be null.',
                    'data': None
                }
            if params['title'] is None or params['title'] == 'null':
                return {
                    'error': 'Title can not be null.',
                    'data': None
                }
            item_from_uid = self.get_one({'uid': params['uid']})['data']
            if item_from_uid is not None:
                return {
                    'error': 'Duplicate name, please choose other one.',
                    'data': None
                }
            item = Category(**params)
            item.save()
            result = {
                'id': item.pk,
                'uid': item.uid,
                'title': item.title
            }
            return {
                'error': None,
                'data': result
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error

    def edit_item(self, pk, params):
        try:
            if params['uid'] is None or params['uid'] == 'null':
                return {
                    'error': 'ID can not be null.',
                    'data': None
                }
            if params['title'] is None or params['title'] == 'null':
                return {
                    'error': 'Title can not be null.',
                    'data': None
                }
            item = self.get(pk=int(pk))
            if not item:
                return {
                    'error': "Item not found!",
                    'data': None
                }
            item_from_uid = self.get_one({'uid': params['uid']})['data']
            if item_from_uid is not None:
                if int(item_from_uid['id']) != int(pk):
                    return {
                        'error': 'Duplicate name, please choose other one.',
                        'data': None
                    }
            item.__dict__.update(params)
            item.save()
            result = {
                'id': item.pk,
                'uid': item.uid,
                'title': item.title
            }
            return {
                'error': None,
                'data': result
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error

    def remove_item(self, pk):
        try:
            input_pk = pk
            if '|' in str(pk):
                pk = [int(x) for x in pk.split('|')]
            if type(pk) is not list:
                items = self.get(pk=int(pk))
                if not items:
                    return {
                        'error': "Item not found!",
                        'data': None
                    }
                path = MEDIA_ROOT + str(items.pk)
                if os.path.isdir(path) is not False:
                    shutil.rmtree(path)
            else:
                items = self.filter(id__in=pk)
                for i in pk:
                    path = MEDIA_ROOT + str(i)
                    if os.path.isdir(path) is not False:
                        shutil.rmtree(path)
            items.delete()
            result = {
                'id': input_pk
            }
            return {
                'error': None,
                'data': result
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error


class Category(models.Model):
    id = models.AutoField(primary_key=True)
    uid = models.CharField(max_length=150)
    title = models.CharField(max_length=150)
    cattype = models.CharField(max_length=50)
    level = models.IntegerField()

    objects = CategoryModel()

    class Meta:
        db_table = "category"
