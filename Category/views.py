import hashlib
import json
from django.shortcuts import render
from django.http import StreamingHttpResponse
from django.shortcuts import redirect
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.db.models import F
from datetime import datetime
from django.utils.timezone import utc
from django.core.context_processors import csrf
from bson.json_util import dumps
from Common.Tools import Tools
from smereport.settings import MAX_DELAY_LOGIN
from smereport.settings import MAX_FAIL_LOGIN
from smereport.settings import CAT_TYPES
#from Category.models import Category
from Category.models import Category
tools = Tools()
main_model = Category


def index(request):
    data = {}
    list_item = main_model.objects.get_all()['data']
    data['js_vars'] = {}
    data['js_vars']['list_item'] = Tools.standardize_array(list_item)
    data['js_vars']['cattype'] = CAT_TYPES
    if len(data['js_vars']) != 0:
        data['js_vars'] = tools.encode_server_vars(data['js_vars'])
    else:
        data['js_vars'] = ''
    data.update(csrf(request))
    return render(
        request,
        tools.get_app_name(__name__) + 'category.html',
        data,
        context_instance=RequestContext(request)
    )


def get_all(request):
    conditions = None
    page = int(request.POST.get('page'))
    keyword = request.POST.get('keyword', None)

    result = main_model.objects.get_all(conditions, keyword, page)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def get_one(request):
    conditions = {
        'id': request.POST.get('id', None),
    }
    if conditions['id'] is not None:
        conditions['id'] = int(conditions['id'])
    result = main_model.objects.get_one(conditions)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def add_item(request):
    data = {
        'uid': request.POST.get('uid', None),
        'title': request.POST.get('title', None),
        'cattype': request.POST.get('cattype', None),
        'level': request.POST.get('level', None),
    }
    data['level'] = int(data['level'])
    result = main_model.objects.add_item(data)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def edit_item(request):
    data = {
        'uid': request.POST.get('uid', None),
        'title': request.POST.get('title', None),
    }
    pk = request.POST.get('id', None)
    result = main_model.objects.edit_item(pk, data)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def remove_item(request):
    pk = request.POST.get('id', None)
    result = main_model.objects.remove_item(pk)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )
