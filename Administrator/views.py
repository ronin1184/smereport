import hashlib
from django.http import Http404
from django.shortcuts import render
from django.http import StreamingHttpResponse
from django.shortcuts import redirect
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.db.models import F
from datetime import datetime
from django.utils.timezone import utc
from django.core.context_processors import csrf
from bson.json_util import dumps
from Common.Tools import Tools
from smereport.settings import MAX_DELAY_LOGIN, BASE_URL
from smereport.settings import MAX_FAIL_LOGIN
from smereport.settings import TOKEN_LIFE
from Administrator.models import Administrator
from User.models import User
from Company.models import Company
from Zone.models import Zone
from Category.models import Category
from Dropdown.models import Dropdown
tools = Tools()
main_model = Administrator
zone_model = Zone
company_model = Company
user_model = User
category_model = Category
dropdown_model = Dropdown


def index(request):
    data = {}
    admin_item = main_model.objects.get_one({'id': request.session['ADMIN_ID']})['data']
    # conditions = {'parent': zone_model.objects.get_one({'pk': 387}, True)['data']}
    list_admin_full = main_model.objects.get_full()['data']
    list_zone = zone_model.objects.get_all()['data']
    list_item = main_model.objects.get_all()['data']

    data['js_vars'] = {}
    data['js_vars']['list_item'] = Tools.standardize_array(list_item)
    data['js_vars']['list_zone'] = Tools.standardize_array(list_zone)
    data['js_vars']['list_admin_full'] = Tools.standardize_array(list_admin_full)
    data['js_vars']['admin_role'] = admin_item['role']
    if len(data['js_vars']) != 0:
        data['js_vars'] = tools.encode_server_vars(data['js_vars'])
    else:
        data['js_vars'] = ''
    data.update(csrf(request))
    return render(
        request,
        tools.get_app_name(__name__) + 'administrator.html',
        data,
        context_instance=RequestContext(request)
    )


def home(request):
    # print tools.get_range_date('2014-07-09', 'month')
    data = {}
    list_zone = zone_model.objects.get_all()['data']
    admin_item = main_model.objects.get_one({'id': request.session['ADMIN_ID']})['data']
    if 'password' in admin_item:
        admin_item['password'] = None
    data['js_vars'] = {}
    data['js_vars']['admin_item'] = admin_item
    data['js_vars']['list_zone'] = list_zone
    data['js_vars']['admin_role'] = admin_item['role']
    data['admin_item'] = admin_item
    if len(data['js_vars']) != 0:
        data['js_vars'] = tools.encode_server_vars(data['js_vars'])
    else:
        data['js_vars'] = ''
    data.update(csrf(request))
    return render(
        request,
        tools.get_app_name(__name__) + 'administrator_home.html',
        data,
        context_instance=RequestContext(request)
    )


def quota(request):
    try:
        data = {}
        owner_id = request.session['ADMIN_ID']
        tinh_thanh_condition = tools.str_to_int(request.GET.get('tinh_thanh', None))
        sp_tiep_can_condition = tools.str_to_int(request.GET.get('sp_tiep_can', None))
        kq_cuoc_goi_condition = tools.str_to_int(request.GET.get('kq_cuoc_goi', None))
        kq_cuoc_gap_condition = tools.str_to_int(request.GET.get('kq_cuoc_gap', None))
        tt_khai_thac_condition = tools.str_to_int(request.GET.get('tt_khai_thac', None))
        gt_pk_khac_condition = tools.str_to_str(request.GET.get('gt_pk_khac', None))
        ngay_tra_ho_condition = tools.str_to_date(request.GET.get('ngay_tra_ho', None))

        tinh_thanh_cat = category_model.objects.get_one({'uid': 'tinh-thanh'}, True)['data']
        list_tinh_thanh = dropdown_model.objects.get_full({'category': tinh_thanh_cat})['data']

        owner_item = main_model.objects.get_one({'pk': owner_id}, True)['data']
        list_admin_full = main_model.objects.get_full()['data']
        list_user_full = user_model.objects.get_full()['data']
        list_zone_full = zone_model.objects.get_full()['data']
        if owner_item.level < 2:
            owner_type = 'ADMIN'
        else:
            owner_type = 'USER'
        if owner_item.zone is None:
            admin_level = 0
        else:
            admin_level = owner_item.zone.level

        common_conditions = {}
        if tinh_thanh_condition is not None:
            if tinh_thanh_condition == -1:
                common_conditions['tinh'] = None
            else:
                common_conditions['tinh'] = tinh_thanh_condition

        if sp_tiep_can_condition is not None:
            if sp_tiep_can_condition == -1:
                common_conditions['sp_tiep_can'] = None
            else:
                common_conditions['sp_tiep_can'] = sp_tiep_can_condition

        if kq_cuoc_goi_condition is not None:
            if kq_cuoc_goi_condition == -1:
                common_conditions['kq_cuoc_goi'] = None
            else:
                common_conditions['kq_cuoc_goi'] = kq_cuoc_goi_condition

        if kq_cuoc_gap_condition is not None:
            if kq_cuoc_gap_condition == -1:
                common_conditions['kq_cuoc_gap'] = None
            else:
                common_conditions['kq_cuoc_gap'] = kq_cuoc_gap_condition

        if tt_khai_thac_condition is not None:
            if tt_khai_thac_condition == -1:
                common_conditions['tinh_trang_khai_thac'] = None
            else:
                common_conditions['tinh_trang_khai_thac'] = tt_khai_thac_condition

        if gt_pk_khac_condition is not None:
            if gt_pk_khac_condition == -1:
                common_conditions['gt_pk_khac'] = None
            else:
                common_conditions['gt_pk_khac'] = str(gt_pk_khac_condition)

        if ngay_tra_ho_condition is not None:
            common_conditions['vungdatereturn'] = ngay_tra_ho_condition

        if admin_level == 0:
            conditions = {
                'vung': None,
                'hub': None,
                'sale': None
            }
            conditions.update(common_conditions)

            list_owner = main_model.objects.get_full({'level': 1})['data']

            list_company_owned = company_model.objects.get_full()['data']
            list_company_free_assigned = company_model.objects.get_full({'vung': None})['data']
            assign_ratio_str = tools.get_ratio_str(list_company_owned, list_company_free_assigned, True)

            admin_lv_1 = []
            admin_lv_2 = []
            user_lv_3 = []
            for admin_lv_1_raw in list_admin_full:
                if int(admin_lv_1_raw['level']) == 1:

                    list_company_owned_lv1 = company_model.objects.get_full({'vung': int(admin_lv_1_raw['id'])})['data']
                    list_company_free_assigned_lv1 = company_model.objects.get_full({'vung': int(admin_lv_1_raw['id']), 'hub': None})['data']
                    assign_ratio_str_lv1 = tools.get_ratio_str(list_company_owned_lv1, list_company_free_assigned_lv1)
                    admin_lv_1_item = {
                        'id': int(admin_lv_1_raw['id']),
                        'type': 'ADMIN',
                        'icon': 'user icon',
                        'parent': '#',
                        'text': str(admin_lv_1_raw['name'] + ' (' + assign_ratio_str_lv1 + ')')
                    }
                    admin_lv_1.append(admin_lv_1_item)

                    admin_lv1_zone_id = int(admin_lv_1_raw['zone_id'])
                    admin_lv1_zone_range = []
                    for zone in list_zone_full:
                        if zone['parent_id'] is not None:
                            if int(zone['parent_id']) == admin_lv1_zone_id:
                                admin_lv1_zone_range.append(int(zone['id']))

                    for admin_lv_2_raw in list_admin_full:
                        if int(admin_lv_2_raw['level']) == 2 and int(admin_lv_2_raw['zone_id']) in admin_lv1_zone_range:
                            list_company_owned_lv2 = company_model.objects.get_full({'hub': int(admin_lv_2_raw['id'])})['data']
                            list_company_free_assigned_lv2 = company_model.objects.get_full({'hub': int(admin_lv_2_raw['id']), 'sale': None})['data']
                            assign_ratio_str_lv2 = tools.get_ratio_str(list_company_owned_lv2, list_company_free_assigned_lv2)
                            admin_lv_2_item = {
                                'id': int(admin_lv_2_raw['id']),
                                'type': 'ADMIN',
                                'icon': 'red user icon',
                                'parent': int(admin_lv_1_raw['id']),
                                'text': str(admin_lv_2_raw['name'] + ' (' + assign_ratio_str_lv2 + ')')
                            }
                            admin_lv_2.append(admin_lv_2_item)

                            for user_raw in list_user_full:
                                if int(user_raw['zone_id']) == int(admin_lv_2_raw['zone_id']):
                                    list_company_owned_user = company_model.objects.get_full({'sale': int(user_raw['id'])})['data']
                                    assign_ratio_str_user = tools.get_ratio_str(list_company_owned_user, None, False, True)
                                    admin_lv_3_item = {
                                        'id': int(user_raw['id']),
                                        'type': 'USER',
                                        'icon': 'purple user icon',
                                        'parent': int(admin_lv_2_raw['id']),
                                        'text': str(user_raw['name'] + ' (' + assign_ratio_str_user + ')')
                                    }
                                    user_lv_3.append(admin_lv_3_item)
            admin_tree = list(admin_lv_1 + admin_lv_2 + user_lv_3)

        elif admin_level == 1:
            conditions = {
                'vung': int(owner_item.pk),
                'hub': None,
                'sale': None
            }
            conditions.update(common_conditions)

            list_child_pk = tools.get_list_hierarchy(int(owner_item.zone.pk), zone_model.objects.get_full()['data'])
            list_owner = main_model.objects.get_full({'zone__in': list_child_pk})['data']

            list_company_owned = company_model.objects.get_full({'vung': int(owner_item.pk)})['data']
            list_company_free_assigned = company_model.objects.get_full({'vung': int(owner_item.pk), 'hub': None})['data']
            assign_ratio_str = tools.get_ratio_str(list_company_owned, list_company_free_assigned, True)

            admin_lv_2 = []
            user_lv_3 = []
            admin_lv_1_raw = {
                'id': int(owner_item.pk),
                'zone_id': int(owner_item.zone.pk)
            }
            admin_lv1_zone_id = int(admin_lv_1_raw['zone_id'])
            admin_lv1_zone_range = []
            for zone in list_zone_full:
                if zone['parent_id'] is not None:
                    if int(zone['parent_id']) == admin_lv1_zone_id:
                        admin_lv1_zone_range.append(int(zone['id']))

            for admin_lv_2_raw in list_admin_full:
                if int(admin_lv_2_raw['level']) == 2 and int(admin_lv_2_raw['zone_id']) in admin_lv1_zone_range:
                    list_company_owned_lv2 = company_model.objects.get_full({'hub': int(admin_lv_2_raw['id'])})['data']
                    list_company_free_assigned_lv2 = company_model.objects.get_full({'hub': int(admin_lv_2_raw['id']), 'sale': None})['data']
                    assign_ratio_str_lv2 = tools.get_ratio_str(list_company_owned_lv2, list_company_free_assigned_lv2)
                    admin_lv_2_item = {
                        'id': int(admin_lv_2_raw['id']),
                        'type': 'ADMIN',
                        'icon': 'red user icon',
                        'parent': "#",
                        'text': str(admin_lv_2_raw['name'] + ' (' + assign_ratio_str_lv2 + ')')
                    }
                    admin_lv_2.append(admin_lv_2_item)

                    for user_raw in list_user_full:
                        if int(user_raw['zone_id']) == int(admin_lv_2_raw['zone_id']):
                            list_company_owned_user = company_model.objects.get_full({'sale': int(user_raw['id'])})['data']
                            assign_ratio_str_user = tools.get_ratio_str(list_company_owned_user, None, False, True)
                            admin_lv_3_item = {
                                'id': int(user_raw['id']),
                                'type': 'USER',
                                'icon': 'purple user icon',
                                'parent': int(admin_lv_2_raw['id']),
                                'text': str(user_raw['name'] + ' (' + assign_ratio_str_user + ')')
                            }
                            user_lv_3.append(admin_lv_3_item)
            admin_tree = list(admin_lv_2 + user_lv_3)
        elif admin_level == 2:
            conditions = {
                'hub': int(owner_item.pk),
                'sale': None
            }
            conditions.update(common_conditions)

            list_owner = user_model.objects.get_full({'zone': int(owner_item.zone.pk)})['data']

            list_company_owned = company_model.objects.get_full({'hub': int(owner_item.pk)})['data']
            list_company_free_assigned = company_model.objects.get_full({'hub': int(owner_item.pk), 'sale': None})['data']
            assign_ratio_str = tools.get_ratio_str(list_company_owned, list_company_free_assigned, True)

            user_lv_3 = []
            admin_lv_2_raw = {
                'id': int(owner_item.pk),
                'zone_id': int(owner_item.zone.pk)
            }
            for user_raw in list_user_full:
                if int(user_raw['zone_id']) == int(admin_lv_2_raw['zone_id']):
                    list_company_owned_user = company_model.objects.get_full({'sale': int(user_raw['id'])})['data']
                    assign_ratio_str_user = tools.get_ratio_str(list_company_owned_user, None, False, True)
                    admin_lv_3_item = {
                        'id': int(user_raw['id']),
                        'type': 'USER',
                        'icon': 'purple user icon',
                        'parent': "#",
                        'text': str(user_raw['name'] + ' (' + assign_ratio_str_user + ')')
                    }
                    user_lv_3.append(admin_lv_3_item)
            admin_tree = user_lv_3
        template = 'administrator_quota.html'

        list_zone_full = zone_model.objects.get_full()['data']

        ket_qua_cuoc_goi_cat = category_model.objects.get_one({'uid': 'ket-qua-cuoc-goi'}, True)['data']
        list_ket_qua_cuoc_goi = dropdown_model.objects.get_full({'category': ket_qua_cuoc_goi_cat})['data']

        ket_qua_cuoc_gap_cat = category_model.objects.get_one({'uid': 'kq-cuoc-gap'}, True)['data']
        list_ket_qua_cuoc_gap = dropdown_model.objects.get_full({'category': ket_qua_cuoc_gap_cat})['data']

        sp_tiep_can_cat = category_model.objects.get_one({'uid': 'sp-tiep-can'}, True)['data']
        list_sp_tiep_can = dropdown_model.objects.get_full({'category': sp_tiep_can_cat.pk})['data']

        tinh_trang_khai_thac_cat = category_model.objects.get_one({'uid': 'tinh-trang-khai-thac'}, True)['data']
        list_tinh_trang_khai_thac = dropdown_model.objects.get_full({'category': tinh_trang_khai_thac_cat.pk})['data']

        list_item = company_model.objects.get_all(conditions)['data']
        data['js_vars'] = {}

        data['js_vars']['list_ket_qua_cuoc_goi'] = Tools.standardize_array(list_ket_qua_cuoc_goi)
        data['js_vars']['list_ket_qua_cuoc_gap'] = Tools.standardize_array(list_ket_qua_cuoc_gap)
        data['js_vars']['list_sp_tiep_can'] = Tools.standardize_array(list_sp_tiep_can)
        data['js_vars']['list_tinh_trang_khai_thac'] = Tools.standardize_array(list_tinh_trang_khai_thac)
        data['js_vars']['list_tinh_thanh'] = Tools.standardize_array(list_tinh_thanh)
        data['js_vars']['list_user'] = Tools.standardize_array(list_user_full)
        data['js_vars']['list_zone'] = Tools.standardize_array(list_zone_full)

        data['js_vars']['assign_ratio_str'] = assign_ratio_str
        data['js_vars']['admin_tree'] = Tools.standardize_array(admin_tree)
        data['js_vars']['list_item'] = Tools.standardize_array(list_item)
        data['js_vars']['list_owner'] = Tools.standardize_array(list_owner)
        data['js_vars']['owner_type'] = owner_type
        data['js_vars']['owner_conditions'] = conditions
        data['js_vars']['user_type'] = 'ADMIN'
        data['js_vars']['user_level'] = admin_level

        data['js_vars']['tinh_thanh_condition'] = tinh_thanh_condition
        data['js_vars']['sp_tiep_can_condition'] = sp_tiep_can_condition
        data['js_vars']['kq_cuoc_goi_condition'] = kq_cuoc_goi_condition
        data['js_vars']['kq_cuoc_gap_condition'] = kq_cuoc_gap_condition
        data['js_vars']['tt_khai_thac_condition'] = tt_khai_thac_condition
        data['js_vars']['ngay_tra_ho_condition'] = ngay_tra_ho_condition

        if len(data['js_vars']) != 0:
            data['js_vars'] = tools.encode_server_vars(data['js_vars'])
        else:
            data['js_vars'] = ''
        data.update(csrf(request))
        return render(
            request,
            tools.get_app_name(__name__) + template,
            data,
            context_instance=RequestContext(request)
        )
    except Exception as e:
            error = tools.return_exception(e)
            # print(error)
            return error


def quota_preview(request, user_type, id):
    data = {}
    if user_type == 'ADMIN':
        owner_item = main_model.objects.get_one({'pk': id}, True)['data']
        admin_level = owner_item.zone.level
        if admin_level == 0:
            raise Http404
        elif admin_level == 1:
            conditions = {
                'vung': int(owner_item.pk),
                # 'hub': None,
                # 'sale': None
            }
        elif admin_level == 2:
            conditions = {
                'hub': int(owner_item.pk),
                # 'sale': None
            }
        else:
            raise Http404
    elif user_type == 'USER':
        owner_item = user_model.objects.get_one({'pk': id}, True)['data']
        conditions = {
            'sale': int(owner_item.pk)
        }
    else:
        raise Http404

    ket_qua_cuoc_goi_cat = category_model.objects.get_one({'uid': 'ket-qua-cuoc-goi'}, True)['data']
    list_ket_qua_cuoc_goi = dropdown_model.objects.get_full({'category': ket_qua_cuoc_goi_cat})['data']

    ket_qua_cuoc_gap_cat = category_model.objects.get_one({'uid': 'ket-qua-cuoc-gap'}, True)['data']
    list_ket_qua_cuoc_gap = dropdown_model.objects.get_full({'category': ket_qua_cuoc_gap_cat})['data']

    sp_tiep_can_cat = category_model.objects.get_one({'uid': 'sp-tiep-can'}, True)['data']
    list_sp_tiep_can = dropdown_model.objects.get_full({'category': sp_tiep_can_cat.pk})['data']

    tinh_trang_khai_thac_cat = category_model.objects.get_one({'uid': 'tinh-trang-khai-thac'}, True)['data']
    list_tinh_trang_khai_thac = dropdown_model.objects.get_full({'category': tinh_trang_khai_thac_cat.pk})['data']

    list_item = company_model.objects.get_all(conditions)['data']

    data['js_vars'] = {}
    data['js_vars']['list_ket_qua_cuoc_goi'] = Tools.standardize_array(list_ket_qua_cuoc_goi)
    data['js_vars']['list_ket_qua_cuoc_gap'] = Tools.standardize_array(list_ket_qua_cuoc_gap)
    data['js_vars']['list_sp_tiep_can'] = Tools.standardize_array(list_sp_tiep_can)
    data['js_vars']['list_tinh_trang_khai_thac'] = Tools.standardize_array(list_tinh_trang_khai_thac)

    data['js_vars']['list_item'] = Tools.standardize_array(list_item)
    data['js_vars']['owner_conditions'] = conditions

    if len(data['js_vars']) != 0:
        data['js_vars'] = tools.encode_server_vars(data['js_vars'])
    else:
        data['js_vars'] = ''
    data['owner'] = owner_item.name
    data['zone_name'] = owner_item.zone.title
    data.update(csrf(request))

    return render(
        request,
        tools.get_app_name(__name__) + 'administrator_quota_preview.html',
        data,
        context_instance=RequestContext(request)
    )


def get_all(request):
    conditions = None
    page = int(request.POST.get('page'))
    keyword = request.POST.get('keyword', None)

    result = main_model.objects.get_all(conditions, keyword, page)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def get_one(request):
    conditions = {
        'id': request.POST.get('id', None),
    }
    if conditions['id'] is not None:
        conditions['id'] = int(conditions['id'])
    result = main_model.objects.get_one(conditions)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def add_item(request):
    admin_item = main_model.objects.get_one({'id': request.session['ADMIN_ID']})['data']
    admin_role = admin_item['role']
    data = {
        'uid': tools.str_to_str(request.POST.get('uid', None)),
        'email': tools.str_to_str(request.POST.get('email', None)),
        'password': tools.str_to_str(request.POST.get('password', None)),
        'name': tools.str_to_str(request.POST.get('name', None)),
        'zone': tools.str_to_int(request.POST.get('zone', None)),
        # 'role': request.POST.get('role', None),
    }
    if data['uid'] is not None:
        data['uid'] = str(data['uid']).lower()

    if data['email'] is not None:
        data['email'] = str(data['email']).lower()

    if data['zone'] == 'null' or int(data['zone']) == 0 or not data['zone']:
        if admin_role == 'SADMIN' or admin_role == 'ADMIN':
            data['zone'] = None
            data['level'] = 0
            data['role'] = 'ADMIN'
        else:
            result = {
                'error': 'Missing zone.',
                'data': None
            }
            return StreamingHttpResponse(
                result,
                content_type='application/json'
            )
    else:
        data['zone'] = zone_model.objects.get_one({'id': int(data['zone'])}, True)['data']
        data['level'] = data['zone'].level
        data['role'] = 'SUBADMIN'
        if data['zone'] is None:
            result = {
                'error': 'Can not assign non exist zone.',
                'data': None
            }
            return StreamingHttpResponse(
                result,
                content_type='application/json'
            )
    result = main_model.objects.add_item(data)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def edit_item(request):
    admin_item = main_model.objects.get_one({'id': request.session['ADMIN_ID']})['data']
    admin_role = admin_item['role']
    data = {
        'name': request.POST.get('name', None),
        'password': request.POST.get('password', None),
        'zone_id': request.POST.get('zone', None),
        # 'role': request.POST.get('role', None),
    }
    # if request.session.get('ADMIN_ROLE') == 'SUBADMIN':
    #    data.pop('zone_id', None)
    #    data.pop('role', None)
    if data['zone_id'] == 'null' or int(data['zone_id']) == 0 or not data['zone_id']:
        if admin_role == 'SADMIN' or admin_role == 'ADMIN':
            data['zone_id'] = None
            data['level'] = 0
            data['role'] = 'ADMIN'
        else:
            result = {
                'error': 'Missing zone.',
                'data': None
            }
            return StreamingHttpResponse(
                result,
                content_type='application/json'
            )
    else:
        data['zone_id'] = zone_model.objects.get_one({'id': int(data['zone_id'])}, True)['data']
        data['level'] = data['zone_id'].level
        data['role'] = 'SUBADMIN'
        if data['zone_id'] is None:
            result = {
                'error': 'Can not assign non exist zone.',
                'data': None
            }
            return StreamingHttpResponse(
                result,
                content_type='application/json'
            )

    # result = main_model.objects.add_item(data)
    pk = request.POST.get('id', None)
    # item = main_model.objects.get_one({'pk': int(pk)})['data']
    # if item['role'] == 'SADMIN':
    #    if 'role' in data:
    #        data.pop('role', None)
    result = main_model.objects.edit_item(pk, data)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def remove_item(request):
    pk = request.POST.get('id', None)
    result = main_model.objects.remove_item(pk)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def forgot_password(request):
    uid = request.POST.get('uid', None)
    user = main_model.objects.get_one({"uid": uid})['data']
    try:
        if user is not None:
            token = tools.random_str(32)
            tokendate = datetime.now()
            main_model.objects.edit_item(int(user['id']), {'token': hashlib.md5(token.encode('utf-8')).hexdigest(), 'tokendate': tokendate})
            email = user['email']
            body = 'Please click <a href="' + BASE_URL + '/administrator/reset_password/' + str(user['id']) + '/' + str(token) + '">here</a> to see your new password.'
            tools.send_email('New password request', body, [email])
            status = 'OK'
        else:
            status = 'Not OK'
        result = {
            'data': status,
            'error': None
        }
        return StreamingHttpResponse(
            tools.encode_server_vars(result),
            content_type='application/json'
        )
    except Exception as e:
        error = tools.return_exception(e)
        # print(error)
        return error


def reset_password(request, user_id, token):
    user = main_model.objects.get_one({"id": user_id})['data']
    if user is not None:
        if user['token'] and user['token'] == hashlib.md5(token.encode('utf-8')).hexdigest():
            tokendate = user['tokendate']
            dif_time = datetime.utcnow().replace(tzinfo=utc) - tokendate
            dif_time = dif_time.total_seconds()
            if dif_time >= TOKEN_LIFE*60:
                raise Http404
            else:
                password = tools.random_str(25)
                main_model.objects.edit_item(int(user['id']), {'password': password, 'token': None, 'tokendate': None})
                return StreamingHttpResponse(
                    password,
                    content_type='text/html'
                )
        else:
            raise Http404
    else:
        raise Http404


def login(request):
    data = {}
    # conditions = {'pk': 6}
    # list_admin = Administrator.objects.get_one(conditions)
    # data['category'] = list_admin
    data.update(csrf(request))
    return render(
        request,
        tools.get_app_name(__name__) + 'login.html',
        data,
        context_instance=RequestContext(request))


def authenticate(request):
    if request.method == 'POST':
        # item = Administrator.objects.get_one({'pk': 6})['data']
        # print item
        uid = request.POST.get('uid', None)
        password = request.POST.get('password', None)
        item = main_model.objects.get_one({'uid': uid})['data']
        if item is None:
            res_data = {
                'error': 'Wrong username/password',
                'data': None
            }
        else:
            cdate = item['cdate']
            if cdate:
                dif_time = datetime.utcnow().replace(tzinfo=utc) - cdate
                dif_time = dif_time.total_seconds()
            else:
                dif_time = 0
            if not item['logfail']:
                item['logfail'] = 0
            if item['logfail'] < MAX_FAIL_LOGIN or dif_time >= MAX_DELAY_LOGIN*60:
                if item is not None:
                    if hashlib.md5(password.encode('utf-8') + item['salt'].encode('utf-8')).hexdigest() == item['password']:
                        request.session['ADMIN_FULLNAME'] = item['name']
                        request.session['ADMIN_ID'] = str(item['id'])
                        request.session['ADMIN_ROLE'] = item['role']
                        res_data = {
                            'error': None,
                            'data': 'OK'
                        }
                        main_model.objects.edit_item(item['id'], {'cdate': datetime.now(), 'logfail': 0})
                    else:
                        # print("Case 1")
                        if dif_time >= MAX_DELAY_LOGIN*60:
                            main_model.objects.edit_item(item['id'], {'cdate': datetime.now(), 'logfail': 0})
                        else:
                            main_model.objects.edit_item(item['id'], {'cdate': datetime.now(), 'logfail': F('logfail') + 1})
                        res_data = {
                            'error': 'Wrong username/password',
                            'data': None
                        }
                else:
                    # print("Case 2")
                    main_model.objects.edit_item(item['id'], {'cdate': datetime.now(), 'logfail': F('logfail')+1})
                    res_data = {
                        'error': 'Wrong username/password',
                        'data': None
                    }
            else:
                res_data = {
                    'error': 'You have been logged fail ' + str(MAX_FAIL_LOGIN) + ' times. Please relax in ' + str(MAX_DELAY_LOGIN) + ' minutes (' + str(int(dif_time/60)) + ' / '+str(MAX_DELAY_LOGIN)+').',
                    'data': None
                }
            # main_model.edit_item({'cdate': datetime.now(), 'logfail': 0}, item['id'])
        return StreamingHttpResponse(dumps(res_data), content_type='application/json')
    return StreamingHttpResponse(
        dumps({}),
        content_type='application/json'
    )


def logout(request):
    if 'ADMIN_FULLNAME' in request.session:
        if request.session['ADMIN_FULLNAME']:
            del request.session['ADMIN_FULLNAME']

    if 'ADMIN_ID' in request.session:
        if request.session['ADMIN_ID']:
            del request.session['ADMIN_ID']

    if 'ADMIN_ROLE' in request.session:
        if request.session['ADMIN_ROLE']:
            del request.session['ADMIN_ROLE']

    return redirect(reverse('administrator:login'))
