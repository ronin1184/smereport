# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Administrator', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='administrator',
            name='logfail',
            field=models.NullBooleanField(default=False),
            preserve_default=True,
        ),
    ]
