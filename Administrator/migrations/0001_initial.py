# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Zone', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Administrator',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('cdate', models.DateTimeField(null=True, blank=True)),
                ('uid', models.CharField(max_length=150)),
                ('email', models.EmailField(max_length=75)),
                ('name', models.CharField(max_length=100)),
                ('password', models.CharField(max_length=100)),
                ('salt', models.CharField(max_length=100)),
                ('level', models.IntegerField()),
                ('token', models.CharField(null=True, max_length=50, blank=True)),
                ('tokendate', models.DateTimeField(null=True, blank=True)),
                ('role', models.CharField(max_length=50)),
                ('logfail', models.BooleanField(default=False)),
                ('zone', models.ForeignKey(null=True, to='Zone.Zone', blank=True)),
            ],
            options={
                'db_table': 'admin',
            },
            bases=(models.Model,),
        ),
    ]
