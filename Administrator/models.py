import os

import shutil
import hashlib
from django.db import models
from django.db.models import Q
from Zone.models import Zone
from Company.models import Company
from Common.Tools import Tools
from smereport.settings import MEDIA_ROOT
from smereport.settings import IS_PROD
# Create your models here.

zone_model = Zone


class AdministratorModel(models.Manager):
    company = Company
    tools = Tools()
    limit = 25

    def get_all(self, conditions=None, keyword=None, page=1, order='-id'):
        try:
            offset = (page - 1) * self.limit

            if conditions is not None:
                query = self.filter(**conditions)
            else:
                query = self.all()
            if keyword is not None:
                query = query.filter(
                    Q(uid__icontains=keyword) |
                    Q(email__icontains=keyword) |
                    Q(name__icontains=keyword)
                )
            query = query.order_by(order)

            if offset == 0:
                query = query[: self.limit]
            else:
                query = query[offset: offset + self.limit]

            reach_bottom = False
            if query.count() < self.limit:
                reach_bottom = True

            result = Tools.standardize_array(list(query.values()))
            new_result = []
            for item in result:
                if item['role'] != 'SADMIN':
                    new_result.append(item)
            return {
                'error': None,
                'reach_bottom': reach_bottom,
                'data': new_result
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error

    def get_full(self, conditions=None):
        try:
            if conditions is None:
                query = self.all()
            else:
                query = self.filter(**conditions)
            result = Tools.standardize_array(list(query.values()))
            new_result = []
            for item in result:
                if item['role'] != 'SADMIN' and item['zone_id'] is not None:
                    new_result.append(item)
            return {
                'error': None,
                'data': new_result
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error

    def get_one(self, conditions, original=False):
        try:
            query = self.filter(**conditions)
            if original is False:
                query = list(query.values())
            if not query:
                query = None
            else:
                query = query[0]
            return {
                'error': None,
                'data': query
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error

    def add_item(self, params):
        try:
            if params['uid'] is None:
                return {
                    'error': 'Missing username.',
                    'data': None
                }
            if params['email'] is None:
                return {
                    'error': 'Missing email.',
                    'data': None
                }
            if params['password'] is None:
                return {
                    'error': 'Missing password.',
                    'data': None
                }
            if params['name'] is None:
                return {
                    'error': 'Missing name.',
                    'data': None
                }
            # if params['zone'] is None:
            #    return {
            #        'error': 'Missing zone.',
            #        'data': None
            #    }
            item_from_uid = self.get_one({'uid': params['uid']})['data']
            if item_from_uid is not None:
                return {
                    'error': 'Duplicate username, please choose other one.',
                    'data': None
                }
            item_from_email = self.get_one({'email': params['email']})['data']
            if item_from_email is not None:
                return {
                    'error': 'Duplicate email, please choose other one.',
                    'data': None
                }
            if params['zone'] is not None:
                item_from_zone = self.get_one({'zone': params['zone']})['data']
                if item_from_zone is not None:
                    return {
                        'error': 'Can not assign one zone to multiple administrators.',
                        'data': None
                    }

            params['salt'] = self.tools.random_str(16)
            params['password'] = hashlib.md5(params['password'].encode('utf-8') + params['salt'].encode('utf-8')).hexdigest()
            item = Administrator(**params)
            item.save()
            if IS_PROD:
                subject = 'User registration success'
                body = 'Your account with id: ' + str(item.uid) + ' can use now!'
                to = item.email
                self.tools.send_email(subject, body, [to])
            if not os.path.exists(MEDIA_ROOT + 'admin/' + str(item.email)):
                os.makedirs(MEDIA_ROOT + 'admin/' + str(item.email))
            result = {
                'id': item.pk
            }
            return {
                'error': None,
                'data': result
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error

    def edit_item(self, pk, params):
        try:
            item = self.get(pk=pk)
            if not item:
                return {
                    'error': "Item not found!",
                    'data': None
                }
            if params['zone_id'] is not None:
                item_from_zone = self.get_one({'zone': params['zone_id']})['data']
                if item_from_zone is not None and item.zone.pk != item_from_zone['zone_id']:
                    return {
                        'error': 'Can not assign one zone to multiple administrators.',
                        'data': None
                    }
            if 'email' in params:
                params.pop('email', None)
            if 'uid' in params:
                params.pop('uid', None)
            if 'password' in params:
                # print(('password: ' + str(params['password'])))
                if not params['password'] or len(params['password'].strip()) < 3 or params['password'] == 'null' or params['password'] == item.password:
                    params.pop('password', None)
                else:
                    params['salt'] = self.tools.random_str(16)
                    params['password'] = hashlib.md5(params['password'].encode('utf-8') + params['salt'].encode('utf-8')).hexdigest()
            item.__dict__.update(params)
            item.save()
            result = {
                'id': item.pk
            }
            return {
                'error': None,
                'data': result
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error

    def remove_item(self, pk):
        try:
            input_pk = pk
            if '|' in str(pk):
                pk = [int(x) for x in pk.split('|')]
            if type(pk) is not list:
                items = self.get(pk=pk)
                if not items:
                    return {
                        'error': "Item not found!",
                        'data': None
                    }
                path = MEDIA_ROOT + 'admin/' + str(items.email)
                if os.path.isdir(path) is not False:
                    shutil.rmtree(path)
                if items.level == 2:
                    owner_type = 'hub'
                elif items.level == 1:
                    owner_type = 'vung'
                else:
                    owner_type = None
                self.company.objects.remove_owner(pk, owner_type)
            else:
                items = self.filter(id__in=pk)
                for i in pk:
                    item = self.get(pk=i)
                    if item:
                        path = MEDIA_ROOT + 'admin/' + str(item.email)
                        if os.path.isdir(path) is not False:
                            shutil.rmtree(path)

                        if item.level == 2:
                            owner_type = 'hub'
                        elif item.level == 1:
                            owner_type = 'vung'
                        else:
                            owner_type = None
                        self.company.objects.remove_owner(i, owner_type)
            items.delete()
            result = {
                'id': input_pk
            }
            return {
                'error': None,
                'data': result
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error


class Administrator(models.Model):
    id = models.AutoField(primary_key=True)
    cdate = models.DateTimeField(null=True, blank=True)
    uid = models.CharField(max_length=150)
    email = models.EmailField()
    name = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    salt = models.CharField(max_length=100)
    zone = models.ForeignKey(Zone, null=True, blank=True)
    level = models.IntegerField()
    token = models.CharField(max_length=50, null=True, blank=True)
    tokendate = models.DateTimeField(null=True, blank=True)
    role = models.CharField(max_length=50)
    logfail = models.NullBooleanField(default=False)

    objects = AdministratorModel()

    class Meta:
        db_table = "admin"
