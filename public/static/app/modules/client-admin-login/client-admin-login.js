'use strict';

/* Controllers */
App.controller("ClientAdminLogin",function($scope, $rootScope, CommonAssess, ClientAdminLoginModel){
	$scope.model			=ClientAdminLoginModel;
	$scope.fid	="";
	$scope.id	="";
	$scope.password	="";
	$scope.captcha="";
	$scope.captchaUrl;

	function getCaptcha(){
		$scope.model.getCaptcha().success(function(result){
			$scope.captchaUrl=result.data.captchaUrl;
		});
	}

	//getCaptcha();

	$scope.submitFormLogin=function(){
		var params = new FormData();
		var uidElmValue = $('#uid').val();
		var passwordElmValue = $('#password').val();
		var uid = $scope.uid?$scope.uid:uidElmValue;
		var password = $scope.password?$scope.password:passwordElmValue;
		params.append('uid', uid);
		params.append('password', password);

		CommonAssess.showWaitingHeader(true);
		
		//console.log($rootScope.baseUrl+"administrator/authenticate");
		$scope.model.authen(params).done(function(result){
			CommonAssess.showWaitingHeader(false);
			//console.log(result);
			if(!result.error){
				if(result.data == 'OK'){
					window.location = $scope.baseUrl+"administrator/home";
				}else{
					$scope.model.other.errorMessage = CommonAssess.processErrorMessage(result.error);
				}
			}else{
				$scope.model.other.errorMessage = CommonAssess.processErrorMessage(result.error);
			}
			$scope.$apply();
		});	
	}
	
	$scope.submitFormForgotPassword = function(){
		var uid = prompt("Please enter your username", '');
        var params = new FormData();
		params.append('uid', uid);
	    if (uid) {
	    	CommonAssess.showWaitingHeader(true);
	        //console.log("Hello " + email + "! How are you today?");
	    	$scope.model.forgotPassword(params).done(function(result){
	    		CommonAssess.showWaitingHeader(false);
	    		alert("Please checking your email to change password.");
	    	});
	    }
	}
});
