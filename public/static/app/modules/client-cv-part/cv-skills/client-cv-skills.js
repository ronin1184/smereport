'use strict';

/* Controllers */
App.controller("ClientCvSkills",function($scope, $filter, ClientCvSkillsModel, CommonAssess){
	$scope.model = ClientCvSkillsModel;

	var serverVarsElm = $('#js-vars');
	var currentPage = 1;
	var maxRecords = 70;
	var init = true;

	var jsVars = serverVarsElm.val()?JSON.parse(serverVarsElm.val()):null;

	$scope.model.vars.top_id = jsVars._id;
	$scope.model.vars.target = 'skills';
	$scope.model.vars.part = 'skills';
	$scope.model.vars.reach_bottom = false;
	$scope.model.vars.isBusy = false;
	$scope.model.vars.isFilter = false;
	//$scope.model.vars.listItemFull = CommonAssess.getStandardResultArray(jsVars.cv_content,'skills');
	$scope.model.vars.listItemFull = jsVars.skills_list;
	$scope.model.vars.skill_ref = jsVars.skill_ref;
	$scope.model.vars.listItemFilter = null;

	$scope.model.vars.sortableOptions = {
		update: function(e, ui) {
			setTimeout(function(){
				
				var arrangeStr="";
				var itemCount = $scope.model.vars.listItemFull.length
				for(var i in $scope.model.vars.listItemFull){
					if(i < itemCount - 1){
						arrangeStr += $scope.model.vars.listItemFull[i]['_id'] + "," + (itemCount - parseInt(i)) + "|";
					}else{
						arrangeStr += $scope.model.vars.listItemFull[i]['_id'] + "," + (itemCount - parseInt(i));
					}	
				}

				var params = new FormData();
		        params.append('top_id', $scope.model.vars.top_id);
		        params.append('target', $scope.model.vars.target);
		        params.append('order', arrangeStr);

				$scope.model.api.rearrange($scope.model.vars.serverController, $scope.model.vars.csrftoken, params).success(function(result){});
				$scope.$apply();
				
			},50);
		},
		axis: 'y'
	};
	/*-------------------------------------Init update list-------------------------------------*/

	$scope.model.act.updateListItem($scope.model.vars.listItemFull);	

	function get_all(is_append){
		CommonAssess.showWaitingHeader(true);
		var params = new FormData();
        
        params.append('top_id', $scope.model.vars.top_id);
        params.append('target', $scope.model.vars.target);
        if($scope.model.vars.inputFilter){
        	params.append('keyword', $scope.model.vars.inputFilter);
        }
        params.append('page', currentPage);
        $scope.model.vars.isBusy = true;
		$scope.model.api.getAll($scope.model.vars.serverController, $scope.model.vars.csrftoken, params).done(function(result){
			CommonAssess.showWaitingHeader(false);
			$scope.model.vars.isBusy = false;

			if(result.data){
				$scope.model.vars.reach_bottom = result.reach_bottom
				if(is_append){
					if(maxRecords < $scope.model.vars.listItemFull.length){
						$scope.model.vars.listItemFull = $scope.model.vars.listItemFull.slice(-5);
					}
					$scope.model.vars.listItemFull = $scope.model.vars.listItemFull.concat(result.data);
				}else{
					$scope.model.vars.listItemFull = result.data;
				}
				$scope.model.act.updateListItem($scope.model.vars.listItemFull);
			}
			$scope.$apply();
		});
	}

	$scope.openFormEdit = function(_id){
		$scope.model.act.openFormEdit(_id);
	};

	$scope.openFormRemove = function(_id){
		if(_id){
			if(_id.split("|").length > 1){
				$scope.formRemoveInfo = "Do you realy want to remove these records ?";
			}else{
				$scope.formRemoveInfo = "Do you realy want to remove this records ?";
			}
			$scope.model.fields._id = _id;
			var confirmRemove = confirm($scope.formRemoveInfo);
			if(confirmRemove){
				submitFormRemove();
			}
		}
	};

	function submitFormRemove(){
		CommonAssess.showWaitingHeader(true);
		var params = new FormData();
		params.append('top_id', $scope.model.vars.top_id);
		params.append('target', $scope.model.fields._id);
		$scope.model.api.removeItem($scope.model.vars.serverController, $scope.model.vars.csrftoken, params).done(function(result){
			$scope.model.act.afterSubmit(result, 'remove');
			CommonAssess.showWaitingHeader(false);
		});
	};

	$scope.selectCheckBox = function(){
		$scope.model.vars.listItemId=getListItemId();
	};

	$scope.selectAllCheckBox = function(){
		var checkedItems = $('.cv-part-skills-checkbox:checked');
		var checkBox = $(".cv-part-skills-checkbox");

		if(checkedItems.length > 0 && checkedItems.length < checkBox.length){
			checkBox.prop("checked", true);
		}else if(checkedItems.length == checkBox.length){
			checkBox.prop("checked", false);
		}else{
			checkBox.prop("checked", true);
		}
		$scope.model.vars.listItemId = getListItemId();
	};

	function getListItemId(){
		var checkedItems = $('.cv-part-skills-checkbox:checked');
		var listItemId = "";
		if(checkedItems.length > 0){
			checkedItems.each(function(key,value){
				if(key == 0){
					listItemId = $(value).attr("data");
				}else{
					listItemId += "|" + $(value).attr("data");
				}
			});
		}
		return listItemId;
	};
});

/*----------------------------------------------MODAL CONTROLLER--------------------------------------------*/

App.controller("ClientCvSkillsModal",function($scope, ClientCvSkillsModel){
	$scope.model = ClientCvSkillsModel;

	$scope.submitFormEdit = function(isContinue){
		isContinue = typeof isContinue !== 'undefined' ? isContinue : false;
		$scope.model.act.submitFormEdit(isContinue);
	};
});