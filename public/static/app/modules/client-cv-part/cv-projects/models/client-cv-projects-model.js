'use strict';

/* Services */
var ClientCvProjectsModel=angular.module('ClientCvProjectsModel',[]);
ClientCvProjectsModel.factory('ClientCvProjectsModel',function($http, $rootScope, CommonAssess){
    var serverController = 'cv/parts';
	var csrftoken = $('[name="csrfmiddlewaretoken"]').val();

    var firstInput = $('#project');
    var secondInput = $('#project');

    var serverVarsElm = $('#js-vars');
    
    var startPoint = 0;
    var perPage = 15;

    /*--------------------------------------------------Variables--------------------------------------------*/    

    var vars = {
        isFilter : false,
        listItem : null,
        inputFilter : null,
        listPercentText : null,
        listItemFull : null,
        listItemFilter : null,
        displayLoading : false,
        errorMessage : null,
        isEdit : false,
        listItemId : null,
        top_id : null,
        startPoint : startPoint,
        perPage : perPage,
        serverController : serverController,
        csrftoken : csrftoken
    };

    /*--------------------------------------------------Addition APIs-----------------------------------------*/

    var api = CommonAssess.api;
    api.rearrange = function(controller, csrftoken, params){
        return $.ajax({
            url : $rootScope.baseUrl + controller + "/rearrange",
            type : 'POST',
            headers : {'X-CSRFToken' : csrftoken},
            processData : false,
            contentType : false,
            data : params,
        });
    };
    
	var model = {
        api : api,

		vars : vars,

        fields : {},

        ref : {},

        act : {
            updateListItem : function(listItemFullInput){
                updateListItem(listItemFullInput);
            },

            openFormEdit : function(_id){
                openFormEdit(_id);
            },

            closeFormEdit : function(){
                closeFormEdit();
            },

            setDisplayLoading : function(isDisplay){
                setDisplayLoading(isDisplay);
            },

            submitFormEdit : function(isContinue){
                submitFormEdit(isContinue);
            },

            afterSubmit : function(result, action){
                afterSubmit(result, action);
            },

            emptyModelFieldsVariables : function(){
                emptyModelFieldsVariables();
            },

            setModelFieldsVariables : function(item){
                setModelFieldsVariables(item);
            }
        }
	} 

    /*-----------------------------------------------Init UI----------------------------------------------*/

    var modal = $.remodal.lookup[$('[data-remodal-id = projects-modal]').data('remodal')];

    $(document).on('opened', '.remodal', function () {
        if(!model.vars.isEdit){
            setTimeout(function(){
                firstInput.focus();
            }, 0);
        }else{
            setTimeout(function(){
                secondInput.focus();
            }, 0);
        }
    });

    /*---------------------------------------------Controller functions---------------------------------------------*/

    var updateListItem = function(listItemFullInput){
        if(!listItemFullInput) return;
        var maxPoint = listItemFullInput.length;
        model.vars.listItem = listItemFullInput;
    }

    var openFormEdit = function(_id){
        emptyModelFieldsVariables();
        setDisplayLoading(false);
        if(_id){
            //if(_id.$oid) _id = _id.$oid;
            CommonAssess.showWaitingHeader(true);
            model.fields._id = _id;
            model.vars.isEdit = true;

            var params = new FormData();
            params.append('top_id', model.vars.top_id);
			params.append('target', model.fields._id);
            model.api.getSingle(serverController, csrftoken, params).done(function(result){
                setModelFieldsVariables(result.data);
                CommonAssess.showWaitingHeader(false);
                modal.open();
                $rootScope.$apply();
            });
        }else{
            model.vars.isEdit = false;
            modal.open();
        }
    };

    var closeFormEdit = function(){
        modal.close();
    };

    var submitFormEdit = function(isContinue){
        setDisplayLoading(true);

        var params = new FormData();

        params.append('top_id' 	,model.vars.top_id);
        
        if(model.vars.isEdit){
            params.append('target'  ,model.fields._id);
		}else{
            params.append('target'  ,model.vars.target);
        }

        params.append('part', model.vars.part);

		params.append('project', model.fields.project);
        params.append('role', model.fields.role);
        params.append('description', model.fields.description);
        params.append('teamSize', model.fields.teamSize);
        params.append('demo', model.fields.demo);
		
        params.append('is_edit', model.vars.isEdit ? 1 : 0);
        

        
        model.api.editItem(serverController, csrftoken, params).done(function(result){
            if(model.vars.isEdit){
                afterSubmit(result, 'edit', isContinue);
            }else{
                afterSubmit(result, 'add', isContinue);
            }
        });
    };

    var afterSubmit = function(result, action, isContinue){

        if(!result.error){
            var newItem = {
                _id : result.data._id.toString(),
                project : result.data.project ? result.data.project : model.fields.project,
                role : result.data.role ? result.data.role : model.fields.role
            };

            switch(action){
                case 'edit':
                    isContinue = false;
                    var index = CommonAssess.getIndexFromId(model.vars.listItemFull, newItem._id);
                    model.vars.listItemFull[index]=newItem;
                break;

                case 'add':
                    model.vars.listItemFull.unshift(newItem);
                    startPoint = 0;
                break;

                case 'remove':
                    isContinue = false;
                    var listId = newItem._id.split('|');
                    for(var i in listId){
                        var index = CommonAssess.getIndexFromId(model.vars.listItemFull, listId[i]);
                        model.vars.listItemFull.splice(index,1);   
                    }
                    startPoint = 0;
                break;
            }

            updateListItem(model.vars.listItemFull);
            if(isContinue){
                emptyModelFieldsVariables();
                firstInput.focus();
            }else{
                closeFormEdit();    
            }
        }else{
            model.vars.errorMessage = CommonAssess.processErrorMessage(result.error);
        }
        setDisplayLoading(false);
        $rootScope.$apply();
    };

    var setDisplayLoading = function(isDisplay){
        model.vars.displayLoading = isDisplay;
    };

    var emptyModelFieldsVariables = function(){
        model.vars.errorMessage = CommonAssess.processErrorMessage(null);

        model.fields.project = '';
        model.fields.role = '';
        model.fields.description = '';
        model.fields.teamSize = 1;
        model.fields.demo = '';
    };

    var setModelFieldsVariables = function(item){
        model.vars.errorMessage = CommonAssess.processErrorMessage(null);

        model.fields.project = item.project;
        model.fields.role = item.role;
        model.fields.description = item.description;
        model.fields.teamSize = parseInt(item.teamSize);
        model.fields.demo = item.demo;
    };

    return model;  
});