'use strict';

/* Controllers */
App.controller("ClientCvExport",function($scope, $filter, ClientCvExportModel, CommonAssess){
	$scope.model = ClientCvExportModel;

	var serverVarsElm = $('#js-vars');

	var jsVars = serverVarsElm.val()?JSON.parse(serverVarsElm.val()):null;
	
	$scope.model.vars.parent = jsVars.parent;
	$scope.model.vars._id = jsVars._id;
	$scope.model.vars.cvContent = jsVars.cv_content;
	$scope.model.vars.cvPartsList = jsVars.cv_parts_list;
	$scope.model.ref.listTemplate = jsVars.list_template;
	$scope.model.ref.listTemplateRef = CommonAssess.listDropdownRef(jsVars.list_template);
	$scope.model.vars.templateThumb = null;

	var savedTemplateValue = null;
	if($scope.model.vars.cvContent.template){
		savedTemplateValue = $scope.model.vars.cvContent.template.$oid;
	}

	var templateSelectOption = {
        maxItems : 1,
        valueField : '_id',
        labelField : 'name',
        searchField : ['name'],
        sortField: [{field: 'order', direction: 'asc'}],
        options : $scope.model.ref.listTemplate
    };

    var template = $('#template').selectize(templateSelectOption);
    $scope.model.fields.template = template[0].selectize;
    if(!savedTemplateValue){
    	$scope.model.fields.template.setValue($scope.model.ref.listTemplate[0]._id);
	}else{
		$scope.model.fields.template.setValue(savedTemplateValue);
	}
    $scope.model.vars.templateThumb = $scope.mediaUrl + $scope.model.ref.listTemplateRef[$scope.model.fields.template.getValue()].thumbnail;

    //console.log($scope.model.vars.templateThumb);

    $scope.model.fields.template.on('change', function(value){
    	$scope.model.vars.templateThumb = $scope.mediaUrl + $scope.model.ref.listTemplateRef[value].thumbnail;
    	$scope.$apply();
    });

	$scope.model.vars.sortableOptions = {
		update: function(e, ui) {
			setTimeout(function(){
				var list_parts = []
				for(var i in $scope.model.vars.cvPartsList){
					var item = $scope.model.vars.cvPartsList[i];
					list_parts.push(item.value);
				}

				var params = new FormData();
				params.append('_id', $scope.model.vars._id);
		        params.append('partsOrder', list_parts.join('|'));

		        $scope.model.api.editPartsOrder(
		        	$scope.model.vars.serverController,
		        	$scope.model.vars.csrftoken, params).success(function(result){}
		        );
				$scope.$apply();
			},50);
		},
		axis: 'y'
	};

	$scope.openFormEdit = function(_id){
		$scope.model.act.openFormEdit(_id);
	};

	$scope.submitFormEdit = function(){
		$scope.model.act.submitFormEdit();
	};	

	$scope.submitFormChangeTemplate = function(){
		$scope.model.act.submitFormChangeTemplate();
	};
});