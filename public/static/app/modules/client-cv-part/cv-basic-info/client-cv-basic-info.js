'use strict';

/* Controllers */
App.controller("ClientCvBasicInfo",function($scope, $filter, ClientCvBasicInfoModel, CommonAssess){
	$scope.model = ClientCvBasicInfoModel;

	var serverVarsElm = $('#js-vars');

	var jsVars = serverVarsElm.val()?JSON.parse(serverVarsElm.val()):null;
	
	$scope.model.vars.parent = jsVars.parent;
	$scope.model.vars.cvContent = jsVars.cv_content;
	$scope.model.vars._id = jsVars._id;
	
	$scope.model.act.emptyModelFieldsVariables();
	$scope.model.act.setModelFieldsVariables($scope.model.vars.cvContent);

	$scope.openFormEdit = function(_id){
		$scope.model.act.openFormEdit(_id);
	};

	$scope.submitFormEdit = function(){
		$scope.model.act.submitFormEdit();
	};	
});