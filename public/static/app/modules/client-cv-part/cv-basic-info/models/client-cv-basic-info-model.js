'use strict';

/* Services */
var ClientCvBasicInfoModel=angular.module('ClientCvBasicInfoModel',[]);
ClientCvBasicInfoModel.factory('ClientCvBasicInfoModel',function($http, $rootScope, CommonAssess){
    var serverController = 'cv';
	var csrftoken = $('[name="csrfmiddlewaretoken"]').val();

    var firstInput = $('#headline');
    var secondInput = $('#headline');

    var serverVarsElm = $('#js-vars');
    
    var startPoint = 0;
    var perPage = 25;

    /*--------------------------------------------------Variables--------------------------------------------*/    

    var vars = {
        isFilter : false,
        listItem : null,
        inputFilter : null,
        listPercentText : null,
        listItemFull : null,
        listItemFilter : null,
        displayLoading : false,
        errorMessage : null,
        isEdit : false,
        listItemId : null,
        startPoint : startPoint,
        perPage : perPage,
        serverController : serverController,
        csrftoken : csrftoken
    };

    /*--------------------------------------------------Addition APIs-----------------------------------------*/

    var api = CommonAssess.api;
    api.editBasicInfo=function(controller, csrftoken, params){
        return $.ajax({
            url : $rootScope.baseUrl + controller + "/edit_basic_info",
            type : 'POST',
            headers : {'X-CSRFToken' : csrftoken},
            processData : false,
            contentType : false,
            data : params,
        });
    }

	var model = {
        api : api,

		vars : vars,

        fields : {},

        ref : {},

        act : {
            updateListItem : function(listItemFullInput){
                updateListItem(listItemFullInput);
            },

            openFormEdit : function(_id){
                openFormEdit(_id);
            },

            closeFormEdit : function(){
                closeFormEdit();
            },

            setDisplayLoading : function(isDisplay){
                setDisplayLoading(isDisplay);
            },

            submitFormEdit : function(){
                submitFormEdit();
            },

            afterSubmit : function(result, action){
                afterSubmit(result, action);
            },

            emptyModelFieldsVariables : function(){
                emptyModelFieldsVariables();
            },

            setModelFieldsVariables : function(item){
                setModelFieldsVariables(item);
            }
        }
	} 

    /*-----------------------------------------------Init UI----------------------------------------------*/

    var modal = $.remodal.lookup[$('[data-remodal-id = modal]').data('remodal')];

    $(document).on('opened', '.remodal', function () {
        if(!model.vars.isEdit){
            setTimeout(function(){
                firstInput.focus();
            }, 0);
        }else{
            setTimeout(function(){
                secondInput.focus();
            }, 0);
        }
    });

    /*---------------------------------------------Controller functions---------------------------------------------*/

    var updateListItem = function(listItemFullInput){
        if(!listItemFullInput) return;
        var maxPoint = listItemFullInput.length;
        model.vars.listItem = listItemFullInput;
    }

    var openFormEdit = function(_id){
        emptyModelFieldsVariables();
        setDisplayLoading(false);
        if(_id){
            CommonAssess.showWaitingHeader(true);
            model.fields._id = _id;
            model.vars.isEdit = true;

            var params = new FormData();
            params.append('_id', _id);
            model.api.getSingle(serverController, csrftoken, params).done(function(result){
                setModelFieldsVariables(result.data);
                CommonAssess.showWaitingHeader(false);
                modal.open();
                $rootScope.$apply();
            });
        }else{
            model.vars.isEdit = false;
            modal.open();
        }
    };

    var closeFormEdit = function(){
        modal.close();
    };

    var submitFormEdit = function(){
        CommonAssess.showWaitingHeader(true);

        var params = new FormData();

		params.append('headline' , model.fields.headline);
        params.append('intro' , model.fields.intro);
        params.append('advantages' , model.fields.advantages);
        params.append('disadvantages' , model.fields.disadvantages);
        params.append('address' , model.fields.address);
        params.append('email' , model.fields.email);
        params.append('phone' , model.fields.phone);
        params.append('website' , model.fields.website);

        params.append('target', model.vars.target);
        params.append('_id'  ,model.vars._id);

        model.api.editBasicInfo(serverController, csrftoken, params).done(function(result){
            afterSubmit(result, 'edit');
        });
    };

    var afterSubmit = function(result, action){
        CommonAssess.showWaitingHeader(false);
        if(!result.error){
            //Nothing todo
        }else{
            model.vars.errorMessage = CommonAssess.processErrorMessage(result.error);
            //setDisplayLoading(false);
        }
        $rootScope.$apply();
    };

    var setDisplayLoading = function(isDisplay){
        model.vars.displayLoading = isDisplay;
    };

    var emptyModelFieldsVariables = function(){
        model.vars.errorMessage = CommonAssess.processErrorMessage(null);

        model.fields.headline = '';
        model.fields.intro = '';
        model.fields.advantages = '';
        model.fields.disadvantages = '';
        model.fields.address = '';
        model.fields.email = '';
        model.fields.phone = '';
        model.fields.website = '';
    };

    var setModelFieldsVariables = function(item){
        model.vars.errorMessage = CommonAssess.processErrorMessage(null);

        model.fields.headline	= item.headline;
        model.fields.intro   = item.intro;
        model.fields.advantages   = item.advantages;
        model.fields.disadvantages   = item.disadvantages;
        model.fields.address   = item.address;
        model.fields.email   = item.email;
        model.fields.phone   = item.phone;
        model.fields.website   = item.website;
    };

    return model;  
});