'use strict';

/* Services */
var ClientCvWorkingHistoryModel=angular.module('ClientCvWorkingHistoryModel',[]);
ClientCvWorkingHistoryModel.factory('ClientCvWorkingHistoryModel',function($http, $rootScope, CommonAssess){
    var serverController = 'cv/parts';
	var csrftoken = $('[name="csrfmiddlewaretoken"]').val();

    var firstInput = $('#company');
    var secondInput = $('#company');

    var serverVarsElm = $('#js-vars');
    
    var startPoint = 0;
    var perPage = 15;

    /*--------------------------------------------------Variables--------------------------------------------*/    

    var vars = {
        isFilter : false,
        listItem : null,
        inputFilter : null,
        listPercentText : null,
        listItemFull : null,
        listItemFilter : null,
        displayLoading : false,
        errorMessage : null,
        isEdit : false,
        listItemId : null,
        top_id : null,
        startPoint : startPoint,
        perPage : perPage,
        serverController : serverController,
        csrftoken : csrftoken
    };

    /*--------------------------------------------------Addition APIs-----------------------------------------*/

    var api = CommonAssess.api;
    api.rearrange = function(controller, csrftoken, params){
        return $.ajax({
            url : $rootScope.baseUrl + controller + "/rearrange",
            type : 'POST',
            headers : {'X-CSRFToken' : csrftoken},
            processData : false,
            contentType : false,
            data : params,
        });
    };
    
	var model = {
        api : api,

		vars : vars,

        fields : {},

        ref : {},

        act : {
            updateListItem : function(listItemFullInput){
                updateListItem(listItemFullInput);
            },

            openFormEdit : function(_id){
                openFormEdit(_id);
            },

            closeFormEdit : function(){
                closeFormEdit();
            },

            setDisplayLoading : function(isDisplay){
                setDisplayLoading(isDisplay);
            },

            submitFormEdit : function(isContinue){
                submitFormEdit(isContinue);
            },

            afterSubmit : function(result, action){
                afterSubmit(result, action);
            },

            emptyModelFieldsVariables : function(){
                emptyModelFieldsVariables();
            },

            setModelFieldsVariables : function(item){
                setModelFieldsVariables(item);
            }
        }
	} 

    /*-----------------------------------------------Init UI----------------------------------------------*/

    var modal = $.remodal.lookup[$('[data-remodal-id = working-history-modal]').data('remodal')];

    $(document).on('opened', '.remodal', function () {
        if(!model.vars.isEdit){
            setTimeout(function(){
                firstInput.focus();
            }, 0);
        }else{
            setTimeout(function(){
                secondInput.focus();
            }, 0);
        }
    });

    /*---------------------------------------------Controller functions---------------------------------------------*/

    var updateListItem = function(listItemFullInput){
        if(!listItemFullInput) return;
        var maxPoint = listItemFullInput.length;
        model.vars.listItem = listItemFullInput;
    }

    var openFormEdit = function(_id){
        emptyModelFieldsVariables();
        setDisplayLoading(false);
        if(_id){
            //if(_id.$oid) _id = _id.$oid;
            CommonAssess.showWaitingHeader(true);
            model.fields._id = _id;
            model.vars.isEdit = true;

            var params = new FormData();
            params.append('top_id', model.vars.top_id);
			params.append('target', model.fields._id);
            model.api.getSingle(serverController, csrftoken, params).done(function(result){
                setModelFieldsVariables(result.data);
                CommonAssess.showWaitingHeader(false);
                modal.open();
                $rootScope.$apply();
            });
        }else{
            model.vars.isEdit = false;
            modal.open();
        }
    };

    var closeFormEdit = function(){
        modal.close();
    };

    var submitFormEdit = function(isContinue){
        setDisplayLoading(true);

        var params = new FormData();

        params.append('top_id' 	,model.vars.top_id);
        
        if(model.vars.isEdit){
            params.append('target'  ,model.fields._id);
		}else{
            params.append('target'  ,model.vars.target);
        }

        params.append('part', model.vars.part);

		params.append('company', model.fields.company);
		params.append('currentWork', model.fields.currentWork ? 1 : 0);
        params.append('startMonth', model.fields.startMonth.getValue());
        params.append('startYear', model.fields.startYear.getValue());
        params.append('endMonth', model.fields.endMonth.getValue());
        params.append('endYear', model.fields.endYear.getValue());
        params.append('jobDescription', model.fields.jobDescription);
        params.append('jobType', model.fields.jobType.getValue());
        params.append('position', model.fields.position);

		params.append('is_edit', model.vars.isEdit ? 1 : 0);
        
        model.api.editItem(serverController, csrftoken, params).done(function(result){
            if(model.vars.isEdit){
                afterSubmit(result, 'edit', isContinue);
            }else{
                afterSubmit(result, 'add', isContinue);
            }
        });
    };

    var afterSubmit = function(result, action, isContinue){

        if(!result.error){
            var newItem = {
                _id : result.data._id.toString(),
                company : result.data.company ? result.data.company : model.fields.company,
                position : result.data.position ? result.data.position : model.fields.position
            };

            switch(action){
                case 'edit':
                    isContinue = false;
                    var index = CommonAssess.getIndexFromId(model.vars.listItemFull, newItem._id);
                    model.vars.listItemFull[index]=newItem;
                break;

                case 'add':
                    model.vars.listItemFull.unshift(newItem);
                    startPoint = 0;
                break;

                case 'remove':
                    isContinue = false;
                    var listId = newItem._id.split('|');
                    for(var i in listId){
                        var index = CommonAssess.getIndexFromId(model.vars.listItemFull, listId[i]);
                        model.vars.listItemFull.splice(index,1);   
                    }
                    startPoint = 0;
                break;
            }

            updateListItem(model.vars.listItemFull);
            if(isContinue){
                emptyModelFieldsVariables();
                firstInput.focus();
            }else{
                closeFormEdit();    
            }
        }else{
            model.vars.errorMessage = CommonAssess.processErrorMessage(result.error);
        }
        setDisplayLoading(false);
        $rootScope.$apply();
    };

    var setDisplayLoading = function(isDisplay){
        model.vars.displayLoading = isDisplay;
    };

    var emptyModelFieldsVariables = function(){
        model.vars.errorMessage = CommonAssess.processErrorMessage(null);

        model.fields.company = null;
        model.fields.currentWork = null;
        model.fields.startMonth.setValue(model.ref.startMonth[0].value);
        model.fields.startYear.setValue(model.ref.startYear[model.ref.startYear.length-1].value);
        model.fields.endMonth.setValue(model.ref.endMonth[0].value);
        model.fields.endYear.setValue(model.ref.endYear[model.ref.endYear.length-1].value);
        model.fields.jobDescription = null;
        model.fields.jobType.setValue(model.ref.jobType[0].value);
        model.fields.position = null;
		
    };

    var setModelFieldsVariables = function(item){
        item.startDate = new Date(item.startDate.$date);
        item.endDate = new Date(item.endDate.$date);
        model.vars.errorMessage = CommonAssess.processErrorMessage(null);

        model.fields.company = item.company;
        model.fields.currentWork = parseInt(item.currentWork)?true:false;
        model.fields.startMonth.setValue(parseInt(item.startDate.getMonth()+1));
        model.fields.startYear.setValue(parseInt(item.startDate.getFullYear()));
        model.fields.endMonth.setValue(parseInt(item.endDate.getMonth()+1));
        model.fields.endYear.setValue(parseInt(item.endDate.getFullYear()));
        model.fields.jobDescription = item.jobDescription;
        model.fields.jobType.setValue(item.jobType);
        model.fields.position = item.position;
    };

    return model;  
});