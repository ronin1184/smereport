'use strict';

/* Services */
var CommonAssess=angular.module('CommonAssess',[]);
CommonAssess.factory('CommonAssess',function($http,$rootScope){
    var model= {
    	api: {
    		getAll:function(controller, csrftoken, params){
                return $.ajax({
                    url : $rootScope.baseUrl + controller + "/get_all",
                    type : 'POST',
                    headers : {'X-CSRFToken' : csrftoken},
                    processData : false,
                    contentType : false,
                    data : params
                });
            },

            filter:function(controller, csrftoken, params){
                return $.ajax({
                    url : $rootScope.baseUrl + controller + "/filter",
                    type : 'POST',
                    headers : {'X-CSRFToken' : csrftoken},
                    processData : false,
                    contentType : false,
                    data : params
                });
            },

    		getSingle:function(controller, csrftoken, params){
                return $.ajax({
                    url : $rootScope.baseUrl + controller + "/get_one",
                    type : 'POST',
                    headers : {'X-CSRFToken' : csrftoken},
                    processData : false,
                    contentType : false,
                    data : params
                });
            },
            
            addItem:function(controller, csrftoken, params){
                return $.ajax({
                    url : $rootScope.baseUrl + controller + "/add_item",
                    type : 'POST',
                    headers : {'X-CSRFToken' : csrftoken},
                    processData : false,
                    contentType : false,
                    data : params
                });
            },
            
            editItem:function(controller, csrftoken, params){
                return $.ajax({
                    url : $rootScope.baseUrl+controller+"/edit_item",
                    type : 'POST',
                    headers : {'X-CSRFToken':csrftoken},
                    processData : false,
                    contentType : false,
                    data : params
                });
            },

            removeItem:function(controller, csrftoken, params){
                return $.ajax({
                    url : $rootScope.baseUrl+controller+"/remove_item",
                    type : 'POST',
                    headers : {'X-CSRFToken':csrftoken},
                    processData : false,
                    contentType : false,
                    data : params
                });
            }
    	},

        datePickerOption: {
            timepicker: false,
            closeOnDateSelect: true,
            mask: false,
            format: 'Y-m-d'
        },

        dateTimePickerOption: {
            mask: false,
            format: 'Y-m-d H:i:s'
        },

       	arrayToString:function(data){
			var i=0;
			var result="";
			for(i in data){
				if(i==0){
					result=data[i];
				}else{
					result+='|'+data[i];
				}
			}
			return result;
		},

		niceUrl:function(str) {  
		    str= str.toLowerCase();
		    str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");  
		    str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");  
		    str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i");  
		    str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");  
		    str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");  
		    str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");  
		    str= str.replace(/đ/g,"d");  
		    str= str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g,"-"); 
		    /* tìm và thay thế các kí tự đặc biệt trong chuỗi sang kí tự - */ 
		    str= str.replace(/-+-/g,"-"); //thay thế 2- thành 1- 
		    str= str.replace(/^\-+|\-+$/g,"");  
		    //cắt bỏ ký tự - ở đầu và cuối chuỗi  
		    return str;
		},

		showWaitingHeader:function(show){
			if(show){
				$('#waiting-heading').show();
			}else{
				$('#waiting-heading').hide();
			}
		},

        listZoneHierarchyLv2:function(list_zone, list_admin, startLevel, isUnique, firstLabel){
            list_zone = list_zone.reverse();
            var result_array = [];
            /*
            var list_zone = [
                {id: 1, 'title': 'zone 1', level: 1, parent: null},
                {id: 2, 'title': 'zone 2', level: 1, parent: null},
                {id: 3, 'title': 'zone 3', level: 1, parent: null},
                {id: 4, 'title': 'zone 10', level: 2, parent: 1},
                {id: 5, 'title': 'zone 11', level: 2, parent: 1},
                {id: 6, 'title': 'zone 12', level: 2, parent: 1},
                {id: 7, 'title': 'zone 20', level: 2, parent: 2},
                {id: 8, 'title': 'zone 21', level: 2, parent: 2},
                {id: 9, 'title': 'zone 22', level: 2, parent: 2},
                {id: 10, 'title': 'zone 30', level: 2, parent: 3},
                {id: 11, 'title': 'zone 31', level: 2, parent: 3},
                {id: 12, 'title': 'zone 32', level: 2, parent: 3},
                {id: 13, 'title': 'zone 100', level: 3, parent: 4},
                {id: 14, 'title': 'zone 101', level: 3, parent: 4},
                {id: 15, 'title': 'zone 102', level: 3, parent: 4},
                {id: 16, 'title': 'zone 110', level: 3, parent: 5},
                {id: 17, 'title': 'zone 111', level: 3, parent: 5},
                {id: 18, 'title': 'zone 112', level: 3, parent: 5}
            ]
            */

            for(var i in list_zone){
                var item = list_zone[i];
                var prefix = "";
                for(var j=startLevel; j<item.level; j++){
                    prefix += "----";
                }
                list_zone[i].title = prefix + list_zone[i].title;
            }

            if(startLevel == 1) {

                for (var i in list_zone) {
                    if (list_zone[i].level == startLevel) {
                        result_array.push(list_zone[i]);
                        for (var j in list_zone) {
                            if (list_zone[j].parent_id == list_zone[i].id) {
                                result_array.push(list_zone[j]);
                            }
                        }
                    }
                }
            }else{
                result_array = list_zone
            }

            for(var i in result_array){
                result_array[i].order = i;
            }

            var zone = result_array;
            if(isUnique) {
                for (var i in zone) {
                    var tmpZoneItem = zone[i];
                    for (var j in list_admin) {
                        var tmpAdminItem = list_admin[j];
                        if (tmpZoneItem.id == tmpAdminItem.zone_id) {
                            zone[i].title += ' (Assigned)';
                            break;
                        }
                    }
                }
            }
            if(firstLabel){
            	zone.unshift({id: 0, title: '----- ' + firstLabel + ' -----'});
        	}
            return zone;
        },

		createDropdownData:function(data, lang){
			var result = [];
			for(var i in data){
				var item = {
					label: data[i][lang],
					value: data[i].id,
					order: parseInt(data[i].order)
				}
				result.push(item);
			}
			return result;
		},

		getDropdownRef:function(data, lang){
			var result = {};
			for(var i in data){
				var key = data[i].id;
				var value = data[i][lang];
				result[key] = value;
			}
			return result;
		},

		generateRandom:function(stringLenght){
		    var text = "";
		    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

		    for( var i=0; i < stringLenght; i++ )
		        text += possible.charAt(Math.floor(Math.random() * possible.length));
		    return text;
		},

		upperFirst:function(input){
			if(!input) return input;
			var array_imput=input.split(" ");
			for(var i in array_imput){
				array_imput[i] = array_imput[i].charAt(0).toUpperCase() + array_imput[i].slice(1);	
			}
			return array_imput.join(" ");
		},


        djangoDateTimeTostr: function(inputDateTime){
            if(!inputDateTime) return '';
            var newDateTime = new Date(inputDateTime);
            var d = newDateTime.getDate();
            var m = newDateTime.getMonth() + 1;
            var y = newDateTime.getFullYear();
            var hour = newDateTime.getHours();
            var minute = newDateTime.getMinutes();
            var second = newDateTime.getSeconds();
            var datePart = '' + y + '-' + (m<=9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d);
            var timePart = (hour<=9 ? '0' + hour : hour) + ':' + (minute<=9 ? '0' + minute : minute) + ':' + (second<=9 ? '0' + second : second)
            return datePart + ' ' + timePart;
        },

		findByid:function(input,id){
			if(!input) return null;
			for(var i in input){
				if(input[i].id==id) return input[i];
			}
			return null;
		},

		listDropdownRef:function(input){
			var result={};
			for(var i in input){
				var item=input[i];
				result[item.id]=item;
			}
			return result;
		},

		processErrorMessage:function(message){
			if(message){
				message=message.toString();
				var listMessage=message.split("<|>");
				if(listMessage.length>1){
					return listMessage;
				}
				return [message];
			}
			return null;
		},

		changePosition:function(source,partid,direction){
			var currentOrder=0;
			var arraySource=Array();
			if(!source) return null;

			$.each(source,function(key,item){
				arraySource.push(item);
			});

			source=arraySource;

			for(var i in source){
				if(source[i].id==partid) {
					currentOrder=i;
					break;
				}
			}
			//console.log("source:",source);
			//console.log("currentOrder:",currentOrder);
			
			//console.log("currentOrder",currentOrder);
			if(direction=="up"){
				if(currentOrder==0) return null;
				var currentItem	=source[Number(currentOrder)];
				//console.log("currentItem:",currentItem);
				var shiftItem	=source[Number(currentOrder)-1];
			}else{
				if(currentOrder==source.length-1) return null;
				var currentItem	=source[Number(currentOrder)];
				//console.log("currentItem:",currentItem);
				//console.log("shiftOrder:",Number(currentOrder)+1);
				var shiftItem	=source[Number(currentOrder)+1];
			}

			return {	partid			:currentItem.id,
						order			:shiftItem.order,
						part_id_shift	:shiftItem.id,
						order_shift		:currentItem.order};
		},

        getRef:function(input_list, key, value){
            var result = {};
            for(var i in input_list){
                result[input_list[i][key]] = input_list[i][value]
            }
            return result;
        },

		getRootUrl:function(url){
			var urlWithoutProtocol;
			if(url.split("//").length<2){
				urlWithoutProtocol=url
			}else{
				urlWithoutProtocol=url.split("//")[1];	
			}

			if(urlWithoutProtocol.split("/").length>=2){
				urlWithoutProtocol=urlWithoutProtocol.split("/")[0];
			}
			
			return "http://"+urlWithoutProtocol;
		},

		fix2Digit:function(input){
			if(!input) return "";
		    input=parseInt(input);
		    if(input<10) input="0"+input.toString();
		    return input;
		},

		updateCaptcha:function(text) {
			var ctx = $("#glyphs")[0].getContext('2d');
			drawCaptcha({
				context: ctx, 
				font: optimer, 
				style: "#0088CC",
				text: text
			});
		},

		sortByField:function(inputArray,sortField,condition){
		    //Sort array of objects
		    if(!sortField) return inputArray;
		    var resultArray =[];
		    var sortArray   =[];
		    for(var i in inputArray){
		        sortArray.push([inputArray[i][sortField],parseInt(i)]);
		    }

		    if(condition=="asc"){
		        sortArray.sort(function(a,b){return a[0]-b[0]});
		    }else{
		        sortArray.sort(function(a,b){return b[0]-a[0]});
		    }
		    
		    for(var i in sortArray){
		        resultArray[i]=inputArray[sortArray[i][1]];
		    }

		    return resultArray;
		},
		/*
		getStandardResultArray:function(getLv1Result,cvPart){
		    var resultList=[];
		    var cvPart=getLv1Result[cvPart];
		    for(var i in cvPart){
		    	var item = cvPart[i];
		    	if(item.id){
		    		if(item.id.$oid){
		    			item.id = item.id.$oid;
		    		}
		    	}
		        resultList.push(item);
		    }
		    
		    //resultList=self.sortByField(resultList,"order","desc");
		    return resultList;
		},
		*/
		dictToList:function(input){
			var result = [];
			for(var i in input){
				var item = input[i];
		    	if(item.id){
		    		if(item.id.$oid){
		    			item.id = item.id.$oid;
		    		}
		    	}
				result.push(item);
			}
			if(result.length == 1){
				return result[0];
			}
			return result;
		},

		subItemSortedArray:function(inputObject,condition){
		    //Sort array of objects
		    if(!inputObject) return [];
		    if(inputObject.length==0) return [];

		    //var inputArray  =[];
		    var sortField="order";
		    var resultArray =[];
		    var sortArray   =[];

		    //console.log("inputObject:",inputObject);
		    var counter=0;
		    for(var i in inputObject){
		        sortArray.push([inputObject[i][sortField],parseInt(counter)]);
		        counter++;
		    }

		    if(condition=="asc"){
		        sortArray.sort(function(a,b){return a[0]-b[0]});
		    }else{
		        sortArray.sort(function(a,b){return b[0]-a[0]});
		    }

		    for(var i in sortArray){
		        resultArray[i]=inputObject[sortArray[i][1]];
		    }

		    return resultArray;
		},

		/*
		subitemObjectArray:function(inputObject,lv1Key,language){
			var rawObject=new Object();
			var arraySubItem=[];
			for(var i in inputObject){
				var arraySubItem=[];
				for(var j in inputObject[i][lv1Key]){
					arraySubItem.push({	id		:inputObject[i][lv1Key][j].id.toString(),
										id		:inputObject[i][lv1Key][j].id,
										title	:inputObject[i][lv1Key][j]["title"+language],
										order	:inputObject[i][lv1Key][j].order});
				}

				arraySubItem=model.subItemSortedArray(arraySubItem,"asc");

				rawObject[inputObject[i].id]=arraySubItem;
			}
			//Implement order for item by order.
			return rawObject;
		},
		*/

		getIndexFromId:function(inputList,id){
			for (var i in inputList){
				var item = inputList[i];
				if(item.id == id){
					return i;
				}
			}
			return null;
		},


		removeListItem:function(inputList,listId){
			var newList=[];
			for (var i in inputList){
				var item = inputList[i];
				if(listId.indexOf(item.id)==-1){
					newList.push(item);
				}
			}
			return newList;
		},


		getCvPart:function(inputCv,cvLanguage,cvPart){
			var result=[];
			cvPart=inputCv[cvLanguage][cvPart];
			for(var i in cvPart){
				result.push(cvPart[i]);
			}
			return result;
		},


		mergeFamily:function(parentList,itemList,cvLanguage){
			//itemList=dropdownItemResult.data;
			var dropdownListFull=new Object();

			for(var i in parentList){
				dropdownListFull[parentList[i].id]=[];
				for(var j in itemList){
					if(itemList[j].parent.toString()==parentList[i].id.toString()){
						//delete itemList[j].parent;
						var tmpObject={
							id		:itemList[j].id.toString(),
							title	:itemList[j][cvLanguage].toString()
						};
						//dropdownListFull[parentList[i].id].push(itemList[j]);
						dropdownListFull[parentList[i].id].push(tmpObject);
					}
				}	
			}
			return dropdownListFull;
		}
    }

    return model;
});

CommonAssess.filter('levelToReadable',function($scope){
	return function(input){
		console.log("input:",input);
		var dictList=$scope.dropdownTransModel.other.listTransLevel;
		/*
		var array_imput=input.split(" ");
		for(var i in array_imput){
			return array_imput[i].charAt(0).toUpperCase() + array_imput[i].slice(1);	
		}
		return array_imput.join(" ");
		*/
		return "test";
	}
});


CommonAssess.filter('upperFirst',function(){
	return function(input){
		console.log("input:",input);
		var array_imput=input.split(" ");
		for(var i in array_imput){
			return array_imput[i].charAt(0).toUpperCase() + array_imput[i].slice(1);	
		}
		return array_imput.join(" ");
	}
});


CommonAssess.filter('toString',function(){
    return function(input,condition) {
		if(condition=='level'){
		    if(input==0){
				return "Admin";
		    }else{
				return "Normal";
		    }
		}else{
		    if(input==0){
				return "<span class='no_style'>No</span>";
                //return 'NO';
		    }else{
				return "<span class='yes_style'>Yes</span>";
                //return 'YES';
		    }
		}  
    }
});

CommonAssess.filter('count',function(){
	return function(input) {
		if(input){
			return input.length;
		}
		return 0;
	}
});

CommonAssess.filter('dateFilter',function(){
    return function(input) {
		if(input=="" || input==null || input==undefined){
			return "HIỆN TẠI";
		}else{
			var dateArray=input.split("-");
			var yearString=dateArray[0];
			var month=Number(dateArray[1]);
			var monthString;
			if(month<10){
				monthString="0"+month;
			}else{
				monthString=month;
			}
			return monthString+"/"+yearString;
		}
    }
});


CommonAssess.filter('imageUrlFilter',function($rootScope){
    return function(input) {
    	if(input=="" || input==null || input==undefined){
    		return $rootScope.apiUrl+$rootScope.defaultImagePath;
    	}else{
	    	if(input.split("data:image").length>=2){
	    		return input;
	    	}else{
	    		return $rootScope.apiUrl+input;
	    	}
    	}
    	/*
		if(input=="" || input==null || input==undefined){
			return "HIỆN TẠI";
		}else{
			var dateArray=input.split("-");
			var year=dateArray[0];
			var month=convertMonth(Number(dateArray[1]));
			
			return month+" "+year;
		}
		*/
    }
});

CommonAssess.filter('convertDate',function(){
    return function(input) {
    	if(!input) return "";
    	var inputDate=new Date(input);
    	return inputDate.getDate()+"/"+(inputDate.getMonth()+1)+"/"+inputDate.getFullYear();
    }
});

CommonAssess.filter('mapTitle',function(){
    return function(input,source) {
    	if(!input) return "";
        if(input in source){
            return source[input];
        }
    	return "";
    	//var inputDate=new Date(input);
    	//return inputDate.getDate()+"/"+(inputDate.getMonth()+1)+"/"+inputDate.getFullYear();
    }
});

CommonAssess.filter('niceUrl',function(){
    return function(input,source) {
    	if(!input) return "";
    	return source(input);
    }
});

CommonAssess.filter('yearFilter',function(){
    return function(input) {
    	if(!input) return "";
    	var inputDate=new Date(input);
    	return inputDate.getFullYear();
    }
});

function convertMonth(input){
	switch(input){
		case 1:
			return "JAN";
		break;
		
		case 2:
			return "FEB";
		break;
		
		case 3:
			return "MAR";
		break;
		
		case 4:
			return "APR";
		break;
		
		case 5:
			return "MAY";
		break;
		
		case 6:
			return "JUN";
		break;
		
		case 7:
			return "JUL";
		break;
		
		case 8:
			return "AUG";
		break;
		
		case 9:
			return "SEP";
		break;
		
		case 10:
			return "OCT";
		break;
		
		case 11:
			return "NOV";
		break;
		
		case 12:
			return "DEC";
		break;
	}
}