'use strict';

/* Controllers */
App.controller("ClientAdmin",function($scope, $filter, ClientAdminModel, CommonAssess){
	$scope.model = ClientAdminModel;

	var mainTable = $('#administrator-table');
	var serverVarsElm = $('#js-vars');
	var currentPage = 1;
	var maxRecords = 70;
	var init = true;

	var jsVars = serverVarsElm.val()?JSON.parse(serverVarsElm.val()):null;

	$scope.model.vars.reach_bottom = false;
	$scope.model.vars.isBusy = false;
	$scope.model.vars.isFilter = false;
	$scope.model.vars.listItemFull = jsVars.list_item;
	$scope.model.vars.adminItem = jsVars.admin_item;
    $scope.model.vars.adminRole = jsVars.admin_role;

    //$scope.model.vars.listAdminFull = jsVars.list_admin_full;

    $scope.model.ref.zone = CommonAssess.listZoneHierarchyLv2(jsVars.list_zone, jsVars.list_admin_full, 1, true, 'Please select one');
    /*
    for(var i in $scope.model.ref.zone){
        var tmpZoneItem = $scope.model.ref.zone[i];
        for(var j in $scope.model.vars.listAdminFull){
            var tmpAdminItem = $scope.model.vars.listAdminFull[j];
            if(tmpZoneItem.id == tmpAdminItem.zone_id){
                $scope.model.ref.zone[i].title += ' (Assigned)';
                break;
            }
        }
    }
	$scope.model.ref.zone.unshift({id: 0, title: '----- Please select one -----'});
    */
	$scope.model.vars.listItemFilter = null;
	if($scope.model.vars.adminRole == 'SUBADMIN'){
        $scope.model.vars.restrict = true;
    }else{
        $scope.model.vars.restrict = false;
    }


	//console.log(CommonAssess.listZoneHierarchyLv2($scope.model.ref.zone));
    /*
	var selectOption = {
        maxItems : 1,
        valueField : 'value',
        labelField : 'label',
        searchField : ['label', 'value'],
        options : $scope.model.ref.roleList
    };

    var selectRole = $('#select-role').selectize(selectOption);
    $scope.model.fields.role = selectRole[0].selectize;
	*/

	var zoneSelectOption = {
        maxItems : 1,
        valueField : 'id',
        labelField : 'title',
        searchField : ['id', 'title'],
        sortField: [{field: 'order', direction: 'asc'}],
        options : $scope.model.ref.zone
    };

    var zone = $('#select-zone').selectize(zoneSelectOption);
    $scope.model.fields.zone = zone[0].selectize;

	/*-------------------------------------Init update list-------------------------------------*/

	$scope.model.act.updateListItem($scope.model.vars.listItemFull);	

	/*-------------------------------------Implement list filter-------------------------------------*/

	$scope.$watch('model.vars.inputFilter', function(newValue, oldValue) {
		currentPage = 1;
		if(newValue){
			if(!$scope.model.vars.isBusy && newValue.length >= 3){
				get_all(false);
			}
		}else{
			if(init){
				init = false
			}else{
				get_all(false);
			}
		}
	});

	/*-------------------------------------Controller functions-------------------------------------*/

	$(window).scroll(function() {
		if($(window).scrollTop() + $(window).height() >= $(document).height()) {
			if(!$scope.model.vars.reach_bottom && !$scope.model.vars.isBusy){
				currentPage++;
				get_all(true);
			}
		}
	});

	function get_all(is_append){
		CommonAssess.showWaitingHeader(true);
		var params = new FormData();
        
        if($scope.model.vars.inputFilter){
        	params.append('keyword', $scope.model.vars.inputFilter);
        }

        $scope.model.vars.isBusy = true;
        params.append('page', currentPage);
		$scope.model.api.getAll($scope.model.vars.serverController, $scope.model.vars.csrftoken, params).done(function(result){
			CommonAssess.showWaitingHeader(false);
			$scope.model.vars.isBusy = false;

			if(result.data){
				$scope.model.vars.reach_bottom = result.reach_bottom
				if(is_append){
					if(maxRecords < $scope.model.vars.listItemFull.length){
						$scope.model.vars.listItemFull = $scope.model.vars.listItemFull.slice(-5);
					}
					$scope.model.vars.listItemFull = $scope.model.vars.listItemFull.concat(result.data);
				}else{
					$scope.model.vars.listItemFull = result.data;
				}
				$scope.model.act.updateListItem($scope.model.vars.listItemFull);
			}
			$scope.$apply();
		});
	}

	$scope.openFormEdit = function(id){
		$scope.model.act.openFormEdit(id);
	};

	$scope.openFormRemove = function(id){
		if(id){
			id = id.toString();
			if(id.split("|").length > 1){
				$scope.formRemoveInfo = "Do you realy want to remove these records ?";
			}else{
				$scope.formRemoveInfo = "Do you realy want to remove this records ?";
			}
			$scope.model.fields.id = id;
			var confirmRemove = confirm($scope.formRemoveInfo);
			if(confirmRemove){
				submitFormRemove();
			}
		}
	};

	function submitFormRemove(){
		CommonAssess.showWaitingHeader(true);
		var params = new FormData();
		params.append('id',$scope.model.fields.id);
		$scope.model.api.removeItem($scope.model.vars.serverController, $scope.model.vars.csrftoken, params).done(function(result){
			$scope.model.act.afterSubmit(result, 'remove');
			CommonAssess.showWaitingHeader(false);
		});
	};

	$scope.selectCheckBox = function(){
		$scope.model.vars.listItemId=getListItemId();
	};

	$scope.selectAllCheckBox = function(){
		var checkedItems = $('.selectCheckBox:checked');
		var checkBox = $(".selectCheckBox");

		if(checkedItems.length > 0 && checkedItems.length < checkBox.length){
			checkBox.prop("checked", true);
		}else if(checkedItems.length == checkBox.length){
			checkBox.prop("checked", false);
		}else{
			checkBox.prop("checked", true);
		}
		$scope.model.vars.listItemId = getListItemId();
	};

	function getListItemId(){
		var checkedItems = $('.selectCheckBox:checked');
		var listItemId = "";
		if(checkedItems.length > 0){
			checkedItems.each(function(key,value){
				if(key == 0){
					listItemId = $(value).attr("data");
				}else{
					listItemId += "|" + $(value).attr("data");
				}
			});
		}
		return listItemId;
	};
});

/*----------------------------------------------MODAL CONTROLLER--------------------------------------------*/

App.controller("ClientAdminModal",function($scope, ClientAdminModel){
	$scope.model = ClientAdminModel;

	$scope.submitFormEdit = function(isContinue){
		isContinue = typeof isContinue !== 'undefined' ? isContinue : false;
		$scope.model.act.submitFormEdit(isContinue);
	};
});