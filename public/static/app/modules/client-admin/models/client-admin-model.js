'use strict';

/* Services */
var ClientAdminModel=angular.module('ClientAdminModel',[]);
ClientAdminModel.factory('ClientAdminModel',function($http, $rootScope, CommonAssess){
    var serverController = 'administrator';
	var csrftoken = $('[name="csrfmiddlewaretoken"]').val();

    var firstInput = $('#uid');
    var secondInput = $('#password');

    var serverVarsElm = $('#js-vars');
    
    var startPoint = 0;
    var perPage = 15;

    /*--------------------------------------------------Variables--------------------------------------------*/    

    var vars = {
        isFilter : false,
        listItem : null,
        inputFilter : null,
        listPercentText : null,
        listItemFull : null,
        listItemFilter : null,
        displayLoading : false,
        errorMessage : null,
        isEdit : false,
        listItemId : null,
        startPoint : startPoint,
        perPage : perPage,
        serverController : serverController,
        csrftoken : csrftoken
    };

    /*--------------------------------------------------Addition APIs-----------------------------------------*/

    var api = CommonAssess.api;
    api.forgotPassword = function(serverController, csrftoken, params){
        return $.ajax({
            url : $rootScope.baseUrl + serverController + "/forgot_password",
            type : 'POST',
            headers : {'X-CSRFToken' : csrftoken},
            processData : false,
            contentType : false,
            data : params
        });
    };

	var model = {
        api : api,

		vars : vars,

        fields : {},

        ref : {
            roleList:[
                {label: "Admin", value : "ADMIN"},
                {label: "Sub Admin", value : "SUBADMIN"}
            ]
        },

        act : {
            updateListItem : function(listItemFullInput){
                updateListItem(listItemFullInput);
            },

            openFormEdit : function(id){
                openFormEdit(id);
            },

            closeFormEdit : function(){
                closeFormEdit();
            },

            setDisplayLoading : function(isDisplay){
                setDisplayLoading(isDisplay);
            },

            submitFormEdit : function(isContinue){
                submitFormEdit(isContinue);
            },

            afterSubmit : function(result, action){
                afterSubmit(result, action);
            },

            emptyModelFieldsVariables : function(){
                emptyModelFieldsVariables();
            },

            setModelFieldsVariables : function(item){
                setModelFieldsVariables(item);
            }
        }
	} 

    /*-----------------------------------------------Init UI----------------------------------------------*/

    var modal = $.remodal.lookup[$('[data-remodal-id = modal]').data('remodal')];

    $(document).on('opened', '.remodal', function () {
        if(!model.vars.isEdit){
            setTimeout(function(){
                firstInput.focus();
            }, 0);
        }else{
            setTimeout(function(){
                secondInput.focus();
            }, 0);
        }
    });

    /*---------------------------------------------Controller functions---------------------------------------------*/

    var updateListItem = function(listItemFullInput){
        if(!listItemFullInput) return;
        var maxPoint = listItemFullInput.length;
        model.vars.listItem = listItemFullInput;
    }

    var openFormEdit = function(id){
        emptyModelFieldsVariables();
        setDisplayLoading(false);
        if(id){
            CommonAssess.showWaitingHeader(true);
            model.fields.id = id;
            model.vars.isEdit = true;

            var params = new FormData();
            params.append('id', id);
            model.api.getSingle(serverController, csrftoken, params).done(function(result){
                setModelFieldsVariables(result.data);
                CommonAssess.showWaitingHeader(false);
                modal.open();
                $rootScope.$apply();
            }).error(function(error, message){
				console.log("On error");
				console.log(message);
			});
        }else{
            model.vars.isEdit = false;
            modal.open();
        }
    };

    var closeFormEdit = function(){
        modal.close();
    };

    var submitFormEdit = function(isContinue){
        setDisplayLoading(true);

        var params = new FormData();

        params.append('uid' , model.fields.uid);
        params.append('email' , model.fields.email);
        params.append('name' , model.fields.name);
        params.append('password' , model.fields.password);
		params.append('zone' , model.fields.zone.getValue());
        //params.append('role' , model.fields.role.getValue());

        if(model.vars.isEdit){
            params.append('id', model.fields.id);
            model.api.editItem(serverController, csrftoken, params).done(function(result){
                afterSubmit(result, 'edit', isContinue);
            });
        }else{
            model.api.addItem(serverController, csrftoken, params).done(function(result){
                afterSubmit(result, 'add', isContinue);
            });
        }
    };

    var afterSubmit = function(result, action, isContinue){
        if(!result.error){
			if (model.vars.adminItem) {
				location.reload();
				return;
			}
            var newItem = {
                id : result.data.id.toString(),
                uid : result.data.uid ? result.data.uid : model.fields.uid,
                email : result.data.email ? result.data.email : model.fields.email,
                name : result.data.name ? result.data.name : model.fields.name
            };

            switch(action){
                case 'edit':
                    var index = CommonAssess.getIndexFromId(model.vars.listItemFull, newItem.id);
                    model.vars.listItemFull[index]=newItem;
                break;

                case 'add':
                    model.vars.listItemFull.unshift(newItem);
                    startPoint = 0;
                break;

                case 'remove':
                    var listId = newItem.id.split('|');
                    for(var i in listId){
                        var index = CommonAssess.getIndexFromId(model.vars.listItemFull, listId[i]);
                        model.vars.listItemFull.splice(index,1);   
                    }
                    startPoint = 0;
                break;
            }

            if(isContinue){
                emptyModelFieldsVariables();
                firstInput.focus();
            }else{
                closeFormEdit();
            }
        }else{
            model.vars.errorMessage = CommonAssess.processErrorMessage(result.error);
            setDisplayLoading(false);
        }
        setDisplayLoading(false);
        $rootScope.$apply();
    };

    var setDisplayLoading = function(isDisplay){
        model.vars.displayLoading = isDisplay;
    };

    var emptyModelFieldsVariables = function(){
        model.vars.errorMessage = CommonAssess.processErrorMessage(null);

        model.fields.uid = null;
        model.fields.email = null;
        model.fields.name = null;
        model.fields.password = null;
		//model.fields.zone.setValue(model.ref.zone[0].id);
		model.fields.zone.setValue(0);
        //model.fields.role.setValue(model.ref.roleList[0].value);
    };

    var setModelFieldsVariables = function(item){
		if (!item.zone_id) {
			item.zone_id = 0;
		}
        model.vars.errorMessage = CommonAssess.processErrorMessage(null);

        model.fields.uid = item.uid;
        model.fields.email = item.email;
        model.fields.name = item.name;
		model.fields.zone.setValue(item.zone_id);
        //model.fields.role.setValue(item.role);
    };

    return model;  
});
