'use strict';

/* Services */
var ClientCompanyModel=angular.module('ClientCompanyModel',[]);
ClientCompanyModel.factory('ClientCompanyModel',function($http, $rootScope, CommonAssess){
    var serverController = 'company';
	var csrftoken = $('[name="csrfmiddlewaretoken"]').val();

    var firstInput = $('#tinh');
    var secondInput = $('#tinh');
    
    var startPoint = 0;
    var perPage = 25;

    /*--------------------------------------------------Variables--------------------------------------------*/    

    var vars = {
        isFilter : false,
        listItem : null,
        inputFilter : null,
        listPercentText : null,
        listItemFull : null,
        listItemFilter : null,
        displayLoading : false,
        errorMessage : null,
        isEdit : false,
        listItemId : null,
        startPoint : startPoint,
        perPage : perPage,
        serverController : serverController,
        csrftoken : csrftoken
    };

    /*--------------------------------------------------Addition APIs-----------------------------------------*/

    var api = CommonAssess.api;
    api.getFull = function(controller, csrftoken, params){
        return $.ajax({
            url : $rootScope.baseUrl + controller + "/get_full",
            type : 'POST',
            headers : {'X-CSRFToken' : csrftoken},
            processData : false,
            contentType : false,
            data : params
        });
    };
    api.changeOwner = function(controller, csrftoken, params){
        return $.ajax({
            url : $rootScope.baseUrl + controller + "/edit_owner",
            type : 'POST',
            headers : {'X-CSRFToken' : csrftoken},
            processData : false,
            contentType : false,
            data : params
        });
    };
    api.addByUpload = function(controller, csrftoken, params){
        return $.ajax({
            url : $rootScope.baseUrl + controller + "/add_by_upload",
            type : 'POST',
            headers : {'X-CSRFToken' : csrftoken},
            processData : false,
            contentType : false,
            data : params
        });
    };
	var model = {
        api : api,

		vars : vars,

        fields : {},

        view : {},

        ref : {},

        act : {
            init: function(listType){
                init(listType)
            },

            updateListItem : function(listItemFullInput){
                updateListItem(listItemFullInput);
            },

            openFormEdit : function(id){
                openFormEdit(id);
            },

            openFormView : function(id){
                openFormView(id);
            },

            closeFormEdit : function(){
                closeFormEdit();
            },

            setDisplayLoading : function(isDisplay){
                setDisplayLoading(isDisplay);
            },

            submitFormEdit : function(isContinue){
                submitFormEdit(isContinue);
            },

            afterSubmit : function(result, action, isContinue){
                afterSubmit(result, action, isContinue);
            },

            emptyModelFieldsVariables : function(){
                emptyModelFieldsVariables();
            },

            setModelFieldsVariables : function(item){
                setModelFieldsVariables(item);
            }
        }
	} 

    /*-----------------------------------------------Init UI----------------------------------------------*/

    var modal = $.remodal.lookup[$('[data-remodal-id = modal]').data('remodal')];
    var modalView = $.remodal.lookup[$('[data-remodal-id = modalView]').data('remodal')];

    $(document).on('opened', '.remodal', function () {
        if(!model.vars.isEdit){
            setTimeout(function(){
                firstInput.focus();
            }, 0);
        }else{
            setTimeout(function(){
                secondInput.focus();
            }, 0);
        }
    });

    /*---------------------------------------------Controller functions---------------------------------------------*/

    var init = function(listType){
        model.vars.listType = listType;
        //console.log(model.vars.listType);
    }

    var updateListItem = function(listItemFullInput){
        if(!listItemFullInput) return;
        var maxPoint = listItemFullInput.length;
        model.vars.listItem = listItemFullInput;
        model.vars.listItemCount = listItemFullInput.length;
    }

    var openFormEdit = function(id){
        emptyModelFieldsVariables();
        setDisplayLoading(false);
        if(id){
            CommonAssess.showWaitingHeader(true);
            model.fields.id = id;
            model.vars.isEdit = true;

            var params = new FormData();
            params.append('id', id);
            model.api.getSingle(serverController, csrftoken, params).done(function(result){
                setModelFieldsVariables(result.data);
                CommonAssess.showWaitingHeader(false);
                modal.open();
                $rootScope.$apply();
            });
        }else{
            model.vars.isEdit = false;
            modal.open();
        }
    };

    var openFormView = function(id){
        CommonAssess.showWaitingHeader(true);
        var params = new FormData();
        params.append('id', id);
        model.api.getSingle(serverController, csrftoken, params).done(function(result){
            setViewData(result.data);
            CommonAssess.showWaitingHeader(false);
            modalView.open();
            $rootScope.$apply();
        });
    };

    var closeFormEdit = function(){
        modal.close();
    };

    var submitFormEdit = function(isContinue){
        setDisplayLoading(true);

        var params = new FormData();

        params.append('tinh' , model.fields.tinh.getValue());
		params.append('chinhanh' , model.fields.chinhanh);
        params.append('cty' , model.fields.cty);
        params.append('mst' , model.fields.mst);
        params.append('huyen' , model.fields.huyen);
        params.append('xa' , model.fields.xa);
        params.append('loaidn' , model.fields.loaidn);
        params.append('diachi' , model.fields.diachi);
        params.append('namsxkd' , model.fields.namsxkd);
        params.append('phone' , model.fields.phone);
        params.append('email' , model.fields.email);
        params.append('nganhnghe' , model.fields.nganhnghe);
        params.append('doanhthu' , model.fields.doanhthu);
        params.append('lntruocthue' , model.fields.lntruocthue);
        params.append('lntusx' , model.fields.lntusx);
        params.append('dscr' , model.fields.dscr);
        params.append('ploai' , model.fields.ploai);
        params.append('note' , model.fields.note);

        if(model.vars.isEdit){
            params.append('id', model.fields.id);
            model.api.editItem(serverController, csrftoken, params).done(function(result){
                afterSubmit(result, 'edit', isContinue);
            });
        }else{
            model.api.addItem(serverController, csrftoken, params).done(function(result){
                afterSubmit(result, 'add', isContinue);
            });
        }
    };

    var afterSubmit = function(result, action, isContinue){
        if(!result.error){
            var newItem = {
                id : result.data.id.toString(),
                cty : result.data.cty ? result.data.cty : model.fields.cty,
                phone : result.data.phone ? result.data.phone : model.fields.phone,
                email : result.data.email ? result.data.email : model.fields.email
            };
            switch(action){
                case 'edit':
					isContinue = false;
                    var index = CommonAssess.getIndexFromId(model.vars.listItemFull, newItem.id);
                    model.vars.listItemFull[index]=newItem;
                break;

                case 'add':
                    model.vars.listItemFull.unshift(newItem);
                    startPoint = 0;
                break;

                case 'remove':
					isContinue = true;
                    var listId = newItem.id.split('|');
                    for(var i in listId){
                        var index = CommonAssess.getIndexFromId(model.vars.listItemFull, listId[i]);
                        model.vars.listItemFull.splice(index,1);   
                    }
                    startPoint = 0;
                break;

                case 'changeOwner':
                    isContinue = true;
                    var listId = newItem.id.split('|');
                    for(var i in listId){
                        var index = CommonAssess.getIndexFromId(model.vars.listItemFull, listId[i]);
                        /*
                        var newItem = {
                            id : model.vars.listItemFull[index].id,
                            cty : model.vars.listItemFull[index].cty,
                            phone : model.vars.listItemFull[index].phone,
                            email : model.vars.listItemFull[index].email
                        };
                        model.vars.listItemOwner.unshift(newItem);
                        */
                        model.vars.listItemFull.splice(index, 1);
                    }
                    startPoint = 0;
                    location.reload();
                break;
            }
            if(isContinue){
                emptyModelFieldsVariables();
                firstInput.focus();
            }else{
                closeFormEdit();    
            }
        }else{
            model.vars.errorMessage = CommonAssess.processErrorMessage(result.error);
        }
        model.vars.listItemId = '';
		setDisplayLoading(false);
        $rootScope.$apply();
    };

    var setDisplayLoading = function(isDisplay){
        model.vars.displayLoading = isDisplay;
    };

    var emptyModelFieldsVariables = function(){
        model.vars.errorMessage = CommonAssess.processErrorMessage(null);
        model.fields.tinh.setValue(model.ref.list_tinh_thanh[0].uid);
		model.fields.chinhanh = '';
        model.fields.cty = '';
        model.fields.mst = '';
        model.fields.huyen = '';
        model.fields.xa = '';
        model.fields.loaidn = '';
        model.fields.diachi = '';
        model.fields.namsxkd = '';
        model.fields.phone = '';
        model.fields.email = '';
        model.fields.nganhnghe = '';
        model.fields.doanhthu = '';
        model.fields.lntruocthue = '';
        model.fields.lntusx = '';
        model.fields.dscr = '';
        model.fields.ploai = '';
        model.fields.note = '';
    };

    var setModelFieldsVariables = function(item){
        model.vars.errorMessage = CommonAssess.processErrorMessage(null);

        model.fields.tinh.setValue(item.tinh);
		model.fields.chinhanh = item.chinhanh;
        model.fields.cty = item.cty;
        model.fields.mst = item.mst;
        model.fields.huyen = item.huyen;
        model.fields.xa = item.xa;
        model.fields.loaidn = item.loaidn;
        model.fields.diachi = item.diachi;
        model.fields.namsxkd = item.namsxkd;
        model.fields.phone = item.phone;
        model.fields.email = item.email;
        model.fields.nganhnghe = item.nganhnghe;
        model.fields.doanhthu = item.doanhthu;
        model.fields.lntruocthue = item.lntruocthue;
        model.fields.lntusx = item.lntusx;
        model.fields.dscr = item.dscr;
        model.fields.ploai = item.ploai;
        model.fields.note = item.note;
    };

    var setViewData = function(item){
        model.view.kq_cuoc_goi = item.kq_cuoc_goi;
        model.view.sp_tiep_can = item.sp_tiep_can;
        model.view.tinh_trang_khai_thac = item.tinh_trang_khai_thac;

        model.view.tinh = item.tinh;
		model.view.chinhanh = item.chinhanh;
        model.view.cty = item.cty;
        model.view.mst = item.mst;
        model.view.huyen = item.huyen;
        model.view.xa = item.xa;
        model.view.loaidn = item.loaidn;
        model.view.diachi = item.diachi;
        model.view.namsxkd = item.namsxkd;
        model.view.phone = item.phone;
        model.view.email = item.email;
        model.view.nganhnghe = item.nganhnghe;
        model.view.doanhthu = item.doanhthu;
        model.view.lntruocthue = item.lntruocthue;
        model.view.lntusx = item.lntusx;
        model.view.dscr = item.dscr;
        model.view.ploai = item.ploai;
        model.view.note = item.note;
    }

    return model;  
});
