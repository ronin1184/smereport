'use strict';

/* Controllers */
App.controller("ClientCompany",function($scope, $filter, ClientCompanyModel, ClientReportModel, CommonAssess){
    $scope.model = ClientCompanyModel;

    var serverVarsElm = $('#js-vars');
    var currentPage = 1;
    var maxRecords = 5000;
    var init = true;

    var jsVars = serverVarsElm.val()?JSON.parse(serverVarsElm.val()):null;

    $scope.model.vars.totalCheck = 0;

    $scope.model.vars.user_level = jsVars.user_level;
    $scope.model.vars.user_type = jsVars.user_type;
    $scope.model.vars.owner_type = jsVars.owner_type;
    $scope.model.vars.owner_conditions = jsVars.owner_conditions;
    $scope.model.vars.reach_bottom = false;
    $scope.model.vars.isBusy = false;
    $scope.model.vars.isFilter = false;
    $scope.model.vars.listItemFull = jsVars.list_item;
    $scope.model.vars.listItemFilter = null;

    if(!jsVars.tinh_thanh_condition){
        jsVars.tinh_thanh_condition = null;
    }
    $scope.model.vars.tinh_thanh_condition = jsVars.tinh_thanh_condition;
    $scope.model.vars.sp_tiep_can_condition = jsVars.sp_tiep_can_condition;
    $scope.model.vars.kq_cuoc_goi_condition = jsVars.kq_cuoc_goi_condition;
    $scope.model.vars.kq_cuoc_gap_condition = jsVars.kq_cuoc_gap_condition;
    $scope.model.vars.tt_khai_thac_condition = jsVars.tt_khai_thac_condition;

    if(!jsVars.list_tinh_thanh){
        jsVars.list_tinh_thanh = [];
    }
    $scope.model.ref.list_tinh_thanh = jsVars.list_tinh_thanh;
    $scope.model.ref.list_tinh_thanh_ref = CommonAssess.getRef($scope.model.ref.list_tinh_thanh, 'uid', 'value');
    $scope.model.ref.list_tinh_thanh.unshift({uid: -1, value: '-- Blank --'});
    $scope.model.ref.list_tinh_thanh.unshift({uid: -2, value: '-- Tỉnh thành --'});

    $scope.model.ref.list_ket_qua_cuoc_goi = jsVars.list_ket_qua_cuoc_goi;
    $scope.model.ref.list_ket_qua_cuoc_goi_ref = CommonAssess.getRef($scope.model.ref.list_ket_qua_cuoc_goi, 'id', 'title');
    $scope.model.ref.list_ket_qua_cuoc_goi.unshift({id: -1, title: '-- Blank --'});
    $scope.model.ref.list_ket_qua_cuoc_goi.unshift({id: -2, title: '-- KQ cuộc gọi --'});

    $scope.model.ref.list_ket_qua_cuoc_gap = jsVars.list_ket_qua_cuoc_gap;
    $scope.model.ref.list_ket_qua_cuoc_gap_ref = CommonAssess.getRef($scope.model.ref.list_ket_qua_cuoc_gap, 'id', 'title');
    $scope.model.ref.list_ket_qua_cuoc_gap.unshift({id: -1, title: '-- Blank --'});
    $scope.model.ref.list_ket_qua_cuoc_gap.unshift({id: -2, title: '-- KQ cuộc gặp --'});

    $scope.model.ref.list_sp_tiep_can = jsVars.list_sp_tiep_can;
    $scope.model.ref.list_sp_tiep_can_ref = CommonAssess.getRef($scope.model.ref.list_sp_tiep_can, 'id', 'title');
    $scope.model.ref.list_sp_tiep_can.unshift({id: -1, title: '-- Blank --'});
    $scope.model.ref.list_sp_tiep_can.unshift({id: -2, title: '-- SP tiếp cận --'});

    $scope.model.ref.list_tinh_trang_khai_thac = jsVars.list_tinh_trang_khai_thac;
    $scope.model.ref.list_tinh_trang_khai_thac_ref = CommonAssess.getRef($scope.model.ref.list_tinh_trang_khai_thac, 'id', 'title');
    $scope.model.ref.list_tinh_trang_khai_thac.unshift({id: -1, title: '-- Blank --'});
    $scope.model.ref.list_tinh_trang_khai_thac.unshift({id: -2, title: '-- TT khai thác --'});

    $scope.model.ref.gt_pk_khac = ClientReportModel.ref.list_gt_pk_khac;
    $scope.model.ref.gt_pk_khac.unshift({id: -1, title: '-- Blank --'});
    $scope.model.ref.gt_pk_khac.unshift({id: -2, title: '-- GT PK Khác --'});

    /*
    console.log($scope.model.vars.sp_tiep_can_condition);
    console.log($scope.model.vars.kq_cuoc_goi_condition);
    console.log($scope.model.vars.kq_cuoc_gap_condition);
    console.log($scope.model.vars.tt_khai_thac_condition);

    console.log($scope.model.vars.owner_conditions);
    */
    /*
    $scope.model.ref.list_ket_qua_cuoc_goi = jsVars.list_ket_qua_cuoc_goi;
    $scope.model.ref.list_ket_qua_cuoc_goi_ref = CommonAssess.getRef($scope.model.ref.list_ket_qua_cuoc_goi, 'id', 'title');

    $scope.model.ref.list_ket_qua_cuoc_gap = jsVars.list_ket_qua_cuoc_gap;
    $scope.model.ref.list_ket_qua_cuoc_gap_ref = CommonAssess.getRef($scope.model.ref.list_ket_qua_cuoc_gap, 'id', 'title');

    $scope.model.ref.list_sp_tiep_can = jsVars.list_sp_tiep_can;
    $scope.model.ref.list_sp_tiep_can_ref = CommonAssess.getRef($scope.model.ref.list_sp_tiep_can, 'id', 'title');

    $scope.model.ref.list_tinh_trang_khai_thac = jsVars.list_tinh_trang_khai_thac;
    $scope.model.ref.list_tinh_trang_khai_thac_ref = CommonAssess.getRef($scope.model.ref.list_tinh_trang_khai_thac, 'id', 'title');
    */

    $scope.model.ref.list_zone = jsVars.list_zone;
    $scope.model.ref.list_zone_ref = CommonAssess.getRef($scope.model.ref.list_zone, 'id', 'title');

    $scope.model.ref.list_user = jsVars.list_user;
    $scope.model.ref.list_user_ref = CommonAssess.getRef($scope.model.ref.list_user, 'id', 'uid');

    var mainTable = $("#main-table");
    var floatPanel = $("#float-panel");
    var anchorPosItem = $("#anchor-pos");

    var anchorPos = anchorPosItem.offset().top + anchorPosItem.height();

    $(window).scroll(function () {
        var currentScroll = $(this).scrollTop();
        if (currentScroll <= anchorPos) {
            floatPanel.hide();
        } else {
            floatPanel.css({ left: mainTable.offset().left + 'px'});
            floatPanel.width(mainTable.width());
            floatPanel.show();
        }
    });

    /*---------------------------------------------------------------------------------------------------*/
    if($('#select-tinh').length) {
        var tinhSelectOption = {
            maxItems: 1,
            valueField: 'uid',
            labelField: 'value',
            searchField: ['uid', 'value'],
            sortField: [
                {field: 'uid', direction: 'asc'}
            ],
            options: $scope.model.ref.list_tinh_thanh
        };
        var selectTinh = $('#select-tinh').selectize(tinhSelectOption);
        $scope.model.fields.tinh = selectTinh[0].selectize;
    }

    if($('#select-tinh_thanh').length) {
        var tinhThanhelectOption = {
            maxItems: 1,
            valueField: 'uid',
            labelField: 'value',
            searchField: ['uid', 'value'],
            sortField: [
                {field: 'uid', direction: 'asc'}
            ],
            options: $scope.model.ref.list_tinh_thanh
        };
        var selectTinhThanh = $('#select-tinh_thanh').selectize(tinhThanhelectOption);
        $scope.model.fields.tinh_thanh = selectTinhThanh[0].selectize;
        if (!$scope.model.vars.tinh_thanh_condition) {
            $scope.model.fields.tinh_thanh.setValue($scope.model.ref.list_tinh_thanh[0].uid);
        } else {
            $scope.model.fields.tinh_thanh.setValue($scope.model.vars.tinh_thanh_condition);
        }
        var value = $scope.model.fields.tinh_thanh.getValue();
        if(value != -2){
            $("#tinh_thanh").val(value);
        } else {
            $("#tinh_thanh").val(null);
        }
        $scope.model.fields.tinh_thanh.on('change', function (value) {
            if(value != -2){
                $("#tinh_thanh").val(value);
            } else {
                $("#tinh_thanh").val(null);
            }
            //console.log('sp_tiep_can', value);
        });
    }
    if($('#select-sp_tiep_can').length){
        var spTiepCanSelectOption = {
            maxItems : 1,
            valueField : 'id',
            labelField : 'title',
            searchField : ['id', 'title'],
            sortField: [{field: 'id', direction: 'asc'}],
            options : $scope.model.ref.list_sp_tiep_can
        };
        var selectSpTiepCan = $('#select-sp_tiep_can').selectize(spTiepCanSelectOption);
        $scope.model.fields.sp_tiep_can = selectSpTiepCan[0].selectize;
    

        if(!$scope.model.vars.sp_tiep_can_condition){
            $scope.model.fields.sp_tiep_can.setValue($scope.model.ref.list_sp_tiep_can[0].id);
        }else{
            $scope.model.fields.sp_tiep_can.setValue($scope.model.vars.sp_tiep_can_condition);
        }
        var value = $scope.model.fields.sp_tiep_can.getValue();
        if(value != -2){
            $("#sp_tiep_can").val(value);
        }else{
            $("#sp_tiep_can").val(null);
        }
        $scope.model.fields.sp_tiep_can.on('change', function(value){
            if(value != -2){
                $("#sp_tiep_can").val(value);
            }else{
                $("#sp_tiep_can").val(null);
            }
            //console.log('sp_tiep_can', value);
        });
    }

    if($('#select-kq_cuoc_goi').length){
        var ketQuaCuocGoiSelectOption = {
            maxItems : 1,
            valueField : 'id',
            labelField : 'title',
            searchField : ['id', 'title'],
            sortField: [{field: 'id', direction: 'asc'}],
            options : $scope.model.ref.list_ket_qua_cuoc_goi
        };
        var selectKetQuaCuocGoi = $('#select-kq_cuoc_goi').selectize(ketQuaCuocGoiSelectOption);
        $scope.model.fields.kq_cuoc_goi = selectKetQuaCuocGoi[0].selectize;
        if(!$scope.model.vars.kq_cuoc_goi_condition){
            $scope.model.fields.kq_cuoc_goi.setValue($scope.model.ref.list_ket_qua_cuoc_goi[0].id);
        }else{
            $scope.model.fields.kq_cuoc_goi.setValue($scope.model.vars.kq_cuoc_goi_condition);
        }
        var value = $scope.model.fields.kq_cuoc_goi.getValue();
        if(value != -2){
            $("#kq_cuoc_goi").val(value);
        }else{
            $("#kq_cuoc_goi").val(null);
        }
        $scope.model.fields.kq_cuoc_goi.on('change', function(value){
            if(value != -2){
                $("#kq_cuoc_goi").val(value);
            }else{
                $("#kq_cuoc_goi").val(null);
            }
        });
    }

    if($('#select-kq_cuoc_gap').length){
        var ketQuaCuocGapSelectOption = {
            maxItems : 1,
            valueField : 'id',
            labelField : 'title',
            searchField : ['id', 'title'],
            sortField: [{field: 'id', direction: 'asc'}],
            options : $scope.model.ref.list_ket_qua_cuoc_gap
        };
        var selectKetQuaCuocGap = $('#select-kq_cuoc_gap').selectize(ketQuaCuocGapSelectOption);
        $scope.model.fields.kq_cuoc_gap = selectKetQuaCuocGap[0].selectize;
        if(!$scope.model.vars.kq_cuoc_gap_condition){
            $scope.model.fields.kq_cuoc_gap.setValue($scope.model.ref.list_ket_qua_cuoc_gap[0].id);
        }else{
            $scope.model.fields.kq_cuoc_gap.setValue($scope.model.vars.kq_cuoc_gap_condition);
        }
        var value = $scope.model.fields.kq_cuoc_gap.getValue();
        if(value != -2){
            $("#kq_cuoc_gap").val(value);
        }else{
            $("#kq_cuoc_gap").val(null);
        }
        $scope.model.fields.kq_cuoc_gap.on('change', function(value){
            if(value != -2){
                $("#kq_cuoc_gap").val(value);
            }else{
                $("#kq_cuoc_gap").val(null);
            }
        });
    }

    if($('#select-tinh_trang_khai_thac').length){
        var tinhTrangKhaiThacSelectOption = {
            maxItems : 1,
            valueField : 'id',
            labelField : 'title',
            searchField : ['id', 'title'],
            sortField: [{field: 'id', direction: 'asc'}],
            options : $scope.model.ref.list_tinh_trang_khai_thac
        };
        var selectTinhTrangKhaiThac = $('#select-tinh_trang_khai_thac').selectize(tinhTrangKhaiThacSelectOption);
        $scope.model.fields.tinh_trang_khai_thac = selectTinhTrangKhaiThac[0].selectize;
        if(!$scope.model.vars.tt_khai_thac_condition){
            $scope.model.fields.tinh_trang_khai_thac.setValue($scope.model.ref.list_tinh_trang_khai_thac[0].id);
        }else{
            $scope.model.fields.tinh_trang_khai_thac.setValue($scope.model.vars.tt_khai_thac_condition);
        }
        var value = $scope.model.fields.tinh_trang_khai_thac.getValue();
        if(value != -2){
            $("#tt_khai_thac").val(value);
        }else{
            $("#tt_khai_thac").val(null);
        }
        $scope.model.fields.tinh_trang_khai_thac.on('change', function(value){
            if(value != -2){
                $("#tt_khai_thac").val(value);
            }else{
                $("#tt_khai_thac").val(null);
            }
        });
    }

    if($('#select-gt_pk_khac').length){
        var gtPkKhacSelectOption = {
            maxItems : 1,
            valueField : 'id',
            labelField : 'title',
            searchField : ['id', 'title'],
            sortField: [{field: 'id', direction: 'asc'}],
            options : $scope.model.ref.gt_pk_khac
        };
        var selectGtPkKhac = $('#select-gt_pk_khac').selectize(gtPkKhacSelectOption);
        $scope.model.fields.gt_pk_khac = selectGtPkKhac[0].selectize;

        if(!$scope.model.vars.gt_pk_khac_condition){
            $scope.model.fields.gt_pk_khac.setValue($scope.model.ref.gt_pk_khac[0].id);
        }else{
            $scope.model.fields.gt_pk_khac.setValue($scope.model.vars.gt_pk_khac_condition);
        }
        var value = $scope.model.fields.gt_pk_khac.getValue();
        if(value != -2){
            $("#gt_pk_khac").val(value);
        }else{
            $("#gt_pk_khac").val(null);
        }
        $scope.model.fields.gt_pk_khac.on('change', function(value){
            if(value != -2){
                $("#gt_pk_khac").val(value);
            }else{
                $("#gt_pk_khac").val(null);
            }
        });
    }

    if($("#ngay_tra_ho").length){
        var ngay_tra_ho = $('#ngay_tra_ho');
        ngay_tra_ho.datetimepicker(CommonAssess.datePickerOption);
    }

    /*-------------------------------------Init update list-------------------------------------*/

    $scope.model.act.updateListItem($scope.model.vars.listItemFull);    

    /*-------------------------------------Implement list filter-------------------------------------*/
    
    $scope.$watch('model.vars.inputFilter', function(newValue, oldValue) {
        currentPage = 1;
        if(newValue){
            if(!$scope.model.vars.isBusy && newValue.length >= 3){
                get_all(false);
            }
        }else{
            if(init){
                init = false
            }else{
                get_all(false);
            }
        }
    });
    
    /*-------------------------------------Controller functions-------------------------------------*/

    $(window).scroll(function() {
        if($(window).scrollTop() + $(window).height() >= $(document).height()) {
            if(!$scope.model.vars.reach_bottom && !$scope.model.vars.isBusy){
                currentPage++;
                get_all(true);
            }
        }
    });

    function get_all(is_append){
        CommonAssess.showWaitingHeader(true);
        var params = new FormData();
        
        if($scope.model.vars.inputFilter){
            params.append('keyword', $scope.model.vars.inputFilter);
        }

        $scope.model.vars.isBusy = true;
        params.append('page', currentPage);
        var testCondition = {};
        //if($scope.model.vars.listType == 'quota' || $scope.model.vars.listType == 'quota_preview'){
            if($scope.model.vars.owner_conditions){
                for(var i in $scope.model.vars.owner_conditions){
                    var key = i;
                    var value = $scope.model.vars.owner_conditions[i];
                    params.append(key, value);
                    testCondition[key] = value;
                }
            }
        //}

        $scope.model.api.getAll($scope.model.vars.serverController, $scope.model.vars.csrftoken, params).done(function(result){
            CommonAssess.showWaitingHeader(false);
            $scope.model.vars.isBusy = false;

            if(result.data){
                $scope.model.vars.reach_bottom = result.reach_bottom
                if(is_append){
                    if(maxRecords < $scope.model.vars.listItemFull.length){
                        $scope.model.vars.listItemFull = $scope.model.vars.listItemFull.slice(-5);
                    }
                    $scope.model.vars.listItemFull = $scope.model.vars.listItemFull.concat(result.data);
                }else{
                    $scope.model.vars.listItemFull = result.data;
                }
                $scope.model.act.updateListItem($scope.model.vars.listItemFull);
            }
            $scope.$apply();
        });
    }

    $scope.openFormEdit = function(id){
        $scope.model.act.openFormEdit(id);
    };

    $scope.openFormView = function(id){
        $scope.model.act.openFormView(id);
    };

    $scope.openFormRemove = function(id){
        if(id){
            var idLength = id.toString().split("|").length;
            if(idLength > 1){
                $scope.formRemoveInfo = "Do you realy want to remove these (" + idLength.toString() + ") records ?";
            }else{
                $scope.formRemoveInfo = "Do you realy want to remove this records ?";
            }
            $scope.model.fields.id = id;
            var confirmRemove = confirm($scope.formRemoveInfo);
            if(confirmRemove){
                submitFormRemove();
            }
        }
    };

    function submitFormRemove(){
        CommonAssess.showWaitingHeader(true);
        var params = new FormData();
        params.append('id',$scope.model.fields.id);
        $scope.model.api.removeItem($scope.model.vars.serverController, $scope.model.vars.csrftoken, params).done(function(result){
            $scope.model.act.afterSubmit(result, 'remove');
            CommonAssess.showWaitingHeader(false);
        });
    };


    $scope.openFormChangeOwner = function(id, status){
        if(status == 'send'){
            if($scope.model.vars.owner_id){
                if(id){
                    $scope.formRemoveInfo = "Do you really want to set quota for this user?";
                    $scope.model.fields.id = id;
                    var confirmRemove = confirm($scope.formRemoveInfo);
                    if(confirmRemove){
                        submitFormChangeOwner(status);
                    }
                }
            }else{
                alert('You must to select one user to assign.');
            }
        }else{
            if(id){
                $scope.formRemoveInfo = "Do you really want to reject these record?";
                $scope.model.fields.id = id;
                var confirmRemove = confirm($scope.formRemoveInfo);
                if(confirmRemove){
                    submitFormChangeOwner(status);
                }
            }
        }

    }

    function submitFormChangeOwner(status){
        CommonAssess.showWaitingHeader(true);
        var params = new FormData();
        params.append('id', $scope.model.fields.id);
        params.append('user_level' , $scope.model.vars.user_level);
        if(status =='send'){
            params.append('owner_id', $scope.model.vars.owner_id);
            params.append('owner_type', $scope.model.vars.owner_type);
        }else{
            params.append('owner_type', $scope.model.vars.user_type);
        }

        //console.log($scope.model.fields.id, $scope.model.vars.owner_id, $scope.model.vars.owner_type);
        $scope.model.api.changeOwner($scope.model.vars.serverController, $scope.model.vars.csrftoken, params).done(function(result){
            if(status =='send'){
                $scope.model.act.afterSubmit(result, 'changeOwner', true);
            }else{
                $scope.model.act.afterSubmit(result, 'changeOwner', true);
            }
            CommonAssess.showWaitingHeader(false);
        });
    };

    $scope.selectCheckBox = function(){
        $scope.model.vars.listItemId=getListItemId();
    };

    $scope.selectAllCheckBox = function(){
        var checkedItems = $('.selectCheckBox:checked');
        var checkBox = $(".selectCheckBox");

        if(checkedItems.length > 0 && checkedItems.length < checkBox.length){
            checkBox.prop("checked", true);
        }else if(checkedItems.length == checkBox.length){
            checkBox.prop("checked", false);
        }else{
            checkBox.prop("checked", true);
        }
        $scope.model.vars.listItemId = getListItemId();
    };

    function getListItemId(){
        var checkedItems = $('.selectCheckBox:checked');
        var listItemId = "";
        $scope.model.vars.totalCheck = checkedItems.length?checkedItems.length:0;

        if(checkedItems.length > 0){
            checkedItems.each(function(key,value){
                if(key == 0){
                    listItemId = $(value).attr("data");
                }else{
                    listItemId += "|" + $(value).attr("data");
                }
            });
        }
        return listItemId;
    };


    $scope.submitFormAddByUpload = function(){
        if(!$scope.model.fields.data_file){
            alert('You must to select one XLS/XLSX fle');
            return;
        }
        CommonAssess.showWaitingHeader(true);
        var params = new FormData();
        params.append('id', $scope.model.fields.id);
        params.append('data_file' , $scope.model.fields.data_file);

        $scope.model.api.addByUpload($scope.model.vars.serverController, $scope.model.vars.csrftoken, params).done(function(result){
            CommonAssess.showWaitingHeader(false);
            if(result.error){
                alert(result.error);
            }else{
                location.reload();
            }

        });
    };

    $(document).on('change','#data_file',
        function (e) {
            var file    = this.files[0];
            var reader  = new FileReader();
            reader.onload = function(e) {

                $scope.model.fields.data_file = file;
                //console.log($scope.model.fields.data_file);
                //$scope.$apply();
            };
            reader.readAsDataURL(file);
        }
    );
});

/*----------------------------------------------MODAL CONTROLLER--------------------------------------------*/

App.controller("ClientCompanyModal",function($scope, ClientCompanyModel){
    $scope.model = ClientCompanyModel;

    $scope.submitFormEdit = function(isContinue){
        isContinue = typeof isContinue !== 'undefined' ? isContinue : false;
        $scope.model.act.submitFormEdit(isContinue);
    };
});
