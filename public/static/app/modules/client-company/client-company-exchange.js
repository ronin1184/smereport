'use strict';

/* Controllers */
App.controller("ClientCompanyExchange",function($scope, $filter, ClientCompanyModel, CommonAssess){
	$scope.model = ClientCompanyModel;

	var serverVarsElm = $('#js-vars');
	var jsVars = serverVarsElm.val()?JSON.parse(serverVarsElm.val()):null;
	//$scope.model.vars.listItemFull = [];

    $scope.model.vars.assign_ratio_str = jsVars.assign_ratio_str;
    $scope.model.vars.admin_tree = jsVars.admin_tree;
    $scope.model.vars.user_level = jsVars.user_level;
    $scope.model.vars.user_table = jsVars.user_table;
    $scope.model.vars.owner_type = jsVars.owner_type;
    $scope.model.vars.owner_conditions = jsVars.owner_conditions;
    $scope.model.ref.list_zone_full = jsVars.list_zone_full;
    $scope.model.ref.list_zone_ref = CommonAssess.getRef($scope.model.ref.list_zone_full, 'id', 'title');

    //console.log($scope.model.vars.assign_ratio);
    //console.log($scope.model.vars.assign_ratio_str);
    $(function(){
        $('#quota_tree').on('changed.jstree', function(event, data){
        //if(data.node.original.type == 'ADMIN'){
            //console.log('Admin:', data.node.id);
        var type = data.node.original.type;
        if(!type){
            type = data.node.original.original.type;
        }
        if(!type){
            type = data.node.original.original.original.type;
        }
        var new_url = $scope.baseUrl + 'administrator/quota_preview/' + type + '/' + data.node.original.id.toString();
        window.open(new_url, '_blank');
        //}else if(data.node.original.type == 'USER'){
            //console.log('User:', data.node.id);
        //}
    }).jstree({ 'core' : {
            data: $scope.model.vars.admin_tree
            /*
            'data' : [
                { "id" : "ajson1", "parent" : "#", "text" : "Simple root node" },
                { "id" : "ajson2", "parent" : "#", "text" : "Root node 2" },
                { "id" : "ajson3", "parent" : "ajson2", "text" : "Child 1" },
                { "id" : "ajson4", "parent" : "ajson2", "text" : "Child 2" }
            ]
            */
        } });
    });

    $scope.model.ref.listOwner = jsVars.list_owner;
    for(var i in $scope.model.ref.listOwner){
        var email = $scope.model.ref.listOwner[i].email;
        var zone = $scope.model.ref.listOwner[i].zone_id;
        $scope.model.ref.listOwner[i].name += ' - ' + email + ($scope.model.ref.list_zone_ref[zone]?' - '+$scope.model.ref.list_zone_ref[zone]:'');
    }
    $scope.model.ref.listOwner.unshift({id: 0, name: '----- Please select one -----'});
    var ownerSelectOption = {
        maxItems : 1,
        valueField : 'id',
        labelField : 'name',
        searchField : ['id', 'name'],
        //sortField: [{field: 'order', direction: 'asc'}],
        options : $scope.model.ref.listOwner,
        onChange: function(value){

            value = parseInt(value);
            if(value > 0){
                $scope.model.vars.owner_id = value;

                return;
                CommonAssess.showWaitingHeader(true);
                var params = new FormData();
                params.append('owner_id' , $scope.model.vars.owner_id);
                params.append('owner_type' , $scope.model.vars.owner_type);
                params.append('user_level' , $scope.model.vars.user_level);

                //console.log($scope.model.vars.owner_id, $scope.model.vars.owner_type);
                ClientCompanyModel.api.getFull($scope.model.vars.serverController, $scope.model.vars.csrftoken, params).done(function(result){
                    if(result.data){
                        $scope.model.vars.listItemOwner = result.data;
                    }else{
                        $scope.model.vars.listItemOwner = [];
                    }
                    CommonAssess.showWaitingHeader(false);
                    $scope.$apply();
                });
            }
        }
    };

    var owner = $('#owner-select').selectize(ownerSelectOption);
    $scope.model.fields.owner = owner[0].selectize;
    $scope.model.fields.owner.setValue(0);
});