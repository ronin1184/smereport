'use strict';

/* Services */
var ClientUserLoginModel=angular.module('ClientUserLoginModel',[]);
ClientUserLoginModel.factory('ClientUserLoginModel',function($http,$rootScope){
	//var csrftoken = $.cookie('csrftoken');
	var csrftoken = $('[name="csrfmiddlewaretoken"]').val();
	return {

		authen:function(params){
			return $.ajax({
                url         :$rootScope.baseUrl + "user/authenticate",
                type        :'POST',
                cache		: false,
                //crossDomain :true,
                headers     :{'X-CSRFToken':csrftoken},
                processData: false,
                contentType: false,
                data        :params
            });
		},

        forgotPassword:function(params){
			return $.ajax({
                url         :$rootScope.baseUrl + "user/forgot_password",
                type        :'POST',
                cache		: false,
                //crossDomain :true,
                headers     :{'X-CSRFToken':csrftoken},
                processData: false,
                contentType: false,
                data        :params
            });
		},


		fields:{
			title 		:null,
			order 		:0
		},

		other:{
			displayLoading 	:false,
			errorMessage	:null,
			submitSuccess	:null,
			sexIcon			:null,
			isEdit			:false,
			_id				:null,
			listItemId		:null
		},
	}   
});
