'use strict';

/* Controllers */
App.controller("ClientStaff",function($scope, $filter, ClientStaffModel, CommonAssess){
	$scope.model = ClientStaffModel;

	var mainTable = $('#staff-table');
	var serverVarsElm = $('#js-vars');
	var currentPage = 1;
	var maxRecords = 250;
	var init = true;

	var jsVars = serverVarsElm.val()?JSON.parse(serverVarsElm.val()):null;

	$scope.model.vars.reach_bottom = false;
	$scope.model.vars.isBusy = false;
	$scope.model.vars.isFilter = false;
	$scope.model.vars.listItemFull = jsVars.list_item;
	$scope.model.ref.zone = jsVars.list_zone;
	$scope.model.ref.title = jsVars.list_title;
	//$scope.model.ref.zone.unshift({id: 0, title: '----- Please select one -----'});
	$scope.model.vars.listItemFilter = null;
	
	//console.log($scope.model.ref.title);
	
	/*--------------------------------------------------------------------*/
	
	var roleSelectOption = {
        maxItems : 1,
        valueField : 'value',
        labelField : 'label',
        searchField : ['value', 'label'],
        options : $scope.model.ref.roleList
    };

    var selectRole = $('#select-role').selectize(roleSelectOption);
    $scope.model.fields.role = selectRole[0].selectize;
	
	/*--------------------------------------------------------------------*/
	
	var titleSelectOption = {
        maxItems : 1,
        valueField : 'id',
        labelField : 'title',
        searchField : ['id', 'title'],
        options : $scope.model.ref.title
    };

    var title = $('#select-title').selectize(titleSelectOption);
    $scope.model.fields.title = title[0].selectize;
	
	/*--------------------------------------------------------------------*/
	
	var zoneSelectOption = {
        maxItems : 1,
        valueField : 'id',
        labelField : 'title',
        searchField : ['id', 'title'],
        //sortField: [{field: 'order', direction: 'asc'}],
        options : $scope.model.ref.zone
    };

    var zone = $('#select-zone').selectize(zoneSelectOption);
    $scope.model.fields.zone = zone[0].selectize;

	/*-------------------------------------Init update list-------------------------------------*/

	$scope.model.act.updateListItem($scope.model.vars.listItemFull);	

	/*-------------------------------------Implement list filter-------------------------------------*/

	$scope.$watch('model.vars.inputFilter', function(newValue, oldValue) {
		currentPage = 1;
		if(newValue){
			if(!$scope.model.vars.isBusy && newValue.length >= 3){
				get_all(false);
			}
		}else{
			if(init){
				init = false
			}else{
				get_all(false);
			}
		}
	});

	/*-------------------------------------Controller functions-------------------------------------*/

	$(window).scroll(function() {
		if($(window).scrollTop() + $(window).height() >= $(document).height()) {
			if(!$scope.model.vars.reach_bottom && !$scope.model.vars.isBusy){
				currentPage++;
				get_all(true);
			}
		}
	});

	function get_all(is_append){
		CommonAssess.showWaitingHeader(true);
		var params = new FormData();
        
        if($scope.model.vars.inputFilter){
        	params.append('keyword', $scope.model.vars.inputFilter);
        }

        $scope.model.vars.isBusy = true;
        params.append('page', currentPage);
		$scope.model.api.getAll($scope.model.vars.serverController, $scope.model.vars.csrftoken, params).done(function(result){
			CommonAssess.showWaitingHeader(false);
			$scope.model.vars.isBusy = false;

			if(result.data){
				$scope.model.vars.reach_bottom = result.reach_bottom
				if(is_append){
					if(maxRecords < $scope.model.vars.listItemFull.length){
						$scope.model.vars.listItemFull = $scope.model.vars.listItemFull.slice(-5);
					}
					$scope.model.vars.listItemFull = $scope.model.vars.listItemFull.concat(result.data);
				}else{
					$scope.model.vars.listItemFull = result.data;
				}
				$scope.model.act.updateListItem($scope.model.vars.listItemFull);
			}
			$scope.$apply();
		});
	}

	$scope.openFormEdit = function(id){
		$scope.model.act.openFormEdit(id);
	};

	$scope.openFormRemove = function(id){
		if(id){
			id = id.toString();
			if(id.split("|").length > 1){
				$scope.formRemoveInfo = "Do you realy want to remove these records ?";
			}else{
				$scope.formRemoveInfo = "Do you realy want to remove this records ?";
			}
			$scope.model.fields.id = id;
			var confirmRemove = confirm($scope.formRemoveInfo);
			if(confirmRemove){
				submitFormRemove();
			}
		}
	};

	function submitFormRemove(){
		CommonAssess.showWaitingHeader(true);
		var params = new FormData();
		params.append('id',$scope.model.fields.id);
		$scope.model.api.removeItem($scope.model.vars.serverController, $scope.model.vars.csrftoken, params).done(function(result){
			$scope.model.act.afterSubmit(result, 'remove');
			CommonAssess.showWaitingHeader(false);
		});
	};

	$scope.selectCheckBox = function(){
		$scope.model.vars.listItemId=getListItemId();
	};

	$scope.selectAllCheckBox = function(){
		var checkedItems = $('.selectCheckBox:checked');
		var checkBox = $(".selectCheckBox");

		if(checkedItems.length > 0 && checkedItems.length < checkBox.length){
			checkBox.prop("checked", true);
		}else if(checkedItems.length == checkBox.length){
			checkBox.prop("checked", false);
		}else{
			checkBox.prop("checked", true);
		}
		$scope.model.vars.listItemId = getListItemId();
	};

	function getListItemId(){
		var checkedItems = $('.selectCheckBox:checked');
		var listItemId = "";
		if(checkedItems.length > 0){
			checkedItems.each(function(key,value){
				if(key == 0){
					listItemId = $(value).attr("data");
				}else{
					listItemId += "|" + $(value).attr("data");
				}
			});
		}
		return listItemId;
	};
});

/*----------------------------------------------MODAL CONTROLLER--------------------------------------------*/

App.controller("ClientStaffModal",function($scope, ClientStaffModel){
	$scope.model = ClientStaffModel;

	$scope.submitFormEdit = function(){
		$scope.model.act.submitFormEdit();
	};
});