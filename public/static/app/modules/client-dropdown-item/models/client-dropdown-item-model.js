'use strict';

/* Services */
var ClientDropdownItemModel=angular.module('ClientDropdownItemModel',[]);
ClientDropdownItemModel.factory('ClientDropdownItemModel',function($http, $rootScope, CommonAssess){
    var serverController = 'dropdown/item';
	var csrftoken = $('[name="csrfmiddlewaretoken"]').val();

    var firstInput = $('#VI');
    var secondInput = $('#VI');

    var serverVarsElm = $('#js-vars');
    
    var startPoint = 0;
    var perPage = 15;

    /*--------------------------------------------------Variables--------------------------------------------*/    

    var vars = {
        isFilter : false,
        listItem : null,
        inputFilter : null,
        listPercentText : null,
        listItemFull : null,
        listItemFilter : null,
        displayLoading : false,
        errorMessage : null,
        isEdit : false,
        listItemId : null,
        top_id : null,
        startPoint : startPoint,
        perPage : perPage,
        serverController : serverController,
        csrftoken : csrftoken
    };

    /*--------------------------------------------------Addition APIs-----------------------------------------*/

    var api = CommonAssess.api;
	var model = {
        api : api,

		vars : vars,

        fields : {},

        ref : {},

        act : {
            updateListItem : function(listItemFullInput){
                updateListItem(listItemFullInput);
            },

            openFormEdit : function(_id){
                openFormEdit(_id);
            },

            closeFormEdit : function(){
                closeFormEdit();
            },

            setDisplayLoading : function(isDisplay){
                setDisplayLoading(isDisplay);
            },

            submitFormEdit : function(){
                submitFormEdit();
            },

            afterSubmit : function(result, action){
                afterSubmit(result, action);
            },

            emptyModelFieldsVariables : function(){
                emptyModelFieldsVariables();
            },

            setModelFieldsVariables : function(item){
                setModelFieldsVariables(item);
            }
        }
	} 

    /*-----------------------------------------------Init UI----------------------------------------------*/

    var modal = $.remodal.lookup[$('[data-remodal-id = modal]').data('remodal')];

    $(document).on('opened', '.remodal', function () {
        if(!model.vars.isEdit){
            setTimeout(function(){
                firstInput.focus();
            }, 0);
        }else{
            setTimeout(function(){
                secondInput.focus();
            }, 0);
        }
    });

    /*---------------------------------------------Controller functions---------------------------------------------*/

    var updateListItem = function(listItemFullInput){
        if(!listItemFullInput) return;
        var maxPoint = listItemFullInput.length;
        model.vars.listItem = listItemFullInput;
    }

    var openFormEdit = function(_id){
        emptyModelFieldsVariables();
        setDisplayLoading(false);
        if(_id){
            CommonAssess.showWaitingHeader(true);
            model.fields._id = _id;
            model.vars.isEdit = true;

            var params = new FormData();
            params.append('top_id', model.vars.top_id);
			params.append('target', model.fields._id);
            model.api.getSingle(serverController, csrftoken, params).done(function(result){
                setModelFieldsVariables(result.data);
                CommonAssess.showWaitingHeader(false);
                modal.open();
                $rootScope.$apply();
            });
        }else{
            model.vars.isEdit = false;
            modal.open();
        }
    };

    var closeFormEdit = function(){
        modal.close();
    };

    var submitFormEdit = function(){
        setDisplayLoading(true);

        var params = new FormData();

        params.append('top_id' 	,model.vars.top_id);
		
		params.append('VI', model.fields.VI);
		params.append('EN', model.fields.EN);
		params.append('order', model.fields.order);
		params.append('is_edit', model.vars.isEdit ? 1 : 0);

        if(model.vars.isEdit){
            params.append('target', model.fields._id);
            model.api.editItem(serverController, csrftoken, params).done(function(result){
                afterSubmit(result, 'edit');
            });
        }else{
        	params.append('target', 'item');
            model.api.addItem(serverController, csrftoken, params).done(function(result){
                afterSubmit(result, 'add');
            });
        }
    };

    var afterSubmit = function(result, action){

        if(!result.error){
            var newItem = {
                _id : result.data._id.toString(),
                VI : result.data.VI ? result.data.VI : model.fields.VI,
                EN : result.data.EN ? result.data.EN : model.fields.EN,
                order : result.data.order ? result.data.order : model.fields.order
            };

            switch(action){
                case 'edit':
                    var index = CommonAssess.getIndexFromId(model.vars.listItemFull, newItem._id);
                    model.vars.listItemFull[index]=newItem;
                break;

                case 'add':
                    model.vars.listItemFull.unshift(newItem);
                    startPoint = 0;
                break;

                case 'remove':
                    var listId = newItem._id.split('|');
                    for(var i in listId){
                        var index = CommonAssess.getIndexFromId(model.vars.listItemFull, listId[i]);
                        model.vars.listItemFull.splice(index,1);   
                    }
                    startPoint = 0;
                break;
            }

            updateListItem(model.vars.listItemFull);
            closeFormEdit();
        }else{
            model.vars.errorMessage = CommonAssess.processErrorMessage(result.error);
            setDisplayLoading(false);
        }
        $rootScope.$apply();
    };

    var setDisplayLoading = function(isDisplay){
        model.vars.displayLoading = isDisplay;
    };

    var emptyModelFieldsVariables = function(){
        model.vars.errorMessage = CommonAssess.processErrorMessage(null);

        model.fields.VI = null;
		model.fields.EN = null;
		model.fields.order = 0;
    };

    var setModelFieldsVariables = function(item){
        model.vars.errorMessage = CommonAssess.processErrorMessage(null);

        model.fields.VI	= item.VI;
		model.fields.EN	= item.EN;
		model.fields.order = parseInt(item.order);
    };

    return model;  
});