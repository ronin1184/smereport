'use strict';

/* Controllers */
App.controller("ClientTemplate",function($scope, $filter, ClientTemplateModel, CommonAssess){
	$scope.model = ClientTemplateModel;

	var serverVarsElm = $('#js-vars');
	var currentPage = 1;
	var maxRecords = 70;
	var init = true;

	var jsVars = serverVarsElm.val()?JSON.parse(serverVarsElm.val()):null;

	$scope.model.vars.reach_bottom = false;
	$scope.model.vars.isBusy = false;
	$scope.model.vars.isFilter = false;
	$scope.model.vars.listItemFull = jsVars.list_item;
	$scope.model.vars.listItemFilter = null;

	/*-------------------------------------Init update list-------------------------------------*/

	$scope.model.act.updateListItem($scope.model.vars.listItemFull);	

	/*-------------------------------------Implement list filter-------------------------------------*/

	$scope.$watch('model.vars.inputFilter', function(newValue, oldValue) {
		currentPage = 1;
		if(newValue){
			if(!$scope.model.vars.isBusy && newValue.length >= 3){
				get_all(false);
			}
		}else{
			if(init){
				init = false
			}else{
				get_all(false);
			}
		}
	});

	/*-------------------------------------Controller functions-------------------------------------*/

	$(window).scroll(function() {
		if($(window).scrollTop() + $(window).height() >= $(document).height()) {
			if(!$scope.model.vars.reach_bottom && !$scope.model.vars.isBusy){
				currentPage++;
				get_all(true);
			}
		}
	});

	function get_all(is_append){
		CommonAssess.showWaitingHeader(true);
		var params = new FormData();
        
        if($scope.model.vars.inputFilter){
        	params.append('keyword', $scope.model.vars.inputFilter);
        }

        $scope.model.vars.isBusy = true;
        params.append('page', currentPage);
		$scope.model.api.getAll($scope.model.vars.serverController, $scope.model.vars.csrftoken, params).done(function(result){
			CommonAssess.showWaitingHeader(false);
			$scope.model.vars.isBusy = false;

			if(result.data){
				$scope.model.vars.reach_bottom = result.reach_bottom
				if(is_append){
					if(maxRecords < $scope.model.vars.listItemFull.length){
						$scope.model.vars.listItemFull = $scope.model.vars.listItemFull.slice(-5);
					}
					$scope.model.vars.listItemFull = $scope.model.vars.listItemFull.concat(result.data);
				}else{
					$scope.model.vars.listItemFull = result.data;
				}
				$scope.model.act.updateListItem($scope.model.vars.listItemFull);
			}
			$scope.$apply();
		});
	}

	$scope.openFormEdit = function(_id){
		$scope.model.act.openFormEdit(_id);
	};

	$scope.openFormRemove = function(_id){
		if(_id){
			if(_id.split("|").length > 1){
				$scope.formRemoveInfo = "Do you realy want to remove these records ?";
			}else{
				$scope.formRemoveInfo = "Do you realy want to remove this records ?";
			}
			$scope.model.fields._id = _id;
			var confirmRemove = confirm($scope.formRemoveInfo);
			if(confirmRemove){
				submitFormRemove();
			}
		}
	};

	function submitFormRemove(){
		CommonAssess.showWaitingHeader(true);
		var params = new FormData();
		params.append('_id',$scope.model.fields._id);
		$scope.model.api.removeItem($scope.model.vars.serverController, $scope.model.vars.csrftoken, params).done(function(result){
			$scope.model.act.afterSubmit(result, 'remove');
			CommonAssess.showWaitingHeader(false);
		});
	};

	$scope.selectCheckBox = function(){
		$scope.model.vars.listItemId=getListItemId();
	};

	$scope.selectAllCheckBox = function(){
		var checkedItems = $('.selectCheckBox:checked');
		var checkBox = $(".selectCheckBox");

		if(checkedItems.length > 0 && checkedItems.length < checkBox.length){
			checkBox.prop("checked", true);
		}else if(checkedItems.length == checkBox.length){
			checkBox.prop("checked", false);
		}else{
			checkBox.prop("checked", true);
		}
		$scope.model.vars.listItemId = getListItemId();
	};

	function getListItemId(){
		var checkedItems = $('.selectCheckBox:checked');
		var listItemId = "";
		if(checkedItems.length > 0){
			checkedItems.each(function(key,value){
				if(key == 0){
					listItemId = $(value).attr("data");
				}else{
					listItemId += "|" + $(value).attr("data");
				}
			});
		}
		return listItemId;
	};
});

/*----------------------------------------------MODAL CONTROLLER--------------------------------------------*/

App.controller("ClientTemplateModal",function($scope, ClientTemplateModel){
	$scope.model = ClientTemplateModel;

	$scope.submitFormEdit = function(){
		$scope.model.act.submitFormEdit();
	};

	$(document).on('change','#thumbnail',
        function (e) {
            var file 	= this.files[0];
            var reader 	= new FileReader();
            reader.onload = function(e) {
                $scope.model.fields.thumbnail = file;
                //$scope.$apply();
            };
            reader.readAsDataURL(file);
        }
    );
});