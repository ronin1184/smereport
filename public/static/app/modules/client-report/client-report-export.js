/**
 * Created by tbson on 7/27/14.
 */
'use strict';

/* Controllers */
App.controller("ClientReportExport",function($scope, $filter, ClientReportModel, CommonAssess){
	$scope.model = ClientReportModel;

	var serverVarsElm = $('#js-vars');

	var jsVars = serverVarsElm.val()?JSON.parse(serverVarsElm.val()):null;

    $scope.model.ref.list_tm = jsVars.list_tm;
    $scope.model.ref.list_tm_ref = CommonAssess.getRef($scope.model.ref.list_tm, 'id', 'name');

    $scope.model.ref.list_hub = jsVars.list_hub;
    $scope.model.ref.list_hub_ref = CommonAssess.getRef($scope.model.ref.list_hub, 'id', 'title');

    $scope.model.vars.report_type = jsVars.report_type;

    $scope.model.ref.level = jsVars.level;
    $scope.model.ref.isAdmin = jsVars.isAdmin;

    $(function(){
        var current_date = new Date();
        var year_str = current_date.getFullYear().toString();
        var month_str = "";
        var day_str = "";
        if(current_date.getMonth() + 1 < 10 ){
            month_str = "0" + (current_date.getMonth() + 1).toString();
        }else{
            month_str = (current_date.getMonth() + 1).toString();
        }
        if(current_date.getDate() < 10 ){
            day_str = "0" + current_date.getDate().toString();
        }else{
            day_str = current_date.getDate().toString();
        }

        var date_str = year_str + "-" + month_str + "-" + day_str;
        var dateOption = {
            timepicker: false,
            closeOnDateSelect: true,
            mask: true,
            format: 'Y-m-d'
        };

        var start_date = $('#start_date')
        start_date.datetimepicker(dateOption);
        start_date.val(date_str);

        var end_date = $('#end_date')
        end_date.datetimepicker(dateOption);
        end_date.val(date_str);
    });

    /*--------------------------------------------------------------------------------------------------*/

    var tmSelectOption = {
        maxItems : 1,
        valueField : 'id',
        labelField : 'name',
        searchField : ['id', 'name'],
        options : $scope.model.ref.list_tm
    };
    var selectTm = $('#select-tm').selectize(tmSelectOption);
    $scope.model.vars.tm = selectTm[0].selectize;
    $scope.model.vars.tm.setValue($scope.model.ref.list_tm[0].id);
    $('#select-tm').val($scope.model.vars.tm.getValue());
    $scope.model.vars.tm.on('change', function(value){
        $('#select-tm').val(value);
    });

    if($scope.model.ref.list_hub) {

        if($scope.model.ref.level == 0){
            $scope.model.ref.list_hub = CommonAssess.listZoneHierarchyLv2($scope.model.ref.list_hub, jsVars.list_admin_full, 1, false, null);
        }else{
            $scope.model.ref.list_hub = CommonAssess.listZoneHierarchyLv2($scope.model.ref.list_hub, jsVars.list_admin_full, 2, false, null);
        }

        if($scope.model.ref.level == 2){
            if($scope.model.ref.list_hub.length == 2){
                $scope.model.ref.list_hub.shift();
            }
        }

        var hubSelectOption = {
            maxItems: 1,
            valueField: 'id',
            labelField: 'title',
            searchField: ['id', 'title'],
            sortField: [{field: 'order', direction: 'asc'}],
            options: $scope.model.ref.list_hub
        };
        if($('#select-hub').length) {
            var selectHub = $('#select-hub').selectize(hubSelectOption);
            $scope.model.vars.hub = selectHub[0].selectize;
            $scope.model.vars.hub.setValue($scope.model.ref.list_hub[0].id);
            $('#select-hub').val($scope.model.vars.hub.getValue());
            $scope.model.vars.hub.on('change', function (value) {
                $('#select-hub').val(value);
            });
            var filterHub = $('[name="input_filter_hub"]');
        }

        if($('#select-hub-detail').length) {
            var selectHubDetail = $('#select-hub-detail').selectize(hubSelectOption);
            $scope.model.vars.hubDetail = selectHubDetail[0].selectize;
            $scope.model.vars.hubDetail.setValue($scope.model.ref.list_hub[0].id);
            $('#select-hub-detail').val($scope.model.vars.hubDetail.getValue());
            $scope.model.vars.hubDetail.on('change', function (value) {
                $('#select-hub-detail').val(value);
            });
            var filterHubDetail = $('[name="input_filter_hub_detail"]');
        }
    }
    
    var rangeSelectOption = {
        maxItems : 1,
        valueField : 'value',
        labelField : 'title',
        searchField : ['value', 'title'],
        options : $scope.model.ref.range_type
    };

    var range_type = $('#range_type').selectize(rangeSelectOption);
    $scope.model.vars.range_type = range_type[0].selectize;
    $scope.model.vars.range_type.setValue($scope.model.ref.range_type[0].value);
    $scope.model.vars.range_type.on('change', function(data){
        if(data == 'custom'){
            $scope.model.vars.isCustom = true;
        }else{
            $scope.model.vars.isCustom = false;
        }
        if(!$scope.$$phase) {
            $scope.$apply();
        }
    });

    var filterTm = $('[name="input_filter_tm"]');
    var filterType = $('[name="filter_type"]');

    var exportForm = $(".export-form");

    $scope.submitFormFilter = function(){
        $scope.model.act.submitFormFilter();
    }
});
