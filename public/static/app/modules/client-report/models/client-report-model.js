'use strict';

/* Services */
var ClientReportModel=angular.module('ClientReportModel',[]);
ClientReportModel.factory('ClientReportModel',function($http, $rootScope, CommonAssess){
    var serverController = 'report';
	var csrftoken = $('[name="csrfmiddlewaretoken"]').val();

    var firstInput = $('#dia_chi_xac_nhan_lai');
    var secondInput = $('#dia_chi_xac_nhan_lai');

    var serverVarsElm = $('#js-vars');
    
    var startPoint = 0;
    var perPage = 25;

    /*--------------------------------------------------Variables--------------------------------------------*/    

    var vars = {
        isFilter : false,
        listItem : null,
        inputFilter : null,
        listPercentText : null,
        listItemFull : null,
        listItemFilter : null,
        displayLoading : false,
        errorMessage : null,
        isEdit : false,
        listItemId : null,
        startPoint : startPoint,
        perPage : perPage,
        serverController : serverController,
        csrftoken : csrftoken,
        isCustom: false,
        filter_type: 'tm'
    };

    /*--------------------------------------------------Addition APIs-----------------------------------------*/

    var api = CommonAssess.api;
    api.getReport = function(controller, csrftoken, params, reportType){
        return $.ajax({
            url : $rootScope.baseUrl + controller + "/export/" + reportType + '_filter',
            type : 'POST',
            headers : {'X-CSRFToken' : csrftoken},
            processData : false,
            contentType : false,
            data : params
        });
    };
	var model = {
        api : api,

		vars : vars,

        fields : {},

        view : {},

        ref : {
            range_type : [
                {value: 'day', title: 'Day'},
                {value: 'week', title: 'Week'},
                {value: 'month', title: 'Month'},
                {value: 'year', title: 'Year'},
                {value: 'custom', title: 'Custom'}
            ],
            list_gt_pk_khac: [
                //{id: 'null', title: '----- Please select one -----'},
                {id: 'full', title: 'Full'},
                {id: 'la', title: 'LA'},
            ]
        },

        act : {
            updateListItem : function(listItemFullInput){
                updateListItem(listItemFullInput);
            },

            openFormEdit : function(id){
                openFormEdit(id);
            },

            openFormView : function(id){
                openFormView(id);
            },

            closeFormEdit : function(){
                closeFormEdit();
            },

            closeFormView : function(){
                closeFormView();
            },

            updateListCompany: function(){
                updateListCompany();
            },

            setDisplayLoading : function(isDisplay){
                setDisplayLoading(isDisplay);
            },

            submitFormEdit : function(isContinue){
                submitFormEdit(isContinue);
            },

            submitFormFilter : function(){
                submitFormFilter();
            },

            afterSubmit : function(result, action){
                afterSubmit(result, action);
            },

            emptyModelFieldsVariables : function(){
                emptyModelFieldsVariables();
            },

            setModelFieldsVariables : function(item){
                setModelFieldsVariables(item);
            }
        }
	} 

    /*-----------------------------------------------Init UI----------------------------------------------*/

    var modal = $.remodal.lookup[$('[data-remodal-id = modal]').data('remodal')];
    var modalView = $.remodal.lookup[$('[data-remodal-id = modalView]').data('remodal')];

    $(document).on('opened', '.remodal', function () {
        if(!model.vars.isEdit){
            setTimeout(function(){
                firstInput.focus();
            }, 0);
        }else{
            setTimeout(function(){
                secondInput.focus();
            }, 0);
        }
    });

    /*---------------------------------------------Controller functions---------------------------------------------*/

    var updateListItem = function(listItemFullInput){
        if(!listItemFullInput) return;
        var maxPoint = listItemFullInput.length;
        model.vars.listItem = listItemFullInput;
    }

    var updateListCompany = function(){
        var list_company = [];
        for(var i in model.ref.list_company){
            var item = model.ref.list_company[i];
            if(model.ref.list_reported_company.indexOf(parseInt(item.id)) == -1){
                list_company.push(item);
            }
        }
        model.ref.list_company = list_company;
        //model.ref.list_company;
        //model.ref.list_reported_company;
    }

    var openFormEdit = function(id){
        emptyModelFieldsVariables();

        setDisplayLoading(false);
        if(id){
            CommonAssess.showWaitingHeader(true);
            model.fields.cty.disable();
            model.fields.id = id;
            model.vars.isEdit = true;

            var params = new FormData();
            params.append('id', id);
            model.api.getSingle(serverController, csrftoken, params).done(function(result){
                //console.log(result)
                setModelFieldsVariables(result.data);
                CommonAssess.showWaitingHeader(false);
                modal.open();
                $rootScope.$apply();
            });
        }else{
            model.fields.cty.enable();
            model.vars.isEdit = false;
            modal.open();
        }
        console.log(model.vars.isEdit);
    };

    var openFormView = function(id){
        CommonAssess.showWaitingHeader(true);

        var params = new FormData();
        params.append('id', id);
        if(model.vars.is_log){
            params.append('is_log', 1);
        }
        model.api.getSingle(serverController, csrftoken, params).done(function(result){
            //console.log(result)
            setViewData(result.data);
            CommonAssess.showWaitingHeader(false);
            modalView.open();
            $rootScope.$apply();
        });
    };

    var closeFormEdit = function(){
        model.vars.floatPanel.hide();
        modal.close();
    };

    var closeFormView = function(){
        modalView.close();
    };

    var submitFormEdit = function(isContinue){
        setDisplayLoading(true);

        var params = new FormData();

        params.append('kq_cuoc_goi', model.fields.kq_cuoc_goi.getValue());
        params.append('kq_cuoc_gap', model.fields.kq_cuoc_gap.getValue());
		params.append('sp_tiep_can', model.fields.sp_tiep_can.getValue());
        params.append('tinh_trang_khai_thac', model.fields.tinh_trang_khai_thac.getValue());
        params.append('cty', model.fields.cty.getValue());

        params.append('gt_pk_khac', model.fields.gt_pk_khac.getValue());

        params.append('ngay_goi_dien_gan_nhat', model.fields.ngay_goi_dien_gan_nhat.val());
        params.append('ngay_hen', model.fields.ngay_hen.val());
        params.append('ngay_tiep_can_1', model.fields.ngay_tiep_can_1.val());
        params.append('ngay_tiep_can_2', model.fields.ngay_tiep_can_2.val());
        params.append('ngay_tiep_can_3', model.fields.ngay_tiep_can_3.val());
        params.append('ngay_tiep_can_4', model.fields.ngay_tiep_can_4.val());
        params.append('ngay_kh_dong_y_nop_hs', model.fields.ngay_kh_dong_y_nop_hs.val());
        params.append('ngay_check_prescreen', model.fields.ngay_check_prescreen.val());
        params.append('ngay_bao_kh_thu_thap_hs', model.fields.ngay_bao_kh_thu_thap_hs.val());
        params.append('ngay_thu_thap_xong_hs', model.fields.ngay_thu_thap_xong_hs.val());
        params.append('ngay_risk_yeu_cau_bo_sung', model.fields.ngay_risk_yeu_cau_bo_sung.val());
        params.append('ngay_risk_xac_nhan', model.fields.ngay_risk_xac_nhan.val());
        params.append('ngay_ci_bat_dau_dieu_tra', model.fields.ngay_ci_bat_dau_dieu_tra.val());
        params.append('ngay_ci_co_bao_cao', model.fields.ngay_ci_co_bao_cao.val());
        params.append('ngay_ca_phan_tich', model.fields.ngay_ca_phan_tich.val());
        params.append('ngay_ca_hoan_thanh_hs', model.fields.ngay_ca_hoan_thanh_hs.val());
        params.append('ngay_cm_nhan_du_hs', model.fields.ngay_cm_nhan_du_hs.val());
        params.append('ngay_phe_duyet', model.fields.ngay_phe_duyet.val());
        params.append('ngay_thong_bao_phe_duyet', model.fields.ngay_thong_bao_phe_duyet.val());
        params.append('ngay_docdis_chuan_bi_hd', model.fields.ngay_docdis_chuan_bi_hd.val());
        params.append('ngay_docdis_chuan_bi_hd_xong', model.fields.ngay_docdis_chuan_bi_hd_xong.val());
        params.append('ngay_chuyen_hd_cho_kh', model.fields.ngay_chuyen_hd_cho_kh.val());
        params.append('ngay_kh_ky_hd', model.fields.ngay_kh_ky_hd.val());
        params.append('ngay_msb_ky_hd_phe_duyet', model.fields.ngay_msb_ky_hd_phe_duyet.val());
        params.append('ngay_kh_yeu_cau_giai_ngan', model.fields.ngay_kh_yeu_cau_giai_ngan.val());
        params.append('ngay_kh_giai_ngan_thanh_cong', model.fields.ngay_kh_giai_ngan_thanh_cong.val());
        params.append('ngay_mo_han_muc_tren_he_thong', model.fields.ngay_mo_han_muc_tren_he_thong.val());

        params.append('dia_chi_xac_nhan_lai', model.fields.dia_chi_xac_nhan_lai);
        params.append('nguoi_lien_lac', model.fields.nguoi_lien_lac);
        params.append('phone', model.fields.phone);
        params.append('ghi_chu', model.fields.ghi_chu);

        if(model.vars.isEdit){
            params.append('id', model.fields.id);
            model.api.editItem(serverController, csrftoken, params).done(function(result){
                afterSubmit(result, 'edit', isContinue);
            });
        }else{
            model.api.addItem(serverController, csrftoken, params).done(function(result){
                afterSubmit(result, 'add', isContinue);
            });
        }
    };

    var submitFormFilter = function(){
        var params = new FormData();
        var input_filter_hub = '';
        params.append('filter_type', model.vars.filter_type);
        params.append('input_filter_tm', model.vars.tm.getValue());

        if($('#select-hub').length) {
            input_filter_hub = model.vars.hub.getValue()
        }
        params.append('input_filter_hub', input_filter_hub);

        params.append('range_type', model.vars.range_type.getValue());
        params.append('start_date', $('#start_date').val());
        params.append('end_date', $('#end_date').val());

        model.vars.queryStr = 'filter_type=' + model.vars.filter_type + '&' +
        'input_filter_tm=' + model.vars.tm.getValue() + '&' +
        'input_filter_hub=' + input_filter_hub + '&' +
        'range_type=' + model.vars.range_type.getValue() + '&' +
        'start_date=' + $('#start_date').val() + '&' +
        'end_date=' + $('#end_date').val();
        model.vars.pdfUrl = $rootScope.baseUrl + serverController + "/export/" + model.vars.report_type + '_filter';
        console.log(model.vars.pdfUrl);
        model.vars.pdfUrl += "?"+model.vars.queryStr;
        setDisplayLoading(true);
        model.api.getReport(serverController, csrftoken, params, model.vars.report_type).done(function(result){
            setDisplayLoading(false);
            if(result.data){
                model.vars.listReport = result.data.report;
                model.vars.listReportTotal = result.data.report_total;
                model.vars.listReportTitle = result.data.report_title;
                $rootScope.$apply();
            }
        }).fail(function(error, message){
            setDisplayLoading(false);
            $rootScope.$apply();
        });
    }

    var afterSubmit = function(result, action, isContinue){
        if(!result.error){
            var newItem = {
                id : result.data.id.toString(),
                user : result.data.user,
                zone_id : result.data.zone_id,
                cty_name : result.data.cty_name ? result.data.cty_name : model.fields.cty.getValue(),
                sp_tiep_can : result.data.sp_tiep_can ? result.data.sp_tiep_can : model.fields.sp_tiep_can.getValue(),
                kq_cuoc_goi : result.data.kq_cuoc_goi ? result.data.kq_cuoc_goi : model.fields.kq_cuoc_goi.getValue(),
                kq_cuoc_gap : result.data.kq_cuoc_gap ? result.data.kq_cuoc_gap : model.fields.kq_cuoc_gap.getValue(),
                tinh_trang_khai_thac : result.data.tinh_trang_khai_thac ? result.data.tinh_trang_khai_thac : model.fields.tinh_trang_khai_thac.getValue(),
                commit : result.data.commit
            };
            switch(action){
                case 'edit':
					isContinue = false;
                    var index = CommonAssess.getIndexFromId(model.vars.listItemFull, newItem.id);
                    model.vars.listItemFull[index]=newItem;
                    model.vars.floatPanel.hide();
                break;

                case 'add':
                    model.vars.listItemFull.unshift(newItem);
                    startPoint = 0;
                    model.vars.floatPanel.hide();
                    location.reload();
                break;

                case 'remove':
					isContinue = false;
                    var listId = newItem.id.split('|');
                    for(var i in listId){
                        var index = CommonAssess.getIndexFromId(model.vars.listItemFull, listId[i]);
                        model.vars.listItemFull.splice(index,1);   
                    }
                    startPoint = 0;
                    location.reload();
                break;
            }
            if(isContinue){
                emptyModelFieldsVariables();
                firstInput.focus();
            }else{
                closeFormEdit();    
            }
        }else{
            if(action == 'remove'){
                alert(result.error);
            }else {
                model.vars.errorMessage = CommonAssess.processErrorMessage(result.error);
            }

        }
		setDisplayLoading(false);
        $rootScope.$apply();
    };

    var setDisplayLoading = function(isDisplay){
        model.vars.displayLoading = isDisplay;
    };

    var emptyModelFieldsVariables = function(){
        model.vars.errorMessage = CommonAssess.processErrorMessage(null);

        model.fields.kq_cuoc_goi.setValue(model.ref.list_ket_qua_cuoc_goi[0].id);
        model.fields.kq_cuoc_gap.setValue(model.ref.list_ket_qua_cuoc_gap[0].id);
        model.fields.sp_tiep_can.setValue(model.ref.list_sp_tiep_can[0].id);
        model.fields.tinh_trang_khai_thac.setValue(model.ref.list_tinh_trang_khai_thac[0].id);
        if(model.ref.list_company[0]){
            model.fields.cty.setValue(model.ref.list_company[0].id);
        }
        model.fields.gt_pk_khac.setValue(model.ref.list_gt_pk_khac[0].id);

        model.fields.ngay_goi_dien_gan_nhat.val("");
        model.fields.ngay_hen.val("");
        model.fields.ngay_tiep_can_1.val("");
        model.fields.ngay_tiep_can_2.val("");
        model.fields.ngay_tiep_can_3.val("");
        model.fields.ngay_tiep_can_4.val("");
        model.fields.ngay_kh_dong_y_nop_hs.val("");
        model.fields.ngay_check_prescreen.val("");
        model.fields.ngay_bao_kh_thu_thap_hs.val("");
        model.fields.ngay_thu_thap_xong_hs.val("");
        model.fields.ngay_risk_yeu_cau_bo_sung.val("");
        model.fields.ngay_risk_xac_nhan.val("");
        model.fields.ngay_ci_bat_dau_dieu_tra.val("");
        model.fields.ngay_ci_co_bao_cao.val("");
        model.fields.ngay_ca_phan_tich.val("");
        model.fields.ngay_ca_hoan_thanh_hs.val("");
        model.fields.ngay_cm_nhan_du_hs.val("");
        model.fields.ngay_phe_duyet.val("");
        model.fields.ngay_thong_bao_phe_duyet.val("");
        model.fields.ngay_docdis_chuan_bi_hd.val("");
        model.fields.ngay_docdis_chuan_bi_hd_xong.val("");
        model.fields.ngay_chuyen_hd_cho_kh.val("");
        model.fields.ngay_kh_ky_hd.val("");
        model.fields.ngay_msb_ky_hd_phe_duyet.val("");
        model.fields.ngay_kh_yeu_cau_giai_ngan.val("");
        model.fields.ngay_kh_giai_ngan_thanh_cong.val("");
        model.fields.ngay_mo_han_muc_tren_he_thong.val("");

        model.fields.dia_chi_xac_nhan_lai = "";
        model.fields.nguoi_lien_lac = "";
        model.fields.phone = "";
        model.fields.ghi_chu = "";
    };

    var setModelFieldsVariables = function(item){
        model.vars.errorMessage = CommonAssess.processErrorMessage(null);
        model.fields.kq_cuoc_goi.setValue(item.kq_cuoc_goi);
        model.fields.kq_cuoc_gap.setValue(item.kq_cuoc_gap==null?0:item.kq_cuoc_gap);
        model.fields.sp_tiep_can.setValue(item.sp_tiep_can);
        model.fields.tinh_trang_khai_thac.setValue(item.tinh_trang_khai_thac==null?0:item.tinh_trang_khai_thac);
        model.fields.cty.setValue(item.cty_id);
        model.fields.cty_name = item.cty_name;

        model.fields.gt_pk_khac.setValue(item.gt_pk_khac==null?0:item.gt_pk_khac);

        model.fields.ngay_goi_dien_gan_nhat.val(item.ngay_goi_dien_gan_nhat);
        model.fields.ngay_hen.val(CommonAssess.djangoDateTimeTostr(item.ngay_hen));
        model.fields.ngay_tiep_can_1.val(item.ngay_tiep_can_1);
        model.fields.ngay_tiep_can_2.val(item.ngay_tiep_can_2);
        model.fields.ngay_tiep_can_3.val(item.ngay_tiep_can_3);
        model.fields.ngay_tiep_can_4.val(item.ngay_tiep_can_4);
        model.fields.ngay_kh_dong_y_nop_hs.val(item.ngay_kh_dong_y_nop_hs);
        model.fields.ngay_check_prescreen.val(item.ngay_check_prescreen);
        model.fields.ngay_bao_kh_thu_thap_hs.val(item.ngay_bao_kh_thu_thap_hs);
        model.fields.ngay_thu_thap_xong_hs.val(item.ngay_thu_thap_xong_hs);
        model.fields.ngay_risk_yeu_cau_bo_sung.val(item.ngay_risk_yeu_cau_bo_sung);
        model.fields.ngay_risk_xac_nhan.val(item.ngay_risk_xac_nhan);
        model.fields.ngay_ci_bat_dau_dieu_tra.val(item.ngay_ci_bat_dau_dieu_tra);
        model.fields.ngay_ci_co_bao_cao.val(item.ngay_ci_co_bao_cao);
        model.fields.ngay_ca_phan_tich.val(item.ngay_ca_phan_tich);
        model.fields.ngay_ca_hoan_thanh_hs.val(item.ngay_ca_hoan_thanh_hs);
        model.fields.ngay_cm_nhan_du_hs.val(item.ngay_cm_nhan_du_hs);
        model.fields.ngay_phe_duyet.val(item.ngay_phe_duyet);
        model.fields.ngay_thong_bao_phe_duyet.val(item.ngay_thong_bao_phe_duyet);
        model.fields.ngay_docdis_chuan_bi_hd.val(item.ngay_docdis_chuan_bi_hd);
        model.fields.ngay_docdis_chuan_bi_hd_xong.val(item.ngay_docdis_chuan_bi_hd_xong);
        model.fields.ngay_chuyen_hd_cho_kh.val(item.ngay_chuyen_hd_cho_kh);
        model.fields.ngay_kh_ky_hd.val(item.ngay_kh_ky_hd);
        model.fields.ngay_msb_ky_hd_phe_duyet.val(item.ngay_msb_ky_hd_phe_duyet);
        model.fields.ngay_kh_yeu_cau_giai_ngan.val(item.ngay_kh_yeu_cau_giai_ngan);
        model.fields.ngay_kh_giai_ngan_thanh_cong.val(item.ngay_kh_giai_ngan_thanh_cong);
        model.fields.ngay_mo_han_muc_tren_he_thong.val(item.ngay_mo_han_muc_tren_he_thong);

        model.fields.dia_chi_xac_nhan_lai = item.dia_chi_xac_nhan_lai;
        model.fields.nguoi_lien_lac = item.nguoi_lien_lac;
        model.fields.phone = item.phone;
        model.fields.ghi_chu = item.ghi_chu;
    };

    var setViewData = function(item){
        model.view.kq_cuoc_goi = item.kq_cuoc_goi;
        model.view.kq_cuoc_gap = item.kq_cuoc_gap;
        model.view.sp_tiep_can = item.sp_tiep_can;
        model.view.tinh_trang_khai_thac = item.tinh_trang_khai_thac;
        model.view.cty_name = item.cty_name;

        model.view.ngay_goi_dien_gan_nhat = item.ngay_goi_dien_gan_nhat;
        model.view.ngay_hen = CommonAssess.djangoDateTimeTostr(item.ngay_hen);
        model.view.ngay_tiep_can_1 = item.ngay_tiep_can_1;
        model.view.ngay_tiep_can_2 = item.ngay_tiep_can_2;
        model.view.ngay_tiep_can_3 = item.ngay_tiep_can_3;
        model.view.ngay_tiep_can_4 = item.ngay_tiep_can_4;
        model.view.ngay_kh_dong_y_nop_hs = item.ngay_kh_dong_y_nop_hs;
        model.view.ngay_check_prescreen = item.ngay_check_prescreen;
        model.view.ngay_bao_kh_thu_thap_hs = item.ngay_bao_kh_thu_thap_hs;
        model.view.ngay_thu_thap_xong_hs = item.ngay_thu_thap_xong_hs;
        model.view.ngay_risk_yeu_cau_bo_sung = item.ngay_risk_yeu_cau_bo_sung;
        model.view.ngay_risk_xac_nhan = item.ngay_risk_xac_nhan;
        model.view.ngay_ci_bat_dau_dieu_tra = item.ngay_ci_bat_dau_dieu_tra;
        model.view.ngay_ci_co_bao_cao = item.ngay_ci_co_bao_cao;
        model.view.ngay_ca_phan_tich = item.ngay_ca_phan_tich;
        model.view.ngay_ca_hoan_thanh_hs = item.ngay_ca_hoan_thanh_hs;
        model.view.ngay_cm_nhan_du_hs = item.ngay_cm_nhan_du_hs;
        model.view.ngay_phe_duyet = item.ngay_phe_duyet;
        model.view.ngay_thong_bao_phe_duyet = item.ngay_thong_bao_phe_duyet;
        model.view.ngay_docdis_chuan_bi_hd = item.ngay_docdis_chuan_bi_hd;
        model.view.ngay_docdis_chuan_bi_hd_xong = item.ngay_docdis_chuan_bi_hd_xong;
        model.view.ngay_chuyen_hd_cho_kh = item.ngay_chuyen_hd_cho_kh;
        model.view.ngay_kh_ky_hd = item.ngay_kh_ky_hd;
        model.view.ngay_msb_ky_hd_phe_duyet = item.ngay_msb_ky_hd_phe_duyet;
        model.view.ngay_kh_yeu_cau_giai_ngan = item.ngay_kh_yeu_cau_giai_ngan;
        model.view.ngay_kh_giai_ngan_thanh_cong = item.ngay_kh_giai_ngan_thanh_cong;
        model.view.ngay_mo_han_muc_tren_he_thong = item.ngay_mo_han_muc_tren_he_thong;

        model.view.dia_chi_xac_nhan_lai = item.dia_chi_xac_nhan_lai;
        model.view.nguoi_lien_lac = item.nguoi_lien_lac;
        model.view.phone = item.phone;
        model.view.ghi_chu = item.ghi_chu;
    }

    return model;  
});
