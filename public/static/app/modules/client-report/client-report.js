'use strict';

/* Controllers */
App.controller("ClientReport",function($scope, $filter, ClientReportModel, CommonAssess){
	$scope.model = ClientReportModel;

	var serverVarsElm = $('#js-vars');
	var currentPage = 1;
	var maxRecords = 500;
	var init = true;

	var jsVars = serverVarsElm.val()?JSON.parse(serverVarsElm.val()):null;

	$scope.model.vars.reach_bottom = false;
	$scope.model.vars.isBusy = false;
	$scope.model.vars.isFilter = false;
	$scope.model.vars.listItemFull = jsVars.list_item;
	$scope.model.vars.listItemFilter = null;

    $scope.init = function(is_log){
        $scope.model.vars.is_log = is_log;
    }
    //console.log($scope.model.vars.listItemFull);
    $scope.model.vars.conditions = jsVars.conditions;

    console.log($scope.model.vars.conditions);

    $scope.model.ref.list_ket_qua_cuoc_goi = jsVars.list_ket_qua_cuoc_goi;
    $scope.model.ref.list_ket_qua_cuoc_goi_ref = CommonAssess.getRef($scope.model.ref.list_ket_qua_cuoc_goi, 'id', 'title');

    $scope.model.ref.list_ket_qua_cuoc_gap = jsVars.list_ket_qua_cuoc_gap;
    $scope.model.ref.list_ket_qua_cuoc_gap_ref = CommonAssess.getRef($scope.model.ref.list_ket_qua_cuoc_gap, 'id', 'title');
    $scope.model.ref.list_ket_qua_cuoc_gap.unshift({id: 0, title: '----- Please select one -----'});

    $scope.model.ref.list_sp_tiep_can = jsVars.list_sp_tiep_can;
    $scope.model.ref.list_sp_tiep_can_ref = CommonAssess.getRef($scope.model.ref.list_sp_tiep_can, 'id', 'title');

    $scope.model.ref.list_tinh_trang_khai_thac = jsVars.list_tinh_trang_khai_thac;
    $scope.model.ref.list_tinh_trang_khai_thac_ref = CommonAssess.getRef($scope.model.ref.list_tinh_trang_khai_thac, 'id', 'title');
    $scope.model.ref.list_tinh_trang_khai_thac.unshift({id: 0, title: '----- Please select one -----'});

    $scope.model.ref.list_zone = jsVars.list_zone;
    $scope.model.ref.list_zone_ref = CommonAssess.getRef($scope.model.ref.list_zone, 'id', 'title');

    $scope.model.ref.list_user = jsVars.list_user;
    $scope.model.ref.list_user_ref = CommonAssess.getRef($scope.model.ref.list_user, 'id', 'uid');

    $scope.model.ref.list_company = jsVars.list_company;
    $scope.model.ref.list_reported_company = jsVars.list_reported_company;

    $scope.model.ref.list_gt_pk_khac.unshift({id: 0, title: '----- Please select one -----'});

    //console.log($scope.model.ref.list_zone_ref);
    //console.log($scope.model.ref.list_user_ref);
    /*--------------------------------------------------------------------------------------------------*/

    $scope.model.vars.sp_tiep_can_filter_condition = jsVars.sp_tiep_can_condition;
    $scope.model.vars.kq_cuoc_goi_filter_condition = jsVars.kq_cuoc_goi_condition;
    $scope.model.vars.kq_cuoc_gap_filter_condition = jsVars.kq_cuoc_gap_condition;
    $scope.model.vars.tt_khai_thac_filter_condition = jsVars.tt_khai_thac_condition;

    $scope.model.ref.list_ket_qua_cuoc_goi_filter = jsVars.list_ket_qua_cuoc_goi_filter;
    $scope.model.ref.list_ket_qua_cuoc_goi_ref_filter = CommonAssess.getRef($scope.model.ref.list_ket_qua_cuoc_goi_filter, 'id', 'title');
    $scope.model.ref.list_ket_qua_cuoc_goi_filter.unshift({id: -1, title: '-- KQ cuộc gọi --'});

    $scope.model.ref.list_ket_qua_cuoc_gap_filter = jsVars.list_ket_qua_cuoc_gap_filter;
    $scope.model.ref.list_ket_qua_cuoc_gap_ref_filter = CommonAssess.getRef($scope.model.ref.list_ket_qua_cuoc_gap_filter, 'id', 'title');
    $scope.model.ref.list_ket_qua_cuoc_gap_filter.unshift({id: -1, title: '-- KQ cuộc gặp --'});

    $scope.model.ref.list_sp_tiep_can_filter = jsVars.list_sp_tiep_can_filter;
    $scope.model.ref.list_sp_tiep_can_ref_filter = CommonAssess.getRef($scope.model.ref.list_sp_tiep_can_filter, 'id', 'title');
    $scope.model.ref.list_sp_tiep_can_filter.unshift({id: -1, title: '-- SP tiếp cận --'});

    $scope.model.ref.list_tinh_trang_khai_thac_filter = jsVars.list_tinh_trang_khai_thac_filter;
    $scope.model.ref.list_tinh_trang_khai_thac_ref_filter = CommonAssess.getRef($scope.model.ref.list_tinh_trang_khai_thac_filter, 'id', 'title');
    $scope.model.ref.list_tinh_trang_khai_thac_filter.unshift({id: -1, title: '-- TT khai thác --'});

    /*--------------------------------------------------------------------------------------------------*/

    $scope.model.fields.ngay_goi_dien_gan_nhat = $('#ngay_goi_dien_gan_nhat')
    $scope.model.fields.ngay_goi_dien_gan_nhat.datetimepicker(CommonAssess.datePickerOption);

    $scope.model.fields.ngay_hen = $('#ngay_hen');
    $scope.model.fields.ngay_hen.datetimepicker(CommonAssess.dateTimePickerOption);

    $scope.model.fields.ngay_tiep_can_1 = $('#ngay_tiep_can_1');
    $scope.model.fields.ngay_tiep_can_1.datetimepicker(CommonAssess.datePickerOption);

    $scope.model.fields.ngay_tiep_can_2 = $('#ngay_tiep_can_2');
    $scope.model.fields.ngay_tiep_can_2.datetimepicker(CommonAssess.datePickerOption);

    $scope.model.fields.ngay_tiep_can_3 = $('#ngay_tiep_can_3');
    $scope.model.fields.ngay_tiep_can_3.datetimepicker(CommonAssess.datePickerOption);

    $scope.model.fields.ngay_tiep_can_4 = $('#ngay_tiep_can_4');
    $scope.model.fields.ngay_tiep_can_4.datetimepicker(CommonAssess.datePickerOption);

    $scope.model.fields.ngay_kh_dong_y_nop_hs = $('#ngay_kh_dong_y_nop_hs');
    $scope.model.fields.ngay_kh_dong_y_nop_hs.datetimepicker(CommonAssess.datePickerOption);

    $scope.model.fields.ngay_check_prescreen = $('#ngay_check_prescreen');
    $scope.model.fields.ngay_check_prescreen.datetimepicker(CommonAssess.datePickerOption);

    $scope.model.fields.ngay_bao_kh_thu_thap_hs = $('#ngay_bao_kh_thu_thap_hs');
    $scope.model.fields.ngay_bao_kh_thu_thap_hs.datetimepicker(CommonAssess.datePickerOption);

    $scope.model.fields.ngay_thu_thap_xong_hs = $('#ngay_thu_thap_xong_hs');
    $scope.model.fields.ngay_thu_thap_xong_hs.datetimepicker(CommonAssess.datePickerOption);

    $scope.model.fields.ngay_risk_yeu_cau_bo_sung = $('#ngay_risk_yeu_cau_bo_sung');
    $scope.model.fields.ngay_risk_yeu_cau_bo_sung.datetimepicker(CommonAssess.datePickerOption);

    $scope.model.fields.ngay_risk_xac_nhan = $('#ngay_risk_xac_nhan');
    $scope.model.fields.ngay_risk_xac_nhan.datetimepicker(CommonAssess.datePickerOption);

    $scope.model.fields.ngay_ci_bat_dau_dieu_tra = $('#ngay_ci_bat_dau_dieu_tra');
    $scope.model.fields.ngay_ci_bat_dau_dieu_tra.datetimepicker(CommonAssess.datePickerOption);

    $scope.model.fields.ngay_ci_co_bao_cao = $('#ngay_ci_co_bao_cao');
    $scope.model.fields.ngay_ci_co_bao_cao.datetimepicker(CommonAssess.datePickerOption);

    $scope.model.fields.ngay_ca_phan_tich = $('#ngay_ca_phan_tich');
    $scope.model.fields.ngay_ca_phan_tich.datetimepicker(CommonAssess.datePickerOption);

    $scope.model.fields.ngay_ca_hoan_thanh_hs = $('#ngay_ca_hoan_thanh_hs');
    $scope.model.fields.ngay_ca_hoan_thanh_hs.datetimepicker(CommonAssess.datePickerOption);

    $scope.model.fields.ngay_cm_nhan_du_hs = $('#ngay_cm_nhan_du_hs');
    $scope.model.fields.ngay_cm_nhan_du_hs.datetimepicker(CommonAssess.datePickerOption);

    $scope.model.fields.ngay_phe_duyet = $('#ngay_phe_duyet');
    $scope.model.fields.ngay_phe_duyet.datetimepicker(CommonAssess.datePickerOption);

    $scope.model.fields.ngay_thong_bao_phe_duyet = $('#ngay_thong_bao_phe_duyet');
    $scope.model.fields.ngay_thong_bao_phe_duyet.datetimepicker(CommonAssess.datePickerOption);

    $scope.model.fields.ngay_docdis_chuan_bi_hd = $('#ngay_docdis_chuan_bi_hd');
    $scope.model.fields.ngay_docdis_chuan_bi_hd.datetimepicker(CommonAssess.datePickerOption);

    $scope.model.fields.ngay_docdis_chuan_bi_hd_xong = $('#ngay_docdis_chuan_bi_hd_xong');
    $scope.model.fields.ngay_docdis_chuan_bi_hd_xong.datetimepicker(CommonAssess.datePickerOption);

    $scope.model.fields.ngay_chuyen_hd_cho_kh = $('#ngay_chuyen_hd_cho_kh');
    $scope.model.fields.ngay_chuyen_hd_cho_kh.datetimepicker(CommonAssess.datePickerOption);

    $scope.model.fields.ngay_kh_ky_hd = $('#ngay_kh_ky_hd');
    $scope.model.fields.ngay_kh_ky_hd.datetimepicker(CommonAssess.datePickerOption);

    $scope.model.fields.ngay_msb_ky_hd_phe_duyet = $('#ngay_msb_ky_hd_phe_duyet');
    $scope.model.fields.ngay_msb_ky_hd_phe_duyet.datetimepicker(CommonAssess.datePickerOption);

    $scope.model.fields.ngay_kh_yeu_cau_giai_ngan = $('#ngay_kh_yeu_cau_giai_ngan');
    $scope.model.fields.ngay_kh_yeu_cau_giai_ngan.datetimepicker(CommonAssess.datePickerOption);

    $scope.model.fields.ngay_kh_giai_ngan_thanh_cong = $('#ngay_kh_giai_ngan_thanh_cong');
    $scope.model.fields.ngay_kh_giai_ngan_thanh_cong.datetimepicker(CommonAssess.datePickerOption);

    $scope.model.fields.ngay_mo_han_muc_tren_he_thong = $('#ngay_mo_han_muc_tren_he_thong');
    $scope.model.fields.ngay_mo_han_muc_tren_he_thong.datetimepicker(CommonAssess.datePickerOption);

    /*--------------------------------------------------------------------------------------------------*/

    var spTiepCanFilterSelectOption = {
        maxItems : 1,
        valueField : 'id',
        labelField : 'title',
        searchField : ['id', 'title'],
        sortField: [{field: 'id', direction: 'asc'}],
        options : $scope.model.ref.list_sp_tiep_can_filter
    };
    var selectSpTiepCanFilter = $('#select-sp_tiep_can_filter').selectize(spTiepCanFilterSelectOption);
    $scope.model.fields.sp_tiep_can_filter = selectSpTiepCanFilter[0].selectize;
    if(!$scope.model.vars.sp_tiep_can_filter_condition){
        $scope.model.fields.sp_tiep_can_filter.setValue($scope.model.ref.list_sp_tiep_can_filter[0].id);
    }else{
        $scope.model.fields.sp_tiep_can_filter.setValue($scope.model.vars.sp_tiep_can_filter_condition);
    }
    var value = $scope.model.fields.sp_tiep_can_filter.getValue();
    if(value != -1){
        $("#sp_tiep_can").val(value);
    }else{
        $("#sp_tiep_can").val(null);
    }
    $scope.model.fields.sp_tiep_can_filter.on('change', function(value){
        if(value != -1){
            $("#sp_tiep_can").val(value);
        }else{
            $("#sp_tiep_can").val(null);
        }
        //console.log('sp_tiep_can_filter', value);
    });


    //console.log($scope.model.ref.list_ket_qua_cuoc_goi[0].id);
    var ketQuaCuocGoiFilterSelectOption = {
        maxItems : 1,
        valueField : 'id',
        labelField : 'title',
        searchField : ['id', 'title'],
        sortField: [{field: 'id', direction: 'asc'}],
        options : $scope.model.ref.list_ket_qua_cuoc_goi_filter
    };
    var selectKetQuaCuocGoiFilter = $('#select-kq_cuoc_goi_filter').selectize(ketQuaCuocGoiFilterSelectOption);
    $scope.model.fields.kq_cuoc_goi_filter = selectKetQuaCuocGoiFilter[0].selectize;
    if(!$scope.model.vars.kq_cuoc_goi_filter_condition){
        $scope.model.fields.kq_cuoc_goi_filter.setValue($scope.model.ref.list_ket_qua_cuoc_goi_filter[0].id);
    }else{
        console.log($scope.model.vars.kq_cuoc_goi_filter_condition);
        $scope.model.fields.kq_cuoc_goi_filter.setValue($scope.model.vars.kq_cuoc_goi_filter_condition);
    }
    var value = $scope.model.fields.kq_cuoc_goi_filter.getValue();
    if(value != -1){
        $("#kq_cuoc_goi").val(value);
    }else{
        $("#kq_cuoc_goi").val(null);
    }
    $scope.model.fields.kq_cuoc_goi_filter.on('change', function(value){
        if(value != -1){
            $("#kq_cuoc_goi").val(value);
        }else{
            $("#kq_cuoc_goi").val(null);
        }
        //console.log('kq_cuoc_goi_filter', value);
    });


    var ketQuaCuocGapFilterSelectOption = {
        maxItems : 1,
        valueField : 'id',
        labelField : 'title',
        searchField : ['id', 'title'],
        sortField: [{field: 'id', direction: 'asc'}],
        options : $scope.model.ref.list_ket_qua_cuoc_gap_filter
    };
    var selectKetQuaCuocGapFilter = $('#select-kq_cuoc_gap_filter').selectize(ketQuaCuocGapFilterSelectOption);
    $scope.model.fields.kq_cuoc_gap_filter = selectKetQuaCuocGapFilter[0].selectize;
    if(!$scope.model.vars.kq_cuoc_gap_filter_condition){
        $scope.model.fields.kq_cuoc_gap_filter.setValue($scope.model.ref.list_ket_qua_cuoc_gap_filter[0].id);
    }else{
        $scope.model.fields.kq_cuoc_gap_filter.setValue($scope.model.vars.kq_cuoc_gap_filter_condition);
    }
    var value = $scope.model.fields.kq_cuoc_gap_filter.getValue();
    if(value != -1){
        $("#kq_cuoc_gap").val(value);
    }else{
        $("#kq_cuoc_gap").val(null);
    }
    $scope.model.fields.kq_cuoc_gap_filter.on('change', function(value){
        if(value != -1){
            $("#kq_cuoc_gap").val(value);
        }else{
            $("#kq_cuoc_gap").val(null);
        }
        //console.log('kq_cuoc_gap_filter', value);
    });


    var tinhTrangKhaiThacFilterSelectOption = {
        maxItems : 1,
        valueField : 'id',
        labelField : 'title',
        searchField : ['id', 'title'],
        sortField: [{field: 'id', direction: 'asc'}],
        options : $scope.model.ref.list_tinh_trang_khai_thac_filter
    };
    var selectTinhTrangKhaiThacFilter = $('#select-tinh_trang_khai_thac_filter').selectize(tinhTrangKhaiThacFilterSelectOption);
    $scope.model.fields.tinh_trang_khai_thac_filter = selectTinhTrangKhaiThacFilter[0].selectize;
    if(!$scope.model.vars.tt_khai_thac_filter_condition){
        $scope.model.fields.tinh_trang_khai_thac_filter.setValue($scope.model.ref.list_tinh_trang_khai_thac_filter[0].id);
    }else{
        $scope.model.fields.tinh_trang_khai_thac_filter.setValue($scope.model.vars.tt_khai_thac_filter_condition);
    }
    var value = $scope.model.fields.tinh_trang_khai_thac_filter.getValue();
    if(value != -1){
        $("#tt_khai_thac").val(value);
    }else{
        $("#tt_khai_thac").val(null);
    }
    $scope.model.fields.tinh_trang_khai_thac_filter.on('change', function(value){
        if(value != -1){
            $("#tt_khai_thac").val(value);
        }else{
            $("#tt_khai_thac").val(null);
        }
        //console.log('tt_khai_thac_filter', value);
    });

    /*--------------------------------------------------------------------------------------------------*/
    var ketQuaCuocGoiSelectOption = {
        maxItems : 1,
        valueField : 'id',
        labelField : 'title',
        searchField : ['id', 'title'],
        options : $scope.model.ref.list_ket_qua_cuoc_goi
    };
    var selectKetQuaCuocGoi = $('#select-kq_cuoc_goi').selectize(ketQuaCuocGoiSelectOption);
    $scope.model.fields.kq_cuoc_goi = selectKetQuaCuocGoi[0].selectize;

    var ketQuaCuocGapSelectOption = {
        maxItems : 1,
        valueField : 'id',
        labelField : 'title',
        searchField : ['id', 'title'],
        options : $scope.model.ref.list_ket_qua_cuoc_gap
    };
    var selectKetQuaCuocGap = $('#select-kq_cuoc_gap').selectize(ketQuaCuocGapSelectOption);
    $scope.model.fields.kq_cuoc_gap = selectKetQuaCuocGap[0].selectize;

    var spTiepCanSelectOption = {
        maxItems : 1,
        valueField : 'id',
        labelField : 'title',
        searchField : ['id', 'title'],
        options : $scope.model.ref.list_sp_tiep_can
    };
    var selectSpTiepCan = $('#select-sp_tiep_can').selectize(spTiepCanSelectOption);
    $scope.model.fields.sp_tiep_can = selectSpTiepCan[0].selectize;


    var tinhTrangKhaiThacSelectOption = {
        maxItems : 1,
        valueField : 'id',
        labelField : 'title',
        searchField : ['id', 'title'],
        options : $scope.model.ref.list_tinh_trang_khai_thac
    };
    var selectTinhTrangKhaiThac = $('#select-tinh_trang_khai_thac').selectize(tinhTrangKhaiThacSelectOption);
    $scope.model.fields.tinh_trang_khai_thac = selectTinhTrangKhaiThac[0].selectize;

    $scope.model.act.updateListCompany();
    var ctySelectOption = {
        maxItems : 1,
        valueField : 'id',
        labelField : 'cty',
        searchField : ['id', 'cty'],
        options : $scope.model.ref.list_company
    };

    var selectCty = $('#select-cty').selectize(ctySelectOption);
    $scope.model.fields.cty = selectCty[0].selectize;


    var gtPkKhacSelectOption = {
        maxItems : 1,
        valueField : 'id',
        labelField : 'title',
        searchField : ['id', 'title'],
        options : $scope.model.ref.list_gt_pk_khac
    };
    var selectGtPkKhac = $('#select-gt_pk_khac').selectize(gtPkKhacSelectOption);
    $scope.model.fields.gt_pk_khac = selectGtPkKhac[0].selectize;

	/*-------------------------------------Init update list-------------------------------------*/

	$scope.model.act.updateListItem($scope.model.vars.listItemFull);	

	/*-------------------------------------Implement list filter-------------------------------------*/
	
	$scope.$watch('model.vars.inputFilter', function(newValue, oldValue) {
		currentPage = 1;
		if(newValue){
			if(!$scope.model.vars.isBusy && newValue.length >= 3){
				get_all(false);
			}
		}else{
			if(init){
				init = false
			}else{
				get_all(false);
			}
		}
	});
	
	/*-------------------------------------Controller functions-------------------------------------*/

	$(window).scroll(function() {
		if($(window).scrollTop() + $(window).height() >= $(document).height()) {
			if(!$scope.model.vars.reach_bottom && !$scope.model.vars.isBusy){
				currentPage++;
				get_all(true);
			}
		}
	});

	function get_all(is_append){
		CommonAssess.showWaitingHeader(true);
		var params = new FormData();
        
        if($scope.model.vars.inputFilter){
        	params.append('keyword', $scope.model.vars.inputFilter);
        }

        $scope.model.vars.isBusy = true;
        params.append('page', currentPage);

        if($scope.model.vars.conditions){
            for(var i in $scope.model.vars.conditions){
                var key = i;
                var value = $scope.model.vars.conditions[i];
                params.append(key, value);
                //testCondition[key] = value;
            }
        }
		$scope.model.api.getAll($scope.model.vars.serverController, $scope.model.vars.csrftoken, params).done(function(result){
			CommonAssess.showWaitingHeader(false);
			$scope.model.vars.isBusy = false;

			if(result.data){
				$scope.model.vars.reach_bottom = result.reach_bottom
				if(is_append){
					if(maxRecords < $scope.model.vars.listItemFull.length){
						$scope.model.vars.listItemFull = $scope.model.vars.listItemFull.slice(-5);
					}
					$scope.model.vars.listItemFull = $scope.model.vars.listItemFull.concat(result.data);
				}else{
					$scope.model.vars.listItemFull = result.data;
				}
				$scope.model.act.updateListItem($scope.model.vars.listItemFull);
			}
			$scope.$apply();
		});
	}

	$scope.openFormEdit = function(id){
		$scope.model.act.openFormEdit(id);
	};

    $scope.openFormView = function(id){
		$scope.model.act.openFormView(id);
	};

	$scope.openFormRemove = function(id){
		if(id){
			var idLength = id.toString().split("|").length;
			if(idLength > 1){
				$scope.formRemoveInfo = "Do you realy want to remove these (" + idLength.toString() + ") records ?";
			}else{
				$scope.formRemoveInfo = "Do you realy want to remove this records ?";
			}
			$scope.model.fields.id = id;
			var confirmRemove = confirm($scope.formRemoveInfo);
			if(confirmRemove){
				submitFormRemove();
			}
		}
	};

	function submitFormRemove(){
		CommonAssess.showWaitingHeader(true);
		var params = new FormData();
		params.append('id',$scope.model.fields.id);
		$scope.model.api.removeItem($scope.model.vars.serverController, $scope.model.vars.csrftoken, params).done(function(result){
			$scope.model.act.afterSubmit(result, 'remove');
			CommonAssess.showWaitingHeader(false);
		});
	};

	$scope.selectCheckBox = function(){
		$scope.model.vars.listItemId=getListItemId();
	};

	$scope.selectAllCheckBox = function(){
		var checkedItems = $('.selectCheckBox:checked');
		var checkBox = $(".selectCheckBox");

		if(checkedItems.length > 0 && checkedItems.length < checkBox.length){
			checkBox.prop("checked", true);
		}else if(checkedItems.length == checkBox.length){
			checkBox.prop("checked", false);
		}else{
			checkBox.prop("checked", true);
		}
		$scope.model.vars.listItemId = getListItemId();
	};

	function getListItemId(){
		var checkedItems = $('.selectCheckBox:checked');
		var listItemId = "";
		if(checkedItems.length > 0){
			checkedItems.each(function(key,value){
				if(key == 0){
					listItemId = $(value).attr("data");
				}else{
					listItemId += "|" + $(value).attr("data");
				}
			});
		}
		return listItemId;
	};
});

/*----------------------------------------------MODAL CONTROLLER--------------------------------------------*/

App.controller("ClientReportModal",function($scope, ClientReportModel){
	$scope.model = ClientReportModel;

	$scope.submitFormEdit = function(isContinue){
		isContinue = typeof isContinue !== 'undefined' ? isContinue : false;
		$scope.model.act.submitFormEdit(isContinue);
	};

    var reportModal = $("#report-modal");
    $scope.model.vars.floatPanel = $("#float-panel");
    var container = $(".remodal-overlay");
    container.scroll(function () {
        var currentScroll = $(this).scrollTop();
        if (currentScroll == 0 || currentScroll + $(window).height() >= reportModal.height()) {
            $scope.model.vars.floatPanel.hide();
        } else {
            $scope.model.vars.floatPanel.show();
        }
    });
});