'use strict';

/* Controllers */
App.controller("ClientCvList",function($scope, $filter, ClientCvModel, CommonAssess){
	$scope.model = ClientCvModel;

	var serverVarsElm = $('#js-vars');
	var jsVars = serverVarsElm.val()?JSON.parse(serverVarsElm.val()):null;
	$scope.model.vars.user_id = jsVars.user_id;
	$scope.model.vars.cv_id = jsVars._id;
	$scope.model.vars.list_cv_side = [];

	CommonAssess.showWaitingHeader(true);
	var params = new FormData();
	params.append('parent', $scope.model.vars.user_id);

	updateList();

	function updateList(){
		$scope.model.api.getAll($scope.model.vars.serverController, $scope.model.vars.csrftoken, params).done(function(result){
			CommonAssess.showWaitingHeader(false);
			$scope.model.vars.list_cv_side = [];
			if(result['data']){
				result = result['data'];
				for(var i in result){
					var item = {
						_id: result[i]['_id'],
						name: result[i]['name'],
						lang: result[i]['lang']
					}
					$scope.model.vars.list_cv_side.push(item);
				}
			}
			$scope.$apply();
			var currentCv = "#cv_" + $scope.model.vars.cv_id;
			var currentCvSide = "#cv_" + $scope.model.vars.cv_id + '-side';
			
			if($(currentCv).length){
				$(currentCv).addClass('active');
				$("#side-main-menu a").removeClass('active');
			}

			if($(currentCvSide).length){
				$(currentCvSide).addClass('active');
				$("#side-main-menu a").removeClass('active');
			}
		});
	}

	$scope.$on('UPDATE_LIST_SIDE_CV', function(){
		updateList();
	});
});