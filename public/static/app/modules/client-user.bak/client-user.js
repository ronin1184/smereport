'use strict';

/* Controllers */
App.controller("ClientUser",function($scope,$rootScope,$filter,ClientUserModel,CommonAssess){
	$scope.model 		=ClientUserModel;
	$scope.inputFilter 	=null;

	var mainTable		=$('#user-table');
	var mainModal 		=$('#user-modal');
	var firstInput 		=$('#email');
	var serverVarsElm	=$('#js-vars');
	
	var startPoint		=0;
	var perPage			=15;

	var jsVars 			= serverVarsElm.val()?JSON.parse(serverVarsElm.val()):null;
	var listItemFull 	= jsVars.list_item;
	var listItemFilter 	= null;

	updateListItem(listItemFull,startPoint);

	function updateListItem(listItemFullInput,startPointInput){
		if(!listItemFullInput) return;
		var maxPoint 			= listItemFullInput.length;
		$scope.listItem 		= listItemFullInput.slice(startPointInput, startPointInput+perPage);
		$scope.listPercentText 	=(startPointInput+(perPage>maxPoint?maxPoint:perPage)).toString() + '/' + listItemFullInput.length.toString();
	}

	$scope.$watch('inputFilter', function(newValue, oldValue) {
		if(newValue){
			listItemFilter = $filter('filter')(listItemFull,$scope.inputFilter);
		}else{
			listItemFilter = listItemFull;
		}
		updateListItem(listItemFilter,0);
	});

	mainTable.mousewheel(function(event, delta) {
		var activeListItem=null;
		if($scope.inputFilter){
			activeListItem=listItemFilter;
		}else{
			activeListItem=listItemFull;
		}

		var direction=delta==-1?'down':'up'
        if(direction=='down'){
        	if(startPoint+perPage >= activeListItem.length) return;
        	startPoint++;
        }else{
        	if(startPoint <=0 ) return;
        	startPoint--;
        }

        updateListItem(activeListItem,startPoint);
        
        $scope.$apply();
    });

	

	$scope.openFormEdit=function(_id){
		emptyModelFieldsVariables();
		mainModal.on('shown.bs.modal', function () {
			firstInput.focus();
		});

		if(_id){
			CommonAssess.showWaitingHeader(true);
			$scope.model.fields._id		=_id;
			$scope.model.other.isEdit	=true;

			var params = new FormData();
			params.append('_id',_id);
			$scope.model.getSingle(params).done(function(result){
				setModelFieldsVariables(result.data);
				CommonAssess.showWaitingHeader(false);
				mainModal.modal('show');
				$scope.$apply();
			});
		}else{
			$scope.model.other.isEdit=false;
			mainModal.modal('show');
		}
	}

	$scope.closeFormEdit=function(){
		closeFormEdit();
	}

	function closeFormEdit(){
		mainModal.modal('hide');
	}

	function refreshBrowser(){
		location.reload();
	}

	function afterSubmit(result,action){
		if(!result.error){
			var newItem = {
				_id 	: result.data,
				email 	: $scope.model.fields.email,
				name 	: $scope.model.fields.name,
				role 	: $scope.model.fields.role
			}

			switch(action){
				case 'edit':
					var index=CommonAssess.getIndexFromId(listItemFull,newItem._id);
					listItemFull[index]=newItem;
				break;

				case 'add':
					listItemFull.unshift(newItem);
					startPoint=0;
				break;

				case 'remove':
					var listId=newItem._id.split('|');
					for(var i in listId){
						var index=CommonAssess.getIndexFromId(listItemFull,listId[i]);
						listItemFull.splice(index,1);	
					}
					startPoint=0;
				break;
			}

			updateListItem(listItemFull,startPoint);
			closeFormEdit();
		}else{
			//$scope.model.other.errorMessage=CommonAssess.processErrorMessage(result.error);
			$scope.model.other.errorMessage=result.error;
		}
		setDisplayLoading(false);
		$scope.$apply();
	}

	$scope.submitFormEdit=function(){
		setDisplayLoading(true);
		var newItem = {};
		var params = new FormData();

		params.append('email' 	,$scope.model.fields.email);
		params.append('name'	,$scope.model.fields.name);
		params.append('password',$scope.model.fields.password);
		params.append('role'	,$scope.model.fields.role);

		if($scope.model.other.isEdit){
			params.append('_id',$scope.model.fields._id);
			$scope.model.editItem(params).done(function(result){
				afterSubmit(result,'edit');
			});
		}else{
			$scope.model.addItem(params).done(function(result){
				afterSubmit(result,'add');
			});
		}
	}


	$scope.openFormRemove=function(_id){
		if(_id){
			if(_id.split("|").length>1){
				$scope.formRemoveInfo="Do you realy want to remove these records ?";
			}else{
				$scope.formRemoveInfo="Do you realy want to remove this records ?";
			}
			$scope.model.fields._id=_id;
			var confirmRemove=confirm($scope.formRemoveInfo);
			if(confirmRemove){
				submitFormRemove();
			}
		}
	}

	function submitFormRemove(){
		CommonAssess.showWaitingHeader(true);
		var params = new FormData();
		params.append('_id',$scope.model.fields._id);
		$scope.model.removeItem(params).done(function(result){
			CommonAssess.showWaitingHeader(false);
			afterSubmit(result,'remove');
		});
	}


	function setDisplayLoading(isDisplay){
		$scope.model.other.displayLoading=isDisplay;
	}
	 
	function emptyModelFieldsVariables(){
		$scope.model.other.errorMessage	=CommonAssess.processErrorMessage(null);

		$scope.model.fields.email		=null;
		$scope.model.fields.name		=null;
		$scope.model.fields.password	=null;
		$scope.model.fields.role		=$scope.model.other.roleList[0];
	};

	function setModelFieldsVariables(item){
		$scope.model.other.errorMessage	=CommonAssess.processErrorMessage(null);
		
		$scope.model.fields.email		=item.email;
		$scope.model.fields.name		=item.name;
		//$scope.model.fields.password	=item.password;
		$scope.model.fields.role		=item.role;
		
	};

	$scope.selectCheckBox=function(){
		$scope.model.other.listItemId=getListItemId();
	};

	$scope.selectAllCheckBox=function(){
		var checkedItems=$('.selectCheckBox:checked');
		var checkBox 	=$(".selectCheckBox");

		if(checkedItems.length>0 && checkedItems.length < checkBox.length){
			checkBox.prop("checked",true);
		}else if(checkedItems.length == checkBox.length){
			checkBox.prop("checked",false);
		}else{
			checkBox.prop("checked",true);
		}
		$scope.model.other.listItemId=getListItemId();
	};

	function getListItemId(){
		var checkedItems=$('.selectCheckBox:checked');
		var listItemId="";
		if(checkedItems.length>0){
			checkedItems.each(function(key,value){
				if(key==0){
					listItemId=$(value).attr("data");
				}else{
					listItemId+="|"+$(value).attr("data");
				}
			});
		}
		return listItemId;
	}
});
