'use strict';

/* Services */
var ClientUserModel=angular.module('ClientUserModel',[]);
ClientUserModel.factory('ClientUserModel',function($http,$rootScope){
	var csrftoken = $('[name="csrfmiddlewaretoken"]').val();
	return {
		getAll:function(params){
			return $http.post("/admin/get_all",params);
		},

		getSingle:function(params){
			return $.ajax({
                url         :$rootScope.baseUrl+"user/get_one",
                type        :'POST',
                //crossDomain :true,
                headers     :{'X-CSRFToken':csrftoken},
                processData: false,
                contentType: false,
                data        :params,
            });
		},
		
		addItem:function(params){
			return $.ajax({
                url         :$rootScope.baseUrl+"user/add_item",
                type        :'POST',
                //crossDomain :true,
                headers     :{'X-CSRFToken':csrftoken},
                processData: false,
                contentType: false,
                data        :params,
            });
		},
		
		editItem:function(params){
			return $.ajax({
                url         :$rootScope.baseUrl+"user/edit_item",
                type        :'POST',
                //crossDomain :true,
                headers     :{'X-CSRFToken':csrftoken},
                processData: false,
                contentType: false,
                data        :params,
            });
		},

		removeItem:function(params){
			return $.ajax({
                url         :$rootScope.baseUrl+"user/remove_item",
                type        :'POST',
                //crossDomain :true,
                headers     :{'X-CSRFToken':csrftoken},
                processData: false,
                contentType: false,
                data        :params,
            });
		},

		forgotPassword:function(params){
			return $.ajax({
                url         :$rootScope.baseUrl+"admin/forgot_password",
                type        :'POST',
                crossDomain :true,
                processData: false,
                contentType: false,
                data        :params,
            });
		},

		fields:{
			id 			:null,
			value 		:null
		},

		other:{
			displayLoading 	:false,
			errorMessage	:null,
			submitSuccess	:null,
			sexIcon			:null,
			isEdit			:false,
			searchPattern	:null,
			_id				:null,
			listItemId		:null,
			roleList		:["ADMIN","MOD"]
		},
	}   
});
