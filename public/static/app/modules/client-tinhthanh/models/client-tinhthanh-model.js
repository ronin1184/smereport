'use strict';

/* Services */
var ClientTinhthanhModel=angular.module('ClientTinhthanhModel',[]);
ClientTinhthanhModel.factory('ClientTinhthanhModel',function($http, $rootScope, CommonAssess){
    var serverController = 'tinhthanh';
	var csrftoken = $('[name="csrfmiddlewaretoken"]').val();

    var firstInput = $('#uid');
    var secondInput = $('#value');

    var serverVarsElm = $('#js-vars');
    
    var startPoint = 0;
    var perPage = 25;

    /*--------------------------------------------------Variables--------------------------------------------*/    

    var vars = {
        isFilter : false,
        listItem : null,
        inputFilter : null,
        listPercentText : null,
        listItemFull : null,
        listItemFilter : null,
        displayLoading : false,
        errorMessage : null,
        isEdit : false,
        listItemId : null,
        startPoint : startPoint,
        perPage : perPage,
        serverController : serverController,
        csrftoken : csrftoken
    };

    /*--------------------------------------------------Addition APIs-----------------------------------------*/

    var api = CommonAssess.api;

	var model = {
        api : api,

		vars : vars,

        fields : {},

        ref : {},

        act : {
            updateListItem : function(listItemFullInput){
                updateListItem(listItemFullInput);
            },

            openFormEdit : function(id){
                openFormEdit(id);
            },

            closeFormEdit : function(){
                closeFormEdit();
            },

            setDisplayLoading : function(isDisplay){
                setDisplayLoading(isDisplay);
            },

            submitFormEdit : function(isContinue){
                submitFormEdit(isContinue);
            },

            afterSubmit : function(result, action){
                afterSubmit(result, action);
            },

            emptyModelFieldsVariables : function(){
                emptyModelFieldsVariables();
            },

            setModelFieldsVariables : function(item){
                setModelFieldsVariables(item);
            }
        }
	} 

    /*-----------------------------------------------Init UI----------------------------------------------*/

    var modal = $.remodal.lookup[$('[data-remodal-id = modal]').data('remodal')];

    $(document).on('opened', '.remodal', function () {
        if(!model.vars.isEdit){
            setTimeout(function(){
                firstInput.focus();
            }, 0);
        }else{
            setTimeout(function(){
                secondInput.focus();
            }, 0);
        }
    });

    /*---------------------------------------------Controller functions---------------------------------------------*/

    var updateListItem = function(listItemFullInput){
        if(!listItemFullInput) return;
        var maxPoint = listItemFullInput.length;
        model.vars.listItem = listItemFullInput;
    }

    var openFormEdit = function(id){
        emptyModelFieldsVariables();
        setDisplayLoading(false);
        if(id){
            CommonAssess.showWaitingHeader(true);
            model.fields.id = id;
            model.vars.isEdit = true;

            var params = new FormData();
            params.append('id', id);
            model.api.getSingle(serverController, csrftoken, params).done(function(result){
                setModelFieldsVariables(result.data);
                CommonAssess.showWaitingHeader(false);
                modal.open();
                $rootScope.$apply();
            });
        }else{
            model.vars.isEdit = false;
            modal.open();
        }
    };

    var closeFormEdit = function(){
        modal.close();
    };

    var submitFormEdit = function(isContinue){
        setDisplayLoading(true);

        var params = new FormData();

        params.append('uid' , model.fields.uid);
		params.append('value' , model.fields.value);

        if(model.vars.isEdit){
            params.append('id', model.fields.id);
            model.api.editItem(serverController, csrftoken, params).done(function(result){
                afterSubmit(result, 'edit', isContinue);
            });
        }else{
            model.api.addItem(serverController, csrftoken, params).done(function(result){
                afterSubmit(result, 'add', isContinue);
            });
        }
    };

    var afterSubmit = function(result, action, isContinue){
        if(!result.error){
            var newItem = {
                id : result.data.id.toString(),
                uid : result.data.uid ? result.data.uid : model.fields.uid,
                value : result.data.value ? result.data.value : model.fields.value
            };
            switch(action){
                case 'edit':
					isContinue = false;
                    var index = CommonAssess.getIndexFromId(model.vars.listItemFull, newItem.id);
                    model.vars.listItemFull[index]=newItem;
                break;

                case 'add':
                    model.vars.listItemFull.unshift(newItem);
                    startPoint = 0;
                break;

                case 'remove':
					isContinue = false;
                    var listId = newItem.id.split('|');
                    for(var i in listId){
                        var index = CommonAssess.getIndexFromId(model.vars.listItemFull, listId[i]);
                        model.vars.listItemFull.splice(index,1);   
                    }
                    startPoint = 0;
                break;
            }
            if(isContinue){
                emptyModelFieldsVariables();
                firstInput.focus();
            }else{
                closeFormEdit();    
            }
        }else{
            model.vars.errorMessage = CommonAssess.processErrorMessage(result.error);
        }
		setDisplayLoading(false);
        $rootScope.$apply();
    };

    var setDisplayLoading = function(isDisplay){
        model.vars.displayLoading = isDisplay;
    };

    var emptyModelFieldsVariables = function(){
        model.vars.errorMessage = CommonAssess.processErrorMessage(null);

        model.fields.uid = null;
		model.fields.value = null;
    };

    var setModelFieldsVariables = function(item){
        model.vars.errorMessage = CommonAssess.processErrorMessage(null);

        model.fields.uid = item.uid;
		model.fields.value = item.value;
    };

    return model;  
});
