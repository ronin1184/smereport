'use strict';

/* App Module */

var App = angular.module("App",	[	'ngSanitize',
                                    'ui.tinymce',
									'ui.sortable',
									'CommonAssess',
									'ClientAdminLoginModel',
									'ClientAdminModel',
									'ClientStaffModel',
									'ClientUserModel',
									'ClientUserLoginModel',
									'ClientConfigModel',
									'ClientTinhthanhModel',
									'ClientCategoryModel',
									'ClientZoneModel',
									'ClientDropdownModel',
                                    'ClientCompanyModel',
                                    'ClientReportModel'
								]);

App.config(function($interpolateProvider)
	{
        $interpolateProvider.startSymbol('{$').endSymbol('$}');
    }
);

App.run(function($rootScope){
	$rootScope.baseUrl 				= $("title").attr("data-base-url");
	$rootScope.staticUrl 			= $("title").attr("data-static-url");
	$rootScope.mediaUrl 			= $("title").attr("data-media-url");
	$rootScope.loadingImage			= $rootScope.staticUrl+"img/loading-line.gif";
	$rootScope.defaultThumbnail		= $rootScope.staticUrl+"img/default-thumbnail.jpg";
});
