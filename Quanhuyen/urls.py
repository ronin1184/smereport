from django.conf.urls import patterns, url


urlpatterns = patterns(
    '',
    url(
        r'^$',
        'Tinhthanh.views.index',
        name='index'),
    url(
        r'^get_all',
        'Tinhthanh.views.get_all',
        name='get_all'),
    url(
        r'^get_one',
        'Tinhthanh.views.get_one',
        name='get_one'),
    url(
        r'^add_item',
        'Tinhthanh.views.add_item',
        name='add_item'),
    url(
        r'^edit_item',
        'Tinhthanh.views.edit_item',
        name='edit_item'),
    url(
        r'^remove_item',
        'Tinhthanh.views.remove_item',
        name='remove_item'),
)
