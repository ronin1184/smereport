# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Quanhuyen',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('uid', models.CharField(max_length=150)),
                ('value', models.CharField(max_length=150)),
                ('tinhthanh', models.CharField(max_length=150)),
            ],
            options={
                'db_table': 'quanhuyen',
            },
            bases=(models.Model,),
        ),
    ]
