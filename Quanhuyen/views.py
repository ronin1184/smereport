from django.shortcuts import render
from django.http import StreamingHttpResponse
from django.template import RequestContext
from django.core.context_processors import csrf
from Common.Tools import Tools
# from Category.models import Category
from Tinhthanh.models import Tinhthanh
tools = Tools()
main_model = Tinhthanh


def index(request):
    data = {}
    list_item = main_model.objects.get_all()['data']
    data['js_vars'] = {}
    data['js_vars']['list_item'] = Tools.standardize_array(list_item)
    if len(data['js_vars']) != 0:
        data['js_vars'] = tools.encode_server_vars(data['js_vars'])
    else:
        data['js_vars'] = ''
    data.update(csrf(request))
    return render(
        request,
        tools.get_app_name(__name__) + 'tinhthanh.html',
        data,
        context_instance=RequestContext(request)
    )


def get_all(request):
    conditions = None
    page = int(request.POST.get('page'))
    keyword = request.POST.get('keyword', None)

    result = main_model.objects.get_all(conditions, keyword, page)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def get_full(request):
    conditions = None

    result = main_model.objects.get_all(conditions)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def get_one(request):
    conditions = {
        'id': request.POST.get('id', None),
    }
    if conditions['id'] is not None:
        conditions['id'] = int(conditions['id'])
    result = main_model.objects.get_one(conditions)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def add_item(request):
    data = {
        'uid': request.POST.get('uid', None),
        'value': request.POST.get('value', None),
    }
    result = main_model.objects.add_item(data)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def edit_item(request):
    data = {
        'uid': request.POST.get('uid', None),
        'value': request.POST.get('value', None),
    }
    pk = request.POST.get('id', None)
    result = main_model.objects.edit_item(pk, data)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def remove_item(request):
    pk = request.POST.get('id', None)
    result = main_model.objects.remove_item(pk)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )
