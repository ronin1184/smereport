import hashlib
from django.http import Http404
from django.shortcuts import render
from django.http import StreamingHttpResponse
from django.shortcuts import redirect
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.db.models import F
from datetime import datetime
from django.utils.timezone import utc
from django.core.context_processors import csrf
from bson.json_util import dumps
from Common.Tools import Tools
from smereport.settings import MAX_DELAY_LOGIN, BASE_URL
from smereport.settings import MAX_FAIL_LOGIN
from smereport.settings import TOKEN_LIFE
from User.models import User
from Category.models import Category
from Zone.models import Zone
from Dropdown.models import Dropdown
from Company.models import Company

tools = Tools()
main_model = User
category_model = Category
zone_model = Zone
dropdown_model = Dropdown
company_model = Company
main_model = User


def index(request):
    data = {}
    list_zone = zone_model.objects.get_all()['data']
    list_item = main_model.objects.get_all()['data']
    data['js_vars'] = {}
    data['js_vars']['list_item'] = Tools.standardize_array(list_item)
    data['js_vars']['list_zone'] = list_zone
    if len(data['js_vars']) != 0:
        data['js_vars'] = tools.encode_server_vars(data['js_vars'])
    else:
        data['js_vars'] = ''
    data.update(csrf(request))
    return render(
        request,
        tools.get_app_name(__name__) + 'user.html',
        data,
        context_instance=RequestContext(request)
    )


def quota(request):
    data = {}
    owner_id = request.session.get('USER_ID', None)
    owner_item = main_model.objects.get_one({'pk': owner_id}, True)['data']

    owner_type = 'USER'

    conditions = {
        # 'hasadmin': None,
        'sale': owner_item.pk
    }
    # template = 'quota_lv' + str(admin_level) + '.html'

    ket_qua_cuoc_goi_cat = category_model.objects.get_one({'uid': 'ket-qua-cuoc-goi'}, True)['data']
    list_ket_qua_cuoc_goi = dropdown_model.objects.get_full({'category': ket_qua_cuoc_goi_cat})['data']

    ket_qua_cuoc_gap_cat = category_model.objects.get_one({'uid': 'kq-cuoc-gap'}, True)['data']
    list_ket_qua_cuoc_gap = dropdown_model.objects.get_full({'category': ket_qua_cuoc_gap_cat})['data']

    sp_tiep_can_cat = category_model.objects.get_one({'uid': 'sp-tiep-can'}, True)['data']
    list_sp_tiep_can = dropdown_model.objects.get_full({'category': sp_tiep_can_cat.pk})['data']

    tinh_trang_khai_thac_cat = category_model.objects.get_one({'uid': 'tinh-trang-khai-thac'}, True)['data']
    list_tinh_trang_khai_thac = dropdown_model.objects.get_full({'category': tinh_trang_khai_thac_cat.pk})['data']

    list_user = main_model.objects.get_full()['data']
    list_zone = zone_model.objects.get_full()['data']

    template = 'user_quota.html'

    list_item = company_model.objects.get_all(conditions)['data']
    data['js_vars'] = {}
    data['js_vars']['list_item'] = Tools.standardize_array(list_item)
    data['js_vars']['list_owner'] = []

    data['js_vars']['list_ket_qua_cuoc_goi'] = Tools.standardize_array(list_ket_qua_cuoc_goi)
    data['js_vars']['list_ket_qua_cuoc_gap'] = Tools.standardize_array(list_ket_qua_cuoc_gap)
    data['js_vars']['list_sp_tiep_can'] = Tools.standardize_array(list_sp_tiep_can)
    data['js_vars']['list_tinh_trang_khai_thac'] = Tools.standardize_array(list_tinh_trang_khai_thac)
    data['js_vars']['list_user'] = Tools.standardize_array(list_user)
    data['js_vars']['list_zone'] = Tools.standardize_array(list_zone)

    data['js_vars']['owner_type'] = owner_type
    data['js_vars']['owner_conditions'] = conditions
    data['js_vars']['user_type'] = 'USER'
    if len(data['js_vars']) != 0:
        data['js_vars'] = tools.encode_server_vars(data['js_vars'])
    else:
        data['js_vars'] = ''
    data.update(csrf(request))
    return render(
        request,
        tools.get_app_name(__name__) + template,
        data,
        context_instance=RequestContext(request)
    )


def home(request):
    data = {}
    category_item = category_model.objects.get_one({'uid': 'chuc-danh'}, True)['data']
    list_zone = zone_model.objects.get_all()['data']
    list_title = dropdown_model.objects.get_all({'category': category_item})['data']
    user_item = main_model.objects.get_one({'id': request.session['USER_ID']})['data']
    if 'password' in user_item:
        user_item['password'] = None
    data['js_vars'] = {}
    data['js_vars']['user_item'] = user_item
    data['js_vars']['list_zone'] = list_zone
    data['js_vars']['list_title'] = list_title
    data['admin_item'] = user_item
    if len(data['js_vars']) != 0:
        data['js_vars'] = tools.encode_server_vars(data['js_vars'])
    else:
        data['js_vars'] = ''
    data['user_item'] = user_item
    data.update(csrf(request))
    return render(
        request,
        tools.get_app_name(__name__) + 'user_home.html',
        data,
        context_instance=RequestContext(request)
    )


def get_one(request):
    conditions = {
        'id': request.POST.get('id', None),
    }
    if conditions['id'] is not None:
        conditions['id'] = int(conditions['id'])
    result = main_model.objects.get_one(conditions)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def edit_item(request):
    data = {
        'password': request.POST.get('password', None),
        'name': request.POST.get('name', None),
        'phone': request.POST.get('phone', None),
    }

    pk = request.POST.get('id', None)
    result = main_model.objects.edit_item(pk, data)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def forgot_password(request):
    uid = request.POST.get('uid', None)
    user = main_model.objects.get_one({"uid": uid})['data']
    try:
        if user is not None:
            token = tools.random_str(32)
            tokendate = datetime.now()
            main_model.objects.edit_item(int(user['id']), {'token': hashlib.md5(token.encode('utf-8')).hexdigest(), 'tokendate': tokendate})
            email = user['email']
            body = 'Please click <a href="' + BASE_URL + '/user/reset_password/' + str(user['id']) + '/' + str(token) + '">here</a> to see your new password.'
            tools.send_email('New password request', body, [email])
            status = 'OK'
        else:
            status = 'Not OK'
        result = {
            'data': status,
            'error': None
        }
        return StreamingHttpResponse(
            tools.encode_server_vars(result),
            content_type='application/json'
        )
    except Exception as e:
        error = tools.return_exception(e)
        # print(error)
        return error


def reset_password(request, user_id, token):
    user = main_model.objects.get_one({"id": user_id})['data']
    if user is not None:
        if user['token'] and user['token'] == hashlib.md5(token.encode('utf-8')).hexdigest():
            tokendate = user['tokendate']
            dif_time = datetime.utcnow().replace(tzinfo=utc) - tokendate
            dif_time = dif_time.total_seconds()
            if dif_time >= TOKEN_LIFE*60:
                raise Http404
            else:
                password = tools.random_str(25)
                main_model.objects.edit_item(int(user['id']), {'password': password, 'token': None, 'tokendate': None})
                return StreamingHttpResponse(
                    password,
                    content_type='text/html'
                )
        else:
            raise Http404
    else:
        raise Http404


def login(request):
    data = {}
    # conditions = {'pk': 6}
    # list_user = main_model.objects.get_one(conditions)
    # data['category'] = list_user
    data.update(csrf(request))
    return render(
        request,
        tools.get_app_name(__name__) + 'login.html',
        data,
        context_instance=RequestContext(request))


def authenticate(request):
    if request.method == 'POST':
        # item = User.objects.get_one({'pk': 6})['data']
        # print item
        uid = request.POST.get('uid', None)
        password = request.POST.get('password', None)
        item = main_model.objects.get_one({'uid': uid})['data']
        if item is None:
            res_data = {
                'error': 'Wrong username/password',
                'data': None
            }
        else:
            cdate = item['cdate']
            if cdate:
                dif_time = datetime.utcnow().replace(tzinfo=utc) - cdate
                dif_time = dif_time.total_seconds()
            else:
                dif_time = 0
            if not item['logfail']:
                item['logfail'] = 0
            if item['logfail'] < MAX_FAIL_LOGIN or dif_time >= MAX_DELAY_LOGIN*60:
                if item is not None:
                    if hashlib.md5(password.encode('utf-8') + item['salt'].encode('utf-8')).hexdigest() == item['password']:
                        request.session['USER_FULLNAME'] = item['name']
                        request.session['USER_ID'] = str(item['id'])
                        request.session['USER_ROLE'] = item['role']
                        res_data = {
                            'error': None,
                            'data': 'OK'
                        }
                        main_model.objects.edit_item(item['id'], {'cdate': datetime.now(), 'logfail': 0})
                    else:
                        if dif_time >= MAX_DELAY_LOGIN*60:
                            main_model.objects.edit_item(item['id'], {'cdate': datetime.now(), 'logfail': 0})
                        else:
                            main_model.objects.edit_item(item['id'], {'cdate': datetime.now(), 'logfail': F('logfail') + 1})
                        res_data = {
                            'error': 'Wrong username/password',
                            'data': None
                        }
                else:
                    main_model.objects.edit_item(item['id'], {'cdate': datetime.now(), 'logfail': F('logfail')+1})
                    res_data = {
                        'error': 'Wrong username/password',
                        'data': None
                    }
            else:
                res_data = {
                    'error': 'You have been logged fail ' + str(MAX_FAIL_LOGIN) + ' times. Please relax in ' + str(MAX_DELAY_LOGIN) + ' minutes (' + str(int(dif_time/60)) + ' / '+str(MAX_DELAY_LOGIN)+').',
                    'data': None
                }
                # main_model.edit_item({'cdate': datetime.now(), 'logfail': 0}, item['id'])
        return StreamingHttpResponse(dumps(res_data), content_type='application/json')
    return StreamingHttpResponse(
        dumps({}),
        content_type='application/json'
    )


def logout(request):
    if 'USER_FULLNAME' in request.session:
        if request.session['USER_FULLNAME']:
            del request.session['USER_FULLNAME']

    if 'USER_ID' in request.session:
        if request.session['USER_ID']:
            del request.session['USER_ID']

    if 'USER_ROLE' in request.session:
        if request.session['USER_ROLE']:
            del request.session['USER_ROLE']

    return redirect(reverse('user:login'))
