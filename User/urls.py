from django.conf.urls import patterns, url


urlpatterns = patterns(
    '',
    url(
        r'^$',
        'User.views.index',
        name='index'),
    url(
        r'^quota$',
        'User.views.quota',
        name='quota'),
    url(
        r'^home$',
        'User.views.home',
        name='home'),
    url(
        r'^login$',
        'User.views.login',
        name='login'),
     url(
        r'^authenticate',
        'User.views.authenticate',
        name='authenticate'),
    url(
        r'^logout',
        'User.views.logout',
        name='logout'),
    url(
        r'^get_one',
        'User.views.get_one',
        name='get_one'),
    url(
        r'^edit_item',
        'User.views.edit_item'),
    url(
        r'^forgot_password$',
        'User.views.forgot_password',
        name='forgot_password'),
    url(
        r'^reset_password/(?P<user_id>\d+)/(?P<token>[\w-]+)$',
        'User.views.reset_password',
        name='reset_password'),
)
