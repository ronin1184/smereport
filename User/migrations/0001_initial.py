# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Zone', '__first__'),
        ('Dropdown', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('cdate', models.DateTimeField(blank=True, null=True)),
                ('uid', models.CharField(max_length=50)),
                ('email', models.EmailField(max_length=75)),
                ('phone', models.CharField(max_length=20)),
                ('name', models.CharField(max_length=100)),
                ('password', models.CharField(max_length=100)),
                ('salt', models.CharField(max_length=100)),
                ('token', models.CharField(max_length=50, blank=True, null=True)),
                ('tokendate', models.DateTimeField(blank=True, null=True)),
                ('role', models.CharField(max_length=50)),
                ('logfail', models.BooleanField(default=False)),
                ('title', models.ForeignKey(to='Dropdown.Dropdown')),
                ('zone', models.ForeignKey(to='Zone.Zone')),
            ],
            options={
                'db_table': 'user',
            },
            bases=(models.Model,),
        ),
    ]
