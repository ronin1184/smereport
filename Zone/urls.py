from django.conf.urls import patterns, url


urlpatterns = patterns(
    '',
    url(
        r'^(?P<category_id>\d+)$',
        'Zone.views.index',
        name='index'),
    url(
        r'^get_all',
        'Zone.views.get_all',
        name='get_all'),
    url(
        r'^get_one',
        'Zone.views.get_one',
        name='get_one'),
    url(
        r'^add_item',
        'Zone.views.add_item',
        name='add_item'),
    url(
        r'^edit_item',
        'Zone.views.edit_item',
        name='edit_item'),
    url(
        r'^remove_item',
        'Zone.views.remove_item',
        name='remove_item'),
)
