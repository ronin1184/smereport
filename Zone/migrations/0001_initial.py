# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Category', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Zone',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('uid', models.CharField(max_length=150)),
                ('title', models.CharField(max_length=150)),
                ('level', models.IntegerField()),
                ('category', models.ForeignKey(to='Category.Category')),
                ('parent', models.ForeignKey(null=True, blank=True, to='Zone.Zone')),
            ],
            options={
                'db_table': 'zone',
            },
            bases=(models.Model,),
        ),
    ]
