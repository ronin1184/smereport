import hashlib
import json
from django.http import Http404
from django.shortcuts import render
from django.http import StreamingHttpResponse
from django.shortcuts import redirect
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.db.models import F
from datetime import datetime
from django.utils.timezone import utc
from django.core.context_processors import csrf
from bson.json_util import dumps
from Common.Tools import Tools
from smereport.settings import MAX_DELAY_LOGIN
from smereport.settings import MAX_FAIL_LOGIN
#from Category.models import Category
from Zone.models import Zone
from Category.models import Category
tools = Tools()
main_model = Zone
category_model = Category


def index(request, category_id):
    data = {}
    category = category_model.objects.get_one({'id': category_id})['data']
    if not category:
        raise Http404
    conditions = {'category': category_id}
    list_item = main_model.objects.get_all(conditions)['data']
    '''
    if category['level'] > 0:
        ref_condition = {'level': category['level']-1}
        list_zone_dropdown = main_model.objects.get_full(ref_condition)['data']
    else:
        list_zone_dropdown = []
    '''
    ref_condition = {'level': category['level'] - 1}
    list_zone_dropdown = main_model.objects.get_full(ref_condition)['data']

    data['js_vars'] = {}
    data['js_vars']['list_item'] = Tools.standardize_array(list_item)
    data['js_vars']['list_zone_dropdown'] = Tools.standardize_array(list_zone_dropdown)
    data['js_vars']['category'] = category
    if len(data['js_vars']) != 0:
        data['js_vars'] = tools.encode_server_vars(data['js_vars'])
    else:
        data['js_vars'] = ''
    data.update(csrf(request))
    return render(
        request,
        tools.get_app_name(__name__) + 'zone.html',
        data,
        context_instance=RequestContext(request)
    )


def get_all(request):
    conditions = None
    page = int(request.POST.get('page'))
    keyword = request.POST.get('keyword', None)

    result = main_model.objects.get_all(conditions, keyword, page)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def get_full(self, conditions=None):
        try:
            if conditions is None:
                query = self.all()
            else:
                query = self.filter(**conditions)
            result = Tools.standardize_array(list(query.values()))
            return {
                'error': None,
                'data': result
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error


def get_one(request):
    conditions = {
        'id': request.POST.get('id', None),
    }
    if conditions['id'] is not None:
        conditions['id'] = int(conditions['id'])
    result = main_model.objects.get_one(conditions)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def add_item(request):
    data = {
        'title': request.POST.get('title', None),
        'category': request.POST.get('category', None),
        'level': request.POST.get('level', None),
        'parent': request.POST.get('parent', None),
    }
    data['uid'] = tools.nice_url(data['title'])
    data['category'] = category_model.objects.get_one({'id': int(data['category'])}, True)['data']
    data['level'] = int(data['level'])
    if data['parent'] == 'null' or not data['parent']:
        data['parent'] = None
    else:
        data['parent'] = main_model.objects.get_one({'id': int(data['parent'])}, True)['data']
    result = main_model.objects.add_item(data)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def edit_item(request):
    data = {
        'title': request.POST.get('title', None),
        'level': request.POST.get('level', None),
        'parent_id': request.POST.get('parent', None),
    }
    data['uid'] = tools.nice_url(data['title'])
    data['level'] = int(data['level'])
    if data['parent_id'] == 'null' or not data['parent_id']:
        data['parent_id'] = None
    else:
        data['parent_id'] = main_model.objects.get_one({'id': int(data['parent_id'])}, True)['data']

    pk = request.POST.get('id', None)
    result = main_model.objects.edit_item(pk, data)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def remove_item(request):
    pk = request.POST.get('id', None)
    result = main_model.objects.remove_item(pk)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )
