from django.conf.urls import patterns, include, url

urlpatterns = patterns(
    '',
    url(
        r'^$',
        'User.views.login',
        name='login'),
    url(
        r'^administrator/',
        include('Administrator.urls', namespace='administrator')),
    url(
        r'^admin$',
        'Administrator.views.login',
        name='admin'),
    url(
        r'^user/',
        include('User.urls', namespace='user')),
    url(
        r'^staff/',
        include('Staff.urls', namespace='staff')),
    url(
        r'^config/',
        include('Config.urls', namespace='config')),
    url(
        r'^tinhthanh/',
        include('Tinhthanh.urls', namespace='tinhthanh')),
    url(
        r'^category/',
        include('Category.urls', namespace='category')),
    url(
        r'^zone/',
        include('Zone.urls', namespace='zone')),
    url(
        r'^dropdown/',
        include('Dropdown.urls', namespace='dropdown')),
    url(
        r'^company/',
        include('Company.urls', namespace='company')),
    url(
        r'^report/',
        include('Report.urls', namespace='report')),
)
