# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Report', '0002_auto_20150115_1937'),
    ]

    operations = [
        migrations.AlterField(
            model_name='report',
            name='cty_mst',
            field=models.CharField(blank=True, max_length=50, null=True),
            preserve_default=True,
        ),
    ]
