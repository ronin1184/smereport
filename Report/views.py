# coding=utf8
# -*- coding: utf8 -*-
from django.shortcuts import render
from django.http import StreamingHttpResponse
from django.http import HttpResponse
from django.http import HttpResponseServerError
from django.http import Http404
from django.template import RequestContext
from django.core.context_processors import csrf
from Common.Tools import Tools
from Report.models import Report
from Report.models import ReportLog
from User.models import User
from Administrator.models import Administrator
from Company.models import Company
from Zone.models import Zone
from Category.models import Category
from Dropdown.models import Dropdown
from django.utils import timezone
from collections import OrderedDict
import copy
import xlsxwriter
import io
tools = Tools()
main_model = Report
report_log_model = ReportLog
user_model = User
admin_model = Administrator
company_model = Company
zone_model = Zone
dropdown_model = Dropdown
category_model = Category


def index(request):
    data = {}
    pk = request.session.get('USER_ID', None)
    user_item = user_model.objects.get_one({'pk': pk}, True)['data']
    list_company = company_model.objects.get_full({'sale': pk})['data']

    conditions = {
        'user': user_item.pk
    }

    sp_tiep_can_condition = tools.str_to_int(request.GET.get('sp_tiep_can', None))
    kq_cuoc_goi_condition = tools.str_to_int(request.GET.get('kq_cuoc_goi', None))
    kq_cuoc_gap_condition = tools.str_to_int(request.GET.get('kq_cuoc_gap', None))
    tt_khai_thac_condition = tools.str_to_int(request.GET.get('tt_khai_thac', None))

    if sp_tiep_can_condition is not None:
        conditions['sp_tiep_can'] = sp_tiep_can_condition
    if kq_cuoc_goi_condition is not None:
        conditions['kq_cuoc_goi'] = kq_cuoc_goi_condition
    if kq_cuoc_gap_condition is not None:
        conditions['kq_cuoc_gap'] = kq_cuoc_gap_condition
    if tt_khai_thac_condition is not None:
        conditions['tinh_trang_khai_thac'] = tt_khai_thac_condition

    ket_qua_cuoc_goi_cat = category_model.objects.get_one({'uid': 'ket-qua-cuoc-goi'}, True)['data']
    list_ket_qua_cuoc_goi = dropdown_model.objects.get_full({'category': ket_qua_cuoc_goi_cat})['data']

    ket_qua_cuoc_gap_cat = category_model.objects.get_one({'uid': 'kq-cuoc-gap'}, True)['data']
    list_ket_qua_cuoc_gap = dropdown_model.objects.get_full({'category': ket_qua_cuoc_gap_cat})['data']

    sp_tiep_can_cat = category_model.objects.get_one({'uid': 'sp-tiep-can'}, True)['data']
    list_sp_tiep_can = dropdown_model.objects.get_full({'category': sp_tiep_can_cat.pk})['data']

    tinh_trang_khai_thac_cat = category_model.objects.get_one({'uid': 'tinh-trang-khai-thac'}, True)['data']
    list_tinh_trang_khai_thac = dropdown_model.objects.get_full({'category': tinh_trang_khai_thac_cat.pk})['data']

    list_zone = zone_model.objects.get_full()['data']
    list_user = user_model.objects.get_full()['data']

    list_item = main_model.objects.get_all(conditions)['data']

    list_reported_company = []
    list_reported_company_object = main_model.objects.get_full({'user': user_item.pk}, ['cty'])['data']
    for item in list_reported_company_object:
        list_reported_company.append(int(item['cty']))

    data['js_vars'] = {}
    data['js_vars']['list_ket_qua_cuoc_goi'] = Tools.standardize_array(list_ket_qua_cuoc_goi)
    data['js_vars']['list_ket_qua_cuoc_gap'] = Tools.standardize_array(list_ket_qua_cuoc_gap)
    data['js_vars']['list_sp_tiep_can'] = Tools.standardize_array(list_sp_tiep_can)
    data['js_vars']['list_tinh_trang_khai_thac'] = Tools.standardize_array(list_tinh_trang_khai_thac)

    data['js_vars']['list_ket_qua_cuoc_goi_filter'] = Tools.standardize_array(list_ket_qua_cuoc_goi)
    data['js_vars']['list_ket_qua_cuoc_gap_filter'] = Tools.standardize_array(list_ket_qua_cuoc_gap)
    data['js_vars']['list_sp_tiep_can_filter'] = Tools.standardize_array(list_sp_tiep_can)
    data['js_vars']['list_tinh_trang_khai_thac_filter'] = Tools.standardize_array(list_tinh_trang_khai_thac)

    data['js_vars']['list_company'] = Tools.standardize_array(list_company)
    data['js_vars']['list_reported_company'] = list_reported_company
    data['js_vars']['list_zone'] = Tools.standardize_array(list_zone)
    data['js_vars']['list_user'] = Tools.standardize_array(list_user)
    data['js_vars']['list_item'] = Tools.standardize_array(list_item)

    data['js_vars']['conditions'] = conditions

    data['js_vars']['sp_tiep_can_condition'] = sp_tiep_can_condition
    data['js_vars']['kq_cuoc_goi_condition'] = kq_cuoc_goi_condition
    data['js_vars']['kq_cuoc_gap_condition'] = kq_cuoc_gap_condition
    data['js_vars']['tt_khai_thac_condition'] = tt_khai_thac_condition

    if len(data['js_vars']) != 0:
        data['js_vars'] = tools.encode_server_vars(data['js_vars'])
    else:
        data['js_vars'] = ''
    data['is_log'] = False
    data.update(csrf(request))
    return render(
        request,
        tools.get_app_name(__name__) + 'report.html',
        data,
        context_instance=RequestContext(request)
    )


def index_admin(request):
    data = {}
    conditions = {}
    pk = request.session.get('ADMIN_ID', None)
    admin_item = admin_model.objects.get_one({'pk': pk}, True)['data']

    sp_tiep_can_condition = tools.str_to_int(request.GET.get('sp_tiep_can', None))
    kq_cuoc_goi_condition = tools.str_to_int(request.GET.get('kq_cuoc_goi', None))
    kq_cuoc_gap_condition = tools.str_to_int(request.GET.get('kq_cuoc_gap', None))
    tt_khai_thac_condition = tools.str_to_int(request.GET.get('tt_khai_thac', None))

    if sp_tiep_can_condition is not None:
        conditions['sp_tiep_can'] = sp_tiep_can_condition
    if kq_cuoc_goi_condition is not None:
        conditions['kq_cuoc_goi'] = kq_cuoc_goi_condition
    if kq_cuoc_gap_condition is not None:
        conditions['kq_cuoc_gap'] = kq_cuoc_gap_condition
    if tt_khai_thac_condition is not None:
        conditions['tinh_trang_khai_thac'] = tt_khai_thac_condition

    ket_qua_cuoc_goi_cat = category_model.objects.get_one({'uid': 'ket-qua-cuoc-goi'}, True)['data']
    list_ket_qua_cuoc_goi = dropdown_model.objects.get_full({'category': ket_qua_cuoc_goi_cat})['data']

    ket_qua_cuoc_gap_cat = category_model.objects.get_one({'uid': 'kq-cuoc-gap'}, True)['data']
    list_ket_qua_cuoc_gap = dropdown_model.objects.get_full({'category': ket_qua_cuoc_gap_cat})['data']

    sp_tiep_can_cat = category_model.objects.get_one({'uid': 'sp-tiep-can'}, True)['data']
    list_sp_tiep_can = dropdown_model.objects.get_full({'category': sp_tiep_can_cat.pk})['data']

    tinh_trang_khai_thac_cat = category_model.objects.get_one({'uid': 'tinh-trang-khai-thac'}, True)['data']
    list_tinh_trang_khai_thac = dropdown_model.objects.get_full({'category': tinh_trang_khai_thac_cat.pk})['data']

    list_zone = zone_model.objects.get_full()['data']
    list_user = user_model.objects.get_full()['data']

    if admin_item.role == 'SADMIN' or admin_item.role == 'ADMIN':
        if not conditions:
            conditions = None
        list_item = main_model.objects.get_all(conditions)['data']
    else:
        zone_id = admin_item.zone.pk
        list_zone = zone_model.objects.get_full()['data']
        list_zone_pk = tools.get_list_hierarchy(zone_id, list_zone)
        list_zone_pk.append(zone_id)
        if not conditions:
            conditions = {
                'zone__in': list_zone_pk
            }
        else:
            conditions['zone__in'] = list_zone_pk
        list_item = main_model.objects.get_all(conditions)['data']
    data['js_vars'] = {}
    data['js_vars']['list_ket_qua_cuoc_goi'] = Tools.standardize_array(list_ket_qua_cuoc_goi)
    data['js_vars']['list_ket_qua_cuoc_gap'] = Tools.standardize_array(list_ket_qua_cuoc_gap)
    data['js_vars']['list_sp_tiep_can'] = Tools.standardize_array(list_sp_tiep_can)
    data['js_vars']['list_tinh_trang_khai_thac'] = Tools.standardize_array(list_tinh_trang_khai_thac)

    data['js_vars']['list_ket_qua_cuoc_goi_filter'] = Tools.standardize_array(list_ket_qua_cuoc_goi)
    data['js_vars']['list_ket_qua_cuoc_gap_filter'] = Tools.standardize_array(list_ket_qua_cuoc_gap)
    data['js_vars']['list_sp_tiep_can_filter'] = Tools.standardize_array(list_sp_tiep_can)
    data['js_vars']['list_tinh_trang_khai_thac_filter'] = Tools.standardize_array(list_tinh_trang_khai_thac)

    data['js_vars']['list_zone'] = Tools.standardize_array(list_zone)
    data['js_vars']['list_user'] = Tools.standardize_array(list_user)
    data['js_vars']['list_company'] = []
    data['js_vars']['list_item'] = Tools.standardize_array(list_item)

    data['js_vars']['conditions'] = conditions

    data['js_vars']['sp_tiep_can_condition'] = sp_tiep_can_condition
    data['js_vars']['kq_cuoc_goi_condition'] = kq_cuoc_goi_condition
    data['js_vars']['kq_cuoc_gap_condition'] = kq_cuoc_gap_condition
    data['js_vars']['tt_khai_thac_condition'] = tt_khai_thac_condition

    if len(data['js_vars']) != 0:
        data['js_vars'] = tools.encode_server_vars(data['js_vars'])
    else:
        data['js_vars'] = ''

    data['is_log'] = False
    data.update(csrf(request))
    return render(
        request,
        tools.get_app_name(__name__) + 'report_admin.html',
        data,
        context_instance=RequestContext(request)
    )


def history(request):
    data = {}
    pk = request.session.get('USER_ID', None)
    user_item = user_model.objects.get_one({'pk': pk}, True)['data']
    # list_company = company_model.objects.get_full({'sale': pk})['data']

    conditions = {
        'user': user_item.pk
    }

    sp_tiep_can_condition = tools.str_to_int(request.GET.get('sp_tiep_can', None))
    kq_cuoc_goi_condition = tools.str_to_int(request.GET.get('kq_cuoc_goi', None))
    kq_cuoc_gap_condition = tools.str_to_int(request.GET.get('kq_cuoc_gap', None))
    tt_khai_thac_condition = tools.str_to_int(request.GET.get('tt_khai_thac', None))

    if sp_tiep_can_condition is not None:
        conditions['sp_tiep_can'] = sp_tiep_can_condition
    if kq_cuoc_goi_condition is not None:
        conditions['kq_cuoc_goi'] = kq_cuoc_goi_condition
    if kq_cuoc_gap_condition is not None:
        conditions['kq_cuoc_gap'] = kq_cuoc_gap_condition
    if tt_khai_thac_condition is not None:
        conditions['tinh_trang_khai_thac'] = tt_khai_thac_condition

    ket_qua_cuoc_goi_cat = category_model.objects.get_one({'uid': 'ket-qua-cuoc-goi'}, True)['data']
    list_ket_qua_cuoc_goi = dropdown_model.objects.get_full({'category': ket_qua_cuoc_goi_cat})['data']

    ket_qua_cuoc_gap_cat = category_model.objects.get_one({'uid': 'ket-qua-cuoc-gap'}, True)['data']
    list_ket_qua_cuoc_gap = dropdown_model.objects.get_full({'category': ket_qua_cuoc_gap_cat})['data']

    sp_tiep_can_cat = category_model.objects.get_one({'uid': 'sp-tiep-can'}, True)['data']
    list_sp_tiep_can = dropdown_model.objects.get_full({'category': sp_tiep_can_cat.pk})['data']

    tinh_trang_khai_thac_cat = category_model.objects.get_one({'uid': 'tinh-trang-khai-thac'}, True)['data']
    list_tinh_trang_khai_thac = dropdown_model.objects.get_full({'category': tinh_trang_khai_thac_cat.pk})['data']

    list_zone = zone_model.objects.get_full()['data']
    list_user = user_model.objects.get_full()['data']

    list_item = report_log_model.objects.get_all(conditions)['data']
    data['js_vars'] = {}
    data['js_vars']['list_ket_qua_cuoc_goi'] = Tools.standardize_array(list_ket_qua_cuoc_goi)
    data['js_vars']['list_ket_qua_cuoc_gap'] = Tools.standardize_array(list_ket_qua_cuoc_gap)
    data['js_vars']['list_sp_tiep_can'] = Tools.standardize_array(list_sp_tiep_can)
    data['js_vars']['list_tinh_trang_khai_thac'] = Tools.standardize_array(list_tinh_trang_khai_thac)

    data['js_vars']['list_ket_qua_cuoc_goi_filter'] = Tools.standardize_array(list_ket_qua_cuoc_goi)
    data['js_vars']['list_ket_qua_cuoc_gap_filter'] = Tools.standardize_array(list_ket_qua_cuoc_gap)
    data['js_vars']['list_sp_tiep_can_filter'] = Tools.standardize_array(list_sp_tiep_can)
    data['js_vars']['list_tinh_trang_khai_thac_filter'] = Tools.standardize_array(list_tinh_trang_khai_thac)

    data['js_vars']['list_user'] = Tools.standardize_array(list_user)
    data['js_vars']['list_zone'] = Tools.standardize_array(list_zone)
    data['js_vars']['list_company'] = []
    data['js_vars']['list_item'] = Tools.standardize_array(list_item)

    data['js_vars']['conditions'] = conditions

    data['js_vars']['sp_tiep_can_condition'] = sp_tiep_can_condition
    data['js_vars']['kq_cuoc_goi_condition'] = kq_cuoc_goi_condition
    data['js_vars']['kq_cuoc_gap_condition'] = kq_cuoc_gap_condition
    data['js_vars']['tt_khai_thac_condition'] = tt_khai_thac_condition

    if len(data['js_vars']) != 0:
        data['js_vars'] = tools.encode_server_vars(data['js_vars'])
    else:
        data['js_vars'] = ''
    data['is_log'] = True
    data.update(csrf(request))
    return render(
        request,
        tools.get_app_name(__name__) + 'report_history.html',
        data,
        context_instance=RequestContext(request)
    )


def history_admin(request):
    data = {}
    conditions = {}
    pk = request.session.get('ADMIN_ID', None)
    admin_item = admin_model.objects.get_one({'pk': pk}, True)['data']

    sp_tiep_can_condition = tools.str_to_int(request.GET.get('sp_tiep_can', None))
    kq_cuoc_goi_condition = tools.str_to_int(request.GET.get('kq_cuoc_goi', None))
    kq_cuoc_gap_condition = tools.str_to_int(request.GET.get('kq_cuoc_gap', None))
    tt_khai_thac_condition = tools.str_to_int(request.GET.get('tt_khai_thac', None))

    if sp_tiep_can_condition is not None:
        conditions['sp_tiep_can'] = sp_tiep_can_condition
    if kq_cuoc_goi_condition is not None:
        conditions['kq_cuoc_goi'] = kq_cuoc_goi_condition
    if kq_cuoc_gap_condition is not None:
        conditions['kq_cuoc_gap'] = kq_cuoc_gap_condition
    if tt_khai_thac_condition is not None:
        conditions['tinh_trang_khai_thac'] = tt_khai_thac_condition

    if admin_item.role == 'SADMIN' or admin_item.role == 'ADMIN':
        if not conditions:
            conditions = None
        list_item = report_log_model.objects.get_all(conditions)['data']
    else:
        zone_id = admin_item.zone.pk
        list_zone = zone_model.objects.get_full()['data']
        list_zone_pk = tools.get_list_hierarchy(zone_id, list_zone)
        list_zone_pk.append(zone_id)
        if not conditions:
            conditions = {
                'zone__in': list_zone_pk
            }
        else:
            conditions['zone__in'] = list_zone_pk
        list_item = report_log_model.objects.get_all(conditions)['data']

    ket_qua_cuoc_goi_cat = category_model.objects.get_one({'uid': 'ket-qua-cuoc-goi'}, True)['data']
    list_ket_qua_cuoc_goi = dropdown_model.objects.get_full({'category': ket_qua_cuoc_goi_cat})['data']

    ket_qua_cuoc_gap_cat = category_model.objects.get_one({'uid': 'ket-qua-cuoc-gap'}, True)['data']
    list_ket_qua_cuoc_gap = dropdown_model.objects.get_full({'category': ket_qua_cuoc_gap_cat})['data']

    sp_tiep_can_cat = category_model.objects.get_one({'uid': 'sp-tiep-can'}, True)['data']
    list_sp_tiep_can = dropdown_model.objects.get_full({'category': sp_tiep_can_cat.pk})['data']

    tinh_trang_khai_thac_cat = category_model.objects.get_one({'uid': 'tinh-trang-khai-thac'}, True)['data']
    list_tinh_trang_khai_thac = dropdown_model.objects.get_full({'category': tinh_trang_khai_thac_cat.pk})['data']

    list_zone = zone_model.objects.get_full()['data']
    list_user = user_model.objects.get_full()['data']

    data['js_vars'] = {}
    data['js_vars']['list_ket_qua_cuoc_goi'] = Tools.standardize_array(list_ket_qua_cuoc_goi)
    data['js_vars']['list_ket_qua_cuoc_gap'] = Tools.standardize_array(list_ket_qua_cuoc_gap)
    data['js_vars']['list_sp_tiep_can'] = Tools.standardize_array(list_sp_tiep_can)
    data['js_vars']['list_tinh_trang_khai_thac'] = Tools.standardize_array(list_tinh_trang_khai_thac)

    data['js_vars']['list_ket_qua_cuoc_goi_filter'] = Tools.standardize_array(list_ket_qua_cuoc_goi)
    data['js_vars']['list_ket_qua_cuoc_gap_filter'] = Tools.standardize_array(list_ket_qua_cuoc_gap)
    data['js_vars']['list_sp_tiep_can_filter'] = Tools.standardize_array(list_sp_tiep_can)
    data['js_vars']['list_tinh_trang_khai_thac_filter'] = Tools.standardize_array(list_tinh_trang_khai_thac)

    data['js_vars']['list_zone'] = Tools.standardize_array(list_zone)
    data['js_vars']['list_user'] = Tools.standardize_array(list_user)
    data['js_vars']['list_company'] = []
    data['js_vars']['list_item'] = Tools.standardize_array(list_item)

    data['js_vars']['conditions'] = conditions

    data['js_vars']['sp_tiep_can_condition'] = sp_tiep_can_condition
    data['js_vars']['kq_cuoc_goi_condition'] = kq_cuoc_goi_condition
    data['js_vars']['kq_cuoc_gap_condition'] = kq_cuoc_gap_condition
    data['js_vars']['tt_khai_thac_condition'] = tt_khai_thac_condition

    if len(data['js_vars']) != 0:
        data['js_vars'] = tools.encode_server_vars(data['js_vars'])
    else:
        data['js_vars'] = ''

    data['is_log'] = True
    data.update(csrf(request))
    return render(
        request,
        tools.get_app_name(__name__) + 'report_admin.html',
        data,
        context_instance=RequestContext(request)
    )


def export(request):
    data = {}
    user_id = request.session.get('USER_ID', None)
    if user_id is not None:
        data['extends_template'] = 'back/base_user.html'
    else:
        data['extends_template'] = 'back/base_admin.html'
    data.update(csrf(request))
    return render(
        request,
        tools.get_app_name(__name__) + 'report_export.html',
        data,
        context_instance=RequestContext(request)
    )


def history_export(request):
    data = {}
    user_id = request.session.get('USER_ID', None)
    if user_id is not None:
        data['extends_template'] = 'back/base_user.html'
    else:
        data['extends_template'] = 'back/base_admin.html'
    data.update(csrf(request))
    return render(
        request,
        tools.get_app_name(__name__) + 'report_history_export.html',
        data,
        context_instance=RequestContext(request)
    )


def call_filter(request):
    try:
        data = {}
        user_id = request.session.get('USER_ID', None)
        admin_id = request.session.get('ADMIN_ID', None)
        if user_id is not None:
            data['user_type'] = 'USER'
            data['extends_template'] = 'back/base_user.html'

            user_item = user_model.objects.get_one({'pk': user_id}, True)['data']
            list_admin_full = []
            list_tm = user_model.objects.get_full({'pk': int(user_id)})['data']
            list_hub = []
            data['js_vars'] = {}

            data['js_vars']['list_admin_full'] = Tools.standardize_array(list_admin_full)
            data['js_vars']['isAdmin'] = 0
            data['js_vars']['level'] = user_item.zone.level
            data['js_vars']['list_tm'] = Tools.standardize_array(list_tm)
            data['js_vars']['list_hub'] = list_hub
            data['js_vars']['report_type'] = 'call'
        else:
            data['user_type'] = 'ADMIN'
            data['extends_template'] = 'back/base_admin.html'
            admin_item = admin_model.objects.get_one({'pk': admin_id}, True)['data']
            list_admin_full = admin_model.objects.get_full()['data']
            if admin_item.level == 2:
                zone_id = admin_item.zone.pk

                list_tm = user_model.objects.get_full({'zone_id': zone_id})['data']
                list_hub = zone_model.objects.get_full({'pk': zone_id})['data']
            elif admin_item.level == 1:
                zone_id = admin_item.zone.pk
                list_zone_full = zone_model.objects.get_full()['data']
                list_zone_hierarchy = tools.get_list_hierarchy(zone_id, list_zone_full)
                list_zone_hierarchy.append(zone_id)

                list_tm = user_model.objects.get_full({'zone_id__in': list_zone_hierarchy})['data']
                list_hub = zone_model.objects.get_full({'parent_id__in': list_zone_hierarchy})['data']
            elif admin_item.level == 0:

                list_tm = user_model.objects.get_full()['data']
                # list_hub = zone_model.objects.get_full({'level': 2})['data']
                list_hub = zone_model.objects.get_full()['data']
            else:
                raise Http404
            # print list_tm
            data['js_vars'] = {}
            data['js_vars']['list_admin_full'] = Tools.standardize_array(list_admin_full)
            data['js_vars']['isAdmin'] = 1
            data['js_vars']['report_type'] = 'call'
            if admin_item.zone is not None:
                data['js_vars']['level'] = admin_item.zone.level
            else:
                data['js_vars']['level'] = 0
            data['js_vars']['list_tm'] = Tools.standardize_array(list_tm)
            data['js_vars']['list_hub'] = Tools.standardize_array(list_hub)

        if len(data['js_vars']) != 0:
            data['js_vars'] = tools.encode_server_vars(data['js_vars'])
        else:
            data['js_vars'] = ''

        data.update(csrf(request))
        return render(
            request,
            tools.get_app_name(__name__) + 'report_call_filter.html',
            data,
            context_instance=RequestContext(request)
        )
    except Exception as e:
        error = tools.return_exception(e)
        print(error)
        return error


def meet_filter(request):
    data = {}
    user_id = request.session.get('USER_ID', None)
    admin_id = request.session.get('ADMIN_ID', None)
    if user_id is not None:
        data['user_type'] = 'USER'
        data['extends_template'] = 'back/base_user.html'

        user_item = user_model.objects.get_one({'pk': user_id}, True)['data']
        list_admin_full = []
        list_tm = user_model.objects.get_full({'pk': int(user_id)})['data']
        list_hub = []
        data['js_vars'] = {}

        data['js_vars']['list_admin_full'] = Tools.standardize_array(list_admin_full)
        data['js_vars']['isAdmin'] = 0
        data['js_vars']['level'] = user_item.zone.level
        data['js_vars']['list_tm'] = Tools.standardize_array(list_tm)
        data['js_vars']['list_hub'] = list_hub
        data['js_vars']['report_type'] = 'meet'
    else:
        data['user_type'] = 'ADMIN'
        data['extends_template'] = 'back/base_admin.html'
        admin_item = admin_model.objects.get_one({'pk': admin_id}, True)['data']
        list_admin_full = admin_model.objects.get_full()['data']
        if admin_item.level == 2:
            zone_id = admin_item.zone.pk

            list_tm = user_model.objects.get_full({'zone_id': zone_id})['data']
            list_hub = zone_model.objects.get_full({'pk': zone_id})['data']
        elif admin_item.level == 1:
            zone_id = admin_item.zone.pk
            list_zone_full = zone_model.objects.get_full()['data']
            list_zone_hierarchy = tools.get_list_hierarchy(zone_id, list_zone_full)
            list_zone_hierarchy.append(zone_id)

            list_tm = user_model.objects.get_full({'zone_id__in': list_zone_hierarchy})['data']
            list_hub = zone_model.objects.get_full({'parent_id__in': list_zone_hierarchy})['data']
        elif admin_item.level == 0:

            list_tm = user_model.objects.get_full()['data']
            # list_hub = zone_model.objects.get_full({'level': 2})['data']
            list_hub = zone_model.objects.get_full()['data']
        else:
            raise Http404
        # print list_tm
        data['js_vars'] = {}
        data['js_vars']['list_admin_full'] = Tools.standardize_array(list_admin_full)
        data['js_vars']['isAdmin'] = 1
        data['js_vars']['report_type'] = 'meet'
        if admin_item.zone is not None:
            data['js_vars']['level'] = admin_item.zone.level
        else:
            data['js_vars']['level'] = 0
        data['js_vars']['list_tm'] = Tools.standardize_array(list_tm)
        data['js_vars']['list_hub'] = Tools.standardize_array(list_hub)

    if len(data['js_vars']) != 0:
        data['js_vars'] = tools.encode_server_vars(data['js_vars'])
    else:
        data['js_vars'] = ''

    data.update(csrf(request))
    return render(
        request,
        tools.get_app_name(__name__) + 'report_meet_filter.html',
        data,
        context_instance=RequestContext(request)
    )


def mining_filter(request):
    data = {}
    user_id = request.session.get('USER_ID', None)
    admin_id = request.session.get('ADMIN_ID', None)
    if user_id is not None:
        data['user_type'] = 'USER'
        data['extends_template'] = 'back/base_user.html'

        user_item = user_model.objects.get_one({'pk': user_id}, True)['data']
        list_admin_full = []
        list_tm = user_model.objects.get_full({'pk': int(user_id)})['data']
        list_hub = []
        data['js_vars'] = {}

        data['js_vars']['list_admin_full'] = Tools.standardize_array(list_admin_full)
        data['js_vars']['isAdmin'] = 0
        data['js_vars']['level'] = user_item.zone.level
        data['js_vars']['list_tm'] = Tools.standardize_array(list_tm)
        data['js_vars']['list_hub'] = list_hub
        data['js_vars']['report_type'] = 'mining'
    else:
        data['user_type'] = 'ADMIN'
        data['extends_template'] = 'back/base_admin.html'

        admin_item = admin_model.objects.get_one({'pk': admin_id}, True)['data']
        list_admin_full = admin_model.objects.get_full()['data']
        if admin_item.level == 2:
            zone_id = admin_item.zone.pk

            list_tm = user_model.objects.get_full({'zone_id': zone_id})['data']
            list_hub = zone_model.objects.get_full({'pk': zone_id})['data']
        elif admin_item.level == 1:
            zone_id = admin_item.zone.pk
            list_zone_full = zone_model.objects.get_full()['data']
            list_zone_hierarchy = tools.get_list_hierarchy(zone_id, list_zone_full)
            list_zone_hierarchy.append(zone_id)

            list_tm = user_model.objects.get_full({'zone_id__in': list_zone_hierarchy})['data']
            list_hub = zone_model.objects.get_full({'parent_id__in': list_zone_hierarchy})['data']
        elif admin_item.level == 0:

            list_tm = user_model.objects.get_full()['data']
            # list_hub = zone_model.objects.get_full({'level': 2})['data']
            list_hub = zone_model.objects.get_full()['data']
        else:
            raise Http404
        # print list_tm
        data['js_vars'] = {}
        data['js_vars']['list_admin_full'] = Tools.standardize_array(list_admin_full)
        data['js_vars']['isAdmin'] = 1
        data['js_vars']['report_type'] = 'mining'
        if admin_item.zone is not None:
            data['js_vars']['level'] = admin_item.zone.level
        else:
            data['js_vars']['level'] = 0
        data['js_vars']['list_tm'] = Tools.standardize_array(list_tm)
        data['js_vars']['list_hub'] = Tools.standardize_array(list_hub)

    if len(data['js_vars']) != 0:
        data['js_vars'] = tools.encode_server_vars(data['js_vars'])
    else:
        data['js_vars'] = ''

    data.update(csrf(request))
    return render(
        request,
        tools.get_app_name(__name__) + 'report_mining_filter.html',
        data,
        context_instance=RequestContext(request)
    )


def approve_filter(request):
    data = {}
    user_id = request.session.get('USER_ID', None)
    admin_id = request.session.get('ADMIN_ID', None)
    if user_id is not None:
        data['user_type'] = 'USER'
        data['extends_template'] = 'back/base_user.html'

        user_item = user_model.objects.get_one({'pk': user_id}, True)['data']
        list_admin_full = []
        list_tm = user_model.objects.get_full({'pk': int(user_id)})['data']
        list_hub = []
        data['js_vars'] = {}

        data['js_vars']['list_admin_full'] = Tools.standardize_array(list_admin_full)
        data['js_vars']['isAdmin'] = 0
        data['js_vars']['level'] = user_item.zone.level
        data['js_vars']['list_tm'] = Tools.standardize_array(list_tm)
        data['js_vars']['list_hub'] = list_hub
        data['js_vars']['report_type'] = 'approve'
    else:
        data['user_type'] = 'ADMIN'
        data['extends_template'] = 'back/base_admin.html'

        admin_item = admin_model.objects.get_one({'pk': admin_id}, True)['data']
        list_admin_full = admin_model.objects.get_full()['data']
        if admin_item.level == 2:
            zone_id = admin_item.zone.pk

            list_tm = user_model.objects.get_full({'zone_id': zone_id})['data']
            list_hub = zone_model.objects.get_full({'pk': zone_id})['data']
        elif admin_item.level == 1:
            zone_id = admin_item.zone.pk
            list_zone_full = zone_model.objects.get_full()['data']
            list_zone_hierarchy = tools.get_list_hierarchy(zone_id, list_zone_full)
            list_zone_hierarchy.append(zone_id)

            list_tm = user_model.objects.get_full({'zone_id__in': list_zone_hierarchy})['data']
            list_hub = zone_model.objects.get_full({'parent_id__in': list_zone_hierarchy})['data']
        elif admin_item.level == 0:

            list_tm = user_model.objects.get_full()['data']
            # list_hub = zone_model.objects.get_full({'level': 2})['data']
            list_hub = zone_model.objects.get_full()['data']
        else:
            raise Http404
        # print list_tm
        data['js_vars'] = {}
        data['js_vars']['list_admin_full'] = Tools.standardize_array(list_admin_full)
        data['js_vars']['isAdmin'] = 1
        data['js_vars']['report_type'] = 'approve'
        if admin_item.zone is not None:
            data['js_vars']['level'] = admin_item.zone.level
        else:
            data['js_vars']['level'] = 0
        data['js_vars']['list_tm'] = Tools.standardize_array(list_tm)
        data['js_vars']['list_hub'] = Tools.standardize_array(list_hub)

    if len(data['js_vars']) != 0:
        data['js_vars'] = tools.encode_server_vars(data['js_vars'])
    else:
        data['js_vars'] = ''

    data.update(csrf(request))
    return render(
        request,
        tools.get_app_name(__name__) + 'report_approve_filter.html',
        data,
        context_instance=RequestContext(request)
    )


def export_call_filter(request):
    report_tpye = 'call'
    user_id = request.session.get('USER_ID', None)
    admin_id = request.session.get('ADMIN_ID', None)

    only_view = True
    filter_type = tools.str_to_str(request.POST.get('filter_type', None))
    if filter_type is None:
        filter_type = tools.str_to_str(request.GET.get('filter_type', None))
        only_view = False

    input_filter_tm = tools.str_to_int(request.POST.get('input_filter_tm', None))
    if input_filter_tm is None:
        input_filter_tm = tools.str_to_int(request.GET.get('input_filter_tm', None))

    input_filter_hub = tools.str_to_int(request.POST.get('input_filter_hub', None))
    if input_filter_hub is None:
        input_filter_hub = tools.str_to_int(request.GET.get('input_filter_hub', None))

    range_type = tools.str_to_str(request.POST.get('range_type', None))
    if range_type is None:
        range_type = tools.str_to_str(request.GET.get('range_type', None))

    start_date = tools.str_to_str(request.POST.get('start_date', None))
    if start_date is None:
        start_date = tools.str_to_str(request.GET.get('start_date', None))

    end_date = tools.str_to_str(request.POST.get('end_date', None))
    if end_date is None:
        end_date = tools.str_to_str(request.GET.get('end_date', None))

    try:
        if user_id is not None:
            user_item = user_model.objects.get_one({'pk': user_id}, True)['data']

            if user_item is not None:
                display_uid = user_item.uid
                exporter = user_item.name
            else:
                display_uid = ''
                exporter = ''
        if admin_id is not None:
            admin_item = admin_model.objects.get_one({'pk': admin_id}, True)['data']
            if admin_item is not None:
                display_uid = admin_item.uid
                exporter = admin_item.name
            else:
                display_uid = ''
                exporter = ''

        params = []
        query_str = 'WHERE 1=0'
        if filter_type == 'tm':
            query_str = 'WHERE r.user=%s'
            params.append(str(input_filter_tm))
        elif filter_type == 'hub':
            list_zone = zone_model.objects.get_full()['data']
            hub_hierarchy = tools.get_list_hierarchy(input_filter_hub, list_zone)
            hub_hierarchy.append(input_filter_hub)
            query_str = "WHERE r.zone_id IN " + "(" + ", ".join(str(v) for v in hub_hierarchy) + ")"
        elif filter_type == 'all':
            if admin_item.zone is None:
                query_str = 'WHERE 1=1'
            else:
                list_zone = zone_model.objects.get_full()['data']
                hub_hierarchy = tools.get_list_hierarchy(admin_item.zone.pk, list_zone)
                hub_hierarchy.append(input_filter_hub)
                query_str = "WHERE r.zone_id IN " + "(" + ", ".join(str(v) for v in hub_hierarchy) + ")"
        else:
            pass

        if range_type != 'custom':
            range_date = tools.get_basic_range_date(start_date, range_type, True)
            fname = 'list-' + report_tpye + '-report-' + str(start_date) + '.xlsx'
        else:
            range_date = tools.get_custom_range_date(start_date, end_date, True)
            fname = 'list-' + report_tpye + '-report-' + range_date[0] + '-to-' + range_date[1] + '.xlsx'

        params += range_date
        sql_str = "SELECT "\
            "r.id, u.name as user_name, "\
            "SUM(CASE WHEN r.kq_cuoc_goi=378 THEN 1 ELSE 0 END) as chua_tiep_can_duoc_lanh_dao_don_vi, "\
            "SUM(CASE WHEN r.kq_cuoc_goi=379 THEN 1 ELSE 0 END) as hoan_thanh_da_co_kq_tu_risk, "\
            "SUM(CASE WHEN r.kq_cuoc_goi=380 THEN 1 ELSE 0 END) as kh_can_tiep_can_lai, "\
            "SUM(CASE WHEN r.kq_cuoc_goi=381 THEN 1 ELSE 0 END) as kh_khong_quan_tam, "\
            "SUM(CASE WHEN r.kq_cuoc_goi=382 THEN 1 ELSE 0 END) as kh_quan_tam_fail_tieu_chi_so_bo, "\
            "SUM(CASE WHEN r.kq_cuoc_goi=383 THEN 1 ELSE 0 END) as kh_quan_tam_pass_tieu_chi_so_bo_da_dat_lich, "\
            "SUM(CASE WHEN r.kq_cuoc_goi=384 THEN 1 ELSE 0 END) as kh_khong_tra_loi_dt, "\
            "SUM(CASE WHEN r.kq_cuoc_goi=385 THEN 1 ELSE 0 END) as thong_tin_ve_so_dt_sai, "\
            "SUM(CASE WHEN r.kq_cuoc_goi=386 THEN 1 ELSE 0 END) as cong_ty_nha_nuoc, "\
            "SUM(CASE WHEN r.kq_cuoc_goi=387 THEN 1 ELSE 0 END) as nganh_nghe_han_che, "\
            "SUM(CASE WHEN r.kq_cuoc_goi=388 THEN 1 ELSE 0 END) as khac "\
            "FROM report AS r "\
            "INNER JOIN dropdown AS d ON r.kq_cuoc_goi = d.id "\
            "INNER JOIN \"user\" AS u ON r.user = u.id " + query_str + " "\
            "AND Date(r.date_edit) >= Date(%s) "\
            "AND Date(r.date_edit) <= Date(%s) "\
            "GROUP BY u.id, r.id"
        list_item = main_model.objects.raw(sql_str, params)
        map_title = OrderedDict([
            ('user_name', 'Tên TM'),
            ('chua_tiep_can_duoc_lanh_dao_don_vi', 'Chưa tiếp cận được lãnh đạo đơn vị'),
            ('hoan_thanh_da_co_kq_tu_risk', 'Hoàn thành đã có kq từ risk'),
            ('kh_can_tiep_can_lai', 'KH cần tiếp cận lại'),
            ('kh_khong_quan_tam', 'KH không quan tâm'),
            ('kh_quan_tam_fail_tieu_chi_so_bo', 'KH quan tâm, fail tiêu chí sơ bộ'),
            ('kh_quan_tam_pass_tieu_chi_so_bo_da_dat_lich', 'KH quan tâm, pass tiêu chí sơ bộ, đã đặt lịch'),
            ('kh_khong_tra_loi_dt', 'KH không trả lời điện thoại'),
            ('thong_tin_ve_so_dt_sai', 'Thông tin về số điện thoại sai'),
            ('cong_ty_nha_nuoc', 'Công ty nhà nước'),
            ('nganh_nghe_han_che', 'Ngành nghề hạn chế'),
            ('khac', 'Khác')
        ])
        # return(getattr(map_title[0], 'user_name'))

        # for key, value in map_title.items():
        #    print(key)
        result = []
        result_total = [0] * len(map_title)
        result_title = []
        for title in list(map_title.values()):
            result_title.append(title)

        for item in list_item:
            result_item = []
            index = 0
            for key, value in map_title.items():
                result_item.append(getattr(item, key))
                if index > 0:
                    result_total[index] += getattr(item, key)
                index += 1
            result.append(result_item)
        result_total[0] = 'Total'

        # return 'test'
        full_result = copy.deepcopy(result)
        full_result.insert(0, result_title)
        full_result.append(result_total)

        if only_view is True:
            return StreamingHttpResponse(
                tools.encode_server_vars({
                    'data': {
                        'report': result,
                        'report_total': result_total,
                        'report_title': result_title
                    },
                    'error': None
                }),
                content_type='application/json'
            )
        else:
            fname = display_uid + '-' + fname
            output = io.BytesIO()
            wb = xlsxwriter.Workbook(output, {'in_memory': True})
            ws = wb.add_worksheet('Call report')

            start_row = 4

            ws.write(0, 0, 'BÁO CÁO KẾT QUẢ CUỘC GỌI')
            ws.write(1, 0, 'Ngày: ' + tools.get_date_part(str(range_date[0])) + ' đến ' + tools.get_date_part(str(range_date[1])))
            ws.write(2, 0, 'Người export: ' + exporter)
            # print result
            if len(full_result) > 0:
                for i, row in enumerate(full_result):
                    for j, item in enumerate(row):
                        ws.write(i + start_row + 1, j, item)
            wb.close()
            output.seek(0)
            response = HttpResponse(output.read(), content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            response['Content-Disposition'] = "attachment; filename=" + fname

            return response
    except Exception as e:
        error = tools.return_exception(e)
        # print(error)
        return error


def export_meet_filter(request):
    report_tpye = 'meet'
    user_id = request.session.get('USER_ID', None)
    admin_id = request.session.get('ADMIN_ID', None)

    only_view = True
    filter_type = tools.str_to_str(request.POST.get('filter_type', None))
    if filter_type is None:
        filter_type = tools.str_to_str(request.GET.get('filter_type', None))
        only_view = False

    input_filter_tm = tools.str_to_int(request.POST.get('input_filter_tm', None))
    if input_filter_tm is None:
        input_filter_tm = tools.str_to_int(request.GET.get('input_filter_tm', None))

    input_filter_hub = tools.str_to_int(request.POST.get('input_filter_hub', None))
    if input_filter_hub is None:
        input_filter_hub = tools.str_to_int(request.GET.get('input_filter_hub', None))

    range_type = tools.str_to_str(request.POST.get('range_type', None))
    if range_type is None:
        range_type = tools.str_to_str(request.GET.get('range_type', None))

    start_date = tools.str_to_str(request.POST.get('start_date', None))
    if start_date is None:
        start_date = tools.str_to_str(request.GET.get('start_date', None))

    end_date = tools.str_to_str(request.POST.get('end_date', None))
    if end_date is None:
        end_date = tools.str_to_str(request.GET.get('end_date', None))

    try:
        if user_id is not None:
            user_item = user_model.objects.get_one({'pk': user_id}, True)['data']

            if user_item is not None:
                display_uid = user_item.uid
                exporter = user_item.name
            else:
                display_uid = ''
                exporter = ''
        if admin_id is not None:
            admin_item = admin_model.objects.get_one({'pk': admin_id}, True)['data']
            if admin_item is not None:
                display_uid = admin_item.uid
                exporter = admin_item.name
            else:
                display_uid = ''
                exporter = ''

        params = []
        query_str = 'WHERE 1=0'
        if filter_type == 'tm':
            query_str = 'WHERE report.user=%s'
            params.append(str(input_filter_tm))
        elif filter_type == 'hub':
            list_zone = zone_model.objects.get_full()['data']
            hub_hierarchy = tools.get_list_hierarchy(input_filter_hub, list_zone)
            hub_hierarchy.append(input_filter_hub)
            query_str = "WHERE report.zone_id IN " + "(" + ", ".join(str(v) for v in hub_hierarchy) + ")"
        elif filter_type == 'all':
            if admin_item.zone is None:
                query_str = 'WHERE 1=1'
            else:
                list_zone = zone_model.objects.get_full()['data']
                hub_hierarchy = tools.get_list_hierarchy(admin_item.zone.pk, list_zone)
                hub_hierarchy.append(input_filter_hub)
                query_str = "WHERE report.zone_id IN " + "(" + ", ".join(str(v) for v in hub_hierarchy) + ")"
        else:
            pass

        if range_type != 'custom':
            range_date = tools.get_basic_range_date(start_date, range_type, True)
            fname = 'list-' + report_tpye + '-report-' + str(start_date) + '.xlsx'
        else:
            range_date = tools.get_custom_range_date(start_date, end_date, True)
            fname = 'list-' + report_tpye + '-report-' + range_date[0] + '-to-' + range_date[1] + '.xlsx'

        params += range_date
        sql_str = "SELECT "\
            "r.id, u.name as user_name, "\
            "SUM(CASE WHEN r.kq_cuoc_gap=396 THEN 1 ELSE 0 END) as can_tiep_can_lai, "\
            "SUM(CASE WHEN r.kq_cuoc_gap=395 THEN 1 ELSE 0 END) as kh_tu_choi, "\
            "SUM(CASE WHEN r.kq_cuoc_gap=394 THEN 1 ELSE 0 END) as chot_ban_duoc "\
            "FROM report as r "\
            "INNER JOIN dropdown as d ON r.kq_cuoc_gap = d.id "\
            "INNER JOIN \"user\" as u ON r.user = u.id " + query_str + " "\
            "AND Date(r.date_edit) >= Date(%s) "\
            "AND Date(r.date_edit) <= Date(%s) "\
            "GROUP BY u.id, r.id"
        list_item = main_model.objects.raw(sql_str, params)
        map_title = OrderedDict([
            ('user_name', 'Tên TM'),
            ('can_tiep_can_lai', 'Cần tiếp cận lại'),
            ('kh_tu_choi', 'KH Từ chối'),
            ('chot_ban_duoc', 'Chốt bán được')
        ])

        result = []
        result_total = [0] * len(map_title)
        result_title = []
        for title in list(map_title.values()):
            result_title.append(title)
        for item in list_item:
            result_item = []
            index = 0
            for key, value in map_title.items():
                result_item.append(getattr(item, key))
                if index > 0:
                    result_total[index] += getattr(item, key)
                index += 1
            result.append(result_item)
        result_total[0] = 'Total'

        full_result = copy.deepcopy(result)
        full_result.insert(0, result_title)
        full_result.append(result_total)

        if only_view is True:
            return StreamingHttpResponse(
                tools.encode_server_vars({
                    'data': {
                        'report': result,
                        'report_total': result_total,
                        'report_title': result_title
                    },
                    'error': None
                }),
                content_type='application/json'
            )
        else:
            fname = display_uid + '-' + fname
            output = io.BytesIO()
            wb = xlsxwriter.Workbook(output, {'in_memory': True})
            ws = wb.add_worksheet('Call report')

            start_row = 4

            ws.write(0, 0, 'BÁO CÁO KẾT QUẢ CUỘC GẶP')
            ws.write(1, 0, 'Ngày: ' + tools.get_date_part(str(range_date[0])) + ' đến ' + tools.get_date_part(str(range_date[1])))
            ws.write(2, 0, 'Người export: ' + exporter)
            # print result
            if len(full_result) > 0:
                for i, row in enumerate(full_result):
                    for j, item in enumerate(row):
                        ws.write(i + start_row + 1, j, item)
            wb.close()
            output.seek(0)
            response = HttpResponse(output.read(), content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            response['Content-Disposition'] = "attachment; filename=" + fname

            return response
    except Exception as e:
        error = tools.return_exception(e)
        # print(error)
        return error


def export_mining_filter(request):
    report_tpye = 'mining'
    user_id = request.session.get('USER_ID', None)
    admin_id = request.session.get('ADMIN_ID', None)

    only_view = True
    filter_type = tools.str_to_str(request.POST.get('filter_type', None))
    if filter_type is None:
        filter_type = tools.str_to_str(request.GET.get('filter_type', None))
        only_view = False

    input_filter_tm = tools.str_to_int(request.POST.get('input_filter_tm', None))
    if input_filter_tm is None:
        input_filter_tm = tools.str_to_int(request.GET.get('input_filter_tm', None))

    input_filter_hub = tools.str_to_int(request.POST.get('input_filter_hub', None))
    if input_filter_hub is None:
        input_filter_hub = tools.str_to_int(request.GET.get('input_filter_hub', None))

    range_type = tools.str_to_str(request.POST.get('range_type', None))
    if range_type is None:
        range_type = tools.str_to_str(request.GET.get('range_type', None))

    start_date = tools.str_to_str(request.POST.get('start_date', None))
    if start_date is None:
        start_date = tools.str_to_str(request.GET.get('start_date', None))

    end_date = tools.str_to_str(request.POST.get('end_date', None))
    if end_date is None:
        end_date = tools.str_to_str(request.GET.get('end_date', None))

    try:
        if user_id is not None:
            user_item = user_model.objects.get_one({'pk': user_id}, True)['data']

            if user_item is not None:
                display_uid = user_item.uid
                exporter = user_item.name
            else:
                display_uid = ''
                exporter = ''
        if admin_id is not None:
            admin_item = admin_model.objects.get_one({'pk': admin_id}, True)['data']
            if admin_item is not None:
                display_uid = admin_item.uid
                exporter = admin_item.name
            else:
                display_uid = ''
                exporter = ''

        params = []
        query_str = 'WHERE 1=0'
        if filter_type == 'tm':
            query_str = 'WHERE report.user=%s'
            params.append(str(input_filter_tm))
        elif filter_type == 'hub':
            list_zone = zone_model.objects.get_full()['data']
            hub_hierarchy = tools.get_list_hierarchy(input_filter_hub, list_zone)
            hub_hierarchy.append(input_filter_hub)
            query_str = "WHERE report.zone_id IN " + "(" + ", ".join(str(v) for v in hub_hierarchy) + ")"
        elif filter_type == 'all':
            if admin_item.zone is None:
                query_str = 'WHERE 1=1'
            else:
                list_zone = zone_model.objects.get_full()['data']
                hub_hierarchy = tools.get_list_hierarchy(admin_item.zone.pk, list_zone)
                hub_hierarchy.append(input_filter_hub)
                query_str = "WHERE report.zone_id IN " + "(" + ", ".join(str(v) for v in hub_hierarchy) + ")"
        else:
            pass

        if range_type != 'custom':
            range_date = tools.get_basic_range_date(start_date, range_type, True)
            fname = 'list-' + report_tpye + '-report-' + str(start_date) + '.xlsx'
        else:
            range_date = tools.get_custom_range_date(start_date, end_date, True)
            fname = 'list-' + report_tpye + '-report-' + range_date[0] + '-to-' + range_date[1] + '.xlsx'

        params += range_date
        sql_str = "SELECT "\
            "r.id, c.cty as cus_name, u.name as user_name, d.title as kq_cuoc_gap "\
            "FROM report as r "\
            "INNER JOIN dropdown as d ON r.kq_cuoc_gap = d.id "\
            "INNER JOIN \"user\" as u ON r.user = u.id "\
            "INNER JOIN company as c ON r.cty_id = c.id " + query_str + " "\
            "AND Date(r.date_edit) >= Date(%s) "\
            "AND Date(r.date_edit) <= Date(%s) "
        list_item = main_model.objects.raw(sql_str, params)

        map_title = OrderedDict([
            ('cus_name', 'Tên KH'),
            ('user_name', 'Tên TM'),
            ('kq_cuoc_gap', 'Kết quả cuộc gặp')
        ])

        result = []
        result_title = []
        for title in list(map_title.values()):
            result_title.append(title)
        for item in list_item:
            result_item = []
            for key, value in map_title.items():
                result_item.append(getattr(item, key))
            result.append(result_item)

        full_result = copy.deepcopy(result)
        full_result.insert(0, result_title)

        if only_view is True:
            return StreamingHttpResponse(
                tools.encode_server_vars({
                    'data': {
                        'report': result,
                        'report_title': result_title
                    },
                    'error': None
                }),
                content_type='application/json'
            )
        else:
            fname = display_uid + '-' + fname
            output = io.BytesIO()
            wb = xlsxwriter.Workbook(output, {'in_memory': True})
            ws = wb.add_worksheet('Call report')

            start_row = 4

            ws.write(0, 0, 'BÁO CÁO KẾT QUẢ KHAI THÁC KH')
            ws.write(1, 0, 'Ngày: ' + tools.get_date_part(str(range_date[0])) + ' đến ' + tools.get_date_part(str(range_date[1])))
            ws.write(2, 0, 'Người export: ' + exporter)
            # print result
            if len(full_result) > 0:
                for i, row in enumerate(full_result):
                    for j, item in enumerate(row):
                        ws.write(i + start_row + 1, j, item)
            wb.close()
            output.seek(0)
            response = HttpResponse(output.read(), content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            response['Content-Disposition'] = "attachment; filename=" + fname

            return response
    except Exception as e:
        error = tools.return_exception(e)
        # print(error)
        return error


def export_approve_filter(request):
    report_tpye = 'approve'
    user_id = request.session.get('USER_ID', None)
    admin_id = request.session.get('ADMIN_ID', None)

    only_view = True
    filter_type = tools.str_to_str(request.POST.get('filter_type', None))
    if filter_type is None:
        filter_type = tools.str_to_str(request.GET.get('filter_type', None))
        only_view = False

    input_filter_tm = tools.str_to_int(request.POST.get('input_filter_tm', None))
    if input_filter_tm is None:
        input_filter_tm = tools.str_to_int(request.GET.get('input_filter_tm', None))

    input_filter_hub = tools.str_to_int(request.POST.get('input_filter_hub', None))
    if input_filter_hub is None:
        input_filter_hub = tools.str_to_int(request.GET.get('input_filter_hub', None))

    range_type = tools.str_to_str(request.POST.get('range_type', None))
    if range_type is None:
        range_type = tools.str_to_str(request.GET.get('range_type', None))

    start_date = tools.str_to_str(request.POST.get('start_date', None))
    if start_date is None:
        start_date = tools.str_to_str(request.GET.get('start_date', None))

    end_date = tools.str_to_str(request.POST.get('end_date', None))
    if end_date is None:
        end_date = tools.str_to_str(request.GET.get('end_date', None))

    try:
        if user_id is not None:
            user_item = user_model.objects.get_one({'pk': user_id}, True)['data']

            if user_item is not None:
                display_uid = user_item.uid
                exporter = user_item.name
            else:
                display_uid = ''
                exporter = ''
        if admin_id is not None:
            admin_item = admin_model.objects.get_one({'pk': admin_id}, True)['data']
            if admin_item is not None:
                display_uid = admin_item.uid
                exporter = admin_item.name
            else:
                display_uid = ''
                exporter = ''

        params = []
        query_str = 'WHERE 1=0'
        if filter_type == 'tm':
            query_str = 'WHERE r.user=%s'
            params.append(str(input_filter_tm))
        elif filter_type == 'hub':
            list_zone = zone_model.objects.get_full()['data']
            hub_hierarchy = tools.get_list_hierarchy(input_filter_hub, list_zone)
            hub_hierarchy.append(input_filter_hub)
            query_str = "WHERE r.zone_id IN " + "(" + ", ".join(str(v) for v in hub_hierarchy) + ")"
        elif filter_type == 'all':
            if admin_item.zone is None:
                query_str = 'WHERE 1=1'
            else:
                list_zone = zone_model.objects.get_full()['data']
                hub_hierarchy = tools.get_list_hierarchy(admin_item.zone.pk, list_zone)
                hub_hierarchy.append(input_filter_hub)
                query_str = "WHERE r.zone_id IN " + "(" + ", ".join(str(v) for v in hub_hierarchy) + ")"
        else:
            pass

        if range_type != 'custom':
            range_date = tools.get_basic_range_date(start_date, range_type, True)
            fname = 'list-' + report_tpye + '-report-' + str(start_date) + '.xlsx'
        else:
            range_date = tools.get_custom_range_date(start_date, end_date, True)
            fname = 'list-' + report_tpye + '-report-' + range_date[0] + '-to-' + range_date[1] + '.xlsx'

        params += range_date

        result = []
        sql_str = "SELECT "\
            "r.id, COUNT(r.id) as count_id "\
            "FROM report as r " + query_str + " "\
            "AND r.ngay_risk_xac_nhan IS NULL "\
            "AND r.tinh_trang_khai_thac=391 "\
            "AND Date(r.date_edit) >= Date(%s) "\
            "AND Date(r.date_edit) <= Date(%s) "\
            "GROUP BY r.id"
        list_item_dang_thu_thap = main_model.objects.raw(sql_str, params)
        if len(list(list_item_dang_thu_thap)) == 0:
            result.append(['Đang thu thập', 0])
        else:
            result.append(['Đang thu thập', list_item_dang_thu_thap[0].count_id])

        sql_str = "SELECT "\
            "r.id, COUNT(r.id) as count_id "\
            "FROM report as r " + query_str + " "\
            "AND r.ngay_risk_xac_nhan IS NULL "\
            "AND r.tinh_trang_khai_thac=389 "\
            "AND Date(r.date_edit) >= Date(%s) "\
            "AND Date(r.date_edit) <= Date(%s) "\
            "GROUP BY r.id"
        list_item_fail_pre_khong_trinh = main_model.objects.raw(sql_str, params)
        if len(list(list_item_fail_pre_khong_trinh)) == 0:
            result.append(['Fail Pre + không trình', 0])
        else:
            result.append(['Fail Pre + không trình', list_item_fail_pre_khong_trinh[0].count_id])

        sql_str = "SELECT "\
            "r.id, COUNT(r.id) as count_id "\
            "FROM report as r " + query_str + " "\
            "AND r.ngay_phe_duyet IS NULL "\
            "AND r.tinh_trang_khai_thac=391 "\
            "AND Date(r.ngay_risk_xac_nhan) >= Date(%s) "\
            "AND Date(r.ngay_risk_xac_nhan) <= Date(%s) "\
            "GROUP BY r.id"
        list_dang_phe_duyet = main_model.objects.raw(sql_str, params)
        if len(list(list_dang_phe_duyet)) == 0:
            result.append(['Đang phê duyệt', 0])
        else:
            result.append(['Đang phê duyệt', list_dang_phe_duyet[0].count_id])

        sql_str = "SELECT "\
            "r.id, COUNT(r.id) as count_id "\
            "FROM report as r " + query_str + " "\
            "AND r.ngay_phe_duyet IS NOT NULL "\
            "AND r.tinh_trang_khai_thac=390 "\
            "AND Date(r.date_edit) >= Date(%s) "\
            "AND Date(r.date_edit) <= Date(%s) "\
            "GROUP BY r.id"
        list_phe_duyet_thanh_cong = main_model.objects.raw(sql_str, params)
        if len(list(list_phe_duyet_thanh_cong)) == 0:
            result.append(['Phê duyệt thành công', 0])
        else:
            result.append(['Phê duyệt thành công', list_phe_duyet_thanh_cong[0].count_id])

        sql_str = "SELECT "\
            "r.id, COUNT(r.id) as count_id "\
            "FROM report as r " + query_str + " "\
            "AND r.ngay_phe_duyet IS NOT NULL "\
            "AND r.tinh_trang_khai_thac=389 "\
            "AND Date(r.date_edit) >= Date(%s) "\
            "AND Date(r.date_edit) <= Date(%s) "\
            "GROUP BY r.id"
        list_item_phe_duyet_tu_choi = main_model.objects.raw(sql_str, params)
        if len(list(list_item_phe_duyet_tu_choi)) == 0:
            result.append(['Phê duyệt từ chối', 0])
        else:
            result.append(['Phê duyệt từ chối', list_item_phe_duyet_tu_choi[0].count_id])

        result_title = ['Trạng thái', 'Tổng cộng']

        full_result = copy.deepcopy(result)
        full_result.insert(0, result_title)

        if only_view is True:
            return StreamingHttpResponse(
                tools.encode_server_vars({
                    'data': {
                        'report': result,
                        'report_title': result_title
                    },
                    'error': None
                }),
                content_type='application/json'
            )
        else:
            fname = display_uid + '-' + fname
            output = io.BytesIO()
            wb = xlsxwriter.Workbook(output, {'in_memory': True})
            ws = wb.add_worksheet('Call report')

            start_row = 4

            ws.write(0, 0, 'BÁO CÁO KẾT QUẢ PHÊ DUYỆT')
            ws.write(1, 0, 'Ngày: ' + tools.get_date_part(str(range_date[0])) + ' đến ' + tools.get_date_part(str(range_date[1])))
            ws.write(2, 0, 'Người export: ' + exporter)
            # print result
            if len(full_result) > 0:
                for i, row in enumerate(full_result):
                    for j, item in enumerate(row):
                        ws.write(i + start_row + 1, j, item)
            wb.close()
            output.seek(0)
            response = HttpResponse(output.read(), content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            response['Content-Disposition'] = "attachment; filename=" + fname

            return response
    except Exception as e:
        error = tools.return_exception(e)
        # print(error)
        return error


def export_file(request):
    user_id = request.session.get('USER_ID', None)
    admin_id = request.session.get('ADMIN_ID', None)

    is_history = request.POST.get('history', None)
    basic_date = request.POST.get('basic_date', None)
    basic_range = request.POST.get('basic_range', None)
    start_date = request.POST.get('start_date', None)
    end_date = request.POST.get('end_date', None)

    list_user_ref = tools.create_ref(user_model.objects.get_full()['data'], 'id', 'name')
    list_zone = zone_model.objects.get_full()['data']
    list_zone_ref = tools.create_ref(list_zone, 'id', 'title')
    list_dropdown_ref = tools.create_ref(dropdown_model.objects.get_full()['data'], 'id', 'title')

    if is_history is None:
        is_history = False
    else:
        if is_history == '0':
            is_history = False
        elif is_history == '1':
            is_history = True

    if basic_date is not None:
        range_date = tools.get_basic_range_date(basic_date, basic_range)
        fname = 'list-report-' + str(basic_date) + '.xlsx'
    else:
        range_date = tools.get_custom_range_date(start_date, end_date)
        fname = 'list-report-' + str(range_date[0]) + '-to-' + str(range_date[1]) + '.xlsx'

    if user_id is not None:
        user_id = int(user_id)
        user_item = user_model.objects.get_one({'pk': user_id}, True)['data']
        if is_history is False:
            query = main_model.objects.filter(date_edit__range=range_date, user=user_item.pk)
        else:
            query = report_log_model.objects.filter(date_edit__range=range_date, user=user_item.pk)
    else:
        admin_id = int(admin_id)
        admin_item = admin_model.objects.get_one({'pk': admin_id}, True)['data']
        if admin_item.zone is None:
            if is_history is False:
                query = main_model.objects.filter(date_edit__range=range_date)
            else:
                query = report_log_model.objects.filter(date_edit__range=range_date)
        else:
            list_zone_hierarchy = tools.get_list_hierarchy(admin_item.zone.pk, list_zone)
            list_zone_hierarchy.append(admin_item.zone.pk)
            if is_history is False:
                query = main_model.objects.filter(date_edit__range=range_date, zone_id__in=list_zone_hierarchy)
            else:
                query = report_log_model.objects.filter(date_edit__range=range_date, zone_id__in=list_zone_hierarchy)

    list_item = tools.standardize_array(list(query.values()))

    data_rule = {
        'date_create': [0, 'datetime'],
        'date_edit': [1, 'datetime'],
        # 'date_trans': [2, 'datetime'],
        'cty_name': [2, 'string'],
        'user': [3, 'user'],
        'zone_id': [4, 'zone'],
        'kq_cuoc_goi': [5, 'dropdown'],
        'ngay_goi_dien_gan_nhat': [6, 'date'],
        'ngay_hen': [7, 'datetime'],
        'dia_chi_xac_nhan_lai': [8, 'string'],
        'nguoi_lien_lac': [9, 'string'],
        'phone': [10, 'string'],
        'sp_tiep_can': [11, 'dropdown'],
        'kq_cuoc_gap': [12, 'dropdown'],
        'tinh_trang_khai_thac': [13, 'dropdown'],
        'ngay_tiep_can_1': [14, 'date'],
        'ngay_tiep_can_2': [15, 'date'],
        'ngay_tiep_can_3': [16, 'date'],
        'ngay_tiep_can_4': [17, 'date'],
        'ngay_kh_dong_y_nop_hs': [18, 'date'],
        'ngay_check_prescreen': [19, 'date'],
        'ngay_bao_kh_thu_thap_hs': [20, 'date'],
        'ngay_thu_thap_xong_hs': [21, 'date'],
        'ngay_risk_yeu_cau_bo_sung': [22, 'date'],
        'ngay_risk_xac_nhan': [23, 'date'],
        'ngay_ci_bat_dau_dieu_tra': [24, 'date'],
        'ngay_ci_co_bao_cao': [25, 'date'],
        'ngay_ca_phan_tich': [26, 'date'],
        'ngay_ca_hoan_thanh_hs': [27, 'date'],
        'ngay_cm_nhan_du_hs': [28, 'date'],
        'ngay_phe_duyet': [29, 'date'],
        'ngay_thong_bao_phe_duyet': [30, 'date'],
        'ngay_docdis_chuan_bi_hd': [31, 'date'],
        'ngay_docdis_chuan_bi_hd_xong': [32, 'date'],
        'ngay_chuyen_hd_cho_kh': [33, 'date'],
        'ngay_kh_ky_hd': [34, 'date'],
        'ngay_msb_ky_hd_phe_duyet': [35, 'date'],
        'ngay_kh_yeu_cau_giai_ngan': [36, 'date'],
        'ngay_kh_giai_ngan_thanh_cong': [37, 'date'],
        'ngay_mo_han_muc_tren_he_thong': [38, 'date'],
        'ghi_chu': [39, 'string'],
        'report': [40, 'string'],
    }
    if is_history is False:
        data_rule.pop('report', None)

    # fname = display_uid + '-' + fname
    output = io.BytesIO()
    wb = xlsxwriter.Workbook(output, {'in_memory': True})
    ws = wb.add_worksheet('List report')

    date_format = wb.add_format({'num_format': 'dd/mm/yy'})
    datetime_format = wb.add_format({'num_format': 'dd/mm/yy hh:mm'})

    if len(list_item) > 0:
        for key, header_item in data_rule.items():
            ws.write(0, data_rule[key][0], key)

    for key, item in enumerate(list_item):
        for subkey, subitem in item.items():
            if subkey in data_rule:
                if subitem is not None:
                    # cell_format = xlwt.XFStyle()
                    cell_format = None
                    if data_rule[subkey][1] == 'date':
                        # cell_format.num_format_str = 'YYYY/MM/DD'
                        cell_format = date_format
                        subitem = tools.get_date(subitem.year, subitem.month, subitem.day)
                    elif data_rule[subkey][1] == 'datetime':
                        # cell_format.num_format_str = 'YYYY/MM/DD HH:MM:SS'
                        subitem = tools.get_datetime(subitem.year, subitem.month, subitem.day, subitem.hour, subitem.minute, subitem.second)
                        cell_format = datetime_format
                    elif data_rule[subkey][1] == 'string':
                        # cell_format = None
                        subitem = str(subitem)
                    elif data_rule[subkey][1] == 'zone':
                        subitem = tools.get_ref(subitem, list_zone_ref)
                    elif data_rule[subkey][1] == 'dropdown':
                        subitem = tools.get_ref(subitem, list_dropdown_ref)
                    elif data_rule[subkey][1] == 'user':
                        subitem = tools.get_ref(subitem, list_user_ref)
                    else:
                        cell_format = None

                    if cell_format is not None:
                        ws.write(key + 1, data_rule[subkey][0], subitem, cell_format)
                    else:
                        ws.write(key + 1, data_rule[subkey][0], subitem)

    wb.close()
    output.seek(0)

    response = HttpResponse(output.read(), content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    response['Content-Disposition'] = "attachment; filename=" + fname

    return response


def get_all(request):
    conditions = None
    page = int(request.POST.get('page'))
    keyword = request.POST.get('keyword', None)

    admin_id = tools.str_to_int(request.session.get('ADMIN_ID', None))
    user_id = tools.str_to_int(request.session.get('USER_ID', None))

    admin_item = admin_model.objects.get_one({'pk': admin_id})['data']
    user_item = user_model.objects.get_one({'pk': user_id})['data']

    if user_id:
        conditions = {
            'user': user_item['id']
        }

    if admin_id:
        if admin_item['zone_id']:
            zone_item = zone_model.objects.get_one({'pk': admin_item['zone_id']})['data']
            admin_level = zone_item['level']
        else:
            admin_level = 0

        if admin_level == 1:
            conditions = {'zone': zone_item['id']}

        if admin_level == 2:
            result = {
                'data': [],
                'error': None
            }
            return StreamingHttpResponse(
                tools.encode_server_vars(result),
                content_type='application/json'
            )

    sp_tiep_can_condition = request.POST.get('sp_tiep_can', None)
    kq_cuoc_goi_condition = request.POST.get('kq_cuoc_goi', None)
    kq_cuoc_gap_condition = request.POST.get('kq_cuoc_gap', None)
    tt_khai_thac_condition = request.POST.get('tinh_trang_khai_thac', None)

    if sp_tiep_can_condition is not None:
        conditions['sp_tiep_can'] = sp_tiep_can_condition
    if kq_cuoc_goi_condition is not None:
        conditions['kq_cuoc_goi'] = kq_cuoc_goi_condition
    if kq_cuoc_gap_condition is not None:
        conditions['kq_cuoc_gap'] = kq_cuoc_gap_condition
    if tt_khai_thac_condition is not None:
        conditions['tinh_trang_khai_thac'] = tt_khai_thac_condition

    result = main_model.objects.get_all(conditions, keyword, page)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def get_full(self, conditions=None):
    try:
        if conditions is None:
            query = self.all()
        else:
            query = self.filter(**conditions)
        result = Tools.standardize_array(list(query.values()))
        return {
            'error': None,
            'data': result
        }
    except Exception as e:
        error = self.tools.return_exception(e)
        # print(error)
        return error


def get_one(request):
    conditions = {
        'id': request.POST.get('id', None),
    }
    is_log = request.POST.get('is_log', None)
    if conditions['id'] is not None:
        conditions['id'] = int(conditions['id'])
    if is_log is None:
        result = main_model.objects.get_one(conditions)
    else:
        result = report_log_model.objects.get_one(conditions)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def add_item(request):
    pk = request.session.get('USER_ID', None)
    user_item = user_model.objects.get_one({'pk': pk}, True)['data']
    data = {}

    data['date_create'] = timezone.now()
    data['date_edit'] = timezone.now()
    data['date_trans'] = None

    data['commit'] = False
    data['cty'] = tools.str_to_int(request.POST.get('cty', None))
    if data['cty'] is not None:
        data['cty'] = company_model.objects.get_one({'pk': data['cty']}, True)['data']
        data['cty_name'] = data['cty'].cty
        data['cty_mst'] = data['cty'].mst
    data['user'] = user_item.pk
    data['zone'] = user_item.zone

    data['kq_cuoc_goi'] = tools.str_to_int(request.POST.get('kq_cuoc_goi', None))
    data['kq_cuoc_gap'] = tools.str_to_int(request.POST.get('kq_cuoc_gap', None), True)
    # if data['kq_cuoc_goi'] is not None:
    #    data['kq_cuoc_goi'] = dropdown_model.objects.get_one({'pk': data['kq_cuoc_goi']}, True)['data']

    data['ngay_goi_dien_gan_nhat'] = tools.str_to_date(request.POST.get('ngay_goi_dien_gan_nhat', None))
    data['ngay_hen'] = tools.str_to_date(request.POST.get('ngay_hen', None), True)
    data['dia_chi_xac_nhan_lai'] = request.POST.get('dia_chi_xac_nhan_lai', None)
    data['nguoi_lien_lac'] = request.POST.get('nguoi_lien_lac', None)
    data['phone'] = request.POST.get('phone', None)

    data['sp_tiep_can'] = tools.str_to_int(request.POST.get('sp_tiep_can', None))
    # if data['sp_tiep_can'] is not None:
    #    data['sp_tiep_can'] = dropdown_model.objects.get_one({'pk': data['sp_tiep_can']}, True)['data']

    data['tinh_trang_khai_thac'] = tools.str_to_int(request.POST.get('tinh_trang_khai_thac', None))

    data['gt_pk_khac'] = tools.str_to_str(request.POST.get('gt_pk_khac', None))
    # if data['tinh_trang_khai_thac'] is not None:
    #    data['tinh_trang_khai_thac'] = dropdown_model.objects.get_one({'pk': data['tinh_trang_khai_thac']}, True)['data']

    data['ngay_tiep_can_1'] = tools.str_to_date(request.POST.get('ngay_tiep_can_1', None))
    data['ngay_tiep_can_2'] = tools.str_to_date(request.POST.get('ngay_tiep_can_2', None))
    data['ngay_tiep_can_3'] = tools.str_to_date(request.POST.get('ngay_tiep_can_3', None))
    data['ngay_tiep_can_4'] = tools.str_to_date(request.POST.get('ngay_tiep_can_4', None))
    data['ngay_kh_dong_y_nop_hs'] = tools.str_to_date(request.POST.get('ngay_kh_dong_y_nop_hs', None))
    data['ngay_check_prescreen'] = tools.str_to_date(request.POST.get('ngay_check_prescreen', None))
    data['ngay_bao_kh_thu_thap_hs'] = tools.str_to_date(request.POST.get('ngay_bao_kh_thu_thap_hs', None))
    data['ngay_thu_thap_xong_hs'] = tools.str_to_date(request.POST.get('ngay_thu_thap_xong_hs', None))
    data['ngay_risk_yeu_cau_bo_sung'] = tools.str_to_date(request.POST.get('ngay_risk_yeu_cau_bo_sung', None))
    data['ngay_risk_xac_nhan'] = tools.str_to_date(request.POST.get('ngay_risk_xac_nhan', None))
    data['ngay_ci_bat_dau_dieu_tra'] = tools.str_to_date(request.POST.get('ngay_ci_bat_dau_dieu_tra', None))
    data['ngay_ci_co_bao_cao'] = tools.str_to_date(request.POST.get('ngay_ci_co_bao_cao', None))
    data['ngay_ca_phan_tich'] = tools.str_to_date(request.POST.get('ngay_ca_phan_tich', None))
    data['ngay_ca_hoan_thanh_hs'] = tools.str_to_date(request.POST.get('ngay_ca_hoan_thanh_hs', None))
    data['ngay_cm_nhan_du_hs'] = tools.str_to_date(request.POST.get('ngay_cm_nhan_du_hs', None))
    data['ngay_phe_duyet'] = tools.str_to_date(request.POST.get('ngay_phe_duyet', None))
    data['ngay_thong_bao_phe_duyet'] = tools.str_to_date(request.POST.get('ngay_thong_bao_phe_duyet', None))
    data['ngay_docdis_chuan_bi_hd'] = tools.str_to_date(request.POST.get('ngay_docdis_chuan_bi_hd', None))
    data['ngay_docdis_chuan_bi_hd_xong'] = tools.str_to_date(request.POST.get('ngay_docdis_chuan_bi_hd_xong', None))
    data['ngay_chuyen_hd_cho_kh'] = tools.str_to_date(request.POST.get('ngay_chuyen_hd_cho_kh', None))
    data['ngay_kh_ky_hd'] = tools.str_to_date(request.POST.get('ngay_kh_ky_hd', None))
    data['ngay_msb_ky_hd_phe_duyet'] = tools.str_to_date(request.POST.get('ngay_msb_ky_hd_phe_duyet', None))
    data['ngay_kh_yeu_cau_giai_ngan'] = tools.str_to_date(request.POST.get('ngay_kh_yeu_cau_giai_ngan', None))
    data['ngay_kh_giai_ngan_thanh_cong'] = tools.str_to_date(request.POST.get('ngay_kh_giai_ngan_thanh_cong', None))
    data['ngay_mo_han_muc_tren_he_thong'] = tools.str_to_date(request.POST.get('ngay_mo_han_muc_tren_he_thong', None))
    data['ghi_chu'] = request.POST.get('ghi_chu', None)

    result = main_model.objects.add_item(data)

    if result['error'] is None:
        params = {
            'sp_tiep_can': data['sp_tiep_can'],
            'tinh_trang_khai_thac': data['tinh_trang_khai_thac'],
            'kq_cuoc_goi': data['kq_cuoc_goi'],
            'kq_cuoc_gap': data['kq_cuoc_gap']
        }
        company_model.objects.edit_item(int(data['cty'].pk), params)

    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def edit_item(request):
    data = {}

    data['date_edit'] = timezone.now()
    data['date_trans'] = None
    '''
    data['cty'] = tools.str_to_int(request.POST.get('cty', None))
    if data['cty'] is not None:
        data['cty'] = company_model.objects.get_one({'pk': data['cty']}, True)['data']
        data['cty_name'] = data['cty'].cty
    '''
    data['kq_cuoc_goi'] = tools.str_to_int(request.POST.get('kq_cuoc_goi', None))
    data['kq_cuoc_gap'] = tools.str_to_int(request.POST.get('kq_cuoc_gap', None), True)
    # if data['kq_cuoc_goi'] is not None:
    #    data['kq_cuoc_goi'] = dropdown_model.objects.get_one({'pk': data['kq_cuoc_goi']}, True)['data']

    data['ngay_goi_dien_gan_nhat'] = tools.str_to_date(request.POST.get('ngay_goi_dien_gan_nhat', None))
    data['ngay_hen'] = tools.str_to_date(request.POST.get('ngay_hen', None), True)
    data['dia_chi_xac_nhan_lai'] = request.POST.get('dia_chi_xac_nhan_lai', None)
    data['nguoi_lien_lac'] = request.POST.get('nguoi_lien_lac', None)
    data['phone'] = request.POST.get('phone', None)

    data['sp_tiep_can'] = tools.str_to_int(request.POST.get('sp_tiep_can', None))
    # if data['sp_tiep_can'] is not None:
    #    data['sp_tiep_can'] = dropdown_model.objects.get_one({'pk': data['sp_tiep_can']}, True)['data']

    data['tinh_trang_khai_thac'] = tools.str_to_int(request.POST.get('tinh_trang_khai_thac', None))

    data['gt_pk_khac'] = tools.str_to_str(request.POST.get('gt_pk_khac', None))
    # if data['tinh_trang_khai_thac'] is not None:
    #    data['tinh_trang_khai_thac'] = dropdown_model.objects.get_one({'pk': data['tinh_trang_khai_thac']}, True)['data']

    data['ngay_tiep_can_1'] = tools.str_to_date(request.POST.get('ngay_tiep_can_1', None))
    data['ngay_tiep_can_2'] = tools.str_to_date(request.POST.get('ngay_tiep_can_2', None))
    data['ngay_tiep_can_3'] = tools.str_to_date(request.POST.get('ngay_tiep_can_3', None))
    data['ngay_tiep_can_4'] = tools.str_to_date(request.POST.get('ngay_tiep_can_4', None))
    data['ngay_kh_dong_y_nop_hs'] = tools.str_to_date(request.POST.get('ngay_kh_dong_y_nop_hs', None))
    data['ngay_check_prescreen'] = tools.str_to_date(request.POST.get('ngay_check_prescreen', None))
    data['ngay_bao_kh_thu_thap_hs'] = tools.str_to_date(request.POST.get('ngay_bao_kh_thu_thap_hs', None))
    data['ngay_thu_thap_xong_hs'] = tools.str_to_date(request.POST.get('ngay_thu_thap_xong_hs', None))
    data['ngay_risk_yeu_cau_bo_sung'] = tools.str_to_date(request.POST.get('ngay_risk_yeu_cau_bo_sung', None))
    data['ngay_risk_xac_nhan'] = tools.str_to_date(request.POST.get('ngay_risk_xac_nhan', None))
    data['ngay_ci_bat_dau_dieu_tra'] = tools.str_to_date(request.POST.get('ngay_ci_bat_dau_dieu_tra', None))
    data['ngay_ci_co_bao_cao'] = tools.str_to_date(request.POST.get('ngay_ci_co_bao_cao', None))
    data['ngay_ca_phan_tich'] = tools.str_to_date(request.POST.get('ngay_ca_phan_tich', None))
    data['ngay_ca_hoan_thanh_hs'] = tools.str_to_date(request.POST.get('ngay_ca_hoan_thanh_hs', None))
    data['ngay_cm_nhan_du_hs'] = tools.str_to_date(request.POST.get('ngay_cm_nhan_du_hs', None))
    data['ngay_phe_duyet'] = tools.str_to_date(request.POST.get('ngay_phe_duyet', None))
    data['ngay_thong_bao_phe_duyet'] = tools.str_to_date(request.POST.get('ngay_thong_bao_phe_duyet', None))
    data['ngay_docdis_chuan_bi_hd'] = tools.str_to_date(request.POST.get('ngay_docdis_chuan_bi_hd', None))
    data['ngay_docdis_chuan_bi_hd_xong'] = tools.str_to_date(request.POST.get('ngay_docdis_chuan_bi_hd_xong', None))
    data['ngay_chuyen_hd_cho_kh'] = tools.str_to_date(request.POST.get('ngay_chuyen_hd_cho_kh', None))
    data['ngay_kh_ky_hd'] = tools.str_to_date(request.POST.get('ngay_kh_ky_hd', None))
    data['ngay_msb_ky_hd_phe_duyet'] = tools.str_to_date(request.POST.get('ngay_msb_ky_hd_phe_duyet', None))
    data['ngay_kh_yeu_cau_giai_ngan'] = tools.str_to_date(request.POST.get('ngay_kh_yeu_cau_giai_ngan', None))
    data['ngay_kh_giai_ngan_thanh_cong'] = tools.str_to_date(request.POST.get('ngay_kh_giai_ngan_thanh_cong', None))
    data['ngay_mo_han_muc_tren_he_thong'] = tools.str_to_date(request.POST.get('ngay_mo_han_muc_tren_he_thong', None))
    data['ghi_chu'] = request.POST.get('ghi_chu', None)

    # print data
    pk = request.POST.get('id', None)
    result = main_model.objects.edit_item(pk, data)
    if result['error'] is None:
        params = {
            'sp_tiep_can': data['sp_tiep_can'],
            'tinh_trang_khai_thac': data['tinh_trang_khai_thac'],
            'kq_cuoc_goi': data['kq_cuoc_goi'],
            'kq_cuoc_gap': data['kq_cuoc_gap'],
            'gt_pk_khac': data['gt_pk_khac'],
        }
        company_model.objects.edit_item(int(data['cty'].pk), params)

    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )


def remove_item(request):
    pk = request.POST.get('id', None)
    result = main_model.objects.remove_item(pk)
    return StreamingHttpResponse(
        tools.encode_server_vars(result),
        content_type='application/json'
    )
