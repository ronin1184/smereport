from django.db import models
from django.db.models import Q
from Zone.models import Zone
from Company.models import Company
from Common.Tools import Tools
# Create your models here.


class ReportModel(models.Manager):
    tools = Tools()
    limit = 25

    def get_all(self, conditions=None, keyword=None, page=1, order='-id'):
        try:
            offset = (page - 1) * self.limit

            if conditions is not None:
                query = self.filter(**conditions)
            else:
                query = self.all()
            if keyword is not None:
                query = query.filter(
                    Q(cty_name__icontains=keyword) |
                    Q(cty_mst__icontains=keyword) |
                    Q(nguoi_lien_lac__icontains=keyword) |
                    Q(phone__icontains=keyword)
                )
            query = query.order_by(order)

            if offset == 0:
                query = query[: self.limit]
            else:
                query = query[offset: offset + self.limit]

            reach_bottom = False
            if query.count() < self.limit:
                reach_bottom = True

            result = Tools.standardize_array(list(query.values()))
            return {
                'error': None,
                'reach_bottom': reach_bottom,
                'data': result
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error

    def get_full(self, conditions=None, fields=None):
        try:
            if conditions is None:
                query = self.all()
            else:
                query = self.filter(**conditions)
            if fields is None:
                result = Tools.standardize_array(list(query.values()))
            else:
                result = query.values(*fields)
            return {
                'error': None,
                'data': result
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error

    def get_one(self, conditions, original=False):
        try:
            query = self.filter(**conditions)
            if original is False:
                query = list(query.values())
            if not query:
                query = None
            else:
                query = query[0]
            return {
                'error': None,
                'data': query
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error

    def add_item(self, params):
        try:
            item = Report(**params)
            item.save()

            report_params = params
            report_params['report'] = int(item.pk)
            log_item = ReportLog(**report_params)
            log_item.save()

            result = {
                'id': item.pk,
                'user': item.user,
                'zone_id': item.zone.pk,
                'cty_name': item.cty_name,
                'cty_mst': item.cty_mst,
                'sp_tiep_can': int(item.sp_tiep_can),
                'kq_cuoc_goi': int(item.kq_cuoc_goi),
                'tinh_trang_khai_thac': int(item.tinh_trang_khai_thac),
                'commit': False
            }
            if item.kq_cuoc_gap is None:
                result['kq_cuoc_gap'] = 0
            else:
                result['kq_cuoc_gap'] = int(item.kq_cuoc_gap)

            return {
                'error': None,
                'data': result
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error

    def edit_item(self, pk, params):
        try:
            item = self.get(pk=int(pk))
            if not item:
                return {
                    'error': "Item not found!",
                    'data': None
                }
            if item.commit is True:
                return {
                    'error': "Commited item can not be edit!",
                    'data': None
                }
            item.__dict__.update(params)
            item.save()

            report_params = params
            report_params['date_create'] = item.date_create
            report_params['date_edit'] = item.date_edit
            report_params['report'] = int(item.pk)
            report_params['user'] = item.user
            report_params['zone'] = item.zone
            report_params['commit'] = item.commit
            report_params['cty'] = item.cty
            report_params['cty_name'] = item.cty_name
            report_params['cty_mst'] = item.cty_mst
            log_item = ReportLog(**report_params)
            log_item.save()

            result = {
                'id': item.pk,
                'user': item.user,
                'zone_id': item.zone.pk,
                'cty_name': item.cty_name,
                'cty_mst': item.cty_mst,
                'sp_tiep_can': int(item.sp_tiep_can),
                'kq_cuoc_goi': int(item.kq_cuoc_goi),
                'tinh_trang_khai_thac': int(item.tinh_trang_khai_thac),
                'commit': False
            }
            if item.kq_cuoc_gap is None:
                result['kq_cuoc_gap'] = 0
            else:
                result['kq_cuoc_gap'] = int(item.kq_cuoc_gap)
            return {
                'error': None,
                'data': result
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error

    def remove_item(self, pk):
        try:
            input_pk = pk
            if '|' in str(pk):
                pk = [int(x) for x in pk.split('|')]
            if type(pk) is not list:
                items = self.get(pk=int(pk))
                if not items:
                    return {
                        'error': "Item not found!",
                        'data': None
                    }
                if items.commit is True or items.commit == 1:
                    return {
                        'error': "Commited items are read only!",
                        'data': None
                    }
            else:
                items = self.filter(id__in=pk)
                for item in items:
                    if item.commit is True or item.commit == 1:
                        return {
                            'error': "Commited items are read only!",
                            'data': None
                        }
            items.delete()
            result = {
                'id': input_pk
            }
            return {
                'error': None,
                'data': result
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error

    def update_commit(self, pk, owner, commit_value):
        try:
            zone_id = owner.zone.pk
            owner = owner.pk

            if commit_value is True:
                params = {
                    'commit': commit_value
                }
            else:
                params = {
                    'commit': commit_value,
                    'user': owner,
                    'zone_id': zone_id
                }
            input_pk = pk
            if '|' in str(pk):
                pk = [int(x) for x in pk.split('|')]
            if type(pk) is not list:
                if commit_value is True:
                    items = self.filter(cty=int(pk), user=owner)
                    if items is not None:
                        for item in items:
                            item.__dict__.update(params)
                            item.save()
                else:
                    items = self.filter(cty=int(pk))
                    if items is not None:
                        for item in items:
                            item.__dict__.update(params)
                            item.pk = None
                            item.save()

            else:
                if commit_value is True:
                    items = self.filter(cty__in=pk, user=owner)
                    if items is not None:
                        for item in items:
                            item.__dict__.update(params)
                            item.save()
                else:
                    items = self.filter(cty__in=pk)
                    if items is not None:
                        for item in items:
                            item.__dict__.update(params)
                            item.pk = None
                            item.save()

            result = {
                'id': input_pk
            }
            return {
                'error': None,
                'data': result
            }
        except Exception as e:
            error = self.tools.return_exception(e)
            # print(error)
            return error
"""
SELECT
user.name as user_name,
SUM(CASE WHEN report.kq_cuoc_goi=378 THEN 1 ELSE 0 END) as chua_tiep_can_duoc_lanh_dao_don_vi,
SUM(CASE WHEN report.kq_cuoc_goi=379 THEN 1 ELSE 0 END) as hoan_thanh_da_co_kq_tu_risk,
SUM(CASE WHEN report.kq_cuoc_goi=380 THEN 1 ELSE 0 END) as kh_can_tiep_can_lai,
SUM(CASE WHEN report.kq_cuoc_goi=381 THEN 1 ELSE 0 END) as kh_khong_quan_tam,
SUM(CASE WHEN report.kq_cuoc_goi=382 THEN 1 ELSE 0 END) as kh_quan_tam_fail_tieu_chi_so_bo,
SUM(CASE WHEN report.kq_cuoc_goi=383 THEN 1 ELSE 0 END) as kh_quan_tam_pass_tieu_chi_so_bo_da_dat_lich,
SUM(CASE WHEN report.kq_cuoc_goi=384 THEN 1 ELSE 0 END) as kh_khong_tra_loi_dt,
SUM(CASE WHEN report.kq_cuoc_goi=385 THEN 1 ELSE 0 END) as thong_tin_ve_so_dt_sai,
SUM(CASE WHEN report.kq_cuoc_goi=386 THEN 1 ELSE 0 END) as cong_ty_nha_nuoc,
SUM(CASE WHEN report.kq_cuoc_goi=387 THEN 1 ELSE 0 END) as nganh_nghe_han_che,
SUM(CASE WHEN report.kq_cuoc_goi=388 THEN 1 ELSE 0 END) as khac
FROM report
INNER JOIN dropdown ON report.kq_cuoc_goi = dropdown.id
INNER JOIN user ON report.user = user.id
WHERE report.user=13
#WHERE report.zone_id=388
AND Date(report.date_edit) >= Date('2014-01-01')
AND Date(report.date_edit) <= Date('2014-12-31')
GROUP BY user.id;


SELECT
company.cty as cus_name,
user.name as user_name,
dropdown.title as kq_cuoc_gap
FROM report
INNER JOIN dropdown ON report.kq_cuoc_gap = dropdown.id
INNER JOIN user ON report.user = user.id
INNER JOIN company ON report.cty_id = company.id
WHERE report.user=13
AND report.zone_id=388
AND Date(report.date_edit) >= Date('2014-01-01')
AND Date(report.date_edit) <= Date('2014-12-31');


SELECT
COUNT(report.id) as count_id
FROM report
WHERE report.user=13
AND report.zone_id=388
AND report.ngay_risk_xac_nhan IS NULL
AND report.tinh_trang_khai_thac=391
AND Date(report.ngay_bao_kh_thu_thap_hs) >= Date('2014-01-01')
AND Date(report.ngay_bao_kh_thu_thap_hs) <= Date('2014-12-31')
GROUP BY report.id;

SELECT
COUNT(report.id) as count_id
FROM report
WHERE report.user=13
AND report.zone_id=388
AND report.ngay_risk_xac_nhan IS NULL
AND report.tinh_trang_khai_thac=389
AND Date(report.ngay_bao_kh_thu_thap_hs) >= Date('2014-01-01')
AND Date(report.ngay_bao_kh_thu_thap_hs) <= Date('2014-12-31')
GROUP BY report.id;

SELECT
COUNT(report.id) as count_id
FROM report
WHERE report.user=13
AND report.zone_id=388
AND report.ngay_phe_duyet IS NULL
AND report.tinh_trang_khai_thac=391
AND Date(report.ngay_risk_xac_nhan) >= Date('2014-01-01')
AND Date(report.ngay_risk_xac_nhan) <= Date('2014-12-31')
GROUP BY report.id;

SELECT
COUNT(report.id) as count_id
FROM report
WHERE report.user=13
AND report.zone_id=388
AND report.ngay_phe_duyet IS NOT NULL
AND report.tinh_trang_khai_thac=390
AND Date(report.date_edit) >= Date('2014-01-01')
AND Date(report.date_edit) <= Date('2014-12-31')
GROUP BY report.id;

SELECT
COUNT(report.id) as count_id
FROM report
WHERE report.user=13
AND report.zone_id=388
AND report.ngay_phe_duyet IS NOT NULL
AND report.tinh_trang_khai_thac=389
AND Date(report.date_edit) >= Date('2014-01-01')
AND Date(report.date_edit) <= Date('2014-12-31')
GROUP BY report.id;

"""


class Report(models.Model):
    id = models.AutoField(primary_key=True)
    date_create = models.DateTimeField(null=True, blank=True)
    date_edit = models.DateTimeField(null=True, blank=True)
    date_trans = models.DateTimeField(null=True, blank=True)

    commit = models.BooleanField(default=False)
    cty = models.ForeignKey(Company, related_name='report_cty')
    cty_name = models.CharField(max_length=250)
    cty_mst = models.CharField(max_length=50, null=True, blank=True)
    user = models.IntegerField()

    zone = models.ForeignKey(Zone)

    kq_cuoc_goi = models.IntegerField(blank=None, null=None)
    ngay_goi_dien_gan_nhat = models.DateField(null=True, blank=True)
    ngay_hen = models.DateTimeField(null=True, blank=True)
    dia_chi_xac_nhan_lai = models.CharField(max_length=250, null=True, blank=True)
    nguoi_lien_lac = models.CharField(max_length=150, null=True, blank=True)
    phone = models.CharField(max_length=20, null=True, blank=True)

    sp_tiep_can = models.IntegerField()
    kq_cuoc_gap = models.IntegerField(null=True, blank=True)
    tinh_trang_khai_thac = models.IntegerField(null=True, blank=True)
    gt_pk_khac = models.CharField(max_length=50, null=True, blank=True)
    ngay_tiep_can_1 = models.DateField(null=True, blank=True)
    ngay_tiep_can_2 = models.DateField(null=True, blank=True)
    ngay_tiep_can_3 = models.DateField(null=True, blank=True)
    ngay_tiep_can_4 = models.DateField(null=True, blank=True)
    ngay_kh_dong_y_nop_hs = models.DateField(null=True, blank=True)
    ngay_check_prescreen = models.DateField(null=True, blank=True)
    ngay_bao_kh_thu_thap_hs = models.DateField(null=True, blank=True)
    ngay_thu_thap_xong_hs = models.DateField(null=True, blank=True)
    ngay_risk_yeu_cau_bo_sung = models.DateField(null=True, blank=True)
    ngay_risk_xac_nhan = models.DateField(null=True, blank=True)
    ngay_ci_bat_dau_dieu_tra = models.DateField(null=True, blank=True)
    ngay_ci_co_bao_cao = models.DateField(null=True, blank=True)
    ngay_ca_phan_tich = models.DateField(null=True, blank=True)
    ngay_ca_hoan_thanh_hs = models.DateField(null=True, blank=True)
    ngay_cm_nhan_du_hs = models.DateField(null=True, blank=True)
    ngay_phe_duyet = models.DateField(null=True, blank=True)
    ngay_thong_bao_phe_duyet = models.DateField(null=True, blank=True)
    ngay_docdis_chuan_bi_hd = models.DateField(null=True, blank=True)
    ngay_docdis_chuan_bi_hd_xong = models.DateField(null=True, blank=True)
    ngay_chuyen_hd_cho_kh = models.DateField(null=True, blank=True)
    ngay_kh_ky_hd = models.DateField(null=True, blank=True)
    ngay_msb_ky_hd_phe_duyet = models.DateField(null=True, blank=True)
    ngay_kh_yeu_cau_giai_ngan = models.DateField(null=True, blank=True)
    ngay_kh_giai_ngan_thanh_cong = models.DateField(null=True, blank=True)
    ngay_mo_han_muc_tren_he_thong = models.DateField(null=True, blank=True)
    ghi_chu = models.CharField(max_length=250, null=True, blank=True)

    objects = ReportModel()

    class Meta:
        db_table = "report"


class ReportLog(models.Model):
    id = models.AutoField(primary_key=True)
    report = models.IntegerField()
    date_create = models.DateTimeField(null=True, blank=True)
    date_edit = models.DateTimeField(null=True, blank=True)
    date_trans = models.DateTimeField(null=True, blank=True)

    commit = models.BooleanField(default=False)

    cty = models.ForeignKey(Company, related_name='report_log_cty')
    cty_name = models.CharField(max_length=250)
    cty_mst = models.CharField(max_length=50, null=True, blank=True)
    user = models.IntegerField()

    zone = models.ForeignKey(Zone)

    kq_cuoc_goi = models.IntegerField(blank=None, null=None)
    ngay_goi_dien_gan_nhat = models.DateField(null=True, blank=True)
    ngay_hen = models.DateTimeField(null=True, blank=True)
    dia_chi_xac_nhan_lai = models.CharField(max_length=250, null=True, blank=True)
    nguoi_lien_lac = models.CharField(max_length=150, null=True, blank=True)
    phone = models.CharField(max_length=20, null=True, blank=True)

    sp_tiep_can = models.IntegerField()
    kq_cuoc_gap = models.IntegerField(null=True, blank=True)
    tinh_trang_khai_thac = models.IntegerField(null=True, blank=True)
    gt_pk_khac = models.CharField(max_length=50, null=True, blank=True)
    ngay_tiep_can_1 = models.DateField(null=True, blank=True)
    ngay_tiep_can_2 = models.DateField(null=True, blank=True)
    ngay_tiep_can_3 = models.DateField(null=True, blank=True)
    ngay_tiep_can_4 = models.DateField(null=True, blank=True)
    ngay_kh_dong_y_nop_hs = models.DateField(null=True, blank=True)
    ngay_check_prescreen = models.DateField(null=True, blank=True)
    ngay_bao_kh_thu_thap_hs = models.DateField(null=True, blank=True)
    ngay_thu_thap_xong_hs = models.DateField(null=True, blank=True)
    ngay_risk_yeu_cau_bo_sung = models.DateField(null=True, blank=True)
    ngay_risk_xac_nhan = models.DateField(null=True, blank=True)
    ngay_ci_bat_dau_dieu_tra = models.DateField(null=True, blank=True)
    ngay_ci_co_bao_cao = models.DateField(null=True, blank=True)
    ngay_ca_phan_tich = models.DateField(null=True, blank=True)
    ngay_ca_hoan_thanh_hs = models.DateField(null=True, blank=True)
    ngay_cm_nhan_du_hs = models.DateField(null=True, blank=True)
    ngay_phe_duyet = models.DateField(null=True, blank=True)
    ngay_thong_bao_phe_duyet = models.DateField(null=True, blank=True)
    ngay_docdis_chuan_bi_hd = models.DateField(null=True, blank=True)
    ngay_docdis_chuan_bi_hd_xong = models.DateField(null=True, blank=True)
    ngay_chuyen_hd_cho_kh = models.DateField(null=True, blank=True)
    ngay_kh_ky_hd = models.DateField(null=True, blank=True)
    ngay_msb_ky_hd_phe_duyet = models.DateField(null=True, blank=True)
    ngay_kh_yeu_cau_giai_ngan = models.DateField(null=True, blank=True)
    ngay_kh_giai_ngan_thanh_cong = models.DateField(null=True, blank=True)
    ngay_mo_han_muc_tren_he_thong = models.DateField(null=True, blank=True)
    ghi_chu = models.CharField(max_length=250, null=True, blank=True)

    objects = ReportModel()

    class Meta:
        db_table = "reportlog"
