import json
from smereport.settings import STATIC_ROOT


langs = dict()
langs['title'] = {
    'VI': 'Tieu de',
    'EN': 'Title'
}

langs['skill'] = {
    'VI': 'Ky nang',
    'EN': 'Skill'
}


label_json_data = open(STATIC_ROOT+'langs/label.json')
message_json_data = open(STATIC_ROOT+'langs/message.json')

label_langs = json.load(label_json_data)
message_langs = json.load(message_json_data)

label_json_data.close()
message_json_data.close()


def trans_label(key, lang_code):
    if key in label_langs:
        if lang_code in label_langs[key]:
            return label_langs[key][lang_code]
    else:
        return key


def trans_message(key, lang_code):
    if key in message_langs:
        if lang_code in message_langs[key]:
            return message_langs[key][lang_code]
    else:
        return key
