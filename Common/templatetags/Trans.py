from Common import Langs
from django import template
#from Dropdown.models import DropdownModel
#from Skill.models import SkillModel
#from Category.models import Category
from Zone.models import Zone
#from Dropdown.models import Dropdown
#dropdown_model = DropdownModel()
#skill_model = SkillModel()
zone_model = Zone
register = template.Library()

'''
@register.filter(name='trans_label')
def trans_label(key, lang_code):
    return Langs.trans_label(key, lang_code)


@register.filter(name='trans_message')
def trans_message(key, lang_code):
    return Langs.trans_message(key, lang_code)


@register.filter(name='trans_dropdown')
def trans_dropdown(key, lang_code):
	key = str(key)
	dropdown_ref = dropdown_model.get_list_dropdown_ref(lang_code)
	if key in dropdown_ref:
		return dropdown_ref[key]
	return key


@register.filter(name='trans_skill')
def trans_skill(key):
	key = str(key)
	skill_ref = skill_model.get_ref()['data']
	if key in skill_ref:
		return skill_ref[key]
	return key
'''


@register.inclusion_tag('back/category_sub_items.html')
def category_sub_items(cattype, category_id):
	data = {}
	if cattype == 'zone':
		data['list_side_item'] = zone_model.objects.get_one({'id': category_id})['data']
	elif cattype == 'dropdown':
		data['list_side_item'] = None
	return data
