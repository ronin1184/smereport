import json
from django.core.serializers.json import DjangoJSONEncoder
import sys
from django.core.mail import EmailMultiAlternatives
import os, string, random
from bson import ObjectId
import unicodedata
import re
from smereport.settings import MEDIA_ROOT, MIN_YEAR
from smereport import settings
import shutil
import datetime
from datetime import datetime, timedelta
import calendar
from PIL import Image
#from Zone.models import Zone
#import base64
#import urllib


class Tools():

    def __init__(self):
        pass

    @staticmethod
    def encode_server_vars(js_vars):
        js_vars = json.dumps(js_vars, cls=DjangoJSONEncoder)
        return js_vars

    @staticmethod
    def obj_id_to_str(input):
        for k, v in input:
            input[k]['_id'] = str(v['_id'])
        return input

    @staticmethod
    def standardize_array(input_list):
        if input_list is None:
            return []
        result_array = []
        for value in input_list:
            if 'password' in value:
                value.pop('password', None)
            result_array.append(value)
        return result_array

    def return_exception(self, e):
        exc_type, exc_obj, exc_tb = sys.exc_info()
        #file_name = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        file_name = exc_tb.tb_frame.f_code.co_filename
        result = {
            'error': str(e) + ' => ' + file_name + ':' + str(exc_tb.tb_lineno),
            'data': None
        }
        return result
        #return StreamingHttpResponse(
        #    self.encode_server_vars(result),
        #    content_type='application/json'
        #)

    @staticmethod
    def to_int(input):
        if input is not None:
            if input.isdigit():
                input = int(input)
            else:
                input = None
        return input

    @staticmethod
    def to_array_id(string_id, for_sub_document=False, key_hierarchy=None):
        result = []
        result_sub = {}
        list_id = string_id.split('|')

        if not for_sub_document:
            for value in list_id:
                result.append(ObjectId(value))
            return result
        else:
            for value in list_id:
                #result.append({str(key_hierarchy) + '.' + str(value) : 1})
                result_sub[str(key_hierarchy) + '.' + str(value)] = 1
            return result_sub

    @staticmethod
    def remove_attach(string_id, prefix):
        list_id = string_id.split('|')
        for value in list_id:
            path = MEDIA_ROOT + prefix + '/' + value
            if os.path.isdir(path):
                shutil.rmtree(path)

    @staticmethod
    def get_date_part(input_date_str):
        return input_date_str.split(" ")[0]

    def find_key_hierarchy(self, d, key):
        for k, v in list(d.items()):
            if k != key and isinstance(v, dict):
                p = self.find_key_hierarchy(v, key)
                if p:
                    return str(k) + '.' + p
            elif k == key:
                return str(k)

    def find_value(self, d, key):
        for k, v in list(d.items()):
            if k != key and isinstance(v, dict):
                p = self.find_value(v, key)
                if p:
                    return p
            elif k == key:
                return v

    @staticmethod
    def get_list_hierarchy(input_pk, full_list):
        list_result = [input_pk]
        for item in full_list:
            if item['parent_id'] in list_result:
                list_result.append(int(item['id']))
        list_result.pop(0)
        return list_result

    def get_list_parent(self, item, result):
        if item.parent is not None:
            if item.parent.pk not in result:
                result.insert(0, int(item.parent.pk))
                self.get_list_parent(item.parent, result)
                return result
        else:
            return result

    @staticmethod
    def str_to_date(input_str, is_time=False):
        if input_str is None or input_str == '' or input_str == 'null' or input_str == 0:
            return None
        else:
            #end_day = start_day + timedelta(seconds=settings.SECONDS_IN_DAY)
            if is_time is False:
                input_str = datetime.strptime(input_str, settings.DATE_FORMAT)
                #input_str = datetime.strptime(input_str, settings.DATE_FORMAT)
            else:
                #hour = int(input_str.split(" ")[1].split(":")[0])
                #print hour
                #input_str = datetime.strptime(input_str, settings.DATE_TIME_FORMAT).replace(tzinfo=timezone.utc) + timedelta(hours=hour)
                input_str = datetime.strptime(input_str, settings.DATE_TIME_FORMAT)
            return input_str

    def get_date(self, year, month, day):
        str_year = str(year)
        if month < 10:
            str_month = '0' + str(month)
        else:
            str_month = str(month)
        if day < 10:
            str_day = '0' + str(day)
        else:
            str_day = str(day)

        return self.str_to_date(str_year + '-' + str_month + '-' + str_day)

    def get_datetime(self, year, month, day, hour, minute, second):
        str_year = str(year)
        if month < 10:
            str_month = '0' + str(month)
        else:
            str_month = str(month)
        if day < 10:
            str_day = '0' + str(day)
        else:
            str_day = str(day)

        if hour < 10:
            str_hour = '0' + str(hour)
        else:
            str_hour = str(hour)
        if minute < 10:
            str_minute = '0' + str(minute)
        else:
            str_minute = str(minute)
        if second < 10:
            str_second = '0' + str(second)
        else:
            str_second = str(second)

        return self.str_to_date(str_year + '-' + str_month + '-' + str_day + ' ' + str_hour + ':' + str_minute + ':' + str_second, True)

    @staticmethod
    def str_to_int(input_str, escape_zero=False):
        if escape_zero is True:
            if input_str is None or input_str == 0 or input_str == '0' or input_str == '' or input_str == 'null':
                return None
            else:
                return int(input_str)
        else:
            if input_str is None or input_str == '' or input_str == 'null':
                return None
            else:
                return int(input_str)

    @staticmethod
    def str_to_float(input_str, escape_zero=False):
        if escape_zero is True:
            if input_str is None or input_str == 0 or input_str == '0' or input_str == '' or input_str == 'null':
                return None
            else:
                return float(input_str)
        else:
            if input_str is None or input_str == '' or input_str == 'null':
                return None
            else:
                return float(input_str)

    @staticmethod
    def str_to_bool(input_str):
        if input_str is None or input_str == '' or input_str == 'null' or input_str == 0 or input_str == '0' or input_str == 'false':
            return False
        else:
            return True

    @staticmethod
    def str_to_str(input_str):
        if input_str is None or input_str == '' or input_str == 'null':
            return None
        else:
            return input_str

    @staticmethod
    def get_basic_range_date(date_str, date_type='day', to_string=False):
        date_array = date_str.split('-')
        year_num = int(date_array[0])
        month_num = int(date_array[1])
        day_num = int(date_array[2])

        year_str = str(year_num)
        month_str = str(month_num)
        day_str = str(day_num)

        if month_num < 10:
            month_str = '0' + month_str
        if day_num < 10:
            day_str = '0' + day_str

        selected_date = datetime.strptime(year_str + '-' + month_str + '-' + day_str, settings.DATE_FORMAT)

        if date_type == 'year':
            start_year = datetime.strptime(year_str + '-01-01', settings.DATE_FORMAT)
            end_year = datetime.strptime(year_str + '-12-31', settings.DATE_FORMAT)
            end_year = end_year + timedelta(seconds=settings.SECONDS_IN_DAY)
            if to_string is False:
                return [start_year, end_year]
            else:
                return [start_year.strftime('%Y-%m-%d'), end_year.strftime('%Y-%m-%d')]
        elif date_type == 'month':
            last_day_of_month = calendar.monthrange(year_num, month_num)[1]
            last_day_of_month_str = str(last_day_of_month)
            if last_day_of_month < 10:
                last_day_of_month_str = '0' + str(last_day_of_month)

            start_month = datetime.strptime(year_str + '-' + month_str + '-01', settings.DATE_FORMAT)
            end_month = datetime.strptime(year_str + '-' + month_str + '-' + last_day_of_month_str, settings.DATE_FORMAT)
            end_month = end_month + timedelta(seconds=settings.SECONDS_IN_DAY)
            if to_string is False:
                return [start_month, end_month]
            else:
                return [start_month.strftime('%Y-%m-%d'), end_month.strftime('%Y-%m-%d')]
        elif date_type == 'week':
            week_day = selected_date.weekday()
            remain_week_day = 6 - week_day
            start_week = selected_date - timedelta(days=week_day)
            end_week = selected_date + timedelta(days=remain_week_day, seconds=settings.SECONDS_IN_DAY)
            if to_string is False:
                return [start_week, end_week]
            else:
                return [start_week.strftime('%Y-%m-%d'), end_week.strftime('%Y-%m-%d')]
        elif date_type == 'day':
            start_day = selected_date
            end_day = start_day + timedelta(seconds=settings.SECONDS_IN_DAY)
            if to_string is False:
                return [start_day, end_day]
            else:
                return [start_day.strftime('%Y-%m-%d'), end_day.strftime('%Y-%m-%d')]
        else:
            return None

    def get_custom_range_date(self, start_date_str, end_date_str, to_string=False):
        start_date = self.str_to_date(start_date_str)
        end_date = self.str_to_date(end_date_str) + timedelta(seconds=settings.SECONDS_IN_DAY)
        if to_string is False:
            return [start_date, end_date]
        else:
            return [start_date.strftime('%Y-%m-%d'), end_date.strftime('%Y-%m-%d')]

    @staticmethod
    def get_ratio_str(list_company_owned, list_company_free_assigned, label=False, is_user=False):

        if list_company_owned is None:
            list_company_owned = 0
            if is_user is True:
                return "Not assigned"
        else:
            list_company_owned = len(list_company_owned)
            if is_user is True:
                if list_company_owned > 0:
                    return str(list_company_owned)
                else:
                    return "Not assigned"

        if list_company_free_assigned is None:
            list_company_free_assigned = 0
        else:
            list_company_free_assigned = len(list_company_free_assigned)

        if list_company_owned > 0:
            list_company_assigned = list_company_owned - list_company_free_assigned
            if label is True:
                assign_ratio_str = "Assigned " + str(list_company_assigned)+' / Total '+str(list_company_owned)
            else:
                assign_ratio_str = str(list_company_assigned)+' / '+str(list_company_owned)
        else:
            assign_ratio_str = "Not assigned"

        return assign_ratio_str

    def find_sub_collection(
        self,
        input_list,
        conditions,
        limit,
        offset,
        sort_field='order',
        direction='DESC',
        get_full=False
    ):
        new_input = []
        for k in input_list:
            new_input.append(input_list[k])

        input_list = list(self.sort_by(new_input, sort_field, direction))

        if get_full:
            return input_list

        result = []
        keyword = None

        if conditions is not None and len(conditions) > 0:
            keyword = None
            field_list = []
            if 'keyword' in conditions:
                keyword = conditions['keyword']
                field_list = conditions['field_list']
                del conditions['keyword']
                del conditions['field_list']

            if len(conditions) > 0:
                for item in input_list:
                    is_match = 0
                    for field in item:
                        if field in conditions and \
                                str(conditions[field]) == str(item[field]):
                            is_match = is_match + 1
                    if is_match == len(conditions):
                        result.append(item)
                input_list = result
                result = []

            if keyword is not None and len(keyword) >= 3:
                for item in input_list:
                        is_match = False
                        for field in item:
                            if field in field_list:
                                if keyword in item[field]:
                                    is_match = True
                        if is_match:
                            result.append(item)
                return result[offset:offset + limit]
            else:
                return input_list[offset:offset + limit]
        else:
            return input_list[offset:offset + limit]

    @staticmethod
    def get_last_order(list_data):
        if list_data is None or len(list_data) == 0:
            return 1
        if type(list_data) is not list:
            return None
        max = 0
        for item in list_data:
            if max < int(item['order']):
                max = int(item['order'])
        return max + 1

    def sort_by(self, input_list, sort_field, direction):
        if type(input_list) is dict:
            input_list = list(input_list.values())

        result = []
        if input_list is None or len(input_list) == 0:
            return result

        for item in input_list:
            item = dict(item)
            if '_id' in item:
                item['_id'] = str(item['_id'])
            if item.get(sort_field, None) is None:
                return input_list
            result.append(item)

        result = sorted(result, key=lambda k: k[sort_field])
        #result.sort(key=lambda x: (int(x[sort_field])))
        if(direction == 'ASC'):
            return result
        else:
            return reversed(result)

    @staticmethod
    def append_key(data, key_hierarchy):
        result = {}
        for key, value in list(data.items()):
            result[key_hierarchy + '.' + str(key)] = value
        return result

    def validate_input(self, data):
        try:
            for key, value in data.items():
                if value == 'undefined' or value == '' or value == "":
                    data[key] = None
            return data
        except Exception as e:
            # print(self.return_exception(e))
            return None

    @staticmethod
    def removeObjectId(data):
        for key, item in data.items():
            if key != 'startDate' and key != 'endDate':
                if item is True:
                    item = 1
                elif item is False:
                    item = 0
                data[key] = str(item)
        return data

    def removeObjectIdFromList(self, data):
        if type(data) is not dict:
            return data
        for key, item in data.items():
            data[key] = self.removeObjectId(item)
        return data

    @staticmethod
    def create_ref(source, key, value):
        result = {}
        if type(source) is not list:
            return result
        for item in source:
            if key in item and value in item:
                result[item[key]] = item[value]
        return result

    @staticmethod
    def get_ref(key, source):
        if key in source:
            return source[key]
        return ''

    @staticmethod
    def list_month_ref():
        return [
            {'label': '01', 'value': 1},
            {'label': '02', 'value': 2},
            {'label': '03', 'value': 3},
            {'label': '04', 'value': 4},
            {'label': '05', 'value': 5},
            {'label': '06', 'value': 6},
            {'label': '07', 'value': 7},
            {'label': '08', 'value': 8},
            {'label': '09', 'value': 9},
            {'label': '10', 'value': 10},
            {'label': '11', 'value': 11},
            {'label': '12', 'value': 12}
        ]

    @staticmethod
    def list_year_ref():
        result = []
        now = datetime.date.today()
        current_year = now.year + 1
        for year in range(MIN_YEAR, current_year):
            item = {'label': str(year), 'value': int(year)}
            result.append(item)
        return result

    @staticmethod
    def nice_url(str):
        if str is None:
            return 'default'
        str = str.strip()
        str = str.encode("utf-8")
        ''' Helper function: Remove Vietnamese accent for string '''
        nkfd_form = unicodedata.normalize('NFKD', str(str, 'utf-8'))
        result = "".join([c for c in nkfd_form if not unicodedata.combining(c)]).replace('\u0111', 'd').replace('\u0110', 'D')
        result = result.lower()
        result = re.sub('[^a-zA-Z0-9\n\.]', '-', result)
        #result = re.sub(r'\W+', '-', result)
        return result

    @staticmethod
    def random_str(size=6, chars=string.ascii_uppercase + string.digits):
        return ''.join(random.choice(chars) for _ in range(size))

    def upload_handle(self, f, destination_dir, exist_file=None, img_width=None):
        if f is not None:
            if exist_file is not None and os.path.isfile(MEDIA_ROOT + exist_file) is not False:
                os.remove(MEDIA_ROOT + exist_file)
            path = destination_dir + self.random_str(9) + '-' + self.nice_url(f.name)
            if not os.path.isdir(MEDIA_ROOT + destination_dir):
                os.makedirs(MEDIA_ROOT + destination_dir)
            destination = open(MEDIA_ROOT + path, 'wb+')
            for chunk in f.chunks():
                destination.write(chunk)
            destination.close()
            # print(str(MEDIA_ROOT + path))
            if img_width is not None:
                self.resize_image(MEDIA_ROOT + path, img_width)
            return path
        else:
            return None

    def resize_image(self, path, basewidth=250):
        try:
            img = Image.open(path)
            wpercent = (basewidth/float(img.size[0]))
            hsize = int((float(img.size[1])*float(wpercent)))
            img = img.resize((basewidth, hsize), Image.ANTIALIAS)
            img.save(path)
        except Exception as e:
            error_content = self.return_exception(e)
            # print(error_content)

    @staticmethod
    def get_app_name(full_view_path):
        path_array = full_view_path.split(".")
        if len(path_array) < 2:
            return full_view_path + '/'
        return path_array[-2] + '/'

    @staticmethod
    def send_email(subject, body, to):
        if type(to) is not list:
            to = [to]
        if settings.IS_PROD is True:

            email = EmailMultiAlternatives(
                subject,
                body,
                settings.DEFAULT_FROM_EMAIL,
                to)
            email.content_subtype = "html"
            email.attach_alternative(body, "text/html")
            email.send()
            '''
            mail.send(
                to,
                sender=settings.DEFAULT_FROM_EMAIL,
                subject=subject,
                message=body,
                html_message=body,
            )
            '''
    @staticmethod
    def lang_trans(lang_code):
        if lang_code == 'EN':
            return 'english'
        return 'vietnam'
