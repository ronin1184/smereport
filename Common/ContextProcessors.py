from django.conf import settings
from django.core.urlresolvers import resolve
#from Administrator.models import Administrator


def common_var(request):
    # return the value you want as a dictionnary.
    # you may add multiple values in there.
    admin_id = request.session.get('ADMIN_ID', None)
    #if admin_id is not None:
    #    admin_item = Administrator.objects.get_one({'id': int(admin_id)}, True)['data']

    current_url_name = resolve(request.path_info).url_name
    current_namespace = resolve(request.path_info).namespace
    arg = resolve(request.path_info).kwargs
    if not arg:
        current_menu = current_namespace + '-' + current_url_name
    else:
        arg = "-".join(list(arg.values()))
        current_menu = current_namespace + '-' + current_url_name + '-' + arg
    return {
        'BASE_URL': settings.BASE_URL,
        'STATIC_URL': settings.STATIC_URL,
        'MEDIA_URL': settings.MEDIA_URL,
        'CURRENT_MENU': current_menu,
        'DEFAULT_IMG': settings.STATIC_URL + 'img/default-thumbnail.jpg',

        'ADMIN_FULLNAME': request.session.get('ADMIN_FULLNAME', None),
        'ADMIN_ID': admin_id,
        'ADMIN_ROLE': request.session.get('ADMIN_ROLE', None),

        'USER_FULLNAME': request.session.get('USER_FULLNAME', None),
        'USER_ID': request.session.get('USER_ID', None),
        'USER_ROLE': request.session.get('USER_ROLE', None),

        'LANG': request.session.get('LANG', 'VI'),
    }
