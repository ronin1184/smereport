from django.core.urlresolvers import resolve
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from smereport.settings import AUTHENTIC, WHITELIST, ADMIN_ROLE, USER_ROLE


class ProxyProcessing(object):
    #@staticmethod
    __white_list = WHITELIST

    def process_request(self, request):
        authentic_list = AUTHENTIC
        #current_url = resolve(request.path_info).url_name
        current_url_name = resolve(request.path_info).url_name
        current_namespace = resolve(request.path_info).namespace
        if current_namespace in authentic_list['administrator']:
            if resolve(request.path_info).url_name not in self.__white_list:
                if request.session.get('ADMIN_ROLE', None) not in ADMIN_ROLE:
                    if current_namespace in authentic_list['user']:
                        return None
                    return redirect(reverse('administrator:login'))
        elif current_namespace in authentic_list['user']:
            if resolve(request.path_info).url_name not in self.__white_list:
                if request.session.get('USER_ROLE', None) not in USER_ROLE:
                    if current_namespace in authentic_list['administrator']:
                        return None
                    return redirect(reverse('user:login'))
        return None

