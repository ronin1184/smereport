# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Config',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('uid', models.CharField(default='', max_length=150)),
                ('value', models.CharField(max_length=150, blank=True, null=True)),
            ],
            options={
                'db_table': 'config',
            },
            bases=(models.Model,),
        ),
    ]
