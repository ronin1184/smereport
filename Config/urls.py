from django.conf.urls import patterns, url


urlpatterns = patterns(
    '',
    url(
        r'^$',
        'Config.views.index',
        name='index'),
    url(
        r'^get_all',
        'Config.views.get_all',
        name='get_all'),
    url(
        r'^get_one',
        'Config.views.get_one',
        name='get_one'),
    url(
        r'^add_item',
        'Config.views.add_item',
        name='add_item'),
    url(
        r'^edit_item',
        'Config.views.edit_item',
        name='edit_item'),
    url(
        r'^remove_item',
        'Config.views.remove_item',
        name='remove_item'),
)
